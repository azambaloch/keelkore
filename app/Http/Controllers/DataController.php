<?php

namespace App\Http\Controllers;
use DB;
use PDF;
use Illuminate\Http\Request;

class DataController extends Controller
{
    
    public function saveData(Request $request)
    {
           //$newId = DB::table('data')->get('id');


           $asset = DB::table('data')->insert(
            array(
                   'name'     =>   $request->name, 
                   'fname'   =>   $request->fname,
                   'cnic'   =>   $request->cnic,
                   'address'   =>   $request->address,
                   'dateOfBirth'     =>   $request->dateOfBirth, 
                   'contact'   =>   $request->contact,
                   'date'   =>   $request->date,
                   'plotNum'   =>   $request->plotNum,
                   'plotSize'     =>   $request->plotSize, 
                   'plotReg'   =>   $request->plotReg,
                   'project'   =>   $request->project,
                   'nName'   =>   $request->nName,
                   'nFname'   =>   $request->nFname,
                   'nRelation'     =>   $request->nRelation, 
                   'nAddress'  =>   $request->nAddress,
                   'nCnic'   =>   $request->nCnic,
                   'nContact'   =>   $request->nContact,
            )
       );
       $newId = DB::getPdo('data')->lastInsertId();

       DB::table('amount')->insert(
        array(
             'totalAmount' => $request->totalAmount,
             'paidAmount'  => $request->paidAmount,
             'remainingAmount' => $request->totalAmount - $request->paidAmount,
             'plot_id' => $newId
        )
        );

               return response()->json(['message'=>'Application Form Added Successfully']);
    }

    public function updateData(Request $request)
    {
           //$newId = DB::table('data')->get('id');


           $asset = DB::table('data')->where('id', '=', $request->id)->update(
            array(
                   'name'     =>   $request->name, 
                   'fname'   =>   $request->fname,
                   'cnic'   =>   $request->cnic,
                   'address'   =>   $request->address,
                   'dateOfBirth'     =>   $request->dateOfBirth, 
                   'contact'   =>   $request->contact,
                   'date'   =>   $request->date,
                   'plotNum'   =>   $request->plotNum,
                   'plotSize'     =>   $request->plotSize, 
                   'plotReg'   =>   $request->plotReg,
                   'project'   =>   $request->project,
                   'nName'   =>   $request->nName,
                   'nFname'   =>   $request->nFname,
                   'nRelation'     =>   $request->nRelation, 
                   'nAddress'  =>   $request->nAddress,
                   'nCnic'   =>   $request->nCnic,
                   'nContact'   =>   $request->nContact,
            )
       );
       $newId = DB::getPdo('data')->lastInsertId();

       DB::table('amount')->where('plot_id', '=', $newId)->update(
        array(
             'totalAmount' => $request->totalAmount,
             'paidAmount'  => $request->paidAmount,
             'remainingAmount' => $request->totalAmount - $request->paidAmount,
             'plot_id' => $newId
        )
        );

               return response()->json(['message'=>'Form Updated Successfully']);
    }

    public function printData($Id)
    {
           
                $data = DB::table('data')->where('id', $Id)
                ->join('amount', 'data.id', '=', 'amount.plot_id')
                ->join('projects', 'data.project', '=', 'projects.project_id')
                ->get();
                  //return response()->json(['customers' => $data]);
                  return view('form', ['data' => $data]);
                //  $pdf = PDF::loadView('form', ['data' => $data]);
            	  //  return $pdf->download();
    }
    
    public function printReceipt($Id)
    {
           
                $data = DB::table('data')
                ->join('amount', 'data.id', '=', 'amount.plot_id')
                ->join('projects', 'data.project', '=', 'projects.project_id')
                ->where('id', $Id)
                ->get();
                  //return response()->json(['customers' => $data]);
                  return view('receipt', ['data' => $data]);
                //  $pdf = PDF::loadView('form', ['data' => $data]);
            	  //  return $pdf->download();
    }

    public function getCustomerDetail($id)
    { 
           
                $data = DB::table('data')->where('id', $id)
                ->join('amount', 'data.id', '=', 'amount.plot_id')
                ->join('projects', 'data.project', '=', 'projects.project_id')
                ->get();
                  return response()->json(['customer' => $data]);
    }
    public function getData()
    {
        $data = DB::table('data')
        ->join('amount', 'data.id', '=', 'amount.plot_id')
        ->join('projects', 'data.project', '=', 'projects.project_id')
        ->get();
		return response()->json(['customers' => $data]);

    }
    public function customerCount()
    {
        $data = DB::table('data')->count();
		return response()->json(['total' => $data]);

    }
    public function saveProject(Request $request)
    {
           
           $asset = DB::table('projects')->insert(
            array(
                   'projectName'     =>   $request->projectName, 
                   'projectAddress'   =>   $request->projectAddress,
                   'projectSize'   =>   $request->projectSize,
                   'projectPartners'   =>   $request->projectPartners,
            )
       );
               return response()->json(['message'=>'Project Added Successfully']);
    }
    public function addPayment(Request $request)
    {         
           DB::table('amount')
           ->where('plot_id', '=', $request->cuss_id)
           ->increment('paidAmount', $request->payment);

           DB::table('amount')
           ->where('plot_id', '=', $request->cuss_id)
           ->decrement('remainingAmount', $request->payment);
           return response()->json(['message'=>'Payment added successfully']);
    }
    

    public function getProjects()
    {
        $data = DB::table('projects')->get();
		return response()->json(['projects' => $data]);

    }

    public function projectCount()
    {
        $data = DB::table('projects')->count();
		return response()->json(['projectCount' => $data]);

    }
    public function getPartner()
    {
        $data = DB::table('partners')->get();
		return response()->json(['partners' => $data]);
    }
    public function savePartner(Request $request)
    {
           
           $asset = DB::table('partners')->insert(
            array(
                   'partnerName'     =>   $request->partnerName, 
                   'cnic'   =>   $request->cnic,
                   'contact'   =>   $request->contact,
                   'address'   =>   $request->address,
                   'project'   =>   $request->project,
                   'percentage'   =>   $request->percentage, 
            )
       );
               return response()->json(['message'=>'Partner Added Successfully']);
    }
    public function partnerCount()
    {
        $data = DB::table('partners')->count();
		return response()->json(['partnerCount' => $data]);

    }

    //loadCustomersById
}
