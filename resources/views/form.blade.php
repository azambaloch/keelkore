<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Customer Profile</title>
    <style>
        body{
            font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace !important;
            letter-spacing: -0.3px;
            font-size:.6em;
        }
        .invoice-wrapper{ width: 700px; margin: auto; }
        .nav-sidebar .nav-header:not(:first-of-type){ padding: 1.7rem 0rem .5rem; }
        .logo{ font-size: 50px; }
        .sidebar-collapse .brand-link .brand-image{ margin-top: -33px; }
        .content-wrapper{ margin: auto !important; }
        .billing-company-image { width: 50px; }
        .billing_name { text-transform: uppercase; }
        .billing_address { text-transform: capitalize; }
        .table{ width: 100%; border-collapse: collapse; }
        th{ text-align: left; padding: 1px; }
        td{ padding: 1px; vertical-align: top; }
        .row{ display: block; clear: both; }
        .text-right{ text-align: right; }
        .text-center{ text-align: center;}
        .table-hover thead tr{ background: #eee; }
        .table-hover tbody tr:nth-child(even){ background: #fbf9f9; }
        address{ font-style: normal; }
        footer{position:fixed; bottom:-60px; left:0px;right:0px; height:50px;}

    </style>
</head>
<body>
    <div>
    <input type="button" id="button" value="Print" onClick="window.print()">
    </div>
    <div class="row invoice-wrapper">
        <div class="col-md-12">
            <div>
            <div style="background-color:lightblue; height:70px; align:center;">
            <h1 style="text-align:center;"> KEELKORE REAL ESTATE <br>TURBAT</h1>
            </div>
                <div class="col-12">
                    <table class="table">
                        <tr>
                        <td class="text-left"><img   src="{{asset('storage/images/frame.png')}}"></img></td>                  
                          
                            <td>
                                <h1>
                                    <span class="">{{$data[0]->name}}</span>
                                    
                                </h1>
                                <h3>
                                @if($data[0]->fname)
                                Father Name/Husband Name: {{$data[0]->fname}}<br>
                                @endif
                                @if($data[0]->cnic)
                               CNIC: {{$data[0]->cnic}} <br>
                                @endif
                                @if($data[0]->contact)
                                Contact: {{$data[0]->contact}} <br>
                                @endif
                                @if($data[0]->address)
                                Address: {{$data[0]->address}}
                                    
                                @endif
                                </h3>
                              
                            </td>
                            <td class="text-right"><img  style="height:70px;width:90px;" src="{{asset('storage/images/keellogo.JPG')}}"></img><br><br><br><strong>Date: {{date("d-M-y")}}</strong></td>                  

                        
                        </tr>
                    </table>
                </div>
            </div>

            <br><br><br><br><br><br>
            <address>
                                    <strong>Plot Information</strong><br><br><br>
                                </address>
                            
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Registration Number</th>
                                <th>Plot Number</th>
                                <th>Plot Size</th>
                                <th>Project</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                            <td>{{$item->plotReg}}</td>
                           
                            <td>{{$item->plotNum}}</td>
                            
                            <td>{{$item->plotSize}}</td>
                            <td>{{$item->projectName}}</td>
                         
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
             <br><br><br><br>
            <address>
                                    <strong>Nomination</strong><br>
                                    <p style="text-align:center;">(Nominee should not be a minor)</p><br>
                                </address>
                            
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Nominee Name</th>
                                <th>Father Name / Husband Name</th>
                                <th>Relation</th>
                                <th>CNIC</th>
                                <th>Address</th>
                                <th>Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                            <td>{{$item->nName}}</td>
                            @if($item->nFname)
                            <td>{{$item->nFname}}</td>
                            @else
                            <td>NA</td>
                            @endif
                            <td>{{$item->nRelation}}</td>
                            
                            @if($item->nCnic)
                            <td>{{$item->nCnic}}</td>
                            @else
                            <td>NA</td>
                            @endif
                            @if($item->nAddress)
                            <td>{{$item->nAddress}}</td>
                            @else
                            <td>NA</td>
                            @endif
                            @if($item->nContact)
                            <td>{{$item->nContact}}</td>
                            @else
                            <td>NA</td>
                            @endif
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <br><br><br><br>
            <address>
                                    <strong>Payment Information</strong><br><br><br>
                                </address>
                            
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                
                                <th>Total Cost</th>
                                <th>Total Paid</th>
                                <th>Total Remaining</th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                            
                            <td>{{$item->totalAmount}}</td>
                            
                            <td>{{$item->paidAmount}}</td>
                            <td>{{$item->remainingAmount}}</td>
                         
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <p>I/we hereby Nominee: <strong> {{$data[0]->nName}}</strong> S/o, D/o, W/o: <strong> {{$data[0]->nFname}} </strong> Relation: <strong>{{$data[0]->nRelation}}</strong> CNIC Number: <strong> {{$data[0]->nCnic}}</strong>
             and declare that in case of death before execution of lease/Sub-lease of the plot alloted to me, my above names nominee shall be successor-in-interest 
             and lease/Sublease for the purpose under this Agreement of Allotment of Plot subject to the compliance of all the terms and condition/undertakings.
           <br><br><br> <br>
            <br>
           
            <br><strong>NOTE:</strong><br>
            1- Monthly Installments shall be paid as per schedule of payment.<br>
            2- Installments should be paid before 10th of every month.<br>
            3- Documents Charges shall be paid by the allotee.<br>
            </p>
            <br>
            <br>
            <br>
        
            <br>
            <br>
            <br>
          
            <div>
    <span style="display:inline;">Authorized Signature/ Stamp</span>
    <span style="display:inline; float:right;">Applicant Signature</span>
</div>                                     
<br>
            <br>
            <br>
            <br>
            <br>
            <br>                                                                                    
            <div style="background-color:lightblue;">
           Near Azad Dasht Market Ghulam Nabi Pump
            <br>
           <strong>Office Ptcl:</strong> 0852414160  <strong>Mobile:</strong> 03132811686 | 03232557159 | 033223202845 03482069620 | 03249915337 | 03218173817
                                            
            </div>

            <div>
            </div>
        </div>
    </div>    
</body>
</html>
<style>
@media print {
  #button {
    display: none;
  }
}
</style>