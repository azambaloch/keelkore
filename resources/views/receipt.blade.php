<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Receipt</title>
    <style>
        body{
            font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace !important;
            letter-spacing: -0.3px;
            font-size:.6em;
        }
        .invoice-wrapper{ width: 700px; margin: auto; }
        .nav-sidebar .nav-header:not(:first-of-type){ padding: 1.7rem 0rem .5rem; }
        .logo{ font-size: 50px; }
        .sidebar-collapse .brand-link .brand-image{ margin-top: -33px; }
        .content-wrapper{ margin: auto !important; }
        .billing-company-image { width: 50px; }
        .billing_name { text-transform: uppercase; }
        .billing_address { text-transform: capitalize; }
        .table{ width: 100%; border-collapse: collapse; }
        th{ text-align: left; padding: 1px; }
        td{ padding: 1px; vertical-align: top; }
        .row{ display: block; clear: both; }
        .text-right{ text-align: right; }
        .text-center{ text-align: center;}
        .table-hover thead tr{ background: #eee; }
        .table-hover tbody tr:nth-child(even){ background: #fbf9f9; }
        address{ font-style: normal; }
        footer{position:fixed; bottom:-60px; left:0px;right:0px; height:50px;}

    </style>
</head>
<body>
    <div>
    <input type="button" id="button" value="Print" onClick="window.print()">
    </div>
    <div class="row invoice-wrapper">
        <div class="col-md-12">
            <div>
            <div style="background-color:lightblue; height:50px; align:center;">
            <h1 style="text-align:center;"> KEELKORE REAL ESTATE <br>TURBAT</h1>
            </div>
                <div class="col-12">
                    <table class="table">
                        <tr>
                                        
                          
                            <td>
                                <h1>
                                    <span class="">{{$data[0]->name}}</span>
                                    
                                </h1>
                                <h3>
                                @if($data[0]->fname)
                                Father Name/Husband Name: {{$data[0]->fname}}<br>
                                @endif
                                @if($data[0]->cnic)
                               CNIC: {{$data[0]->cnic}} <br>
                                @endif
                                @if($data[0]->contact)
                                Contact: {{$data[0]->contact}} <br>
                                @endif
                                @if($data[0]->address)
                                Address: {{$data[0]->address}} <br>
                                @endif
                                
                                Plot: {{$data[0]->plotNum}} <br>
                                Plot Size: {{$data[0]->plotSize}}<br> 
                                Project: {{$data[0]->projectName}} 
                                </h3>
                              
                            </td>
                            <td class="text-right"><img  style="height:70px;width:90px;" src="{{asset('storage/images/keellogo.JPG')}}"></img><br><br><br><strong>Date: {{date("d-M-y")}}</strong></td>                  

                        
                        </tr>
                    </table>
                </div>
            </div>
            <br><br>
            <br>
            <br>
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                
                                <th>Total Cost</th>
                                <th>Total Paid</th>
                                <th>Total Remaining</th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                            
                            <td>{{$item->totalAmount}}</td>
                            
                            <td>{{$item->paidAmount}}</td>
                            <td>{{$item->remainingAmount}}</td>
                         
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
                            
            
            
            <br><br>
            <br>
            <br>
         
          
            <div>
    <span style="display:inline;">Authorized Signature/ Stamp</span>
    <span style="display:inline; float:right;">Applicant Signature</span>
</div>                                     
<br>
            <br>
            
            <br>                                                                                    
            <div style="background-color:lightblue;">
           Near Azad Dasht Market Ghulam Nabi Pump
            <br>
           <strong>Office Ptcl:</strong> 0852414160  <strong>Mobile:</strong> 03132811686 | 03232557159 | 033223202845 03482069620 | 03249915337 | 03218173817
                                            
            </div>

            <div>
            </div>
        </div>
    </div>    
</body>
</html>
<style>
@media print {
  #button {
    display: none;
  }
}
</style>