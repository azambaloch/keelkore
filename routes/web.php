<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::post('/saveData','DataController@saveData')->name('saveData');
Route::get('/printData/{Id}','DataController@printData')->name('printData');
Route::get('/printReceipt/{Id}','DataController@printReceipt')->name('printReceipt');

Route::get('/getCustomers','DataController@getData')->name('getData');
Route::get('/getCustomerDetail/{id}','DataController@getCustomerDetail')->name('getCustomerDetail');
Route::get('/customerCount','DataController@customerCount')->name('customerCount');
Route::post('/saveProject','DataController@saveProject')->name('saveProject');
Route::get('/getProjects','DataController@getProjects')->name('getProjects');
Route::get('/projectCount','DataController@projectCount')->name('projectCount');
Route::post('/savePartner','DataController@savePartner')->name('savePartner');
Route::get('/getPartners','DataController@getPartner')->name('getPartner');
Route::get('/partnerCount','DataController@partnerCount')->name('partnerCount');
Route::post('/addPayment','DataController@addPayment')->name('addPayment');
// Route::get('/loadCustomersById/{id}','DataController@loadCustomersById')->name('loadCustomersById');
Route::post('/updateData','DataController@updateData')->name('updateData');

//
Route::get('{any}', function () { return view('welcome'); })->where('any','.*');

