(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[88],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Cards.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Cards.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: false
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Cards.vue?vue&type=template&id=5d4478d6&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Cards.vue?vue&type=template&id=5d4478d6& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "12", md: "6" } },
                [
                  _c(
                    "v-card",
                    { staticClass: "elevation-5" },
                    [
                      _c("img", {
                        staticClass: "img-responsive",
                        attrs: {
                          src: "/static/img/blog-1.jpg",
                          alt: "Card Image"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-card-title", [
                        _c("h3", { staticClass: "headline primary--text" }, [
                          _vm._v("Blog post layout #1 with image")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("v-card-text", [
                        _c("p", { staticClass: "mb-0 fs-16" }, [
                          _vm._v(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices tortor non quam feugiat pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam gravida justo, ut rhoncus tellus malesuada nec. Fusce dignissim velit eget consequat congue...."
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-card-actions",
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "px-4",
                              attrs: { color: "warning" }
                            },
                            [_vm._v("Share")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              staticClass: "px-4",
                              attrs: { color: "primary" }
                            },
                            [_vm._v("Explore")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("v-col", { attrs: { cols: "12", md: "6" } }, [
                _c("div", { staticClass: "app-card" }, [
                  _c(
                    "div",
                    { staticClass: "app-card-content" },
                    [
                      _c("div", { staticClass: "blog-thumb mb-4" }, [
                        _c("img", {
                          attrs: {
                            src: "/static/img/post-1.png",
                            alt: "blog-1",
                            width: "100%"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "blog-content mb-4" }, [
                        _c("h4", [
                          _vm._v(
                            "Where Can You Find Unique Myspace Layouts Nowadays"
                          )
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "small" }, [
                          _vm._v("11 Nov 2017 , By: Admin , 5 Comments ")
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-card-actions",
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "px-4",
                              attrs: { color: "success" }
                            },
                            [_vm._v("Share")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            { staticClass: "px-4", attrs: { color: "error" } },
                            [_vm._v("Explore")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "12", md: "6" } },
                [
                  _c(
                    "v-card",
                    { staticClass: "elevation-5" },
                    [
                      _c(
                        "v-img",
                        {
                          staticClass: "align-start justify-center white--text",
                          attrs: {
                            src: "/static/img/blog-2.jpg",
                            alt: "Card Image",
                            height: "410"
                          }
                        },
                        [
                          _c(
                            "v-card-title",
                            { staticClass: "justify-center" },
                            [_vm._v("Top 10 Australian beaches")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("v-card-title", { attrs: { "primary-title": "" } }, [
                        _c("h3", { staticClass: "headline primary--text" }, [
                          _vm._v("Blog post layout #1 with image")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("v-card-text", [
                        _c("p", { staticClass: "mb-0 fs-16 " }, [
                          _vm._v(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices tortor non quam feugiat pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam gravida justo, ut rhoncus tellus malesuada nec. Fusce dignissim velit eget consequat congue...."
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-card-actions",
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "px-4",
                              attrs: { color: "primary" }
                            },
                            [_vm._v("Share")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            { staticClass: "px-4", attrs: { color: "error" } },
                            [_vm._v("Explore")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "12", md: "6" } },
                [
                  _c(
                    "v-card",
                    { staticClass: "elevation-5" },
                    [
                      _c("img", {
                        staticClass: "img-responsive",
                        attrs: {
                          src: "/static/img/blog-3.jpg",
                          alt: "Card Image"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-card-title", { attrs: { "primary-title": "" } }, [
                        _c("div", [
                          _c("div", { staticClass: "headline" }, [
                            _vm._v("Top western road trips")
                          ]),
                          _vm._v(" "),
                          _c("span", { staticClass: "grey--text fs-16" }, [
                            _vm._v("1,000 miles of wonder")
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-card-actions",
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "px-4",
                              attrs: { color: "secondary" }
                            },
                            [_vm._v("Share")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            { staticClass: "px-4", attrs: { color: "info" } },
                            [_vm._v("Explore")]
                          ),
                          _vm._v(" "),
                          _c("v-spacer"),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              attrs: { icon: "" },
                              nativeOn: {
                                click: function($event) {
                                  _vm.show = !_vm.show
                                }
                              }
                            },
                            [
                              _c("v-icon", [
                                _vm._v(
                                  _vm._s(
                                    _vm.show
                                      ? "keyboard_arrow_down"
                                      : "keyboard_arrow_up"
                                  )
                                )
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-slide-y-transition",
                        [
                          _c(
                            "v-card-text",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.show,
                                  expression: "show"
                                }
                              ],
                              staticClass: "fs-16"
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\tI'm a thing. But, like most politicians, he promised more than he could deliver. You won't have time for sleeping, soldier, not with all the bed making you'll be doing. Then we'll go with that data file! Hey, you add a one and two zeros to that or we walk! You're going to do his laundry? I've got to find a way to escape.\n\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Cards.vue":
/*!**************************************************!*\
  !*** ./resources/js/views/ui-elements/Cards.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Cards_vue_vue_type_template_id_5d4478d6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Cards.vue?vue&type=template&id=5d4478d6& */ "./resources/js/views/ui-elements/Cards.vue?vue&type=template&id=5d4478d6&");
/* harmony import */ var _Cards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Cards.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Cards.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Cards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Cards_vue_vue_type_template_id_5d4478d6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Cards_vue_vue_type_template_id_5d4478d6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Cards.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Cards.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Cards.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Cards.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Cards.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Cards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Cards.vue?vue&type=template&id=5d4478d6&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Cards.vue?vue&type=template&id=5d4478d6& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Cards_vue_vue_type_template_id_5d4478d6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Cards.vue?vue&type=template&id=5d4478d6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Cards.vue?vue&type=template&id=5d4478d6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Cards_vue_vue_type_template_id_5d4478d6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Cards_vue_vue_type_template_id_5d4478d6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);