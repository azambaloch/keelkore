(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[115],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Pricing-2.vue?vue&type=template&id=eb6982de&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/Pricing-2.vue?vue&type=template&id=eb6982de& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3", attrs: { fluid: "" } },
        [
          _c(
            "div",
            { staticClass: "pricing-wrapper" },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12", sm: "6", md: "6", lg: "3" } },
                    [
                      _c("div", { staticClass: "app-card text-center" }, [
                        _c(
                          "div",
                          {
                            staticClass:
                              "app-card-title primary d-custom-flex justify-space-between"
                          },
                          [
                            _c("h3", { staticClass: "mb-0 white--text mr-2" }, [
                              _vm._v(_vm._s(_vm.$t("message.basic")))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "mb-0 fs-12 white--text" }, [
                              _vm._v(
                                _vm._s(_vm.$t("message.forMostOfTheUsers"))
                              )
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "app-full-content" }, [
                          _c("h2", { staticClass: "mb-5 font-3x" }, [
                            _c("span", { staticClass: "font-xl" }, [
                              _vm._v("$40")
                            ]),
                            _vm._v(" "),
                            _c("sub", [_vm._v("/Mo")])
                          ]),
                          _vm._v(" "),
                          _c(
                            "ul",
                            { staticClass: "list-unstyled list-group-flush" },
                            [
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("10GB of Bandwidth")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("200MB Max File Size")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("2GHZ CPU")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("256MB Memory")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("1 GB Storage")
                              ])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "app-footer" },
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: {
                                  block: "",
                                  color: "primary",
                                  large: ""
                                }
                              },
                              [_vm._v(_vm._s(_vm.$t("message.choosePlan")))]
                            )
                          ],
                          1
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", sm: "6", md: "6", lg: "3" } },
                    [
                      _c("div", { staticClass: "app-card text-center" }, [
                        _c(
                          "div",
                          {
                            staticClass:
                              "app-card-title success white--text d-custom-flex justify-space-between"
                          },
                          [
                            _c("h3", { staticClass: "mb-0 white--text mr-2" }, [
                              _vm._v(_vm._s(_vm.$t("message.standard")))
                            ]),
                            _vm._v(" "),
                            _c(
                              "p",
                              {
                                staticClass: "mb-0 fs-12 fw-normal white--text"
                              },
                              [
                                _vm._v(
                                  _vm._s(_vm.$t("message.forMostOfTheUsers"))
                                )
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "app-full-content" }, [
                          _c("h2", { staticClass: "mb-5 font-3x" }, [
                            _c("span", { staticClass: "font-xl" }, [
                              _vm._v("$50")
                            ]),
                            _vm._v(" "),
                            _c("sub", [_vm._v("/Mo")])
                          ]),
                          _vm._v(" "),
                          _c(
                            "ul",
                            { staticClass: "list-unstyled list-group-flush" },
                            [
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("10GB of Bandwidth")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("200MB Max File Size")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("2GHZ CPU")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("256MB Memory")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("1 GB Storage")
                              ])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "app-footer" },
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: {
                                  block: "",
                                  color: "success",
                                  large: ""
                                }
                              },
                              [_vm._v(_vm._s(_vm.$t("message.choosePlan")))]
                            )
                          ],
                          1
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", sm: "6", md: "6", lg: "3" } },
                    [
                      _c("div", { staticClass: "app-card text-center" }, [
                        _c(
                          "div",
                          {
                            staticClass:
                              "app-card-title warning white--text d-custom-flex justify-space-between"
                          },
                          [
                            _c("h3", { staticClass: "mb-0 white--text mr-2" }, [
                              _vm._v(_vm._s(_vm.$t("message.mega")))
                            ]),
                            _vm._v(" "),
                            _c(
                              "p",
                              {
                                staticClass: "mb-0 fs-12 fw-normal white--text"
                              },
                              [_vm._v(_vm._s(_vm.$t("message.forDeveloper")))]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "app-full-content" }, [
                          _c("h2", { staticClass: "mb-5 font-3x" }, [
                            _c("span", { staticClass: "font-xl" }, [
                              _vm._v("$70")
                            ]),
                            _vm._v(" "),
                            _c("sub", [_vm._v("/Mo")])
                          ]),
                          _vm._v(" "),
                          _c(
                            "ul",
                            { staticClass: "list-unstyled list-group-flush" },
                            [
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("10GB of Bandwidth")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("200MB Max File Size")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("2GHZ CPU")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("256MB Memory")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("1 GB Storage")
                              ])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "app-footer" },
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: {
                                  block: "",
                                  large: "",
                                  color: "warning"
                                }
                              },
                              [_vm._v(_vm._s(_vm.$t("message.choosePlan")))]
                            )
                          ],
                          1
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", sm: "6", md: "6", lg: "3" } },
                    [
                      _c("div", { staticClass: "app-card text-center" }, [
                        _c(
                          "div",
                          {
                            staticClass:
                              "app-card-title error white--text d-custom-flex justify-space-between"
                          },
                          [
                            _c("h3", { staticClass: "mb-0 white--text mr-2" }, [
                              _vm._v(_vm._s(_vm.$t("message.master")))
                            ]),
                            _vm._v(" "),
                            _c(
                              "p",
                              {
                                staticClass: "mb-0 fs-12 fw-normal white--text"
                              },
                              [
                                _vm._v(
                                  _vm._s(_vm.$t("message.forLargeEnterprises"))
                                )
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "app-full-content" }, [
                          _c("h2", { staticClass: "mb-5 font-3x" }, [
                            _c("span", { staticClass: "font-xl" }, [
                              _vm._v("$100")
                            ]),
                            _vm._v(" "),
                            _c("sub", [_vm._v("/Mo")])
                          ]),
                          _vm._v(" "),
                          _c(
                            "ul",
                            { staticClass: "list-unstyled list-group-flush" },
                            [
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("10GB of Bandwidth")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("200MB Max File Size")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("2GHZ CPU")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("256MB Memory")
                              ]),
                              _vm._v(" "),
                              _c("li", { staticClass: "list-group-item" }, [
                                _vm._v("1 GB Storage")
                              ])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "app-footer" },
                          [
                            _c(
                              "v-btn",
                              {
                                attrs: { block: "", color: "error", large: "" }
                              },
                              [_vm._v(_vm._s(_vm.$t("message.choosePlan")))]
                            )
                          ],
                          1
                        )
                      ])
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/pages/Pricing-2.vue":
/*!************************************************!*\
  !*** ./resources/js/views/pages/Pricing-2.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Pricing_2_vue_vue_type_template_id_eb6982de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Pricing-2.vue?vue&type=template&id=eb6982de& */ "./resources/js/views/pages/Pricing-2.vue?vue&type=template&id=eb6982de&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Pricing_2_vue_vue_type_template_id_eb6982de___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Pricing_2_vue_vue_type_template_id_eb6982de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/Pricing-2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/Pricing-2.vue?vue&type=template&id=eb6982de&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/pages/Pricing-2.vue?vue&type=template&id=eb6982de& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Pricing_2_vue_vue_type_template_id_eb6982de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Pricing-2.vue?vue&type=template&id=eb6982de& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Pricing-2.vue?vue&type=template&id=eb6982de&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Pricing_2_vue_vue_type_template_id_eb6982de___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Pricing_2_vue_vue_type_template_id_eb6982de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);