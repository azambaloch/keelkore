(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[110],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Timepicker.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Timepicker.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      e4: null,
      e5: null,
      time: null,
      time2: null,
      menu2: false,
      modal2: false
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Timepicker.vue?vue&type=template&id=9e51f04e&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Timepicker.vue?vue&type=template&id=9e51f04e& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.timePicker"),
                    contentCustomClass: "div-responsive",
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-time-picker", {
                    model: {
                      value: _vm.e4,
                      callback: function($$v) {
                        _vm.e4 = $$v
                      },
                      expression: "e4"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.timePicker"),
                    contentCustomClass: "div-responsive",
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-time-picker", {
                    attrs: { landscape: "" },
                    model: {
                      value: _vm.e5,
                      callback: function($$v) {
                        _vm.e5 = $$v
                      },
                      expression: "e5"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.timePickerInDialogAndMenu"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("div", { staticClass: "mb-4" }, [
                    _c("p", { staticClass: "mb-0" }, [
                      _vm._v(
                        "Due to the flexibility of pickers, you can really dial in the experience exactly how you want it."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      ref: "menu",
                      attrs: {
                        "close-on-content-click": false,
                        transition: "scale-transition",
                        "offset-y": "",
                        "nudge-right": 40,
                        "max-width": "290px",
                        "min-width": "290px",
                        "return-value": _vm.time
                      },
                      on: {
                        "update:returnValue": function($event) {
                          _vm.time = $event
                        },
                        "update:return-value": function($event) {
                          _vm.time = $event
                        }
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  {
                                    attrs: {
                                      label: "Picker in menu",
                                      "prepend-icon": "access_time",
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.time,
                                      callback: function($$v) {
                                        _vm.time = $$v
                                      },
                                      expression: "time"
                                    }
                                  },
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.menu2,
                        callback: function($$v) {
                          _vm.menu2 = $$v
                        },
                        expression: "menu2"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c("v-time-picker", {
                        on: {
                          change: function($event) {
                            return _vm.$refs.menu.save(_vm.time)
                          }
                        },
                        model: {
                          value: _vm.time,
                          callback: function($$v) {
                            _vm.time = $$v
                          },
                          expression: "time"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.timePicker"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c(
                    "v-dialog",
                    {
                      ref: "dialog",
                      attrs: {
                        persistent: "",
                        width: "290px",
                        "return-value": _vm.time2
                      },
                      on: {
                        "update:returnValue": function($event) {
                          _vm.time2 = $event
                        },
                        "update:return-value": function($event) {
                          _vm.time2 = $event
                        }
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  {
                                    attrs: {
                                      label: "Picker in dialog",
                                      "prepend-icon": "access_time",
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.time2,
                                      callback: function($$v) {
                                        _vm.time2 = $$v
                                      },
                                      expression: "time2"
                                    }
                                  },
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.modal2,
                        callback: function($$v) {
                          _vm.modal2 = $$v
                        },
                        expression: "modal2"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c(
                        "v-time-picker",
                        {
                          attrs: { actions: "" },
                          model: {
                            value: _vm.time2,
                            callback: function($$v) {
                              _vm.time2 = $$v
                            },
                            expression: "time2"
                          }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { color: "error" },
                              on: {
                                click: function($event) {
                                  _vm.modal2 = false
                                }
                              }
                            },
                            [_vm._v("Cancel")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              attrs: { color: "primary" },
                              on: {
                                click: function($event) {
                                  return _vm.$refs.dialog.save(_vm.time2)
                                }
                              }
                            },
                            [_vm._v("Save")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Timepicker.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/ui-elements/Timepicker.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Timepicker_vue_vue_type_template_id_9e51f04e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Timepicker.vue?vue&type=template&id=9e51f04e& */ "./resources/js/views/ui-elements/Timepicker.vue?vue&type=template&id=9e51f04e&");
/* harmony import */ var _Timepicker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Timepicker.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Timepicker.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Timepicker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Timepicker_vue_vue_type_template_id_9e51f04e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Timepicker_vue_vue_type_template_id_9e51f04e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Timepicker.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Timepicker.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Timepicker.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Timepicker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Timepicker.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Timepicker.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Timepicker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Timepicker.vue?vue&type=template&id=9e51f04e&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Timepicker.vue?vue&type=template&id=9e51f04e& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Timepicker_vue_vue_type_template_id_9e51f04e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Timepicker.vue?vue&type=template&id=9e51f04e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Timepicker.vue?vue&type=template&id=9e51f04e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Timepicker_vue_vue_type_template_id_9e51f04e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Timepicker_vue_vue_type_template_id_9e51f04e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);