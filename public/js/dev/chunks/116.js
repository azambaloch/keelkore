(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[116],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/timelines/ColoredDots.vue?vue&type=template&id=5443cd40&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/timelines/ColoredDots.vue?vue&type=template&id=5443cd40& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "hover-wrapper colored-dots-wrapper" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl custom-flex pt-0 mt-n3" },
        [
          _c(
            "v-row",
            { attrs: { row: "", wrap: "" } },
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v(
                        "Colors dots create visual breakpoints that make your timelines easier to read."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card",
                    { staticClass: "mx-auto", attrs: { "max-width": "400" } },
                    [
                      _c(
                        "v-card",
                        { staticClass: "white--text", attrs: { flat: "" } },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: {
                                absolute: "",
                                bottom: "",
                                color: "pink",
                                right: "",
                                fab: ""
                              }
                            },
                            [
                              _c("v-icon", { staticClass: "white--text" }, [
                                _vm._v("mdi-plus")
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-title",
                            { staticClass: "pa-2 purple lighten-3" },
                            [
                              _c(
                                "v-btn",
                                { attrs: { icon: "" } },
                                [
                                  _c("v-icon", { staticClass: "white--text" }, [
                                    _vm._v("mdi-menu")
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "h3",
                                {
                                  staticClass:
                                    "title font-weight-light text-center grow"
                                },
                                [_vm._v("Timeline")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-avatar",
                                [
                                  _c("v-img", {
                                    staticClass: "img-responsive",
                                    attrs: {
                                      src: "/static/avatars/user-13.jpg",
                                      height: "40",
                                      width: "40"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-img",
                            {
                              attrs: {
                                src:
                                  "https://cdn.vuetifyjs.com/images/cards/forest.jpg",
                                gradient:
                                  "to top, rgba(0,0,0,.44), rgba(0,0,0,.44)"
                              }
                            },
                            [
                              _c(
                                "v-container",
                                { attrs: { "fill-height": "" } },
                                [
                                  _c(
                                    "v-row",
                                    { staticClass: "align-center" },
                                    [
                                      _c(
                                        "strong",
                                        {
                                          staticClass:
                                            "display-4 font-weight-regular mr-4 text-bold"
                                        },
                                        [_vm._v("8")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-row",
                                        { staticClass: "column justify-end" },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "headline font-weight-light"
                                            },
                                            [_vm._v("Monday")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "text-uppercase font-weight-light"
                                            },
                                            [_vm._v("February 2015")]
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-card-text",
                        { staticClass: "py-0" },
                        [
                          _c(
                            "v-timeline",
                            { attrs: { "align-top": "", dense: "" } },
                            [
                              _c(
                                "v-timeline-item",
                                { attrs: { color: "pink", small: "" } },
                                [
                                  _c(
                                    "v-row",
                                    { staticClass: "pt-3" },
                                    [
                                      _c("v-col", { attrs: { xs: "3" } }, [
                                        _c("strong", [_vm._v("5pm")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-col", [
                                        _c("strong", [_vm._v("New Icon")]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "caption" }, [
                                          _vm._v("Mobile App")
                                        ])
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-timeline-item",
                                {
                                  attrs: { color: "teal lighten-3", small: "" }
                                },
                                [
                                  _c(
                                    "v-row",
                                    { staticClass: "pt-3" },
                                    [
                                      _c("v-col", { attrs: { xs: "3" } }, [
                                        _c("strong", [_vm._v("3-4pm")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-col", [
                                        _c("strong", [
                                          _vm._v("Design Stand Up")
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "caption mb-2" },
                                          [_vm._v("Hangouts")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          [
                                            _c(
                                              "v-avatar",
                                              [
                                                _c("v-img", {
                                                  staticClass: "img-responsive",
                                                  attrs: {
                                                    src:
                                                      "/static/avatars/user-13.jpg",
                                                    height: "40",
                                                    width: "40"
                                                  }
                                                })
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-avatar",
                                              [
                                                _c("v-img", {
                                                  staticClass: "img-responsive",
                                                  attrs: {
                                                    src:
                                                      "/static/avatars/user-14.jpg",
                                                    height: "40",
                                                    width: "40"
                                                  }
                                                })
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-avatar",
                                              [
                                                _c("v-img", {
                                                  staticClass: "img-responsive",
                                                  attrs: {
                                                    src:
                                                      "/static/avatars/user-15.jpg",
                                                    height: "40",
                                                    width: "40"
                                                  }
                                                })
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-timeline-item",
                                { attrs: { color: "pink", small: "" } },
                                [
                                  _c(
                                    "v-row",
                                    { staticClass: "pt-3" },
                                    [
                                      _c("v-col", { attrs: { xs: "3" } }, [
                                        _c("strong", [_vm._v("12pm")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-col", [
                                        _c("strong", [_vm._v("Lunch break")])
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-timeline-item",
                                {
                                  attrs: { color: "teal lighten-3", small: "" }
                                },
                                [
                                  _c(
                                    "v-row",
                                    { staticClass: "pt-3" },
                                    [
                                      _c("v-col", { attrs: { xs: "3" } }, [
                                        _c("strong", [_vm._v("9-11am")])
                                      ]),
                                      _vm._v(" "),
                                      _c("v-col", [
                                        _c("strong", [
                                          _vm._v("Finish Home Screen")
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "caption" }, [
                                          _vm._v("Web App")
                                        ])
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/timelines/ColoredDots.vue":
/*!******************************************************!*\
  !*** ./resources/js/views/timelines/ColoredDots.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ColoredDots_vue_vue_type_template_id_5443cd40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ColoredDots.vue?vue&type=template&id=5443cd40& */ "./resources/js/views/timelines/ColoredDots.vue?vue&type=template&id=5443cd40&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _ColoredDots_vue_vue_type_template_id_5443cd40___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ColoredDots_vue_vue_type_template_id_5443cd40___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/timelines/ColoredDots.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/timelines/ColoredDots.vue?vue&type=template&id=5443cd40&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/timelines/ColoredDots.vue?vue&type=template&id=5443cd40& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColoredDots_vue_vue_type_template_id_5443cd40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColoredDots.vue?vue&type=template&id=5443cd40& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/timelines/ColoredDots.vue?vue&type=template&id=5443cd40&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColoredDots_vue_vue_type_template_id_5443cd40___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColoredDots_vue_vue_type_template_id_5443cd40___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);