(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[93],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ColorPickers.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/ColorPickers.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      color: '#8E00FF',
      hideCanvas: false,
      hideInputs: false,
      hideModeSwitch: false,
      mode: 'rgba',
      modes: ['rgba', 'hsla', 'hexa'],
      showSwatches: false
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ColorPickers.vue?vue&type=template&id=36e20a00&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/ColorPickers.vue?vue&type=template&id=36e20a00& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3" },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.colorPickers"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-color-picker")]),
                      _vm._v(
                        " allows you to select a color using a variety of input methods."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "d-flex justify-center" },
                    [_c("v-color-picker")],
                    1
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "div",
                    [
                      _c(
                        "v-container",
                        [
                          _c(
                            "v-row",
                            { attrs: { justify: "space-around" } },
                            [
                              _c("v-switch", {
                                staticClass: "mx-2",
                                attrs: { label: "Show Swatches" },
                                model: {
                                  value: _vm.showSwatches,
                                  callback: function($$v) {
                                    _vm.showSwatches = $$v
                                  },
                                  expression: "showSwatches"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-switch", {
                                staticClass: "mx-2",
                                attrs: { label: "Hide Inputs" },
                                model: {
                                  value: _vm.hideInputs,
                                  callback: function($$v) {
                                    _vm.hideInputs = $$v
                                  },
                                  expression: "hideInputs"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-switch", {
                                staticClass: "mx-2",
                                attrs: { label: "Hide Canvas" },
                                model: {
                                  value: _vm.hideCanvas,
                                  callback: function($$v) {
                                    _vm.hideCanvas = $$v
                                  },
                                  expression: "hideCanvas"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-switch", {
                                staticClass: "mx-2",
                                attrs: { label: "Hide Mode Switch" },
                                model: {
                                  value: _vm.hideModeSwitch,
                                  callback: function($$v) {
                                    _vm.hideModeSwitch = $$v
                                  },
                                  expression: "hideModeSwitch"
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                {
                                  staticClass: "mx-2",
                                  attrs: { cols: "6", md: "3" }
                                },
                                [
                                  _c("v-select", {
                                    attrs: { items: _vm.modes, label: "Mode" },
                                    model: {
                                      value: _vm.mode,
                                      callback: function($$v) {
                                        _vm.mode = $$v
                                      },
                                      expression: "mode"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("v-color-picker", {
                        staticClass: "mx-auto",
                        attrs: {
                          "hide-canvas": _vm.hideCanvas,
                          "hide-inputs": _vm.hideInputs,
                          "hide-mode-switch": _vm.hideModeSwitch,
                          mode: _vm.mode,
                          "show-swatches": _vm.showSwatches
                        },
                        on: {
                          "update:mode": function($event) {
                            _vm.mode = $event
                          }
                        },
                        model: {
                          value: _vm.color,
                          callback: function($$v) {
                            _vm.color = $$v
                          },
                          expression: "color"
                        }
                      })
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/ColorPickers.vue":
/*!*********************************************************!*\
  !*** ./resources/js/views/ui-elements/ColorPickers.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ColorPickers_vue_vue_type_template_id_36e20a00___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ColorPickers.vue?vue&type=template&id=36e20a00& */ "./resources/js/views/ui-elements/ColorPickers.vue?vue&type=template&id=36e20a00&");
/* harmony import */ var _ColorPickers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ColorPickers.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/ColorPickers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ColorPickers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ColorPickers_vue_vue_type_template_id_36e20a00___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ColorPickers_vue_vue_type_template_id_36e20a00___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/ColorPickers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/ColorPickers.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/ColorPickers.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ColorPickers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColorPickers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ColorPickers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ColorPickers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/ColorPickers.vue?vue&type=template&id=36e20a00&":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/ColorPickers.vue?vue&type=template&id=36e20a00& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColorPickers_vue_vue_type_template_id_36e20a00___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColorPickers.vue?vue&type=template&id=36e20a00& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ColorPickers.vue?vue&type=template&id=36e20a00&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColorPickers_vue_vue_type_template_id_36e20a00___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColorPickers_vue_vue_type_template_id_36e20a00___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);