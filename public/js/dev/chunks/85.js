(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[85],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/AppBars.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/AppBars.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      elevateOnScroll: false,
      hideOnScroll: false,
      fadeOnScroll: false,
      fadeImgOnScroll: false,
      invertedScroll: false,
      collapse: false,
      collapseOnScroll: false,
      shrinkOnScroll: false,
      extended: false,
      color: 'accent',
      colors: ['primary', 'accent', 'warning lighten-2', 'teal', 'orange']
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/AppBars.vue?vue&type=template&id=42c4899a&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/AppBars.vue?vue&type=template&id=42c4899a& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.appBars"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-app-bar")]),
                      _vm._v(
                        " component is pivotal to any graphical user interface (GUI), as it\n\t\t\t\t\t\tgenerally is the primary source of site navigation."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-app-bar",
                    { attrs: { color: "primary", dark: "" } },
                    [
                      _c("v-app-bar-nav-icon"),
                      _vm._v(" "),
                      _c("v-toolbar-title", [_vm._v("Page title")]),
                      _vm._v(" "),
                      _c("div", { staticClass: "flex-grow-1" }),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        { attrs: { icon: "" } },
                        [_c("v-icon", [_vm._v("mdi-heart")])],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        { attrs: { icon: "" } },
                        [_c("v-icon", [_vm._v("mdi-magnify")])],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-menu",
                        {
                          attrs: { left: "", bottom: "" },
                          scopedSlots: _vm._u([
                            {
                              key: "activator",
                              fn: function(ref) {
                                var on = ref.on
                                return [
                                  _c(
                                    "v-btn",
                                    _vm._g({ attrs: { icon: "" } }, on),
                                    [
                                      _c("v-icon", [
                                        _vm._v("mdi-dots-vertical")
                                      ])
                                    ],
                                    1
                                  )
                                ]
                              }
                            }
                          ])
                        },
                        [
                          _vm._v(" "),
                          _c(
                            "v-list",
                            _vm._l(5, function(n) {
                              return _c(
                                "v-list-item",
                                { key: n, on: { click: function() {} } },
                                [
                                  _c("v-list-item-title", [
                                    _vm._v("Option " + _vm._s(n))
                                  ])
                                ],
                                1
                              )
                            }),
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-row",
                    { attrs: { justify: "center" } },
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "4" } },
                        [
                          _c("v-select", {
                            attrs: { label: "Color", items: _vm.colors },
                            model: {
                              value: _vm.color,
                              callback: function($$v) {
                                _vm.color = $$v
                              },
                              expression: "color"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("v-col", { attrs: { cols: "12" } }),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Elevate-on-scroll" },
                            model: {
                              value: _vm.elevateOnScroll,
                              callback: function($$v) {
                                _vm.elevateOnScroll = $$v
                              },
                              expression: "elevateOnScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Hide-on-scroll" },
                            model: {
                              value: _vm.hideOnScroll,
                              callback: function($$v) {
                                _vm.hideOnScroll = $$v
                              },
                              expression: "hideOnScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Fade-on-scroll" },
                            model: {
                              value: _vm.fadeOnScroll,
                              callback: function($$v) {
                                _vm.fadeOnScroll = $$v
                              },
                              expression: "fadeOnScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Fade-img-on-scroll" },
                            model: {
                              value: _vm.fadeImgOnScroll,
                              callback: function($$v) {
                                _vm.fadeImgOnScroll = $$v
                              },
                              expression: "fadeImgOnScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Inverted scroll" },
                            model: {
                              value: _vm.invertedScroll,
                              callback: function($$v) {
                                _vm.invertedScroll = $$v
                              },
                              expression: "invertedScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Collapse" },
                            model: {
                              value: _vm.collapse,
                              callback: function($$v) {
                                _vm.collapse = $$v
                              },
                              expression: "collapse"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Collapse-on-scroll" },
                            model: {
                              value: _vm.collapseOnScroll,
                              callback: function($$v) {
                                _vm.collapseOnScroll = $$v
                              },
                              expression: "collapseOnScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Shrink-on-scroll" },
                            model: {
                              value: _vm.shrinkOnScroll,
                              callback: function($$v) {
                                _vm.shrinkOnScroll = $$v
                              },
                              expression: "shrinkOnScroll"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "3", lg: "2" } },
                        [
                          _c("v-switch", {
                            attrs: { label: "Extended" },
                            model: {
                              value: _vm.extended,
                              callback: function($$v) {
                                _vm.extended = $$v
                              },
                              expression: "extended"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card",
                    { staticClass: "overflow-hidden" },
                    [
                      _c(
                        "v-app-bar",
                        {
                          attrs: {
                            absolute: "",
                            dark: "",
                            "scroll-target": "#playground-example",
                            color: _vm.color,
                            "elevate-on-scroll": _vm.elevateOnScroll,
                            "hide-on-scroll": _vm.hideOnScroll,
                            "fade-on-scroll": _vm.fadeOnScroll,
                            "fade-img-on-scroll": _vm.fadeImgOnScroll,
                            "inverted-scroll": _vm.invertedScroll,
                            collapse: _vm.collapse,
                            "collapse-on-scroll": _vm.collapseOnScroll,
                            "shrink-on-scroll": _vm.shrinkOnScroll,
                            extended: _vm.extended
                          }
                        },
                        [
                          _c("v-app-bar-nav-icon"),
                          _vm._v(" "),
                          _c("v-toolbar-title", [_vm._v("Title")]),
                          _vm._v(" "),
                          _c("div", { staticClass: "flex-grow-1" }),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            { attrs: { icon: "" } },
                            [_c("v-icon", [_vm._v("search")])],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-sheet",
                        {
                          staticClass: "overflow-y-auto",
                          attrs: {
                            id: "playground-example",
                            "min-height": "400"
                          }
                        },
                        [_c("v-container")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/AppBars.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/ui-elements/AppBars.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AppBars_vue_vue_type_template_id_42c4899a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppBars.vue?vue&type=template&id=42c4899a& */ "./resources/js/views/ui-elements/AppBars.vue?vue&type=template&id=42c4899a&");
/* harmony import */ var _AppBars_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AppBars.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/AppBars.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AppBars_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AppBars_vue_vue_type_template_id_42c4899a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AppBars_vue_vue_type_template_id_42c4899a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/AppBars.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/AppBars.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/ui-elements/AppBars.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AppBars_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AppBars.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/AppBars.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AppBars_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/AppBars.vue?vue&type=template&id=42c4899a&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/AppBars.vue?vue&type=template&id=42c4899a& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AppBars_vue_vue_type_template_id_42c4899a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AppBars.vue?vue&type=template&id=42c4899a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/AppBars.vue?vue&type=template&id=42c4899a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AppBars_vue_vue_type_template_id_42c4899a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AppBars_vue_vue_type_template_id_42c4899a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);