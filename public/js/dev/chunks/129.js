(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[129],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SessionSlider.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SessionSlider.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.getTestimonials();
  },
  computed: {
    slickOptions: function slickOptions() {
      return {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false
      };
    }
  },
  data: function data() {
    return {
      loader: true,
      testimonials: null
    };
  },
  methods: {
    getTestimonials: function getTestimonials() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_1__["default"].get("testimonials.js").then(function (response) {
        _this.loader = false;
        _this.testimonials = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/LoginOne.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/session/LoginOne.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Widgets_SessionSlider__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Widgets/SessionSlider */ "./resources/js/components/Widgets/SessionSlider.vue");
/* harmony import */ var Constants_AppConfig__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/AppConfig */ "./resources/js/constants/AppConfig.js");
/* harmony import */ var _auth_AuthService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../auth/AuthService */ "./resources/js/auth/AuthService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import firebase from "firebase/app";
// import { mapGetters } from "vuex";



var auth = new _auth_AuthService__WEBPACK_IMPORTED_MODULE_2__["default"](); // const { login, logout, authenticated, authNotifier } = auth;

var login = auth.login;
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SessionSliderWidget: Components_Widgets_SessionSlider__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      checkbox: false,
      valid: false,
      email: "",
      emailRules: [function (v) {
        return !!v || "E-mail is required";
      }, function (v) {
        return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || "E-mail must be valid";
      }],
      password: "",
      passwordRules: [function (v) {
        return !!v || "Password is required";
      }],
      appLogo: Constants_AppConfig__WEBPACK_IMPORTED_MODULE_1__["default"].appLogo2,
      brand: Constants_AppConfig__WEBPACK_IMPORTED_MODULE_1__["default"].brand
    };
  },
  methods: {
    submit: function submit() {
      var user = {
        email: this.email,
        password: this.password
      };
      this.$store.dispatch("signinUserInFirebase", {
        user: user
      });
    },
    signInWithLaravelPassport: function signInWithLaravelPassport() {
      var user = {
        email: this.email,
        password: this.password
      };
      this.$store.dispatch("signInWithLaravelPassport", {
        user: user
      });
    },
    signInWithFacebook: function signInWithFacebook() {
      this.$store.dispatch("signinUserWithFacebook");
    },
    signInWithGoogle: function signInWithGoogle() {
      this.$store.dispatch("signinUserWithGoogle");
    },
    signInWithTwitter: function signInWithTwitter() {
      this.$store.dispatch("signinUserWithTwitter");
    },
    signInWithGithub: function signInWithGithub() {
      this.$store.dispatch("signinUserWithGithub");
    },
    onCreateAccount: function onCreateAccount() {
      this.$router.push("/session/sign-up");
    },
    signinWithAuth0: function signinWithAuth0() {
      login();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SessionSlider.vue?vue&type=template&id=70e70288&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SessionSlider.vue?vue&type=template&id=70e70288& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _vm.testimonials
        ? _c(
            "slick",
            { attrs: { options: _vm.slickOptions } },
            [
              _vm._l(_vm.testimonials, function(testimonial) {
                return [
                  _c(
                    "div",
                    { key: testimonial.id, staticClass: "session-slider" },
                    [
                      _c("div", { staticClass: "slider-content" }, [
                        _c("img", {
                          staticClass: "rounded-circle img-responsive",
                          attrs: {
                            src: testimonial.avatar,
                            width: "170",
                            height: "170"
                          }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "slider-meta mb-4" }, [
                          _c(
                            "span",
                            { staticClass: "d-block client-name fw-bold" },
                            [_c("i", [_vm._v(_vm._s(testimonial.name))])]
                          ),
                          _vm._v(" "),
                          _c("span", { staticClass: "d-block client-digg" }, [
                            _vm._v(_vm._s(testimonial.designation))
                          ])
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "px-4" }, [
                          _vm._v(_vm._s(testimonial.body))
                        ])
                      ])
                    ]
                  )
                ]
              })
            ],
            2
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/LoginOne.vue?vue&type=template&id=d7480896&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/session/LoginOne.vue?vue&type=template&id=d7480896& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "session-wrapper" }, [
    _c("div", { staticClass: "session-right text-center" }, [
      _c("div", { staticClass: "session-table-cell" }, [
        _c(
          "div",
          { staticClass: "session-content" },
          [
            _c("h2", { staticClass: "mb-4" }, [
              _vm._v(_vm._s(_vm.$t("message.loginToAdmin")))
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "fs-14" }, [
              _vm._v(
                _vm._s(
                  _vm.$t(
                    "message.enterUsernameAndPasswordToAccessControlPanelOf"
                  )
                ) +
                  " " +
                  _vm._s(_vm.brand) +
                  "."
              )
            ]),
            _vm._v(" "),
            _c(
              "v-form",
              {
                staticClass: "mb-5",
                model: {
                  value: _vm.valid,
                  callback: function($$v) {
                    _vm.valid = $$v
                  },
                  expression: "valid"
                }
              },
              [
                _c("v-text-field", {
                  attrs: {
                    label: "E-mail ID",
                    rules: _vm.emailRules,
                    required: ""
                  },
                  model: {
                    value: _vm.email,
                    callback: function($$v) {
                      _vm.email = $$v
                    },
                    expression: "email"
                  }
                }),
                _vm._v(" "),
                _c("v-text-field", {
                  attrs: {
                    label: "Password",
                    type: "password",
                    rules: _vm.passwordRules,
                    required: ""
                  },
                  model: {
                    value: _vm.password,
                    callback: function($$v) {
                      _vm.password = $$v
                    },
                    expression: "password"
                  }
                }),
                _vm._v(" "),
                _c("v-checkbox", {
                  attrs: { color: "primary", label: "Remember me" },
                  model: {
                    value: _vm.checkbox,
                    callback: function($$v) {
                      _vm.checkbox = $$v
                    },
                    expression: "checkbox"
                  }
                }),
                _vm._v(" "),
                _c(
                  "div",
                  [
                    _c(
                      "v-btn",
                      {
                        staticClass: "mb-2",
                        attrs: { large: "", block: "", color: "primary" },
                        on: { click: _vm.submit }
                      },
                      [_vm._v(_vm._s(_vm.$t("message.loginNow")))]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/index.js":
/*!***********************************!*\
  !*** ./resources/js/api/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: 'https://reactify.theironnetwork.org/data/'
}));

/***/ }),

/***/ "./resources/js/components/Widgets/SessionSlider.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/SessionSlider.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SessionSlider_vue_vue_type_template_id_70e70288___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SessionSlider.vue?vue&type=template&id=70e70288& */ "./resources/js/components/Widgets/SessionSlider.vue?vue&type=template&id=70e70288&");
/* harmony import */ var _SessionSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SessionSlider.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SessionSlider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SessionSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SessionSlider_vue_vue_type_template_id_70e70288___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SessionSlider_vue_vue_type_template_id_70e70288___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SessionSlider.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SessionSlider.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SessionSlider.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SessionSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SessionSlider.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SessionSlider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SessionSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SessionSlider.vue?vue&type=template&id=70e70288&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SessionSlider.vue?vue&type=template&id=70e70288& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SessionSlider_vue_vue_type_template_id_70e70288___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SessionSlider.vue?vue&type=template&id=70e70288& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SessionSlider.vue?vue&type=template&id=70e70288&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SessionSlider_vue_vue_type_template_id_70e70288___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SessionSlider_vue_vue_type_template_id_70e70288___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/session/LoginOne.vue":
/*!*************************************************!*\
  !*** ./resources/js/views/session/LoginOne.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _LoginOne_vue_vue_type_template_id_d7480896___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LoginOne.vue?vue&type=template&id=d7480896& */ "./resources/js/views/session/LoginOne.vue?vue&type=template&id=d7480896&");
/* harmony import */ var _LoginOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LoginOne.vue?vue&type=script&lang=js& */ "./resources/js/views/session/LoginOne.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _LoginOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _LoginOne_vue_vue_type_template_id_d7480896___WEBPACK_IMPORTED_MODULE_0__["render"],
  _LoginOne_vue_vue_type_template_id_d7480896___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/session/LoginOne.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/session/LoginOne.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/session/LoginOne.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./LoginOne.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/LoginOne.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/session/LoginOne.vue?vue&type=template&id=d7480896&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/session/LoginOne.vue?vue&type=template&id=d7480896& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginOne_vue_vue_type_template_id_d7480896___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./LoginOne.vue?vue&type=template&id=d7480896& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/session/LoginOne.vue?vue&type=template&id=d7480896&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginOne_vue_vue_type_template_id_d7480896___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoginOne_vue_vue_type_template_id_d7480896___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);