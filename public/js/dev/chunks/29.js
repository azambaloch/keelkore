(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewEmails.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/NewEmails.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      viewEmailDialog: false,
      selectDeletedEmail: null,
      selectEmail: null,
      settings: {
        maxScrollbarLength: 100
      },
      emails: null,
      replyTextBox: false,
      messageReply: "",
      snackbar: false,
      snackbarMessage: "",
      timeout: 2000,
      y: "top"
    };
  },
  // call get email method
  mounted: function mounted() {
    this.getEmails();
  },
  methods: {
    // get data from API
    getEmails: function getEmails() {
      var _this = this;

      this.loader = true;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("newEmails.js").then(function (response) {
        _this.loader = false;
        _this.emails = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    // view email
    onViewEmail: function onViewEmail(getEmail) {
      this.viewEmailDialog = true;
      this.selectEmail = getEmail;
    },
    // confirmation dialog to delete
    onDeleteEmail: function onDeleteEmail(email) {
      this.$refs.deleteConfirmationDialog.openDialog();
      this.selectDeletedEmail = email;
    },
    // delete email if cofirmation is true
    deleteEmail: function deleteEmail() {
      var _this2 = this;

      this.$refs.deleteConfirmationDialog.close();
      this.loader = true;
      var newEmail = this.emails;
      var index = newEmail.indexOf(this.selectDeletedEmail);
      setTimeout(function () {
        _this2.loader = false;
        _this2.selectDeletedEmail = null;

        _this2.emails.splice(index, 1);

        _this2.snackbar = true;
        _this2.snackbarMessage = "Email Deleted Successfully!";
      }, 1500);
    },
    // show reply text box
    showReplyTextBox: function showReplyTextBox(email) {
      var indexOfEmail = this.emails.indexOf(email);
      this.replyTextBox = true;
      this.emails[indexOfEmail].replyTextBox = this.replyTextBox;
    },
    // reply email
    replyEmail: function replyEmail(email) {
      var _this3 = this;

      var indexOfEmail = this.emails.indexOf(email);
      this.loader = true;
      this.replyTextBox = false;
      this.emails[indexOfEmail].replyTextBox = this.replyTextBox;
      setTimeout(function () {
        _this3.messageReply = "";
        _this3.loader = false;
        _this3.snackbar = true;
        _this3.snackbarMessage = "Reply Sent Successfully!";
      }, 1500);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_DoughnutChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/DoughnutChart */ "./resources/js/components/Charts/DoughnutChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    DoughnutChart: _Charts_DoughnutChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Widgets_SupportRequest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Widgets/SupportRequest */ "./resources/js/components/Widgets/SupportRequest.vue");
/* harmony import */ var Components_Widgets_BlogLayoutTwo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Widgets/BlogLayoutTwo */ "./resources/js/components/Widgets/BlogLayoutTwo.vue");
/* harmony import */ var Components_Widgets_BlogLayoutThree__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Widgets/BlogLayoutThree */ "./resources/js/components/Widgets/BlogLayoutThree.vue");
/* harmony import */ var Components_Widgets_NewEmails__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/NewEmails */ "./resources/js/components/Widgets/NewEmails.vue");
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data */ "./resources/js/views/widgets/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SupportRequest: Components_Widgets_SupportRequest__WEBPACK_IMPORTED_MODULE_0__["default"],
    BlogLayoutTwo: Components_Widgets_BlogLayoutTwo__WEBPACK_IMPORTED_MODULE_1__["default"],
    BlogLayoutThree: Components_Widgets_BlogLayoutThree__WEBPACK_IMPORTED_MODULE_2__["default"],
    NewEmails: Components_Widgets_NewEmails__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  data: function data() {
    return {
      activeUser: _data__WEBPACK_IMPORTED_MODULE_4__["activeUser"]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutThree.vue?vue&type=template&id=d9f8c312&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BlogLayoutThree.vue?vue&type=template&id=d9f8c312& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "blog-layout-three" },
    [
      _c(
        "v-img",
        {
          staticClass: "white--text align-end",
          attrs: { src: "/static/img/carousel-slider-2.jpg", height: "330px" }
        },
        [_c("v-card-title", [_vm._v("Top 10 Australian beaches")])],
        1
      ),
      _vm._v(" "),
      _c("v-card-title", { staticClass: "px-6 py-4" }, [
        _c("div", [
          _c("span", { staticClass: "grey--text fs-12 fw-normal" }, [
            _vm._v("Number 10")
          ]),
          _c("br"),
          _vm._v(" "),
          _c("p", { staticClass: "mb-0" }, [
            _vm._v(
              "Whitehaven Beach Whitsunday Island,necessitatibus sit exercitationem aut quo quos inventore, Whitsunday Islands, Ullam expedita,necessitatibus sit exercitationem aut quo quos inventore necessitatibus sit exercitationem aut quo quos inventore,\n            "
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "v-card-actions",
        [
          _c(
            "v-btn",
            { staticClass: "mr-4", attrs: { icon: "" } },
            [_c("v-icon", { attrs: { color: "success" } }, [_vm._v("share")])],
            1
          ),
          _vm._v(" "),
          _c(
            "v-btn",
            { attrs: { icon: "" } },
            [_c("v-icon", { attrs: { color: "error" } }, [_vm._v("favorite")])],
            1
          ),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "v-btn",
            { attrs: { icon: "" } },
            [
              _c("v-icon", { staticClass: "grey--text" }, [
                _vm._v("more_horiz")
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutTwo.vue?vue&type=template&id=780bdf36&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BlogLayoutTwo.vue?vue&type=template&id=780bdf36& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "blog-layout-two" },
    [
      _c("v-card-title", { staticClass: "pa-6" }, [
        _c("div", [
          _c("h4", { staticClass: "mb-0" }, [
            _vm._v("Where Can You Find Unique Myspace Layouts Nowadays")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "grey--text fs-12 fw-normal" }, [
            _vm._v("11 Nov 2017 , By: Admin , 5 Comments ")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("v-img", {
        attrs: { src: "/static/img/blog-6.jpg", height: "240px" }
      }),
      _vm._v(" "),
      _c("v-card-text", { staticClass: "px-6 py-4" }, [
        _vm._v(
          "\n\t\t\tConsectetur adipisicing elit. Ullam expedita, necessitatibus sit exercitationem aut quo quos inventore, similique nulla minima distinctio illo iste dignissimos vero nostrum, magni pariatur delectus natus.      \n\t\t"
        )
      ]),
      _vm._v(" "),
      _c(
        "v-card-actions",
        { staticClass: "px-6 py-2" },
        [
          _c(
            "v-btn",
            { staticClass: "mr-4", attrs: { icon: "" } },
            [_c("v-icon", { attrs: { color: "success" } }, [_vm._v("share")])],
            1
          ),
          _vm._v(" "),
          _c(
            "v-btn",
            { attrs: { icon: "" } },
            [_c("v-icon", { attrs: { color: "error" } }, [_vm._v("favorite")])],
            1
          ),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "v-btn",
            { attrs: { icon: "" } },
            [
              _c("v-icon", { staticClass: "grey--text" }, [
                _vm._v("more_horiz")
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewEmails.vue?vue&type=template&id=85239788&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/NewEmails.vue?vue&type=template&id=85239788& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "540px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "ul",
            { staticClass: "new-mail list-unstyled" },
            _vm._l(_vm.emails, function(email) {
              return _c(
                "li",
                { key: email.id },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "d-flex flex-wrap justify-space-between align-items-center"
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "d-flex justify-start" },
                        [
                          email.sender_avatar !== ""
                            ? _c("v-list-item-avatar", [
                                _c("img", {
                                  staticClass: "img-responsive",
                                  attrs: {
                                    src: email.sender_avatar,
                                    alt: "user"
                                  }
                                })
                              ])
                            : [
                                _c(
                                  "v-avatar",
                                  { staticClass: "warning mr-3" },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticClass: "white--text",
                                        attrs: { small: "" }
                                      },
                                      [
                                        _vm._v(
                                          " " +
                                            _vm._s(email.sender_name.charAt(0))
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ],
                          _vm._v(" "),
                          _c("v-list-item-content", [
                            _c("h5", { staticClass: "mb-0" }, [
                              _vm._v(_vm._s(email.sender_name))
                            ]),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "fs-12 grey--text fw-normal" },
                              [_vm._v(_vm._s(email.from))]
                            )
                          ])
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c("div", {}, [
                        _c("span", { staticClass: "fs-12 fw-normal" }, [
                          _vm._v("19 Mar 2017")
                        ])
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("h6", { staticClass: "mb-1" }, [
                    _vm._v(_vm._s(email.subject))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "mail-content" }, [
                    _c("p", { staticClass: "fs-14" }, [
                      _vm._v(_vm._s(email.message))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "mail-action w-20 text-right" }, [
                      _c(
                        "div",
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "mr-1",
                              attrs: {
                                fab: "",
                                dark: "",
                                small: "",
                                color: "primary"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.onViewEmail(email)
                                }
                              }
                            },
                            [_c("v-icon", [_vm._v("visibility")])],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              staticClass: "mr-1",
                              attrs: {
                                fab: "",
                                dark: "",
                                small: "",
                                color: "error"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.onDeleteEmail(email)
                                }
                              }
                            },
                            [_c("v-icon", [_vm._v("delete")])],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              attrs: {
                                fab: "",
                                dark: "",
                                small: "",
                                color: "success"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.showReplyTextBox(email)
                                }
                              }
                            },
                            [_c("v-icon", [_vm._v("reply")])],
                            1
                          )
                        ],
                        1
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  email.replyTextBox
                    ? [
                        _c(
                          "div",
                          { staticClass: "d-custom-flex" },
                          [
                            _c("v-text-field", {
                              attrs: { placeholder: "Reply Message" },
                              model: {
                                value: _vm.messageReply,
                                callback: function($$v) {
                                  _vm.messageReply = $$v
                                },
                                expression: "messageReply"
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "v-btn",
                              {
                                attrs: { color: "primary" },
                                on: {
                                  click: function($event) {
                                    return _vm.replyEmail(email)
                                  }
                                }
                              },
                              [_vm._v("Reply")]
                            )
                          ],
                          1
                        )
                      ]
                    : _vm._e()
                ],
                2
              )
            }),
            0
          )
        ]
      ),
      _vm._v(" "),
      _c("delete-confirmation-dialog", {
        ref: "deleteConfirmationDialog",
        attrs: {
          heading: "Are You Sure You Want To Delete?",
          message: "Are you sure you want to delete this email permanently?"
        },
        on: { onConfirm: _vm.deleteEmail }
      }),
      _vm._v(" "),
      _vm.selectEmail !== null
        ? _c(
            "v-dialog",
            {
              attrs: { "max-width": "600" },
              model: {
                value: _vm.viewEmailDialog,
                callback: function($$v) {
                  _vm.viewEmailDialog = $$v
                },
                expression: "viewEmailDialog"
              }
            },
            [
              _c(
                "v-list",
                [
                  _c(
                    "v-list-item",
                    { staticClass: "py-2" },
                    [
                      _vm.selectEmail.sender_avatar !== ""
                        ? _c("v-list-item-avatar", [
                            _c("img", {
                              staticClass: "img-responsive",
                              attrs: {
                                src: _vm.selectEmail.sender_avatar,
                                alt: "user"
                              }
                            })
                          ])
                        : _c(
                            "v-list-item-avatar",
                            [
                              _c("v-avatar", { staticClass: "teal" }, [
                                _c(
                                  "span",
                                  { staticClass: "white--text headline" },
                                  [
                                    _vm._v(
                                      " " +
                                        _vm._s(
                                          _vm.selectEmail.sender_name.charAt(0)
                                        )
                                    )
                                  ]
                                )
                              ])
                            ],
                            1
                          ),
                      _vm._v(" "),
                      _c(
                        "v-list-item-content",
                        [
                          _c("v-list-item-subtitle", [
                            _c(
                              "p",
                              { staticClass: "fs-14 fw-bold mb-0 gray--text" },
                              [
                                _vm._v(
                                  "\n\t\t\t\t\t\t\t\t" +
                                    _vm._s(_vm.selectEmail.sender_name) +
                                    "\n\t\t\t\t\t\t\t"
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("span", [_vm._v(_vm._s(_vm.selectEmail.from))])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("v-list-item-action", [
                        _c("span", { staticClass: "fs-14" }, [
                          _vm._v("19 Mar 2017")
                        ])
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "pa-3" }, [
                    _c("h3", { staticClass: "subject" }, [
                      _vm._v(_vm._s(_vm.selectEmail.subject))
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "fs-14 mb-0" }, [
                      _vm._v(
                        "\n                " +
                          _vm._s(_vm.selectEmail.message) +
                          "\n              "
                      )
                    ])
                  ])
                ],
                1
              )
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { top: _vm.y === "top", timeout: _vm.timeout },
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [_vm._v("\n         " + _vm._s(_vm.snackbarMessage) + "\n      ")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "support-widget" },
    [
      _c("doughnut-chart", { attrs: { height: 150 } }),
      _vm._v(" "),
      _c("ul", { staticClass: "list-unstyled" }, [
        _c(
          "li",
          { staticClass: "justify-space-between align-center" },
          [
            _c("p", { staticClass: "mb-0 content-title" }, [
              _vm._v(_vm._s(_vm.$t("message.totalRequest")))
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "badge-wrap" },
              [
                _c(
                  "v-badge",
                  { staticClass: "primary", attrs: { value: false } },
                  [_vm._v("250")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "grey--text" }, [
                  _vm._v("visibility")
                ])
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "justify-space-between align-center" },
          [
            _c("p", { staticClass: "mb-0 content-title" }, [
              _vm._v(_vm._s(_vm.$t("message.new")))
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "badge-wrap" },
              [
                _c(
                  "v-badge",
                  { staticClass: "warning", attrs: { value: false } },
                  [_vm._v("25")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "grey--text" }, [
                  _vm._v("visibility")
                ])
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "justify-space-between align-center" },
          [
            _c("p", { staticClass: "mb-0 content-title" }, [
              _vm._v(_vm._s(_vm.$t("message.pending")))
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "badge-wrap" },
              [
                _c(
                  "v-badge",
                  { staticClass: "error", attrs: { value: false } },
                  [_vm._v("125")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "grey--text" }, [
                  _vm._v("visibility")
                ])
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=template&id=5074fd41&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=template&id=5074fd41& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                    heading: _vm.$t("message.supportRequest"),
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    footer: true,
                    fullBlock: true,
                    customClasses: "support-widget-wrap"
                  }
                },
                [
                  _c("support-request"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "justify-space-between footer-flex align-items-center",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "span",
                        {
                          staticClass:
                            "grey--text d-custom-flex align-items-center"
                        },
                        [
                          _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v("Updated 10 min ago")
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-0",
                          attrs: { color: "primary", small: "" }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.assignNow")))]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    fullBlock: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12"
                  }
                },
                [_c("blog-layout-two")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    fullBlock: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12"
                  }
                },
                [_c("blog-layout-three")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses:
                      "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                    heading: "New Emails",
                    fullBlock: true,
                    closeable: true,
                    reloadable: true,
                    fullScreen: true
                  }
                },
                [_c("new-emails")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/index.js":
/*!***********************************!*\
  !*** ./resources/js/api/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: 'https://reactify.theironnetwork.org/data/'
}));

/***/ }),

/***/ "./resources/js/components/Charts/DoughnutChart.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/Charts/DoughnutChart.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Doughnut Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Doughnut"],
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      options: {
        legend: {
          display: false
        },
        cutoutPercentage: 60,
        padding: 10
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Total Request', 'New', 'Pending'],
      datasets: [{
        data: [250, 25, 125],
        backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger],
        hoverBackgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutThree.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutThree.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogLayoutThree_vue_vue_type_template_id_d9f8c312___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogLayoutThree.vue?vue&type=template&id=d9f8c312& */ "./resources/js/components/Widgets/BlogLayoutThree.vue?vue&type=template&id=d9f8c312&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _BlogLayoutThree_vue_vue_type_template_id_d9f8c312___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogLayoutThree_vue_vue_type_template_id_d9f8c312___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BlogLayoutThree.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutThree.vue?vue&type=template&id=d9f8c312&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutThree.vue?vue&type=template&id=d9f8c312& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutThree_vue_vue_type_template_id_d9f8c312___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogLayoutThree.vue?vue&type=template&id=d9f8c312& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutThree.vue?vue&type=template&id=d9f8c312&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutThree_vue_vue_type_template_id_d9f8c312___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutThree_vue_vue_type_template_id_d9f8c312___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutTwo.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutTwo.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogLayoutTwo_vue_vue_type_template_id_780bdf36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogLayoutTwo.vue?vue&type=template&id=780bdf36& */ "./resources/js/components/Widgets/BlogLayoutTwo.vue?vue&type=template&id=780bdf36&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _BlogLayoutTwo_vue_vue_type_template_id_780bdf36___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogLayoutTwo_vue_vue_type_template_id_780bdf36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BlogLayoutTwo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutTwo.vue?vue&type=template&id=780bdf36&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutTwo.vue?vue&type=template&id=780bdf36& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutTwo_vue_vue_type_template_id_780bdf36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogLayoutTwo.vue?vue&type=template&id=780bdf36& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutTwo.vue?vue&type=template&id=780bdf36&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutTwo_vue_vue_type_template_id_780bdf36___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutTwo_vue_vue_type_template_id_780bdf36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/NewEmails.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/NewEmails.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NewEmails_vue_vue_type_template_id_85239788___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NewEmails.vue?vue&type=template&id=85239788& */ "./resources/js/components/Widgets/NewEmails.vue?vue&type=template&id=85239788&");
/* harmony import */ var _NewEmails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NewEmails.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/NewEmails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _NewEmails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NewEmails_vue_vue_type_template_id_85239788___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NewEmails_vue_vue_type_template_id_85239788___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/NewEmails.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/NewEmails.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Widgets/NewEmails.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewEmails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./NewEmails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewEmails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewEmails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/NewEmails.vue?vue&type=template&id=85239788&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/NewEmails.vue?vue&type=template&id=85239788& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewEmails_vue_vue_type_template_id_85239788___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./NewEmails.vue?vue&type=template&id=85239788& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewEmails.vue?vue&type=template&id=85239788&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewEmails_vue_vue_type_template_id_85239788___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewEmails_vue_vue_type_template_id_85239788___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequest.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequest.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SupportRequest.vue?vue&type=template&id=11de476b& */ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&");
/* harmony import */ var _SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SupportRequest.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SupportRequest.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportRequest.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportRequest.vue?vue&type=template&id=11de476b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/widgets/data.js":
/*!********************************************!*\
  !*** ./resources/js/views/widgets/data.js ***!
  \********************************************/
/*! exports provided: dailySales, trafficChannel, spaceUsed, activeUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailySales", function() { return dailySales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "trafficChannel", function() { return trafficChannel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "spaceUsed", function() { return spaceUsed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "activeUser", function() { return activeUser; });
/* harmony import */ var _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../constants/chart-config */ "./resources/js/constants/chart-config.js");
 // Daily Sales

var dailySales = {
  label: 'Daily Sales',
  chartdata: [100, 200, 125, 250, 200, 150, 200],
  labels: ['9', '10', '11', '12', '13', '14', '15']
}; //Traffic Channel

var trafficChannel = {
  label: 'Direct User',
  labels: ['Direct User', 'Referral', 'Facebook', 'Google', 'Instagram'],
  chartdata: [8.5, 6.75, 5.5, 7, 4.75]
}; // Space Used

var spaceUsed = {
  chartData: {
    labels: ['Space Used', 'Space Left'],
    datasets: [{
      data: [275, 100],
      backgroundColor: [_constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger, _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning],
      hoverBackgroundColor: [_constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger, _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
    }]
  }
}; // Active User

var activeUser = [{
  id: 1,
  flag: 'icons8-usa',
  countryName: 'United States',
  userCount: 150,
  userPercent: 20,
  status: 1
}, {
  id: 2,
  flag: 'icons8-hungary',
  countryName: 'Hungary',
  userCount: 180,
  userPercent: -5,
  status: 0
}, {
  id: 3,
  flag: 'icons8-france',
  countryName: 'France',
  userCount: 86,
  userPercent: 20,
  status: 1
}, {
  id: 4,
  flag: 'icons8-japan',
  countryName: 'Japan',
  userCount: 243,
  userPercent: 20,
  status: 1
}, {
  id: 5,
  flag: 'icons8-china',
  countryName: 'China',
  userCount: 155,
  userPercent: 20,
  status: 0
}, {
  id: 6,
  flag: 'ru',
  countryName: 'Russia',
  userCount: 155,
  userPercent: 20,
  status: 0
}];

/***/ }),

/***/ "./resources/js/views/widgets/user-widgets/UserWidgets.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/widgets/user-widgets/UserWidgets.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserWidgets_vue_vue_type_template_id_5074fd41___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserWidgets.vue?vue&type=template&id=5074fd41& */ "./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=template&id=5074fd41&");
/* harmony import */ var _UserWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserWidgets.vue?vue&type=script&lang=js& */ "./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserWidgets_vue_vue_type_template_id_5074fd41___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserWidgets_vue_vue_type_template_id_5074fd41___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/widgets/user-widgets/UserWidgets.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserWidgets.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=template&id=5074fd41&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=template&id=5074fd41& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserWidgets_vue_vue_type_template_id_5074fd41___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserWidgets.vue?vue&type=template&id=5074fd41& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/user-widgets/UserWidgets.vue?vue&type=template&id=5074fd41&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserWidgets_vue_vue_type_template_id_5074fd41___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserWidgets_vue_vue_type_template_id_5074fd41___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);