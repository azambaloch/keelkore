(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[95],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Dialog.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Dialog.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      dialog: false,
      dialog2: false,
      dialog3: false,
      dialog4: false,
      dialog5: false,
      dialogm1: "",
      notifications: false,
      sound: true,
      widgets: false,
      items: [{
        title: "Click Me"
      }, {
        title: "Click Me"
      }, {
        title: "Click Me"
      }, {
        title: "Click Me 2"
      }],
      select: [{
        text: "State 1"
      }, {
        text: "State 2"
      }, {
        text: "State 3"
      }, {
        text: "State 4"
      }, {
        text: "State 5"
      }, {
        text: "State 6"
      }, {
        text: "State 7"
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Dialog.vue?vue&type=template&id=a396c7f4&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Dialog.vue?vue&type=template&id=a396c7f4& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.simpleDialogs"),
                    colClasses: "col-lg-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-4" }, [
                    _c("p", [
                      _vm._v(
                        "Choosing an option immediately commits the option and closes the menu. Touching outside of the dialog, or pressing Back, cancels the action and closes the dialog."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "primary" },
                      on: {
                        click: function($event) {
                          $event.stopPropagation()
                          _vm.dialog = true
                        }
                      }
                    },
                    [_vm._v("Open Dialog 1")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "success" },
                      on: {
                        click: function($event) {
                          $event.stopPropagation()
                          _vm.dialog2 = true
                        }
                      }
                    },
                    [_vm._v("Open Dialog 2")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "error" },
                      on: {
                        click: function($event) {
                          $event.stopPropagation()
                          _vm.dialog3 = true
                        }
                      }
                    },
                    [_vm._v("Open Dialog 3")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      attrs: { bottom: "", "offset-y": "" },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            return [
                              _c(
                                "v-btn",
                                _vm._g({ attrs: { color: "warning" } }, on),
                                [_vm._v("A Menu")]
                              )
                            ]
                          }
                        }
                      ])
                    },
                    [
                      _vm._v(" "),
                      _c(
                        "v-list",
                        _vm._l(_vm.items, function(item, i) {
                          return _c(
                            "v-list-item",
                            { key: i },
                            [
                              _c("v-list-item-title", [
                                _vm._v(_vm._s(item.title))
                              ])
                            ],
                            1
                          )
                        }),
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: {
                        fullscreen: "",
                        transition: "dialog-bottom-transition",
                        overlay: "false",
                        scrollable: ""
                      },
                      model: {
                        value: _vm.dialog,
                        callback: function($$v) {
                          _vm.dialog = $$v
                        },
                        expression: "dialog"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c(
                            "v-toolbar",
                            {
                              staticClass: "dialog-toolbar mb-6",
                              attrs: { color: "primary", dark: "" }
                            },
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: { icon: "", dark: "" },
                                  nativeOn: {
                                    click: function($event) {
                                      _vm.dialog = false
                                    }
                                  }
                                },
                                [_c("v-icon", [_vm._v("close")])],
                                1
                              ),
                              _vm._v(" "),
                              _c("v-toolbar-title", [_vm._v("Settings")]),
                              _vm._v(" "),
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-toolbar-items",
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: { dark: "", text: "" },
                                      nativeOn: {
                                        click: function($event) {
                                          _vm.dialog = false
                                        }
                                      }
                                    },
                                    [_vm._v("Save")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-menu",
                                    {
                                      attrs: {
                                        bottom: "",
                                        right: "",
                                        "offset-y": ""
                                      },
                                      scopedSlots: _vm._u([
                                        {
                                          key: "activator",
                                          fn: function(ref) {
                                            var on = ref.on
                                            return [
                                              _c(
                                                "v-btn",
                                                _vm._g(
                                                  {
                                                    attrs: {
                                                      dark: "",
                                                      icon: ""
                                                    }
                                                  },
                                                  on
                                                ),
                                                [
                                                  _c("v-icon", [
                                                    _vm._v("more_vert")
                                                  ])
                                                ],
                                                1
                                              )
                                            ]
                                          }
                                        }
                                      ])
                                    },
                                    [
                                      _vm._v(" "),
                                      _c(
                                        "v-list",
                                        _vm._l(_vm.items, function(item, key) {
                                          return _c(
                                            "v-list-item",
                                            { key: key },
                                            [
                                              _c("v-list-item-title", [
                                                _vm._v(_vm._s(item.title))
                                              ])
                                            ],
                                            1
                                          )
                                        }),
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c(
                                "v-btn",
                                {
                                  staticClass: "ma-2",
                                  attrs: { color: "warning" },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      _vm.dialog2 = !_vm.dialog2
                                    }
                                  }
                                },
                                [_vm._v("Open Dialog 2")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-tooltip",
                                {
                                  attrs: { right: "" },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-btn",
                                            _vm._g(
                                              { attrs: { color: "primary" } },
                                              on
                                            ),
                                            [_vm._v("Tool Tip Activator")]
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                },
                                [
                                  _vm._v(
                                    "\n\t\t\t\t\t\t\t\tTool Tip\n\t\t\t\t\t\t\t"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-list",
                                { attrs: { "three-line": "", subheader: "" } },
                                [
                                  _c("v-subheader", [_vm._v("User Controls")]),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item",
                                    [
                                      _c(
                                        "v-list-item-content",
                                        [
                                          _c(
                                            "v-list-item-title",
                                            { staticClass: "fw-normal" },
                                            [_vm._v("Content filtering")]
                                          ),
                                          _vm._v(" "),
                                          _c("v-list-item-subtitle", [
                                            _vm._v(
                                              "Set the content filtering level to restrict apps that can be downloaded\n\t\t\t\t\t\t\t\t\t\t"
                                            )
                                          ])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item",
                                    [
                                      _c(
                                        "v-list-item-content",
                                        [
                                          _c(
                                            "v-list-item-title",
                                            { staticClass: "fw-normal" },
                                            [_vm._v("Password")]
                                          ),
                                          _vm._v(" "),
                                          _c("v-list-item-subtitle", [
                                            _vm._v(
                                              "Require password for purchase or use password to restrict purchase"
                                            )
                                          ])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("v-divider"),
                              _vm._v(" "),
                              _c(
                                "v-list",
                                { attrs: { "three-line": "", subheader: "" } },
                                [
                                  _c("v-subheader", [_vm._v("General")]),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item",
                                    [
                                      _c(
                                        "v-list-item-action",
                                        [
                                          _c("v-checkbox", {
                                            model: {
                                              value: _vm.notifications,
                                              callback: function($$v) {
                                                _vm.notifications = $$v
                                              },
                                              expression: "notifications"
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-list-item-content",
                                        [
                                          _c(
                                            "v-list-item-title",
                                            { staticClass: "fw-normal" },
                                            [_vm._v("Notifications")]
                                          ),
                                          _vm._v(" "),
                                          _c("v-list-item-subtitle", [
                                            _vm._v(
                                              "Notify me about updates to apps or games that I downloaded"
                                            )
                                          ])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item",
                                    [
                                      _c(
                                        "v-list-item-action",
                                        [
                                          _c("v-checkbox", {
                                            model: {
                                              value: _vm.sound,
                                              callback: function($$v) {
                                                _vm.sound = $$v
                                              },
                                              expression: "sound"
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-list-item-content",
                                        [
                                          _c(
                                            "v-list-item-title",
                                            { staticClass: "fw-normal" },
                                            [_vm._v("Sound")]
                                          ),
                                          _vm._v(" "),
                                          _c("v-list-item-subtitle", [
                                            _vm._v(
                                              "Auto-update apps at any time. Data charges may apply"
                                            )
                                          ])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item",
                                    [
                                      _c(
                                        "v-list-item-action",
                                        [
                                          _c("v-checkbox", {
                                            model: {
                                              value: _vm.widgets,
                                              callback: function($$v) {
                                                _vm.widgets = $$v
                                              },
                                              expression: "widgets"
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-list-item-content",
                                        [
                                          _c(
                                            "v-list-item-title",
                                            { staticClass: "fw-normal" },
                                            [_vm._v("Auto-add widgets")]
                                          ),
                                          _vm._v(" "),
                                          _c("v-list-item-subtitle", [
                                            _vm._v(
                                              "Automatically add home screen widgets"
                                            )
                                          ])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", { staticStyle: { flex: "1 1 auto" } })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: { "max-width": "500px" },
                      model: {
                        value: _vm.dialog2,
                        callback: function($$v) {
                          _vm.dialog2 = $$v
                        },
                        expression: "dialog2"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c("v-card-title", [
                            _vm._v("\n\t\t\t\t\t\t\tDialog 2\n\t\t\t\t\t\t")
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "success" },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      _vm.dialog3 = !_vm.dialog3
                                    }
                                  }
                                },
                                [_vm._v("Open Dialog 3")]
                              ),
                              _vm._v(" "),
                              _c("v-select", {
                                attrs: {
                                  items: _vm.select,
                                  label: "A Select List",
                                  "item-value": "text"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "error" },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      _vm.dialog2 = false
                                    }
                                  }
                                },
                                [_vm._v("Close")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: { "max-width": "500px" },
                      model: {
                        value: _vm.dialog3,
                        callback: function($$v) {
                          _vm.dialog3 = $$v
                        },
                        expression: "dialog3"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c(
                            "v-card-title",
                            [
                              _c("span", [_vm._v("Dialog 3")]),
                              _vm._v(" "),
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-menu",
                                {
                                  attrs: { bottom: "", left: "" },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-btn",
                                            _vm._g({ attrs: { icon: "" } }, on),
                                            [
                                              _c("v-icon", [
                                                _vm._v("more_vert")
                                              ])
                                            ],
                                            1
                                          )
                                        ]
                                      }
                                    }
                                  ])
                                },
                                [
                                  _vm._v(" "),
                                  _c(
                                    "v-list",
                                    _vm._l(_vm.items, function(item, i) {
                                      return _c(
                                        "v-list-item",
                                        { key: i },
                                        [
                                          _c("v-list-item-title", [
                                            _vm._v(_vm._s(item.title))
                                          ])
                                        ],
                                        1
                                      )
                                    }),
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "error" },
                                  on: {
                                    click: function($event) {
                                      $event.stopPropagation()
                                      _vm.dialog3 = false
                                    }
                                  }
                                },
                                [_vm._v("Close")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.withoutActivator"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("div", { staticClass: "mb-4" }, [
                    _c("p", [
                      _vm._v(
                        "If for some reason you are unable to use the activator slot, be sure to add the\n\t\t\t\t\t\t"
                      ),
                      _c("code", [_vm._v(".stop")]),
                      _vm._v(" modifier to the event that triggers the dialog.")
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "primary", dark: "" },
                      nativeOn: {
                        click: function($event) {
                          $event.stopPropagation()
                          _vm.dialog4 = true
                        }
                      }
                    },
                    [_vm._v("Open Dialog")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: { "max-width": "500" },
                      model: {
                        value: _vm.dialog4,
                        callback: function($$v) {
                          _vm.dialog4 = $$v
                        },
                        expression: "dialog4"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c("v-card-title", { staticClass: "headline" }, [
                            _vm._v("Use Google's location service?")
                          ]),
                          _vm._v(" "),
                          _c("v-card-text", [
                            _vm._v(
                              "Let Google help apps determine location. This means sending anonymous location data to Google, even when no apps are running."
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "warning", text: "text" },
                                  nativeOn: {
                                    click: function($event) {
                                      _vm.dialog4 = false
                                    }
                                  }
                                },
                                [_vm._v("Disagree")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "error", text: "text" },
                                  nativeOn: {
                                    click: function($event) {
                                      _vm.dialog4 = false
                                    }
                                  }
                                },
                                [_vm._v("Agree")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.scrollable"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("div", { staticClass: "mb-4" }, [
                    _c("p", [
                      _vm._v("Example of a dialog with scrollable content.")
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: { scrollable: "", "max-width": "300px" },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            return [
                              _c(
                                "v-btn",
                                _vm._g(
                                  { attrs: { color: "warning", dark: "" } },
                                  on
                                ),
                                [_vm._v("Open Dialog")]
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.dialog5,
                        callback: function($$v) {
                          _vm.dialog5 = $$v
                        },
                        expression: "dialog5"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c(
                        "v-card",
                        [
                          _c("v-card-title", [_vm._v("Select Country")]),
                          _vm._v(" "),
                          _c("v-divider"),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            { staticStyle: { height: "300px" } },
                            [
                              _c(
                                "v-radio-group",
                                {
                                  attrs: { column: "" },
                                  model: {
                                    value: _vm.dialogm1,
                                    callback: function($$v) {
                                      _vm.dialogm1 = $$v
                                    },
                                    expression: "dialogm1"
                                  }
                                },
                                [
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Bahamas, The",
                                      value: "bahamas"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Bahrain",
                                      value: "bahrain"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Bangladesh",
                                      value: "bangladesh"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Barbados",
                                      value: "barbados"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Belarus",
                                      value: "belarus"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Belgium",
                                      value: "belgium"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "Belize", value: "belize" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "Benin", value: "benin" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "Bhutan", value: "bhutan" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Bolivia",
                                      value: "bolivia"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Bosnia and Herzegovina",
                                      value: "bosnia"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Botswana",
                                      value: "botswana"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "Brazil", value: "brazil" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "Brunei", value: "brunei" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Bulgaria",
                                      value: "bulgaria"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Burkina Faso",
                                      value: "burkina"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "Burma", value: "burma" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "Burundi",
                                      value: "burundi"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("v-divider"),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "secondary" },
                                  nativeOn: {
                                    click: function($event) {
                                      _vm.dialog5 = false
                                    }
                                  }
                                },
                                [_vm._v("Close")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "success" },
                                  nativeOn: {
                                    click: function($event) {
                                      _vm.dialog5 = false
                                    }
                                  }
                                },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Dialog.vue":
/*!***************************************************!*\
  !*** ./resources/js/views/ui-elements/Dialog.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dialog_vue_vue_type_template_id_a396c7f4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dialog.vue?vue&type=template&id=a396c7f4& */ "./resources/js/views/ui-elements/Dialog.vue?vue&type=template&id=a396c7f4&");
/* harmony import */ var _Dialog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dialog.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Dialog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dialog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dialog_vue_vue_type_template_id_a396c7f4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dialog_vue_vue_type_template_id_a396c7f4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Dialog.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Dialog.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Dialog.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dialog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dialog.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Dialog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dialog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Dialog.vue?vue&type=template&id=a396c7f4&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Dialog.vue?vue&type=template&id=a396c7f4& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dialog_vue_vue_type_template_id_a396c7f4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dialog.vue?vue&type=template&id=a396c7f4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Dialog.vue?vue&type=template&id=a396c7f4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dialog_vue_vue_type_template_id_a396c7f4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dialog_vue_vue_type_template_id_a396c7f4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);