(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[37],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/charts/VueChartjs.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/charts/VueChartjs.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vue_chartjs_BarChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vue-chartjs/BarChart */ "./resources/js/views/charts/vue-chartjs/BarChart.js");
/* harmony import */ var _vue_chartjs_BubbleChart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vue-chartjs/BubbleChart */ "./resources/js/views/charts/vue-chartjs/BubbleChart.js");
/* harmony import */ var _vue_chartjs_DoughnutChart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vue-chartjs/DoughnutChart */ "./resources/js/views/charts/vue-chartjs/DoughnutChart.js");
/* harmony import */ var _vue_chartjs_LineChart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vue-chartjs/LineChart */ "./resources/js/views/charts/vue-chartjs/LineChart.js");
/* harmony import */ var _vue_chartjs_PieChart__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./vue-chartjs/PieChart */ "./resources/js/views/charts/vue-chartjs/PieChart.js");
/* harmony import */ var _vue_chartjs_PolarAreaChart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./vue-chartjs/PolarAreaChart */ "./resources/js/views/charts/vue-chartjs/PolarAreaChart.js");
/* harmony import */ var _vue_chartjs_RadarChart__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vue-chartjs/RadarChart */ "./resources/js/views/charts/vue-chartjs/RadarChart.js");
/* harmony import */ var _vue_chartjs_GradientLineChart__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./vue-chartjs/GradientLineChart */ "./resources/js/views/charts/vue-chartjs/GradientLineChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BarChart: _vue_chartjs_BarChart__WEBPACK_IMPORTED_MODULE_0__["default"],
    BubbleChart: _vue_chartjs_BubbleChart__WEBPACK_IMPORTED_MODULE_1__["default"],
    DoughnutChart: _vue_chartjs_DoughnutChart__WEBPACK_IMPORTED_MODULE_2__["default"],
    LineChart: _vue_chartjs_LineChart__WEBPACK_IMPORTED_MODULE_3__["default"],
    PieChart: _vue_chartjs_PieChart__WEBPACK_IMPORTED_MODULE_4__["default"],
    PolarAreaChart: _vue_chartjs_PolarAreaChart__WEBPACK_IMPORTED_MODULE_5__["default"],
    RadarChart: _vue_chartjs_RadarChart__WEBPACK_IMPORTED_MODULE_6__["default"],
    GradientLineChart: _vue_chartjs_GradientLineChart__WEBPACK_IMPORTED_MODULE_7__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/charts/VueChartjs.vue?vue&type=template&id=2a659228&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/charts/VueChartjs.vue?vue&type=template&id=2a659228& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3" },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.barChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [_c("bar-chart", { attrs: { width: 370, height: 246 } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.lineChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [_c("line-chart", { attrs: { width: 370, height: 246 } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.doughnutChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [_c("doughnut-chart", { attrs: { width: 300, height: 110 } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.gradientLineChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [
                  _c("gradient-line-chart", {
                    attrs: { width: 370, height: 246 }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.polarAreaChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [
                  _c("polar-area-chart", { attrs: { width: 370, height: 246 } })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.radarChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [_c("radar-chart", { attrs: { width: 370, height: 246 } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.bubbleChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [_c("bubble-chart", { attrs: { width: 370, height: 246 } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.pieChart"),
                    colClasses: "col-12 col-sm-6 col-md-6"
                  }
                },
                [_c("pie-chart", { attrs: { width: 370, height: 110 } })],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/charts/VueChartjs.vue":
/*!**************************************************!*\
  !*** ./resources/js/views/charts/VueChartjs.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VueChartjs_vue_vue_type_template_id_2a659228___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VueChartjs.vue?vue&type=template&id=2a659228& */ "./resources/js/views/charts/VueChartjs.vue?vue&type=template&id=2a659228&");
/* harmony import */ var _VueChartjs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VueChartjs.vue?vue&type=script&lang=js& */ "./resources/js/views/charts/VueChartjs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VueChartjs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VueChartjs_vue_vue_type_template_id_2a659228___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VueChartjs_vue_vue_type_template_id_2a659228___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/charts/VueChartjs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/charts/VueChartjs.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/charts/VueChartjs.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VueChartjs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./VueChartjs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/charts/VueChartjs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VueChartjs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/charts/VueChartjs.vue?vue&type=template&id=2a659228&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/charts/VueChartjs.vue?vue&type=template&id=2a659228& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VueChartjs_vue_vue_type_template_id_2a659228___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VueChartjs.vue?vue&type=template&id=2a659228& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/charts/VueChartjs.vue?vue&type=template&id=2a659228&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VueChartjs_vue_vue_type_template_id_2a659228___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VueChartjs_vue_vue_type_template_id_2a659228___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/BarChart.js":
/*!***********************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/BarChart.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Bar Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  data: function data() {
    return {
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: true
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        categoryPercentage: 0.5,
        barPercentage: 0.8,
        label: 'Series A',
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger,
        data: [40, 35, 60, 70, 20, 35, 40]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/BubbleChart.js":
/*!**************************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/BubbleChart.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Bubble Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bubble"],
  data: function data() {
    return {
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: true
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      datasets: [{
        label: 'Data One',
        categoryPercentage: 0.5,
        barPercentage: 0.8,
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger,
        data: [{
          x: 20,
          y: 25,
          r: 5
        }, {
          x: 40,
          y: 10,
          r: 10
        }, {
          x: 30,
          y: 22,
          r: 30
        }]
      }, {
        label: 'Data Two',
        categoryPercentage: 0.5,
        barPercentage: 0.8,
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning,
        data: [{
          x: 10,
          y: 30,
          r: 15
        }, {
          x: 20,
          y: 20,
          r: 10
        }, {
          x: 15,
          y: 8,
          r: 30
        }]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/DoughnutChart.js":
/*!****************************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/DoughnutChart.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Doughnut Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Doughnut"],
  data: function data() {
    return {
      options: {
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Men', 'Women', 'Kids'],
      datasets: [{
        data: [250, 100, 70],
        backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning],
        borderWidth: [0, 0, 0],
        hoverBackgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/GradientLineChart.js":
/*!********************************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/GradientLineChart.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
// Gradient Line Chart



/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  data: function data() {
    return {
      gradient: null,
      gradient2: null
    };
  },
  mounted: function mounted() {
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(500, 0, 100, 0);
    this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(500, 0, 100, 0);
    this.gradient.addColorStop(0, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger);
    this.gradient.addColorStop(1, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning);
    this.gradient2.addColorStop(0, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 0.6));
    this.gradient2.addColorStop(1, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 0.6));
    this.renderChart({
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Data',
        borderColor: this.gradient,
        pointBorderColor: this.gradient,
        pointBackgroundColor: this.gradient,
        pointHoverBackgroundColor: this.gradient,
        pointHoverBorderColor: this.gradient,
        pointBorderWidth: 10,
        pointHoverRadius: 10,
        pointHoverBorderWidth: 1,
        pointRadius: 3,
        fill: true,
        backgroundColor: this.gradient2,
        borderWidth: 4,
        data: [100, 120, 150, 170, 180, 170, 160, 190]
      }]
    }, {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        position: 'bottom'
      }
    });
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/LineChart.js":
/*!************************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/LineChart.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Line Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  mounted: function mounted() {
    this.renderChart({
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Data One',
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger,
        data: [40, 39, 10, 40, 39, 80, 40]
      }]
    }, {
      responsive: true,
      maintainAspectRatio: false
    });
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/PieChart.js":
/*!***********************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/PieChart.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Pie chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Pie"],
  data: function data() {
    return {
      options: {
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Men', 'Women', 'Kids'],
      datasets: [{
        data: [250, 100, 70],
        backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning],
        borderWidth: [0, 0, 0],
        hoverBackgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/PolarAreaChart.js":
/*!*****************************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/PolarAreaChart.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
// Polar Chart



/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["PolarArea"],
  data: function data() {
    return {
      options: {
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'],
      datasets: [{
        label: 'Series A',
        backgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 0.2),
        borderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 1),
        pointBackgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 1),
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBorderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 1),
        data: [65, 59, 90, 81, 56, 55, 40]
      }, {
        label: 'Series B',
        backgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 0.2),
        borderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 1),
        pointBackgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 1),
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBorderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 1),
        data: [28, 48, 40, 19, 96, 27, 100]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/views/charts/vue-chartjs/RadarChart.js":
/*!*************************************************************!*\
  !*** ./resources/js/views/charts/vue-chartjs/RadarChart.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
// Radar Chart



/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Radar"],
  mounted: function mounted() {
    this.renderChart({
      labels: ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'],
      datasets: [{
        label: 'Series A',
        backgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 0.2),
        borderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 1),
        pointBackgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 1),
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBorderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, 1),
        data: [65, 59, 90, 81, 56, 55, 40]
      }, {
        label: 'Series B',
        backgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 0.2),
        borderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 1),
        pointBackgroundColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 1),
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointHoverBorderColor: Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger, 1),
        data: [28, 48, 40, 19, 96, 27, 100]
      }]
    }, {
      responsive: true,
      maintainAspectRatio: false
    });
  }
});

/***/ })

}]);