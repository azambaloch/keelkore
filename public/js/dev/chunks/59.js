(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[59],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Magazine.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Magazine.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_0__["TheMask"]
  },
  data: function data() {
    return {
      snackbar: false,
      name: '',
      fname: '',
      cnic: '',
      address: '',
      project: '',
      dateOfBirth: '',
      contact: '',
      date: '',
      plotNum: '',
      plotSize: '',
      plotReg: '',
      nName: '',
      nFname: '',
      nRelation: '',
      nCnic: '',
      id: this.$route.params.id,
      totalAmount: 0,
      paidAmount: 0,
      nContact: '',
      nAddress: '',
      projects: [],
      customer: []
    };
  },
  methods: {
    saveData: function saveData() {
      var _this = this;

      var data = {
        name: this.name,
        fname: this.fname,
        cnic: this.cnic,
        address: this.address,
        project: this.project,
        dateOfBirth: this.dateOfBirth,
        contact: this.contact,
        date: this.date,
        plotNum: this.plotNum,
        plotSize: this.plotSize,
        plotReg: this.plotReg,
        nName: this.nName,
        nFname: this.nFname,
        nRelation: this.nRelation,
        nCnic: this.nCnic,
        nContact: this.nContact,
        nAddress: this.nAddress,
        totalAmount: this.totalAmount,
        paidAmount: this.paidAmount,
        id: this.id
      };
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/updateData", data).then(function (res) {
        _this.message = res.data.message;
        _this.snackbar = true;
        _this.name = '', _this.fname = '', _this.cnic = '', _this.address = '', _this.project = '', _this.dateOfBirth = '', _this.contact = '', _this.date = '', _this.plotNum = '', _this.plotSize = '', _this.plotReg = '', _this.nName = '', _this.nFname = '', _this.nRelation = '', _this.nCnic = '', _this.nContact = '', _this.nAddress = '', _this.totalAmount = '', _this.paidAmount = '';
      });
    },
    getProjects: function getProjects() {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/getProjects").then(function (_ref) {
        var data = _ref.data;
        _this2.projects = data.projects; //console.log('data', this.cuss)
      });
    },
    loadCustomersById: function loadCustomersById() {
      var _this3 = this;

      var id = this.$route.params.id;
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/getCustomerDetail/" + id).then(function (res) {
        _this3.customer = res.data.customer; //console.log('data is this okay', this.customer)
      });
    },
    direction: function direction() {
      this.snackbar = false;
      this.$router.push({
        path: '/default/dashboard/ecommerce'
      });
    }
  },
  created: function created() {
    this.getProjects();
    this.loadCustomersById();
  },
  watch: {
    customer: function customer(val) {
      if (val) {
        this.name = val[0].name, this.fname = val[0].fname, this.cnic = val[0].cnic, this.address = val[0].address, this.project = val[0].project, this.dateOfBirth = val[0].dateOfBirth, this.contact = val[0].contact, this.date = val[0].date, this.plotNum = val[0].plotNum, this.plotSize = val[0].plotSize, this.plotReg = val[0].plotReg, this.nName = val[0].nName, this.nFname = val[0].nFname, this.nRelation = val[0].nRelation, this.nCnic = val[0].nCnic, this.nContact = val[0].nContact, this.nAddress = val[0].nAddress, this.totalAmount = val[0].totalAmount, this.paidAmount = val[0].paidAmount;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Magazine.vue?vue&type=template&id=a688eee4&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Magazine.vue?vue&type=template&id=a688eee4& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-snackbar",
        {
          attrs: { centered: "true", timeout: false, color: "success" },
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [
          _vm._v("\n      " + _vm._s(_vm.message) + "\n      "),
          _c(
            "v-btn",
            { attrs: { text: "", top: "" }, on: { click: _vm.direction } },
            [_vm._v("Close")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "", "py-0": "" } },
        [
          _c(
            "app-card",
            { attrs: { heading: "Basic Information", customClasses: "mb-30" } },
            [
              _c(
                "v-form",
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Name", required: "" },
                            model: {
                              value: _vm.name,
                              callback: function($$v) {
                                _vm.name = $$v
                              },
                              expression: "name"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Father Name", required: "" },
                            model: {
                              value: _vm.fname,
                              callback: function($$v) {
                                _vm.fname = $$v
                              },
                              expression: "fname"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            directives: [
                              {
                                name: "mask",
                                rawName: "v-mask",
                                value: "#####-#######-#",
                                expression: "'#####-#######-#'"
                              }
                            ],
                            attrs: { label: "CNIC Number", required: "" },
                            model: {
                              value: _vm.cnic,
                              callback: function($$v) {
                                _vm.cnic = $$v
                              },
                              expression: "cnic"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Address", required: "" },
                            model: {
                              value: _vm.address,
                              callback: function($$v) {
                                _vm.address = $$v
                              },
                              expression: "address"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Date Of Birth", required: "" },
                            model: {
                              value: _vm.dateOfBirth,
                              callback: function($$v) {
                                _vm.dateOfBirth = $$v
                              },
                              expression: "dateOfBirth"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Contact Number", required: "" },
                            model: {
                              value: _vm.contact,
                              callback: function($$v) {
                                _vm.contact = $$v
                              },
                              expression: "contact"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-select", {
                            attrs: {
                              items: _vm.projects,
                              "item-text": "projectName",
                              "item-value": "id",
                              label: "Select Project"
                            },
                            model: {
                              value: _vm.project,
                              callback: function($$v) {
                                _vm.project = $$v
                              },
                              expression: "project"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        [
                          _c(
                            "v-menu",
                            {
                              ref: "menu",
                              attrs: {
                                "close-on-content-click": true,
                                transition: "scale-transition",
                                "offset-y": "",
                                "min-width": "auto"
                              },
                              scopedSlots: _vm._u([
                                {
                                  key: "activator",
                                  fn: function(ref) {
                                    var on = ref.on
                                    var attrs = ref.attrs
                                    return [
                                      _c(
                                        "v-text-field",
                                        _vm._g(
                                          _vm._b(
                                            {
                                              attrs: { label: "Date" },
                                              model: {
                                                value: _vm.date,
                                                callback: function($$v) {
                                                  _vm.date = $$v
                                                },
                                                expression: "date"
                                              }
                                            },
                                            "v-text-field",
                                            attrs,
                                            false
                                          ),
                                          on
                                        )
                                      )
                                    ]
                                  }
                                }
                              ]),
                              model: {
                                value: _vm.menu,
                                callback: function($$v) {
                                  _vm.menu = $$v
                                },
                                expression: "menu"
                              }
                            },
                            [
                              _vm._v(" "),
                              _c("v-date-picker", {
                                attrs: { "active-picker": _vm.activePicker },
                                on: {
                                  "update:activePicker": function($event) {
                                    _vm.activePicker = $event
                                  },
                                  "update:active-picker": function($event) {
                                    _vm.activePicker = $event
                                  }
                                },
                                model: {
                                  value: _vm.date,
                                  callback: function($$v) {
                                    _vm.date = $$v
                                  },
                                  expression: "date"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Plot Number", required: "" },
                            model: {
                              value: _vm.plotNum,
                              callback: function($$v) {
                                _vm.plotNum = $$v
                              },
                              expression: "plotNum"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: {
                              label: "Registration Number",
                              required: ""
                            },
                            model: {
                              value: _vm.plotReg,
                              callback: function($$v) {
                                _vm.plotReg = $$v
                              },
                              expression: "plotReg"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Plot Size", required: "" },
                            model: {
                              value: _vm.plotSize,
                              callback: function($$v) {
                                _vm.plotSize = $$v
                              },
                              expression: "plotSize"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Total Cost", required: "" },
                            model: {
                              value: _vm.totalAmount,
                              callback: function($$v) {
                                _vm.totalAmount = $$v
                              },
                              expression: "totalAmount"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Advanced Payment", required: "" },
                            model: {
                              value: _vm.paidAmount,
                              callback: function($$v) {
                                _vm.paidAmount = $$v
                              },
                              expression: "paidAmount"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            { attrs: { heading: "Nomination", customClasses: "mb-30" } },
            [
              _c(
                "v-form",
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: {
                              label: "I/We hereby Nominee Mr/Mrs/Ms.",
                              required: ""
                            },
                            model: {
                              value: _vm.nName,
                              callback: function($$v) {
                                _vm.nName = $$v
                              },
                              expression: "nName"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "S/o, D/o, W/o:", required: "" },
                            model: {
                              value: _vm.nFname,
                              callback: function($$v) {
                                _vm.nFname = $$v
                              },
                              expression: "nFname"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Relation", required: "" },
                            model: {
                              value: _vm.nRelation,
                              callback: function($$v) {
                                _vm.nRelation = $$v
                              },
                              expression: "nRelation"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            directives: [
                              {
                                name: "mask",
                                rawName: "v-mask",
                                value: "#####-#######-#",
                                expression: "'#####-#######-#'"
                              }
                            ],
                            attrs: { label: "CNIC", required: "" },
                            model: {
                              value: _vm.nCnic,
                              callback: function($$v) {
                                _vm.nCnic = $$v
                              },
                              expression: "nCnic"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Contact Number", required: "" },
                            model: {
                              value: _vm.nContact,
                              callback: function($$v) {
                                _vm.nContact = $$v
                              },
                              expression: "nContact"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        [
                          _c("v-text-field", {
                            attrs: { label: "Address", required: "" },
                            model: {
                              value: _vm.nAddress,
                              callback: function($$v) {
                                _vm.nAddress = $$v
                              },
                              expression: "nAddress"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "primary left",
                      on: { click: _vm.saveData }
                    },
                    [_vm._v("Submit")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/Magazine.vue":
/*!***************************************************!*\
  !*** ./resources/js/views/dashboard/Magazine.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Magazine_vue_vue_type_template_id_a688eee4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Magazine.vue?vue&type=template&id=a688eee4& */ "./resources/js/views/dashboard/Magazine.vue?vue&type=template&id=a688eee4&");
/* harmony import */ var _Magazine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Magazine.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/Magazine.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Magazine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Magazine_vue_vue_type_template_id_a688eee4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Magazine_vue_vue_type_template_id_a688eee4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/Magazine.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/Magazine.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/dashboard/Magazine.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Magazine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Magazine.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Magazine.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Magazine_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/Magazine.vue?vue&type=template&id=a688eee4&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/dashboard/Magazine.vue?vue&type=template&id=a688eee4& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Magazine_vue_vue_type_template_id_a688eee4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Magazine.vue?vue&type=template&id=a688eee4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Magazine.vue?vue&type=template&id=a688eee4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Magazine_vue_vue_type_template_id_a688eee4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Magazine_vue_vue_type_template_id_a688eee4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);