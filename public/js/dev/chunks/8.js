(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActiveUser.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ActiveUser.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["data"],
  data: function data() {
    return {
      menu: false,
      settings: {
        maxScrollbarLength: 100
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loaderStatus: false,
      quotesOfTheDay: null
    };
  },
  mounted: function mounted() {
    this.getQuotesOfTheDay();
  },
  computed: {
    slickOptions: function slickOptions() {
      return {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        rtl: this.$store.getters.rtlLayout
      };
    }
  },
  methods: {
    getQuotesOfTheDay: function getQuotesOfTheDay() {
      var _this = this;

      this.loaderStatus = true;
      Api__WEBPACK_IMPORTED_MODULE_1__["default"].get("vuely/quotesOfTheDay.js").then(function (response) {
        _this.loaderStatus = false;
        _this.quotesOfTheDay = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActiveUser.vue?vue&type=template&id=c7907ec8&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ActiveUser.vue?vue&type=template&id=c7907ec8& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "app-card-title info white--text d-block py-5" },
        [
          _c(
            "h4",
            { staticClass: "mb-0 d-custom-flex justify-space-between" },
            [
              _c("span", { staticClass: "white--text" }, [
                _vm._v(_vm._s(_vm.$t("message.activeUsers")))
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "white--text" }, [_vm._v("250")])
            ]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "fs-12 fw-normal mb-0" }, [
            _vm._v(_vm._s(_vm.$t("message.updated10MinAgo")))
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        {
          staticClass: "active-user",
          staticStyle: { height: "375px" },
          attrs: { settings: _vm.settings }
        },
        [
          _c(
            "ul",
            { staticClass: "pl-0 list-group-flush list-unstyled" },
            _vm._l(_vm.data, function(activeUser) {
              return _c(
                "li",
                { key: activeUser.id, staticClass: "d-flex px-4 align-center" },
                [
                  _c(
                    "div",
                    { staticClass: "d-custom-flex align-items-center w-50" },
                    [
                      _c("div", { staticClass: "flag-img mr-4" }, [
                        _c("img", {
                          staticClass: "img-responsive",
                          attrs: {
                            src:
                              "/static/flag-icons/" + activeUser.flag + ".png",
                            alt: "flag-img",
                            width: "44",
                            height: "30"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("h6", { staticClass: "fw-bold mb-0" }, [
                        _vm._v(_vm._s(activeUser.countryName))
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-50 d-custom-flex text-center" }, [
                    _c("span", { staticClass: "w-50 grey--text" }, [
                      _vm._v(_vm._s(activeUser.userCount))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "w-50" }, [
                      activeUser.status === 1
                        ? _c("span", [
                            _c("i", {
                              staticClass: "ti-arrow-up success--text mr-2"
                            })
                          ])
                        : _c("span", [
                            _c("i", {
                              staticClass: "ti-arrow-down error--text mr-2"
                            })
                          ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "grey--text" }, [
                        _vm._v(_vm._s(activeUser.userPercent) + "%")
                      ])
                    ])
                  ])
                ]
              )
            }),
            0
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=template&id=7a497e3a&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=template&id=7a497e3a& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "pa-5 quote-wrapper" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loaderStatus } }),
      _vm._v(" "),
      _c("h5", { staticClass: "mb-5 text-center" }, [
        _vm._v("Quote of the day")
      ]),
      _vm._v(" "),
      !_vm.loaderStatus
        ? _c(
            "slick",
            { attrs: { options: _vm.slickOptions } },
            _vm._l(_vm.quotesOfTheDay, function(quotes) {
              return _c("div", { key: quotes.id }, [
                _c("div", { staticClass: "text-center px-4 py-3" }, [
                  _c(
                    "div",
                    {
                      staticClass:
                        "avatar-wrap mb-50 pos-relative d-inline-block"
                    },
                    [
                      _c("img", {
                        staticClass: "img-responsive rounded-circle mx-auto",
                        attrs: {
                          src: quotes.avatar,
                          alt: "reviwers",
                          width: "80",
                          height: "80"
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "quote-icon primary--text" }, [
                        _c("i", {
                          staticClass:
                            "zmdi font-3x zmdi-quote zmdi-hc-rotate-180"
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "quote-content" }, [
                    _c("h6", { staticClass: "mb-5" }, [
                      _vm._v(_vm._s(quotes.author))
                    ]),
                    _vm._v(" "),
                    _c("p", [_vm._v(_vm._s(quotes.body))])
                  ])
                ])
              ])
            }),
            0
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/AdCampaignPerfomance.js":
/*!****************************************************************!*\
  !*** ./resources/js/components/Charts/AdCampaignPerfomance.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Sales Widget


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  Line: vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: ['data'],
  data: function data() {
    return {
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              display: false,
              max: 80
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }],
          xAxes: [{
            ticks: {
              padding: 10
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    var _this$data = this.data,
        labels = _this$data.labels,
        lineChartData = _this$data.lineChartData,
        barChartData = _this$data.barChartData,
        barChartData2 = _this$data.barChartData2;

    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].shadowColor;
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 12;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    this.renderChart({
      labels: labels,
      datasets: [{
        type: 'line',
        label: lineChartData.label,
        borderColor: lineChartData.color,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        spanGaps: false,
        lineTension: 0,
        fill: false,
        cubicInterpolationMode: 'monotone',
        pointBorderWidth: 2,
        pointRadius: 6,
        pointBorderColor: lineChartData.color,
        data: lineChartData.data
      }, {
        type: 'bar',
        barPercentage: 1.1,
        categoryPercentage: 0.4,
        label: barChartData.label,
        backgroundColor: barChartData.color,
        hoverBackgroundColor: barChartData.color,
        borderWidth: 0,
        data: barChartData.data
      }, {
        type: 'bar',
        barPercentage: 1.1,
        categoryPercentage: 0.4,
        label: barChartData2.label,
        backgroundColor: barChartData2.color,
        hoverBackgroundColor: barChartData2.color,
        borderWidth: 0,
        data: barChartData2.data
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/ActiveUser.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/ActiveUser.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ActiveUser_vue_vue_type_template_id_c7907ec8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActiveUser.vue?vue&type=template&id=c7907ec8& */ "./resources/js/components/Widgets/ActiveUser.vue?vue&type=template&id=c7907ec8&");
/* harmony import */ var _ActiveUser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ActiveUser.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ActiveUser.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ActiveUser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ActiveUser_vue_vue_type_template_id_c7907ec8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ActiveUser_vue_vue_type_template_id_c7907ec8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ActiveUser.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ActiveUser.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ActiveUser.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActiveUser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ActiveUser.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActiveUser.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActiveUser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ActiveUser.vue?vue&type=template&id=c7907ec8&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ActiveUser.vue?vue&type=template&id=c7907ec8& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActiveUser_vue_vue_type_template_id_c7907ec8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ActiveUser.vue?vue&type=template&id=c7907ec8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActiveUser.vue?vue&type=template&id=c7907ec8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActiveUser_vue_vue_type_template_id_c7907ec8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActiveUser_vue_vue_type_template_id_c7907ec8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/QuoteOfTheDay.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/QuoteOfTheDay.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _QuoteOfTheDay_vue_vue_type_template_id_7a497e3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./QuoteOfTheDay.vue?vue&type=template&id=7a497e3a& */ "./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=template&id=7a497e3a&");
/* harmony import */ var _QuoteOfTheDay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./QuoteOfTheDay.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _QuoteOfTheDay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _QuoteOfTheDay_vue_vue_type_template_id_7a497e3a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _QuoteOfTheDay_vue_vue_type_template_id_7a497e3a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/QuoteOfTheDay.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuoteOfTheDay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./QuoteOfTheDay.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuoteOfTheDay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=template&id=7a497e3a&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=template&id=7a497e3a& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_QuoteOfTheDay_vue_vue_type_template_id_7a497e3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./QuoteOfTheDay.vue?vue&type=template&id=7a497e3a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuoteOfTheDay.vue?vue&type=template&id=7a497e3a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_QuoteOfTheDay_vue_vue_type_template_id_7a497e3a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_QuoteOfTheDay_vue_vue_type_template_id_7a497e3a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/widgets/data.js":
/*!********************************************!*\
  !*** ./resources/js/views/widgets/data.js ***!
  \********************************************/
/*! exports provided: dailySales, trafficChannel, spaceUsed, activeUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailySales", function() { return dailySales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "trafficChannel", function() { return trafficChannel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "spaceUsed", function() { return spaceUsed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "activeUser", function() { return activeUser; });
/* harmony import */ var _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../constants/chart-config */ "./resources/js/constants/chart-config.js");
 // Daily Sales

var dailySales = {
  label: 'Daily Sales',
  chartdata: [100, 200, 125, 250, 200, 150, 200],
  labels: ['9', '10', '11', '12', '13', '14', '15']
}; //Traffic Channel

var trafficChannel = {
  label: 'Direct User',
  labels: ['Direct User', 'Referral', 'Facebook', 'Google', 'Instagram'],
  chartdata: [8.5, 6.75, 5.5, 7, 4.75]
}; // Space Used

var spaceUsed = {
  chartData: {
    labels: ['Space Used', 'Space Left'],
    datasets: [{
      data: [275, 100],
      backgroundColor: [_constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger, _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning],
      hoverBackgroundColor: [_constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger, _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
    }]
  }
}; // Active User

var activeUser = [{
  id: 1,
  flag: 'icons8-usa',
  countryName: 'United States',
  userCount: 150,
  userPercent: 20,
  status: 1
}, {
  id: 2,
  flag: 'icons8-hungary',
  countryName: 'Hungary',
  userCount: 180,
  userPercent: -5,
  status: 0
}, {
  id: 3,
  flag: 'icons8-france',
  countryName: 'France',
  userCount: 86,
  userPercent: 20,
  status: 1
}, {
  id: 4,
  flag: 'icons8-japan',
  countryName: 'Japan',
  userCount: 243,
  userPercent: 20,
  status: 1
}, {
  id: 5,
  flag: 'icons8-china',
  countryName: 'China',
  userCount: 155,
  userPercent: 20,
  status: 0
}, {
  id: 6,
  flag: 'ru',
  countryName: 'Russia',
  userCount: 155,
  userPercent: 20,
  status: 0
}];

/***/ })

}]);