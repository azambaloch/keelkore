(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/FilesUploaded.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/FilesUploaded.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  mounted: function mounted() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ManagementDetails.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ManagementDetails.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['managementData']
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectGallery.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectGallery.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      images: ["/static/img/gridlist-1.jpg", "/static/img/gridlist-4.jpg", "/static/img/gridlist-5.jpg"],
      slickOptions: {
        infinite: true,
        speed: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        fade: false,
        cssEase: 'linear',
        arrows: false,
        rtl: this.$store.getters.rtlLayout
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Statistics.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Statistics.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartV6__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartV6 */ "./resources/js/components/Charts/LineChartV6.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data', 'labels', 'label', 'labelX', 'labelY'],
  components: {
    LineChartV6: Components_Charts_LineChartV6__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/ProjectDetails.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crm/ProjectDetails.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
/* harmony import */ var Components_Widgets_FilesUploaded__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Widgets/FilesUploaded */ "./resources/js/components/Widgets/FilesUploaded.vue");
/* harmony import */ var Components_Widgets_ProjectGallery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Widgets/ProjectGallery */ "./resources/js/components/Widgets/ProjectGallery.vue");
/* harmony import */ var Components_Widgets_ManagementDetails__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/ManagementDetails */ "./resources/js/components/Widgets/ManagementDetails.vue");
/* harmony import */ var Components_Widgets_Statistics__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/Statistics */ "./resources/js/components/Widgets/Statistics.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Widgets





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    FilesUploaded: Components_Widgets_FilesUploaded__WEBPACK_IMPORTED_MODULE_1__["default"],
    ProjectGallery: Components_Widgets_ProjectGallery__WEBPACK_IMPORTED_MODULE_2__["default"],
    ManagementDetails: Components_Widgets_ManagementDetails__WEBPACK_IMPORTED_MODULE_3__["default"],
    Statistics: Components_Widgets_Statistics__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  data: function data() {
    return {
      selectedManagement: null,
      loader: false,
      projectData: null,
      statisticsData: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        data: ['60', '5', '100', '60', '170', '25', '60'],
        label: 'Progress',
        label1: 'Time',
        label2: 'Cost'
      },
      valueDeterminate: "60",
      valueDeterminate1: "80",
      valueDeterminate2: "100",
      descriptionDetails: {
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        decriptionHeading: "Integer pharetra mi eu libero convallis ultricies",
        descriptionPoints: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Donec auctor sapien eget sem blandit pharetra.", "In sed tellus congue, rhoncus mi quis, iaculis magna.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla."]
      }
    };
  },
  mounted: function mounted() {
    this.getProjectData();
  },
  methods: {
    getProjectData: function getProjectData() {
      var _this = this;

      this.loader = true;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/projectDetails.js").then(function (response) {
        _this.loader = false;
        _this.projectData = response.data;

        for (var i = 0; i < _this.projectData.length; i++) {
          if (_this.projectData[i].id == _this.$route.params.id) {
            _this.selectedManagement = {
              client: _this.projectData[i].client,
              budget: _this.projectData[i].budget,
              duration: _this.projectData[i].duration,
              name: _this.projectData[i].name,
              teamImage: _this.projectData[i].teamImage,
              status: _this.projectData[i].status,
              statusColor: _this.projectData[i].statusColor,
              deadline: _this.projectData[i].deadline,
              department: _this.projectData[i].department,
              projectManager: _this.projectData[i].projectManager
            };
          }
        }
      })["catch"](function (error) {
        console.log("error" + error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/FilesUploaded.vue?vue&type=template&id=711a459c&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/FilesUploaded.vue?vue&type=template&id=711a459c& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "w-100 px-3" }, [
        _c("div", { staticClass: "d-inline-flex align-items-center" }, [
          _c("div", { staticClass: "mr-3" }, [
            _c("i", { staticClass: "zmdi zmdi-collection-pdf font-lg" })
          ]),
          _vm._v(" "),
          _c("div", [
            _c("h5", { staticClass: "fw-normal mb-0" }, [
              _vm._v("AX_Report.pdf")
            ]),
            _vm._v(" "),
            _c("small", [_vm._v("12/May/2019")])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "w-100 px-3" }, [
        _c("div", { staticClass: "d-inline-flex align-items-center" }, [
          _c("div", { staticClass: "mr-3" }, [
            _c("i", { staticClass: "zmdi zmdi-image font-lg" })
          ]),
          _vm._v(" "),
          _c("div", [
            _c("h5", { staticClass: "fw-normal mb-0" }, [
              _vm._v("Blueprint.jpg")
            ]),
            _vm._v(" "),
            _c("small", [_vm._v("08/May/2019")])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ManagementDetails.vue?vue&type=template&id=22c02da4&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ManagementDetails.vue?vue&type=template&id=22c02da4& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-list",
        { staticClass: "card-list top-selling management-wrap" },
        [
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Budget :")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v(_vm._s(_vm.managementData.budget) + " ")
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Client:")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v(_vm._s(_vm.managementData.client))
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Department:")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v(_vm._s(_vm.managementData.department))
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Duration:")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v(_vm._s(_vm.managementData.duration))
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Project Manager:")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v(_vm._s(_vm.managementData.projectManager))
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Team:")])],
                1
              ),
              _vm._v(" "),
              _c(
                "v-list-item-action",
                { staticClass: "d-inline-block py-0 my-0" },
                _vm._l(_vm.managementData.teamImage, function(img, index) {
                  return _c("img", {
                    key: index,
                    staticClass: "img-circle thumb-gap rounded-circle",
                    attrs: { width: "30", height: "30", src: img }
                  })
                }),
                0
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Status:")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c(
                  "h5",
                  {
                    staticClass: "mb-0",
                    class: _vm.managementData.statusColor
                  },
                  [_vm._v(_vm._s(_vm.managementData.status))]
                )
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-list-item",
            { staticClass: "py-4" },
            [
              _c(
                "v-list-item-content",
                { staticClass: "py-0" },
                [_c("v-list-item-title", [_vm._v("Deadline:")])],
                1
              ),
              _vm._v(" "),
              _c("v-list-item-action", { staticClass: "my-0" }, [
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v(_vm._s(_vm.managementData.deadline))
                ])
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectGallery.vue?vue&type=template&id=4474d524&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectGallery.vue?vue&type=template&id=4474d524& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "top-selling-widget project-gallery-wrap" },
      [
        _c(
          "slick",
          { attrs: { options: _vm.slickOptions } },
          _vm._l(_vm.images, function(img, index) {
            return _c("div", { key: index }, [
              _c("img", { staticClass: "img-responsive", attrs: { src: img } })
            ])
          }),
          0
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Statistics.vue?vue&type=template&id=5c159cce&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Statistics.vue?vue&type=template&id=5c159cce& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "statistics-chart-wrap" },
    [
      _c(
        "v-row",
        [
          _c("line-chart-v6", {
            style: { height: "400px", width: "100%", position: "relative" },
            attrs: {
              dataSet: _vm.data,
              labels: _vm.labels,
              labelX: _vm.labelX,
              labelY: _vm.labelY,
              borderColor: _vm.ChartConfig.color.primary,
              label: _vm.label
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/ProjectDetails.vue?vue&type=template&id=03ff18ce&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crm/ProjectDetails.vue?vue&type=template&id=03ff18ce& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0", attrs: { fluid: "" } },
        [
          _vm.selectedManagement !== null
            ? _c(
                "div",
                [
                  _c("div", { staticClass: "pa-6 project-detail-title" }, [
                    _c("h3", { staticClass: "mb-0" }, [
                      _vm._v(
                        _vm._s(
                          _vm.$t("message" + "." + _vm.selectedManagement.name)
                        )
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12 ",
                            customClasses: "mb-0"
                          }
                        },
                        [
                          _c("div", { staticClass: "sec-title mb-4" }, [
                            _c("h4", [_vm._v("Description")])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "sec-content" }, [
                            _c("p", { staticClass: "mb-2 fw-bold fs-14" }, [
                              _vm._v(
                                _vm._s(_vm.descriptionDetails.decriptionHeading)
                              )
                            ]),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(_vm._s(_vm.descriptionDetails.description))
                            ]),
                            _vm._v(" "),
                            _c(
                              "ul",
                              { staticClass: "pl-3" },
                              _vm._l(
                                _vm.descriptionDetails.descriptionPoints,
                                function(point, index) {
                                  return _c("li", { key: index }, [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t\t" +
                                        _vm._s(point) +
                                        "\n\t\t\t\t\t\t\t"
                                    )
                                  ])
                                }
                              ),
                              0
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12 "
                          }
                        },
                        [
                          _c("management-details", {
                            attrs: { managementData: _vm.selectedManagement }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            heading: _vm.$t("message.filesUploaded"),
                            colClasses:
                              "col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12",
                            customClasses: "mb-0"
                          }
                        },
                        [_c("files-uploaded")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "mb-5" },
                            [
                              _c("h5", [
                                _vm._v(
                                  "Progress: " +
                                    _vm._s(_vm.valueDeterminate) +
                                    "%"
                                )
                              ]),
                              _vm._v(" "),
                              _c("v-progress-linear", {
                                staticClass: "my-2",
                                attrs: { color: "primary", height: "7" },
                                model: {
                                  value: _vm.valueDeterminate,
                                  callback: function($$v) {
                                    _vm.valueDeterminate = $$v
                                  },
                                  expression: "valueDeterminate"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "mb-5" },
                            [
                              _c("h5", [
                                _vm._v(
                                  "Progress: " +
                                    _vm._s(_vm.valueDeterminate1) +
                                    "%"
                                )
                              ]),
                              _vm._v(" "),
                              _c("v-progress-linear", {
                                staticClass: "my-2",
                                attrs: { color: "primary", height: "7" },
                                model: {
                                  value: _vm.valueDeterminate1,
                                  callback: function($$v) {
                                    _vm.valueDeterminate1 = $$v
                                  },
                                  expression: "valueDeterminate1"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            [
                              _c("h5", [
                                _vm._v(
                                  "Progress: " +
                                    _vm._s(_vm.valueDeterminate2) +
                                    "%"
                                )
                              ]),
                              _vm._v(" "),
                              _c("v-progress-linear", {
                                staticClass: "my-2",
                                attrs: { color: "primary", height: "7" },
                                model: {
                                  value: _vm.valueDeterminate2,
                                  callback: function($$v) {
                                    _vm.valueDeterminate2 = $$v
                                  },
                                  expression: "valueDeterminate2"
                                }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            heading: _vm.$t("message.statistics"),
                            colClasses:
                              "col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12",
                            customClasses: "mb-0"
                          }
                        },
                        [
                          _c("statistics", {
                            attrs: {
                              data: _vm.statisticsData.data,
                              labels: _vm.statisticsData.labels,
                              label: _vm.statisticsData.label,
                              labelX: _vm.statisticsData.label1,
                              labelY: _vm.statisticsData.label2
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            heading: _vm.$t("message.projectGallery"),
                            colClasses:
                              "col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12"
                          }
                        },
                        [_c("project-gallery")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e()
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/index.js":
/*!***********************************!*\
  !*** ./resources/js/api/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: 'https://reactify.theironnetwork.org/data/'
}));

/***/ }),

/***/ "./resources/js/components/Charts/LineChartV6.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/Charts/LineChartV6.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// News Letter Campaign Widget


var lineTension = lineTension;
var borderWidth = 3;
var pointRadius = 7;
var pointBorderWidth = 2;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: ["dataSet"],
  data: function data() {
    return {
      options: {
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Y-Axis'
            },
            gridLines: {
              display: true,
              drawBorder: false
            },
            ticks: {
              stepSize: 50,
              display: true
            }
          }],
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'x-Axes'
            },
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              display: true
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    // console.log(this.dataSet+ "test");
    this.renderChart({
      labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
      datasets: [{
        label: 'Data',
        lineTension: lineTension,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.dataSet
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/FilesUploaded.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/FilesUploaded.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FilesUploaded_vue_vue_type_template_id_711a459c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FilesUploaded.vue?vue&type=template&id=711a459c& */ "./resources/js/components/Widgets/FilesUploaded.vue?vue&type=template&id=711a459c&");
/* harmony import */ var _FilesUploaded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FilesUploaded.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/FilesUploaded.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FilesUploaded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FilesUploaded_vue_vue_type_template_id_711a459c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FilesUploaded_vue_vue_type_template_id_711a459c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/FilesUploaded.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/FilesUploaded.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/FilesUploaded.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FilesUploaded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./FilesUploaded.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/FilesUploaded.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FilesUploaded_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/FilesUploaded.vue?vue&type=template&id=711a459c&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/FilesUploaded.vue?vue&type=template&id=711a459c& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FilesUploaded_vue_vue_type_template_id_711a459c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./FilesUploaded.vue?vue&type=template&id=711a459c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/FilesUploaded.vue?vue&type=template&id=711a459c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FilesUploaded_vue_vue_type_template_id_711a459c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FilesUploaded_vue_vue_type_template_id_711a459c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ManagementDetails.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/ManagementDetails.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ManagementDetails_vue_vue_type_template_id_22c02da4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ManagementDetails.vue?vue&type=template&id=22c02da4& */ "./resources/js/components/Widgets/ManagementDetails.vue?vue&type=template&id=22c02da4&");
/* harmony import */ var _ManagementDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ManagementDetails.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ManagementDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ManagementDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ManagementDetails_vue_vue_type_template_id_22c02da4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ManagementDetails_vue_vue_type_template_id_22c02da4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ManagementDetails.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ManagementDetails.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ManagementDetails.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ManagementDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ManagementDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ManagementDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ManagementDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ManagementDetails.vue?vue&type=template&id=22c02da4&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ManagementDetails.vue?vue&type=template&id=22c02da4& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ManagementDetails_vue_vue_type_template_id_22c02da4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ManagementDetails.vue?vue&type=template&id=22c02da4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ManagementDetails.vue?vue&type=template&id=22c02da4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ManagementDetails_vue_vue_type_template_id_22c02da4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ManagementDetails_vue_vue_type_template_id_22c02da4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ProjectGallery.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectGallery.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectGallery_vue_vue_type_template_id_4474d524___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectGallery.vue?vue&type=template&id=4474d524& */ "./resources/js/components/Widgets/ProjectGallery.vue?vue&type=template&id=4474d524&");
/* harmony import */ var _ProjectGallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectGallery.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProjectGallery.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectGallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectGallery_vue_vue_type_template_id_4474d524___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectGallery_vue_vue_type_template_id_4474d524___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProjectGallery.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectGallery.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectGallery.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectGallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectGallery.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectGallery.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectGallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectGallery.vue?vue&type=template&id=4474d524&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectGallery.vue?vue&type=template&id=4474d524& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectGallery_vue_vue_type_template_id_4474d524___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectGallery.vue?vue&type=template&id=4474d524& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectGallery.vue?vue&type=template&id=4474d524&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectGallery_vue_vue_type_template_id_4474d524___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectGallery_vue_vue_type_template_id_4474d524___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Statistics.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/Statistics.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Statistics_vue_vue_type_template_id_5c159cce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Statistics.vue?vue&type=template&id=5c159cce& */ "./resources/js/components/Widgets/Statistics.vue?vue&type=template&id=5c159cce&");
/* harmony import */ var _Statistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Statistics.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Statistics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Statistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Statistics_vue_vue_type_template_id_5c159cce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Statistics_vue_vue_type_template_id_5c159cce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Statistics.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Statistics.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/Statistics.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Statistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Statistics.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Statistics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Statistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Statistics.vue?vue&type=template&id=5c159cce&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Statistics.vue?vue&type=template&id=5c159cce& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Statistics_vue_vue_type_template_id_5c159cce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Statistics.vue?vue&type=template&id=5c159cce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Statistics.vue?vue&type=template&id=5c159cce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Statistics_vue_vue_type_template_id_5c159cce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Statistics_vue_vue_type_template_id_5c159cce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/crm/ProjectDetails.vue":
/*!***************************************************!*\
  !*** ./resources/js/views/crm/ProjectDetails.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectDetails_vue_vue_type_template_id_03ff18ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectDetails.vue?vue&type=template&id=03ff18ce& */ "./resources/js/views/crm/ProjectDetails.vue?vue&type=template&id=03ff18ce&");
/* harmony import */ var _ProjectDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectDetails.vue?vue&type=script&lang=js& */ "./resources/js/views/crm/ProjectDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectDetails_vue_vue_type_template_id_03ff18ce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectDetails_vue_vue_type_template_id_03ff18ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/crm/ProjectDetails.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/crm/ProjectDetails.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/crm/ProjectDetails.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/ProjectDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/crm/ProjectDetails.vue?vue&type=template&id=03ff18ce&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/crm/ProjectDetails.vue?vue&type=template&id=03ff18ce& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectDetails_vue_vue_type_template_id_03ff18ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectDetails.vue?vue&type=template&id=03ff18ce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/ProjectDetails.vue?vue&type=template&id=03ff18ce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectDetails_vue_vue_type_template_id_03ff18ce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectDetails_vue_vue_type_template_id_03ff18ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);