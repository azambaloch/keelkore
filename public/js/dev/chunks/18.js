(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[18],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/CandleSticks.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_echarts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-echarts */ "./node_modules/vue-echarts/components/ECharts.vue");
/* harmony import */ var echarts_lib_chart_candlestick__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! echarts/lib/chart/candlestick */ "./node_modules/echarts/lib/chart/candlestick.js");
/* harmony import */ var echarts_lib_chart_candlestick__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_candlestick__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var echarts_lib_component_grid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! echarts/lib/component/grid */ "./node_modules/echarts/lib/component/grid.js");
/* harmony import */ var echarts_lib_component_grid__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_grid__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var echarts_lib_component_dataZoom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! echarts/lib/component/dataZoom */ "./node_modules/echarts/lib/component/dataZoom.js");
/* harmony import */ var echarts_lib_component_dataZoom__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_dataZoom__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! echarts/lib/component/tooltip */ "./node_modules/echarts/lib/component/tooltip.js");
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//







var chartData = splitData([['2019/1/24', 2320.26, 2320.26, 2287.3, 2362.94], ['2019/1/25', 2300, 2291.3, 2288.26, 2308.38], ['2019/1/28', 2295.35, 2346.5, 2295.35, 2346.92], ['2019/1/29', 2347.22, 2358.98, 2337.35, 2363.8], ['2019/1/30', 2360.75, 2382.48, 2347.89, 2383.76], ['2019/1/31', 2383.43, 2385.42, 2371.23, 2391.82], ['2019/2/1', 2377.41, 2419.02, 2369.57, 2421.15], ['2019/2/4', 2425.92, 2428.15, 2417.58, 2440.38], ['2019/2/5', 2411, 2433.13, 2403.3, 2437.42], ['2019/2/6', 2432.68, 2434.48, 2427.7, 2441.73], ['2019/2/7', 2430.69, 2418.53, 2394.22, 2433.89], ['2019/2/8', 2416.62, 2432.4, 2414.4, 2443.03], ['2019/2/18', 2441.91, 2421.56, 2415.43, 2444.8], ['2019/2/19', 2420.26, 2382.91, 2373.53, 2427.07], ['2019/2/20', 2383.49, 2397.18, 2370.61, 2397.94], ['2019/2/21', 2378.82, 2325.95, 2309.17, 2378.82], ['2019/2/22', 2322.94, 2314.16, 2308.76, 2330.88], ['2019/2/25', 2320.62, 2325.82, 2315.01, 2338.78], ['2019/2/26', 2313.74, 2293.34, 2289.89, 2340.71], ['2019/2/27', 2297.77, 2313.22, 2292.03, 2324.63], ['2019/2/28', 2322.32, 2365.59, 2308.92, 2366.16], ['2019/3/1', 2364.54, 2359.51, 2330.86, 2369.65], ['2019/3/4', 2332.08, 2273.4, 2259.25, 2333.54], ['2019/3/5', 2274.81, 2326.31, 2270.1, 2328.14], ['2019/3/6', 2333.61, 2347.18, 2321.6, 2351.44], ['2019/3/7', 2340.44, 2324.29, 2304.27, 2352.02], ['2019/3/8', 2326.42, 2318.61, 2314.59, 2333.67], ['2019/3/11', 2314.68, 2310.59, 2296.58, 2320.96], ['2019/3/12', 2309.16, 2286.6, 2264.83, 2333.29], ['2019/3/13', 2282.17, 2263.97, 2253.25, 2286.33], ['2019/3/14', 2255.77, 2270.28, 2253.31, 2276.22], ['2019/3/15', 2269.31, 2278.4, 2250, 2312.08], ['2019/3/18', 2267.29, 2240.02, 2239.21, 2276.05], ['2019/3/19', 2244.26, 2257.43, 2232.02, 2261.31], ['2019/3/20', 2257.74, 2317.37, 2257.42, 2317.86], ['2019/3/21', 2318.21, 2324.24, 2311.6, 2330.81], ['2019/3/22', 2321.4, 2328.28, 2314.97, 2332]]);

function splitData(rawData) {
  var categoryData = [];
  var values = [];

  for (var i = 0; i < rawData.length; i++) {
    categoryData.push(rawData[i].splice(0, 1)[0]);
    values.push(rawData[i]);
  }

  return {
    categoryData: categoryData,
    values: values
  };
}

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ECharts: vue_echarts__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      option: {
        title: {
          left: 0
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross'
          }
        },
        grid: {
          left: '10%',
          right: '10%',
          bottom: '15%'
        },
        xAxis: {
          type: 'category',
          data: chartData.categoryData,
          scale: true,
          boundaryGap: false,
          axisLine: {
            onZero: false
          },
          splitLine: {
            show: false
          },
          splitNumber: 20,
          min: 'dataMin',
          max: 'dataMax'
        },
        yAxis: {
          scale: true,
          splitArea: {
            show: true
          }
        },
        dataZoom: [{
          type: 'inside',
          start: 0,
          end: 100
        }, {
          show: true,
          type: 'slider',
          start: 50,
          end: 100,
          top: 10,
          height: 30,
          onGap: true,
          handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
          handleSize: '120%'
        }],
        series: [{
          type: 'candlestick',
          data: chartData.values,
          itemStyle: {
            normal: {
              color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_6__["ChartConfig"].color.primary,
              color0: Constants_chart_config__WEBPACK_IMPORTED_MODULE_6__["ChartConfig"].color.success,
              borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_6__["ChartConfig"].color.primary,
              borderColor0: Constants_chart_config__WEBPACK_IMPORTED_MODULE_6__["ChartConfig"].color.success
            }
          }
        }]
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["heading", "amount", "colClasses", "customClasses", "icon"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CoinsList.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/CoinsList.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      isBtc: true,
      isEth: false,
      isEur: false,
      selectedBtn: 'BTC',
      selectedCoin: [],
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      coinsList: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["coinsList"],
      headers: [{
        text: 'No',
        align: "left",
        sortable: false,
        value: 'number'
      }, {
        text: "Coins",
        align: "left",
        sortable: false,
        value: "coins"
      }, {
        text: "Symbol",
        align: "left",
        sortable: false,
        value: "symbol"
      }, {
        text: "Price",
        align: "left",
        sortable: false,
        value: "price"
      }, {
        text: "Direct Vol. 24H",
        align: "left",
        sortable: false,
        value: "directVolume"
      }, {
        text: "Tag",
        align: "left",
        sortable: false,
        value: "tag"
      }, {
        text: "Total Vol. 24H",
        align: "left",
        sortable: false,
        value: "totalVolume"
      }, {
        text: "Market Cap",
        align: "left",
        sortable: false,
        value: "marketCap"
      }, {
        text: "Circulating Supply",
        align: "left",
        sortable: false,
        value: "circulatingSupply"
      }, {
        text: "Status",
        align: "left",
        sortable: false,
        value: "status"
      }, {
        text: "Chg. 24H",
        sortable: false,
        value: "chg"
      }]
    };
  },
  mounted: function mounted() {
    this.getSelectedCurrency();
  },
  methods: {
    getSelectedCurrency: function getSelectedCurrency() {
      this.selectedCoin = [];

      for (var i = 0; i < Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["coinsList"].length; i++) {
        if (Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["coinsList"][i].tag == this.selectedBtn) {
          this.selectedCoin.push(Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["coinsList"][i]);
        }
      } // console.log("this.selectedBtn="+this.selectedBtn) 

    },
    changeBtn: function changeBtn(value) {
      switch (value) {
        case 'ETH':
          this.isBtc = false;
          this.isEth = true;
          this.isEur = false;
          break;

        case 'EUR':
          this.isBtc = false;
          this.isEth = false;
          this.isEur = true;
          break;

        default:
          this.isBtc = true;
          this.isEth = false;
          this.isEur = false;
          break;
      }

      this.selectedBtn = value;
      this.getSelectedCurrency(); // console.log("this.selectedCoin="+this.selectedCoin) 
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      active: null,
      currOne: 'Bitcoin',
      currTwo: 'Ethereum',
      currency: ['Bitcoin', 'Ethereum', 'EOS', 'Litecoin']
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
/* harmony import */ var Components_Charts_LineChartV5__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Charts/LineChartV5 */ "./resources/js/components/Charts/LineChartV5.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"],
    LineChartV5: Components_Charts_LineChartV5__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"],
      exchangeStatistic: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_1__["exchangeStatistic"],
      slickOptions: {
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        fade: true,
        cssEase: 'linear',
        arrows: false,
        rtl: this.$store.getters.rtlLayouts
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuickTrade.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/QuickTrade.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      showPassword: false,
      currOne: 'Bitcoin',
      currTwo: 'Ethereum',
      currency: ['Bitcoin', 'Ethereum', 'EOS', 'Litecoin'],
      payMethodInitial: "Debit Card",
      paymentMethod: ['Debit Card', 'PayPal', 'Bank Transfer', 'Credit Cards'],
      active: null,
      tabs: [{
        icon: "zmdi zmdi-shopping-cart",
        title: "Buy"
      }, {
        icon: "ti-money",
        title: "Sell"
      }, {
        icon: "zmdi zmdi-square-right",
        title: "Transfer"
      }]
    };
  },
  mounted: function mounted() {},
  methods: {
    next: function next() {
      var active = parseInt(this.active);
      this.active = active < 2 ? active + 1 : 0;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      recentTrade: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["recentTrade"],
      loader: false,
      invoice: [],
      headers: [{
        text: "Currency",
        sortable: false,
        value: "currency"
      }, {
        text: "Status",
        sortable: false,
        value: "status"
      }, {
        text: "Price",
        sortable: false,
        value: "price"
      }, {
        text: "Total($)",
        sortable: false,
        value: "total($)"
      }],
      active: null
    };
  },
  mounted: function mounted() {},
  methods: {
    next: function next() {
      var active = parseInt(this.active);
      this.active = active < 2 ? active + 1 : 0;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SafeTrade.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SafeTrade.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
/* harmony import */ var Components_Charts_LineChartShadowV3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Charts/LineChartShadowV3 */ "./resources/js/components/Charts/LineChartShadowV3.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"],
    LineChartShadowV3: Components_Charts_LineChartShadowV3__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      safeTradeContent: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_1__["safeTradeContent"],
      slickOptions: {
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        fade: true,
        cssEase: 'linear',
        arrows: false,
        rtl: this.$store.getters.rtlLayout
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TradeHistory.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TradeHistory.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      tradeHistory: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["tradeHistory"],
      loader: false,
      invoice: [],
      headers: [{
        text: "Currency",
        sortable: false,
        value: "currency"
      }, {
        text: "Txn No",
        sortable: false,
        value: "txn No"
      }, {
        text: "Status",
        sortable: false,
        value: "status"
      }, {
        text: "Price",
        sortable: false,
        value: "price"
      }, {
        text: "Total($)",
        sortable: false,
        value: "total($)"
      }, {
        text: "Date",
        sortable: false,
        value: "date"
      }, {
        text: "To / From",
        sortable: false,
        value: "to / from"
      }, {
        text: "More",
        sortable: false,
        value: "more"
      }],
      active: null
    };
  },
  methods: {
    next: function next() {
      var active = parseInt(this.active);
      this.active = active < 2 ? active + 1 : 0;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Crypto.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crypto/Crypto.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_StatsCardV7_StatsCardV7__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/StatsCardV7/StatsCardV7 */ "./resources/js/components/StatsCardV7/StatsCardV7.vue");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Components_Charts_LineChartShadowV2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Charts/LineChartShadowV2 */ "./resources/js/components/Charts/LineChartShadowV2.js");
/* harmony import */ var Components_Widgets_TradeHistory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/TradeHistory */ "./resources/js/components/Widgets/TradeHistory.vue");
/* harmony import */ var Components_Widgets_QuickTrade__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/QuickTrade */ "./resources/js/components/Widgets/QuickTrade.vue");
/* harmony import */ var Components_Widgets_RecentTrades__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/RecentTrades */ "./resources/js/components/Widgets/RecentTrades.vue");
/* harmony import */ var Components_Widgets_CoinsList__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/CoinsList */ "./resources/js/components/Widgets/CoinsList.vue");
/* harmony import */ var Components_Widgets_ExchangeRate__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/Widgets/ExchangeRate */ "./resources/js/components/Widgets/ExchangeRate.vue");
/* harmony import */ var Components_Widgets_SafeTrade__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Components/Widgets/SafeTrade */ "./resources/js/components/Widgets/SafeTrade.vue");
/* harmony import */ var Components_Widgets_ExchangeStatistics__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Components/Widgets/ExchangeStatistics */ "./resources/js/components/Widgets/ExchangeStatistics.vue");
/* harmony import */ var Components_Charts_CandleSticks__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Components/Charts/CandleSticks */ "./resources/js/components/Charts/CandleSticks.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// Widgets 











/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    StatsCardV7: Components_StatsCardV7_StatsCardV7__WEBPACK_IMPORTED_MODULE_0__["default"],
    LineChartShadowV2: Components_Charts_LineChartShadowV2__WEBPACK_IMPORTED_MODULE_2__["default"],
    TradeHistory: Components_Widgets_TradeHistory__WEBPACK_IMPORTED_MODULE_3__["default"],
    QuickTrade: Components_Widgets_QuickTrade__WEBPACK_IMPORTED_MODULE_4__["default"],
    RecentTrades: Components_Widgets_RecentTrades__WEBPACK_IMPORTED_MODULE_5__["default"],
    CoinsList: Components_Widgets_CoinsList__WEBPACK_IMPORTED_MODULE_6__["default"],
    ExchangeRate: Components_Widgets_ExchangeRate__WEBPACK_IMPORTED_MODULE_7__["default"],
    SafeTrade: Components_Widgets_SafeTrade__WEBPACK_IMPORTED_MODULE_8__["default"],
    ExchangeStatistics: Components_Widgets_ExchangeStatistics__WEBPACK_IMPORTED_MODULE_9__["default"],
    CandleSticks: Components_Charts_CandleSticks__WEBPACK_IMPORTED_MODULE_10__["default"]
  },
  data: function data() {
    return {
      //  statsCardData,
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      bitcoin: {
        icon: "cc BTC primary--text",
        name: "Bitcoin",
        color: "primary",
        duration: "last 4 days",
        market_cap: "2.3",
        market_cap_icon: "fa-arrow-up",
        market_cap_color: "success-text",
        chartLabel: ["A", "B", "C", "D", "E"],
        data: [1, 26, 8, 22, 1],
        chartBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary
      },
      ethereum: {
        icon: "cc ETH success--text",
        name: "Ethereum",
        color: "success",
        duration: "last 4 days",
        market_cap: "2.3",
        market_cap_icon: "fa-arrow-up",
        market_cap_color: "success-text",
        chartLabel: ["A", "B", "C", "D", "E"],
        data: [29, 5, 26, 10, 21],
        chartBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.success
      },
      litecoin: {
        icon: "cc LTC error--text",
        name: "Litecoin",
        color: "warn",
        duration: "last 4 days",
        market_cap: "2.3",
        market_cap_icon: "fa-arrow-up",
        market_cap_color: "success-text",
        chartLabel: ["A", "B", "C", "D", "E"],
        data: [1, 26, 8, 22, 1],
        chartBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger
      },
      zcash: {
        icon: "cc ZEC-alt info--text",
        name: "Zcash",
        color: "accent",
        duration: "last 4 days",
        market_cap: "2.3",
        market_cap_icon: "fa-arrow-up",
        market_cap_color: "success-text",
        chartLabel: ["A", "B", "C", "D", "E"],
        data: [29, 5, 26, 10, 21],
        chartBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.echarts[data-v-631ebfcf] {\n  width: 100%;\n  height: 400px;\n}\n#candleChart[data-v-631ebfcf] > :first-child{\n  width: 100% !important;\n}\n#candleChart[data-v-631ebfcf] > :first-child :first-child{\n  width: 100% !important;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "echarts" },
    [
      _c("ECharts", {
        staticStyle: { width: "100%" },
        attrs: { options: _vm.option, id: "candleChart" }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=template&id=28b1e996&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=template&id=28b1e996& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-col", { class: _vm.colClasses }, [
    _c(
      "div",
      { staticClass: "app-card vue-card-space", class: _vm.customClasses },
      [
        _c("v-card-text", { staticClass: "py-0 mb-4" }, [
          _c("div", { staticClass: "layout justify-space-between" }, [
            _c("div", { staticClass: "align-items-center d-custom-flex" }, [
              _c(
                "div",
                { staticClass: "crypto-icon-wrap inline-block mr-4 font-2x" },
                [_c("i", { class: _vm.icon, attrs: { title: "BCN" } })]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "inline-block" }, [
                _c("h3", { staticClass: "mb-0" }, [_vm._v(_vm._s(_vm.heading))])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _vm._t("default"),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "chart-info d-custom-flex align-items-center justify-space-between"
          },
          [
            _c(
              "v-badge",
              { staticClass: "primary pa-2", attrs: { value: false } },
              [_vm._v("last 4 days")]
            ),
            _vm._v(" "),
            _c("span", { staticClass: "primary--text font-md" }, [
              _c("i", {
                staticClass: "zmdi zmdi-long-arrow-up primary--text mr-2"
              }),
              _vm._v("2.3%")
            ])
          ],
          1
        )
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CoinsList.vue?vue&type=template&id=73bead36&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/CoinsList.vue?vue&type=template&id=73bead36& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("v-card", { attrs: { flat: "" } }, [
        _c(
          "div",
          { staticClass: "table-responsive" },
          [
            _c(
              "div",
              { staticClass: "mb-6" },
              [
                _c(
                  "v-btn",
                  {
                    class: { primary: _vm.isBtc },
                    on: {
                      click: function($event) {
                        return _vm.changeBtn("BTC")
                      }
                    }
                  },
                  [_vm._v("\n\t\t\t\t\t\tBTC\n\t\t\t\t\t")]
                ),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    staticClass: "ml-2",
                    class: { primary: _vm.isEth },
                    on: {
                      click: function($event) {
                        return _vm.changeBtn("ETH")
                      }
                    }
                  },
                  [_vm._v("\n\t\t\t\t\t\tETH\n\t\t\t\t\t")]
                ),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    staticClass: "ml-2",
                    class: { primary: _vm.isEur },
                    on: {
                      click: function($event) {
                        return _vm.changeBtn("EUR")
                      }
                    }
                  },
                  [_vm._v("\n\t\t\t\t\t\tEUR\n\t\t\t\t\t")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("v-data-table", {
              attrs: { headers: _vm.headers, items: _vm.selectedCoin },
              scopedSlots: _vm._u([
                {
                  key: "item",
                  fn: function(ref) {
                    var item = ref.item
                    return [
                      _c("tr", [
                        _c("td", [
                          _vm._v(
                            _vm._s(
                              _vm.selectedCoin
                                .map(function(x) {
                                  return x.serial_number
                                })
                                .indexOf(item.serial_number)
                            ) + "\n\t\t\t\t\t\t\t"
                          )
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.desktop_name))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.mobile_name))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.price))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.volume))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.tag))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.total_volume))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.market_cap))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.circulating_supply))]),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            item.status
                              ? _c(
                                  "v-badge",
                                  {
                                    staticClass: "info",
                                    attrs: { value: false }
                                  },
                                  [_vm._v("High")]
                                )
                              : _c(
                                  "v-badge",
                                  {
                                    staticClass: "error",
                                    attrs: { value: false }
                                  },
                                  [_vm._v("Low")]
                                )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.change))])
                      ])
                    ]
                  }
                }
              ])
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "mb-6" },
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            "menu-props": "auto",
            label: "Select",
            "hide-details": "",
            "prepend-icon": "zmdi zmdi-money-box",
            "single-line": "",
            outlined: ""
          },
          model: {
            value: _vm.currOne,
            callback: function($$v) {
              _vm.currOne = $$v
            },
            expression: "currOne"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "mb-6" },
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            "menu-props": "auto",
            label: "Select",
            "hide-details": "",
            "prepend-icon": "zmdi zmdi-money-box",
            "single-line": "",
            outlined: ""
          },
          model: {
            value: _vm.currTwo,
            callback: function($$v) {
              _vm.currTwo = $$v
            },
            expression: "currTwo"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(3)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("label", [_vm._v("Choose Currency")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center mb-6" }, [
      _c("i", { staticClass: "ti-exchange-vertical font-2x" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("label", [_vm._v("Choose Currency")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text fw-normal" }, [
        _vm._v("1 BTC = 0.45373 ETC")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=template&id=33cebad1&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=template&id=33cebad1& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "top-selling-widget" },
      [
        _c(
          "slick",
          { attrs: { options: _vm.slickOptions } },
          _vm._l(_vm.exchangeStatistic, function(item, index) {
            return _c(
              "div",
              { key: index },
              [
                _c(
                  "v-row",
                  [
                    _c(
                      "v-col",
                      {
                        staticClass: "mb-4",
                        attrs: {
                          xl: "7",
                          lg: "7",
                          md: "8",
                          sm: "8",
                          cols: "12"
                        }
                      },
                      [
                        _c("div", { staticClass: "crypto-icon-wrap mb-sm-0" }, [
                          _c("i", {
                            staticClass: "warn--text font-3x",
                            class: item.icon1
                          }),
                          _vm._v(" "),
                          _c("i", {
                            staticClass: "primary--text font-3x",
                            class: item.icon2
                          })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      {
                        staticClass: "mb-4",
                        attrs: {
                          xl: "5",
                          lg: "5",
                          md: "4",
                          sm: "4",
                          cols: "12"
                        }
                      },
                      [
                        _c("div", [
                          _c("div", { staticClass: "crypto-text-wrap" }, [
                            _c("span", { staticClass: "d-inline-block" }, [
                              _vm._v(_vm._s(item.icon1_name))
                            ]),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "d-inline-block ml-2 mr-2" },
                              [_vm._v("/")]
                            ),
                            _vm._v(" "),
                            _c("span", { staticClass: "d-inline-block" }, [
                              _vm._v(_vm._s(item.icon2_name))
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "crypto-result-price" }, [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "d-custom-flex justify-space-between"
                              },
                              [
                                _c("span", { staticClass: "fw-bold" }, [
                                  _vm._v("Price:")
                                ]),
                                _vm._v(" "),
                                _c("span", [_vm._v(_vm._s(item.price))])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "d-custom-flex justify-space-between"
                              },
                              [
                                _c("span", { staticClass: "fw-bold" }, [
                                  _vm._v("Volume:")
                                ]),
                                _c("span", [_vm._v(_vm._s(item.volume))])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "d-block success--text" },
                              [_vm._v(_vm._s(item.percentage) + "%")]
                            )
                          ])
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("line-chart-v5", {
                  attrs: {
                    label: "Statistics",
                    chartdata: item.data,
                    labels: item.chartLabel,
                    pointBackgroundColor: _vm.ChartConfig.color.primary,
                    height: 80,
                    pointBorderColor: _vm.ChartConfig.color.white,
                    borderWidth: 4,
                    showGridLines: false,
                    enableGradient: false,
                    enableShadow: true,
                    shadowColor: "#1565c0",
                    borderColor: _vm.ChartConfig.color.primary
                  }
                })
              ],
              1
            )
          }),
          0
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuickTrade.vue?vue&type=template&id=5f7f2922&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/QuickTrade.vue?vue&type=template&id=5f7f2922& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-tabs",
        {
          staticClass: "quicktrade-tab-wrap",
          attrs: { "slider-color": "primary" },
          model: {
            value: _vm.active,
            callback: function($$v) {
              _vm.active = $$v
            },
            expression: "active"
          }
        },
        [
          _vm._l(_vm.tabs, function(tab, index) {
            return _c("v-tab", { key: index, attrs: { ripple: "" } }, [
              _c("div", { staticClass: "font-sm text-capitalize" }, [
                _c("i", {
                  staticClass: "v-input__prepend-outer font-md",
                  class: tab.icon
                }),
                _vm._v("\n            " + _vm._s(tab.title) + "\n         ")
              ])
            ])
          }),
          _vm._v(" "),
          _vm._l(3, function(n) {
            return _c(
              "v-tab-item",
              { key: n },
              [
                n == 1
                  ? _c("v-card", { attrs: { flat: "" } }, [
                      _c("div", [
                        _c("div", [
                          _c(
                            "div",
                            { staticClass: "mb-5" },
                            [
                              _c("div", [
                                _c("label", [_vm._v("Choose Currency")])
                              ]),
                              _vm._v(" "),
                              _c("v-select", {
                                attrs: {
                                  items: _vm.currency,
                                  "menu-props": "auto",
                                  label: "Select",
                                  "hide-details": "",
                                  "prepend-icon": "zmdi zmdi-money-box",
                                  "single-line": ""
                                },
                                model: {
                                  value: _vm.currTwo,
                                  callback: function($$v) {
                                    _vm.currTwo = $$v
                                  },
                                  expression: "currTwo"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "mb-5" },
                            [
                              _c("div", [
                                _c("label", [_vm._v("Payment Method")])
                              ]),
                              _vm._v(" "),
                              _c("v-select", {
                                attrs: {
                                  items: _vm.paymentMethod,
                                  "menu-props": "auto",
                                  label: "Select",
                                  "hide-details": "",
                                  "prepend-icon": "zmdi zmdi-card",
                                  "single-line": ""
                                },
                                model: {
                                  value: _vm.payMethodInitial,
                                  callback: function($$v) {
                                    _vm.payMethodInitial = $$v
                                  },
                                  expression: "payMethodInitial"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "mb-5" },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  label: "Select Amount",
                                  value: "200",
                                  type: "number",
                                  min: "1"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "mb-5" },
                            [
                              _c("v-text-field", {
                                attrs: {
                                  "prepend-inner-icon": "cc BTC-alt",
                                  label: "Wallet Address",
                                  value: "AXB35H24ISDJHCISDT",
                                  type: "text",
                                  min: "1"
                                }
                              })
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "mb-4" }, [
                          _c(
                            "h5",
                            { staticClass: "success--text fw-normal mb-0" },
                            [_vm._v("Total amount is 200 $")]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "d-inline-flex align-items-center" },
                          [
                            _c(
                              "div",
                              { staticClass: "mr-4" },
                              [
                                _c("v-btn", { staticClass: "primary ml-0" }, [
                                  _vm._v("Purchase")
                                ])
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", [
                              _c(
                                "h5",
                                { staticClass: "success--text fw-normal mb-0" },
                                [_vm._v("Transaction successfull")]
                              )
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                n == 2
                  ? _c("v-card", { attrs: { flat: "" } }, [
                      _c("div", [
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("div", [
                              _c("label", [_vm._v("Choose Currency")])
                            ]),
                            _vm._v(" "),
                            _c("v-select", {
                              attrs: {
                                items: _vm.currency,
                                "menu-props": "auto",
                                label: "Select",
                                "hide-details": "",
                                "prepend-icon": "zmdi zmdi-money-box",
                                "single-line": ""
                              },
                              model: {
                                value: _vm.currTwo,
                                callback: function($$v) {
                                  _vm.currTwo = $$v
                                },
                                expression: "currTwo"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("v-text-field", {
                              attrs: {
                                label: "Select Amount",
                                value: "200",
                                type: "number",
                                min: "1"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("v-text-field", {
                              attrs: {
                                "append-icon": _vm.showPassword
                                  ? "visibility"
                                  : "visibility_off",
                                type: _vm.showPassword ? "text" : "password",
                                label: "Password",
                                "prepend-icon": "ti-shield"
                              },
                              on: {
                                "click:append": function($event) {
                                  _vm.showPassword = !_vm.showPassword
                                }
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("v-text-field", {
                              attrs: {
                                "prepend-inner-icon": "cc BTC-alt",
                                label: "Wallet Address",
                                value: "AXB35H24ISDJHCISDT",
                                type: "text",
                                min: "1",
                                "prepend-icon": ""
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "mb-4" }, [
                          _c("h5", { staticClass: "success--text fw-normal" }, [
                            _vm._v("Your Account will be credited with 220 $")
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "d-inline-flex align-items-center" },
                          [
                            _c(
                              "div",
                              { staticClass: "mr-3" },
                              [
                                _c("v-btn", { staticClass: "primary ml-0" }, [
                                  _vm._v("Sell")
                                ])
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", [
                              _c(
                                "h5",
                                { staticClass: "success--text fw-normal" },
                                [_vm._v("Transaction successfull")]
                              )
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                n == 3
                  ? _c("v-card", { attrs: { flat: "" } }, [
                      _c("div", [
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("div", [
                              _c("label", [_vm._v("Choose Currency")])
                            ]),
                            _vm._v(" "),
                            _c("v-select", {
                              attrs: {
                                items: _vm.currency,
                                "menu-props": "auto",
                                label: "Select",
                                "hide-details": "",
                                "prepend-icon": "zmdi zmdi-money-box",
                                "single-line": ""
                              },
                              model: {
                                value: _vm.currTwo,
                                callback: function($$v) {
                                  _vm.currTwo = $$v
                                },
                                expression: "currTwo"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("v-text-field", {
                              attrs: {
                                label: "Select Amount",
                                value: "300",
                                type: "number",
                                min: "1"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("v-text-field", {
                              attrs: {
                                "append-icon": _vm.showPassword
                                  ? "visibility"
                                  : "visibility_off",
                                type: _vm.showPassword ? "text" : "password",
                                label: "Password",
                                counter: "",
                                "prepend-icon": "ti-shield"
                              },
                              on: {
                                "click:append": function($event) {
                                  _vm.showPassword = !_vm.showPassword
                                }
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "mb-5" },
                          [
                            _c("v-text-field", {
                              attrs: {
                                "prepend-inner-icon": "cc BTC-alt",
                                label: "Send To",
                                value: "",
                                type: "text",
                                min: "1",
                                "prepend-icon": ""
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "mb-4" }, [
                          _c(
                            "h5",
                            { staticClass: "success--text fw-normal mb-0" },
                            [_vm._v("Total amount transfered 300 $")]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "d-inline-flex align-items-center" },
                          [
                            _c(
                              "div",
                              { staticClass: "mr-3" },
                              [
                                _c("v-btn", { staticClass: "primary ml-0" }, [
                                  _vm._v("Transfer")
                                ])
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("div", [
                              _c(
                                "h5",
                                { staticClass: "success--text mb-0 fw-normal" },
                                [_vm._v("Transaction successfull")]
                              )
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e()
              ],
              1
            )
          })
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("v-card", { attrs: { flat: "" } }, [
        _c(
          "div",
          { staticClass: "table-responsive" },
          [
            _c("app-section-loader", { attrs: { status: _vm.loader } }),
            _vm._v(" "),
            _c("v-data-table", {
              staticClass: "mb-4",
              attrs: {
                headers: _vm.headers,
                items: _vm.recentTrade,
                "hide-default-footer": ""
              },
              scopedSlots: _vm._u([
                {
                  key: "item",
                  fn: function(ref) {
                    var item = ref.item
                    return [
                      _c("tr", [
                        _c("td", [_c("i", { class: item.currencyIcon })]),
                        _vm._v(" "),
                        _c("td", { class: item.statusClass }, [
                          _vm._v(_vm._s(item.status))
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.price))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.total))])
                      ])
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("div", { staticClass: "stats-summary mb-2" }, [
              _c("div", [
                _vm._v("\n               Overall Profit:\n               "),
                _c("span", { staticClass: "success--text" }, [
                  _c("i", { staticClass: "zmdi zmdi-plus" }),
                  _vm._v(" \n                  $35237\n               ")
                ])
              ]),
              _vm._v(" "),
              _c("div", [
                _vm._v("\n               Overall Loss:\n               "),
                _c("span", { staticClass: "error--text" }, [
                  _c("i", { staticClass: "zmdi zmdi-minus" }),
                  _vm._v("\n                  $20\n               ")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("v-btn", { staticClass: "primary m-0" }, [
              _vm._v("Download CSV")
            ])
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SafeTrade.vue?vue&type=template&id=ba478008&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SafeTrade.vue?vue&type=template&id=ba478008& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "top-selling-widget" },
      [
        _c(
          "slick",
          { attrs: { options: _vm.slickOptions } },
          _vm._l(_vm.safeTradeContent, function(item, index) {
            return _c("div", { key: index }, [
              _c(
                "div",
                { staticClass: "align-items-center d-custom-flex mb-4" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "crypto-icon-wrap inline-block mr-3 font-2x"
                    },
                    [_c("i", { class: item.icon })]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "inline-block" }, [
                    _c("h4", { staticClass: "mb-0" }, [
                      _vm._v(_vm._s(item.name))
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mb-4" },
                [
                  _c("line-chart-shadowV3", {
                    attrs: {
                      dataSet: item.data,
                      lineTension: 0.4,
                      dataLabels: item.chartLabel,
                      width: 400,
                      height: 80,
                      borderWidth: 3,
                      enableGradient: false,
                      enableShadow: true,
                      shadowColor: item.chartBorderColor,
                      borderColor: item.chartBorderColor
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "chart-info d-custom-flex align-items-center justify-space-between mb-4"
                },
                [
                  _c(
                    "v-badge",
                    { staticClass: "primary pa-2", attrs: { value: false } },
                    [_vm._v("last 4 days")]
                  ),
                  _vm._v(" "),
                  _c("span", { staticClass: "primary--text font-md" }, [
                    _c("i", {
                      staticClass: "primary--text mr-2",
                      class: item.percentageIcon
                    }),
                    _vm._v("2.3%")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("span", { staticClass: "subheading" }, [
                _c("strong", { staticClass: "success--text mr-1" }, [
                  _vm._v("$" + _vm._s(item.market_cap))
                ]),
                _vm._v(" Market Capitalisation")
              ])
            ])
          }),
          0
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TradeHistory.vue?vue&type=template&id=b8e1f20a&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TradeHistory.vue?vue&type=template&id=b8e1f20a& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("v-card", { attrs: { text: "" } }, [
        _c(
          "div",
          { staticClass: "table-responsive trade-history-table" },
          [
            _c("app-section-loader", { attrs: { status: _vm.loader } }),
            _vm._v(" "),
            _c("v-data-table", {
              attrs: {
                headers: _vm.headers,
                items: _vm.tradeHistory,
                "items-per-page": 5
              },
              scopedSlots: _vm._u([
                {
                  key: "item",
                  fn: function(ref) {
                    var item = ref.item
                    return [
                      _c("tr", [
                        _c("td", [_c("i", { class: item.currencyIcon })]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.price))]),
                        _vm._v(" "),
                        _c("td", { class: item.statusClass }, [
                          _vm._v(_vm._s(item.status))
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.price))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.total))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.date))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.from))]),
                        _vm._v(" "),
                        _c("td", [
                          _c(
                            "div",
                            { staticClass: "d-flex align-items-center" },
                            [
                              _c(
                                "v-dialog",
                                {
                                  attrs: { "max-width": "500px" },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "activator",
                                        fn: function(ref) {
                                          var on = ref.on
                                          return [
                                            _c(
                                              "v-btn",
                                              _vm._g(
                                                {
                                                  staticClass: "my-3 mx-2",
                                                  attrs: {
                                                    text: "",
                                                    icon: "",
                                                    color: "grey",
                                                    small: ""
                                                  }
                                                },
                                                on
                                              ),
                                              [
                                                _c(
                                                  "i",
                                                  {
                                                    staticClass:
                                                      "material-icons"
                                                  },
                                                  [_vm._v("info")]
                                                )
                                              ]
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  )
                                },
                                [
                                  _vm._v(" "),
                                  _c(
                                    "v-card",
                                    [
                                      _c(
                                        "v-card-text",
                                        [
                                          _c(
                                            "v-container",
                                            { staticClass: "grid-list-md" },
                                            [
                                              _c(
                                                "v-row",
                                                [
                                                  _c(
                                                    "v-col",
                                                    { attrs: { cols: "12" } },
                                                    [
                                                      _c("h5", [
                                                        _vm._v(
                                                          "Transaction Details"
                                                        )
                                                      ]),
                                                      _vm._v(" "),
                                                      _c("p", [
                                                        _vm._v(
                                                          "Transaction with Txn No 76237523 was placed on 21/3/102, 11:10 with Payment done being "
                                                        )
                                                      ]),
                                                      _vm._v(" "),
                                                      _c("p", [
                                                        _vm._v("Successful.")
                                                      ]),
                                                      _vm._v(" "),
                                                      _c("table", [
                                                        _c("tr", [
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Currency :"
                                                              )
                                                            ])
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("td", [
                                                            _c("div", [
                                                              _c("i", {
                                                                class:
                                                                  item.currencyIcon
                                                              })
                                                            ])
                                                          ])
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("tr", [
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Payment Currency :"
                                                              )
                                                            ])
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                _vm._s(
                                                                  item.payment_currency
                                                                )
                                                              )
                                                            ])
                                                          ])
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("tr", [
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v("Price :")
                                                            ])
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                _vm._s(
                                                                  item.price
                                                                )
                                                              )
                                                            ])
                                                          ])
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("tr", [
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v("Total :")
                                                            ])
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                _vm._s(
                                                                  item.total
                                                                )
                                                              )
                                                            ])
                                                          ])
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("tr", [
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                "To/From :"
                                                              )
                                                            ])
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("td", [
                                                            _c("p", [
                                                              _vm._v(
                                                                _vm._s(
                                                                  item.from
                                                                )
                                                              )
                                                            ])
                                                          ])
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "tr",
                                                          [
                                                            _c("p", [
                                                              _vm._v(
                                                                "Wallet Address :"
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c("v-text-field", {
                                                              attrs: {
                                                                outline: "",
                                                                solo: "",
                                                                label: "Wallet",
                                                                value:
                                                                  "AXB35H24ISDJHCISDT",
                                                                "prepend-inner-icon":
                                                                  item.currencyIcon
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ])
                      ])
                    ]
                  }
                }
              ])
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Crypto.vue?vue&type=template&id=28601000&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crypto/Crypto.vue?vue&type=template&id=28601000& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0", attrs: { fluid: "" } },
        [
          _c(
            "div",
            { staticClass: "crypto-dash-wrap" },
            [
              _c("crypto-slider"),
              _vm._v(" "),
              _c(
                "v-row",
                { staticClass: "border-rad-sm overflow-hidden" },
                [
                  _c(
                    "stats-card-v7",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 crypto-stats-card",
                        heading: _vm.bitcoin.name,
                        icon: _vm.bitcoin.icon
                      }
                    },
                    [
                      _c("line-chart-shadow-v2", {
                        style: {
                          height: "90px",
                          width: "100%",
                          position: "relative"
                        },
                        attrs: {
                          dataSet: _vm.bitcoin.data,
                          lineTension: 0.4,
                          dataLabels: _vm.bitcoin.chartLabel,
                          borderWidth: 3,
                          enableGradient: false,
                          enableShadow: true,
                          borderColor: _vm.bitcoin.chartBorderColor,
                          shadowColor: _vm.bitcoin.chartBorderColor
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "stats-card-v7",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 crypto-stats-card",
                        heading: _vm.ethereum.name,
                        icon: _vm.ethereum.icon
                      }
                    },
                    [
                      _c("line-chart-shadow-v2", {
                        style: {
                          height: "90px",
                          width: "100%",
                          position: "relative"
                        },
                        attrs: {
                          dataSet: _vm.ethereum.data,
                          lineTension: 0.4,
                          dataLabels: _vm.ethereum.chartLabel,
                          borderWidth: 3,
                          enableGradient: false,
                          enableShadow: true,
                          borderColor: _vm.ethereum.chartBorderColor,
                          shadowColor: _vm.ethereum.chartBorderColor
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "stats-card-v7",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 crypto-stats-card",
                        heading: _vm.litecoin.name,
                        icon: _vm.litecoin.icon
                      }
                    },
                    [
                      _c("line-chart-shadow-v2", {
                        style: {
                          height: "90px",
                          width: "100%",
                          position: "relative"
                        },
                        attrs: {
                          dataSet: _vm.litecoin.data,
                          lineTension: 0.4,
                          dataLabels: _vm.litecoin.chartLabel,
                          borderWidth: 3,
                          enableGradient: false,
                          enableShadow: true,
                          borderColor: _vm.litecoin.chartBorderColor,
                          shadowColor: _vm.litecoin.chartBorderColor
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "stats-card-v7",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 crypto-stats-card",
                        heading: _vm.zcash.name,
                        icon: _vm.zcash.icon
                      }
                    },
                    [
                      _c("line-chart-shadow-v2", {
                        style: {
                          height: "90px",
                          width: "100%",
                          position: "relative"
                        },
                        attrs: {
                          dataSet: _vm.zcash.data,
                          lineTension: 0.4,
                          dataLabels: _vm.zcash.chartLabel,
                          borderWidth: 3,
                          enableGradient: false,
                          enableShadow: true,
                          borderColor: _vm.zcash.chartBorderColor,
                          shadowColor: _vm.zcash.chartBorderColor
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "app-card",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                        customClasses: "mb-0",
                        heading: "BTC/USD",
                        fullScreen: true,
                        reloadable: true,
                        closeable: true
                      }
                    },
                    [_c("CandleSticks")],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    {
                      staticClass: "pa-0",
                      attrs: {
                        xl: "8",
                        lg: "8",
                        md: "12",
                        sm: "12",
                        cols: "12"
                      }
                    },
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                            customClasses: "mb-0 trade-history-warp",
                            heading: _vm.$t("message.tradeHistory"),
                            fullScreen: true,
                            reloadable: true,
                            closeable: true
                          }
                        },
                        [_c("trade-history")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-row",
                        { staticClass: "crypto-row-mrgn" },
                        [
                          _c(
                            "v-col",
                            {
                              staticClass: "pa-0",
                              attrs: { cols: "12", sm: "12", md: "7", lg: "7" }
                            },
                            [
                              _c(
                                "app-card",
                                {
                                  attrs: {
                                    colClasses:
                                      "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                                    customClasses: "mb-0 exchange-statistics",
                                    heading: _vm.$t(
                                      "message.exchangeStatistics"
                                    ),
                                    fullScreen: true,
                                    reloadable: true,
                                    closeable: true
                                  }
                                },
                                [_c("exchange-statistics")],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "app-card",
                                {
                                  attrs: {
                                    colClasses:
                                      "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                                    customClasses: "mb-0",
                                    heading: _vm.$t("message.exchangeRate"),
                                    fullScreen: true,
                                    reloadable: true,
                                    closeable: true
                                  }
                                },
                                [_c("exchange-rate")],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            {
                              staticClass: "pa-0",
                              attrs: { cols: "12", sm: "12", md: "5", lg: "5" }
                            },
                            [
                              _c(
                                "app-card",
                                {
                                  attrs: {
                                    colClasses:
                                      "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                                    customClasses: "mb-0",
                                    heading: _vm.$t("message.quickTrade"),
                                    fullScreen: true,
                                    reloadable: true,
                                    closeable: true
                                  }
                                },
                                [_c("quick-trade")],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    {
                      staticClass: "pa-0",
                      attrs: {
                        cols: "12",
                        sm: "12",
                        md: "12",
                        lg: "4",
                        xl: "4"
                      }
                    },
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                            customClasses: "mb-0",
                            heading: _vm.$t("message.safeTrade"),
                            fullScreen: true,
                            reloadable: true,
                            closeable: true
                          }
                        },
                        [_c("safe-trade")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                            customClasses: "mb-0",
                            heading: _vm.$t("message.recentTrades"),
                            fullScreen: true,
                            reloadable: true,
                            closeable: true
                          }
                        },
                        [_c("recent-trades")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "app-card",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                        customClasses: "mb-0",
                        heading: _vm.$t("message.coinsList"),
                        fullScreen: true,
                        reloadable: true,
                        closeable: true
                      }
                    },
                    [_c("coins-list")],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/CandleSticks.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Charts/CandleSticks.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CandleSticks_vue_vue_type_template_id_631ebfcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true& */ "./resources/js/components/Charts/CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true&");
/* harmony import */ var _CandleSticks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CandleSticks.vue?vue&type=script&lang=js& */ "./resources/js/components/Charts/CandleSticks.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CandleSticks_vue_vue_type_style_index_0_id_631ebfcf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css& */ "./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CandleSticks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CandleSticks_vue_vue_type_template_id_631ebfcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CandleSticks_vue_vue_type_template_id_631ebfcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "631ebfcf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Charts/CandleSticks.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Charts/CandleSticks.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Charts/CandleSticks.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CandleSticks.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css& ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_style_index_0_id_631ebfcf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=style&index=0&id=631ebfcf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_style_index_0_id_631ebfcf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_style_index_0_id_631ebfcf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_style_index_0_id_631ebfcf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_style_index_0_id_631ebfcf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Charts/CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/Charts/CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_template_id_631ebfcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/CandleSticks.vue?vue&type=template&id=631ebfcf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_template_id_631ebfcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CandleSticks_vue_vue_type_template_id_631ebfcf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Charts/LineChartShadowV2.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/Charts/LineChartShadowV2.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Line Chart Shadow


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    enableShadow: {
      type: Boolean,
      "default": true
    },
    dataSet: {
      type: Array,
      "default": function _default() {
        return [10, 30, 39, 65, 85, 10, 10];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0.4;
      }
    },
    dataLabels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F'];
      }
    },
    label1: {
      type: String,
      "default": function _default() {
        return 'Subscribed';
      }
    },
    borderWidth: {
      type: Number,
      "default": function _default() {
        return 3;
      }
    },
    gradientColor1: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    gradientColor2: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    enableGradient: {
      type: Boolean,
      "default": function _default() {
        return true;
      }
    },
    borderColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    shadowColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    }
  },
  data: function data() {
    return {
      gradient1: null,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false,
              drawTicks: false
            }
          }],
          xAxes: [{
            ticks: {
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltip: {
          enabled: true
        }
      }
    };
  },
  mounted: function mounted() {
    var self = this; // console.log("dataSet="+this.dataSet);

    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = self.shadowColor;
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 6;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    var gradientColor = ' ';

    if (this.enableGradient) {
      this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 140, 0);
      this.gradient1.addColorStop(0, this.gradientColor1);
      this.gradient1.addColorStop(1, this.gradientColor2);
      gradientColor = this.gradient1;
    } else {
      gradientColor = this.borderColor;
    }

    this.renderChart({
      labels: this.dataLabels,
      datasets: [{
        labels: this.label1,
        data: this.dataSet,
        lineTension: this.lineTension,
        borderColor: gradientColor,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        barPercentage: 0.8,
        pointHoverBorderWidth: 0,
        borderWidth: this.borderWidth,
        fill: false
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartShadowV3.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/Charts/LineChartShadowV3.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Line Chart Shadow


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    enableShadow: {
      type: Boolean,
      "default": true
    },
    shadowColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    dataSet: {
      type: Array,
      "default": function _default() {
        return [10, 30, 39, 65, 85, 10, 10];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0.4;
      }
    },
    dataLabels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F'];
      }
    },
    borderWidth: {
      type: Number,
      "default": function _default() {
        return 3;
      }
    },
    enableGradient: {
      type: Boolean,
      "default": function _default() {
        return true;
      }
    },
    borderColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    }
  },
  data: function data() {
    return {
      gradient1: null,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false,
              drawTicks: false
            }
          }],
          xAxes: [{
            ticks: {
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltip: {
          enabled: true
        }
      }
    };
  },
  mounted: function mounted() {
    var self = this; // console.log("shadowColor="+self.shadowColor);

    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = self.shadowColor;
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 8;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    var gradientColor = ' ';

    if (this.enableGradient) {
      this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 140, 0);
      this.gradient1.addColorStop(0, this.gradientColor1);
      this.gradient1.addColorStop(1, this.gradientColor2);
      gradientColor = this.gradient1;
    } else {
      gradientColor = this.borderColor;
    }

    this.renderChart({
      labels: this.dataLabels,
      datasets: [{
        label: 'My First dataset',
        data: this.dataSet,
        barPercentage: 0.8,
        lineTension: this.lineTension,
        borderColor: gradientColor,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        pointHoverBorderWidth: 0,
        borderWidth: this.borderWidth,
        fill: false
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartV5.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/Charts/LineChartV5.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/**
 * Sales Chart Component
 */
 // Main Component

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: ['label', 'chartdata', 'labels', 'borderColor', 'pointBackgroundColor', 'height', 'pointBorderColor', 'borderWidth', 'shadowColor'],
  data: function data() {
    return {
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: false,
            ticks: {
              min: 0
            },
            gridLines: {
              display: true,
              drawBorder: false
            }
          }],
          yAxes: [{
            display: false,
            ticks: {
              suggestedMin: 0,
              beginAtZero: true
            }
          }]
        },
        tooltips: {
          enabled: false
        },
        hover: {
          mode: null
        }
      }
    };
  },
  mounted: function mounted() {
    var chartdata = this.chartdata,
        labels = this.labels,
        borderColor = this.borderColor,
        pointBorderColor = this.pointBorderColor,
        pointBackgroundColor = this.pointBackgroundColor;
    var self = this; // console.log("shadowColor="+self.shadowColor);

    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = self.shadowColor;
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 8;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    var gradientColor = ' ';

    if (this.enableGradient) {
      this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 140, 0);
      this.gradient1.addColorStop(0, this.gradientColor1);
      this.gradient1.addColorStop(1, this.gradientColor2);
      gradientColor = this.gradient1;
    } else {
      gradientColor = this.borderColor;
    }

    this.renderChart({
      labels: labels,
      datasets: [{
        fill: false,
        lineTension: 0,
        fillOpacity: 0.3,
        borderColor: gradientColor,
        borderWidth: this.borderWidth,
        pointBorderColor: pointBorderColor,
        pointBackgroundColor: pointBackgroundColor,
        pointBorderWidth: 3,
        pointRadius: 6,
        pointHoverBackgroundColor: pointBackgroundColor,
        pointHoverBorderColor: borderColor,
        pointHoverBorderWidth: 4,
        pointHoverRadius: 7,
        data: chartdata
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/StatsCardV7/StatsCardV7.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/StatsCardV7/StatsCardV7.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatsCardV7_vue_vue_type_template_id_28b1e996___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatsCardV7.vue?vue&type=template&id=28b1e996& */ "./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=template&id=28b1e996&");
/* harmony import */ var _StatsCardV7_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatsCardV7.vue?vue&type=script&lang=js& */ "./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StatsCardV7_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StatsCardV7_vue_vue_type_template_id_28b1e996___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StatsCardV7_vue_vue_type_template_id_28b1e996___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StatsCardV7/StatsCardV7.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV7_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV7.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV7_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=template&id=28b1e996&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=template&id=28b1e996& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV7_vue_vue_type_template_id_28b1e996___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV7.vue?vue&type=template&id=28b1e996& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV7/StatsCardV7.vue?vue&type=template&id=28b1e996&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV7_vue_vue_type_template_id_28b1e996___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV7_vue_vue_type_template_id_28b1e996___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/CoinsList.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/CoinsList.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CoinsList_vue_vue_type_template_id_73bead36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CoinsList.vue?vue&type=template&id=73bead36& */ "./resources/js/components/Widgets/CoinsList.vue?vue&type=template&id=73bead36&");
/* harmony import */ var _CoinsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CoinsList.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/CoinsList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CoinsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CoinsList_vue_vue_type_template_id_73bead36___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CoinsList_vue_vue_type_template_id_73bead36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/CoinsList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/CoinsList.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Widgets/CoinsList.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CoinsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CoinsList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CoinsList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CoinsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/CoinsList.vue?vue&type=template&id=73bead36&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/CoinsList.vue?vue&type=template&id=73bead36& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CoinsList_vue_vue_type_template_id_73bead36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CoinsList.vue?vue&type=template&id=73bead36& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CoinsList.vue?vue&type=template&id=73bead36&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CoinsList_vue_vue_type_template_id_73bead36___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CoinsList_vue_vue_type_template_id_73bead36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeRate.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeRate.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExchangeRate.vue?vue&type=template&id=4d9ad88e& */ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&");
/* harmony import */ var _ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExchangeRate.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ExchangeRate.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExchangeRate.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExchangeRate.vue?vue&type=template&id=4d9ad88e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeStatistics.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeStatistics.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExchangeStatistics_vue_vue_type_template_id_33cebad1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExchangeStatistics.vue?vue&type=template&id=33cebad1& */ "./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=template&id=33cebad1&");
/* harmony import */ var _ExchangeStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExchangeStatistics.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExchangeStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExchangeStatistics_vue_vue_type_template_id_33cebad1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExchangeStatistics_vue_vue_type_template_id_33cebad1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ExchangeStatistics.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExchangeStatistics.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=template&id=33cebad1&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=template&id=33cebad1& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeStatistics_vue_vue_type_template_id_33cebad1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExchangeStatistics.vue?vue&type=template&id=33cebad1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeStatistics.vue?vue&type=template&id=33cebad1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeStatistics_vue_vue_type_template_id_33cebad1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeStatistics_vue_vue_type_template_id_33cebad1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/QuickTrade.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/QuickTrade.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _QuickTrade_vue_vue_type_template_id_5f7f2922___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./QuickTrade.vue?vue&type=template&id=5f7f2922& */ "./resources/js/components/Widgets/QuickTrade.vue?vue&type=template&id=5f7f2922&");
/* harmony import */ var _QuickTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./QuickTrade.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/QuickTrade.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _QuickTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _QuickTrade_vue_vue_type_template_id_5f7f2922___WEBPACK_IMPORTED_MODULE_0__["render"],
  _QuickTrade_vue_vue_type_template_id_5f7f2922___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/QuickTrade.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/QuickTrade.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/QuickTrade.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuickTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./QuickTrade.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuickTrade.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_QuickTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/QuickTrade.vue?vue&type=template&id=5f7f2922&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/QuickTrade.vue?vue&type=template&id=5f7f2922& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_QuickTrade_vue_vue_type_template_id_5f7f2922___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./QuickTrade.vue?vue&type=template&id=5f7f2922& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/QuickTrade.vue?vue&type=template&id=5f7f2922&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_QuickTrade_vue_vue_type_template_id_5f7f2922___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_QuickTrade_vue_vue_type_template_id_5f7f2922___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/RecentTrades.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/RecentTrades.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecentTrades.vue?vue&type=template&id=2ea9e535& */ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&");
/* harmony import */ var _RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecentTrades.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/RecentTrades.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentTrades.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentTrades.vue?vue&type=template&id=2ea9e535& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SafeTrade.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/SafeTrade.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SafeTrade_vue_vue_type_template_id_ba478008___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SafeTrade.vue?vue&type=template&id=ba478008& */ "./resources/js/components/Widgets/SafeTrade.vue?vue&type=template&id=ba478008&");
/* harmony import */ var _SafeTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SafeTrade.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SafeTrade.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SafeTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SafeTrade_vue_vue_type_template_id_ba478008___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SafeTrade_vue_vue_type_template_id_ba478008___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SafeTrade.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SafeTrade.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Widgets/SafeTrade.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SafeTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SafeTrade.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SafeTrade.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SafeTrade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SafeTrade.vue?vue&type=template&id=ba478008&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SafeTrade.vue?vue&type=template&id=ba478008& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SafeTrade_vue_vue_type_template_id_ba478008___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SafeTrade.vue?vue&type=template&id=ba478008& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SafeTrade.vue?vue&type=template&id=ba478008&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SafeTrade_vue_vue_type_template_id_ba478008___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SafeTrade_vue_vue_type_template_id_ba478008___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TradeHistory.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/TradeHistory.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TradeHistory_vue_vue_type_template_id_b8e1f20a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TradeHistory.vue?vue&type=template&id=b8e1f20a& */ "./resources/js/components/Widgets/TradeHistory.vue?vue&type=template&id=b8e1f20a&");
/* harmony import */ var _TradeHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TradeHistory.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TradeHistory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TradeHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TradeHistory_vue_vue_type_template_id_b8e1f20a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TradeHistory_vue_vue_type_template_id_b8e1f20a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TradeHistory.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TradeHistory.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/TradeHistory.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TradeHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TradeHistory.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TradeHistory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TradeHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TradeHistory.vue?vue&type=template&id=b8e1f20a&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TradeHistory.vue?vue&type=template&id=b8e1f20a& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TradeHistory_vue_vue_type_template_id_b8e1f20a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TradeHistory.vue?vue&type=template&id=b8e1f20a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TradeHistory.vue?vue&type=template&id=b8e1f20a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TradeHistory_vue_vue_type_template_id_b8e1f20a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TradeHistory_vue_vue_type_template_id_b8e1f20a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/crypto/Crypto.vue":
/*!**********************************************!*\
  !*** ./resources/js/views/crypto/Crypto.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Crypto_vue_vue_type_template_id_28601000___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Crypto.vue?vue&type=template&id=28601000& */ "./resources/js/views/crypto/Crypto.vue?vue&type=template&id=28601000&");
/* harmony import */ var _Crypto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Crypto.vue?vue&type=script&lang=js& */ "./resources/js/views/crypto/Crypto.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Crypto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Crypto_vue_vue_type_template_id_28601000___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Crypto_vue_vue_type_template_id_28601000___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/crypto/Crypto.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/crypto/Crypto.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/crypto/Crypto.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Crypto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Crypto.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Crypto.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Crypto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/crypto/Crypto.vue?vue&type=template&id=28601000&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/crypto/Crypto.vue?vue&type=template&id=28601000& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Crypto_vue_vue_type_template_id_28601000___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Crypto.vue?vue&type=template&id=28601000& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Crypto.vue?vue&type=template&id=28601000&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Crypto_vue_vue_type_template_id_28601000___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Crypto_vue_vue_type_template_id_28601000___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);