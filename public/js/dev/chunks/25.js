(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[25],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectManagement.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectManagement.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      settings: {
        maxScrollbarLength: 100
      },
      loader: true,
      projectData: []
    };
  },
  mounted: function mounted() {
    this.projectManagement();
  },
  methods: {
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_1___default()(date * 1000).format('DD MMM YYYY');
    },
    projectManagement: function projectManagement() {
      var self = this;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/projectData.js").then(function (response) {
        self.loader = false;
        self.projectData = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    expand: function expand(data) {
      var index = this.projectData.indexOf(data);
      this.projectData[index].collapseStatus = !this.projectData[index].collapseStatus;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      active: false,
      settings: {
        maxScrollbarLength: 100
      },
      selectedProject: '',
      selectDeletedCustomer: null,
      newTask: {
        title: "",
        date: ""
      },
      loader: false,
      snackbar: false,
      snackbarMessage: "",
      timeout: 2000,
      y: "top",
      valid: false,
      projectTaskData: {
        'BookingKoala': [{
          id: 1,
          title: "Wireframing for dashboard sidebar and topbar",
          date: 1528368468,
          status: "Planning",
          color: "primary",
          team: [{
            id: 1,
            name: "Maggie",
            avatar: "/static/avatars/user-1.jpg"
          }, {
            id: 2,
            name: "Lisa",
            avatar: "/static/avatars/user-2.jpg"
          }, {
            id: 3,
            name: "Lucile",
            avatar: "/static/avatars/user-4.jpg"
          }]
        }, {
          id: 2,
          title: "Wireframing for dashboard sidebar and topbar",
          date: 1529346600,
          status: "In Progress",
          color: "success",
          team: [{
            id: 1,
            name: "Maggie",
            avatar: "/static/avatars/user-1.jpg"
          }, {
            id: 2,
            name: "Lisa",
            avatar: "/static/avatars/user-2.jpg"
          }, {
            id: 3,
            name: "Lucile",
            avatar: "/static/avatars/user-4.jpg"
          }]
        }],
        'Reactify': [{
          id: 1,
          title: "Create video placeholder for website",
          date: 1529951400,
          status: "In Progress",
          color: "success",
          team: [{
            id: 1,
            name: "Maggie",
            avatar: "/static/avatars/user-1.jpg"
          }, {
            id: 2,
            name: "Lisa",
            avatar: "/static/avatars/user-2.jpg"
          }, {
            id: 3,
            name: "Lucile",
            avatar: "/static/avatars/user-4.jpg"
          }]
        }, {
          id: 2,
          title: "Social media ads banner for launching",
          date: 1531161000,
          status: "On Hold",
          color: "success",
          team: [{
            id: 1,
            name: "Maggie",
            avatar: "https://reactify.theironnetwork.org/data/images/user-1.jpg"
          }, {
            id: 2,
            name: "Lisa",
            avatar: "https://reactify.theironnetwork.org/data/images/user-2.jpg"
          }, {
            id: 3,
            name: "Lucile",
            avatar: "https://reactify.theironnetwork.org/data/images/user-3.jpg"
          }]
        }],
        'Adminify': [{
          id: 1,
          title: "Create new design for frontend website",
          date: 1530729000,
          status: "Completed",
          color: "error",
          team: [{
            id: 1,
            name: "Maggie",
            avatar: "https://reactify.theironnetwork.org/data/images/user-1.jpg"
          }, {
            id: 2,
            name: "Lisa",
            avatar: "https://reactify.theironnetwork.org/data/images/user-2.jpg"
          }, {
            id: 3,
            name: "Lucile",
            avatar: "https://reactify.theironnetwork.org/data/images/user-3.jpg"
          }]
        }, {
          id: 2,
          title: "Update new logo everywhere",
          date: 1532716200,
          status: "Completed",
          color: "error",
          team: [{
            id: 1,
            name: "Maggie",
            avatar: "https://reactify.theironnetwork.org/data/images/user-1.jpg"
          }, {
            id: 2,
            name: "Lisa",
            avatar: "https://reactify.theironnetwork.org/data/images/user-2.jpg"
          }, {
            id: 3,
            name: "Lucile",
            avatar: "https://reactify.theironnetwork.org/data/images/user-3.jpg"
          }]
        }]
      }
    };
  },
  methods: {
    addTask: function addTask(tasks) {
      var _this = this;

      if (this.newTask.title && this.newTask.date !== "") {
        this.loader = true;
        setTimeout(function () {
          _this.loader = false;
          _this.selectedProject = '';

          var unixTime = moment__WEBPACK_IMPORTED_MODULE_0___default()(_this.newTask.date).unix();

          var newProjectTask = {
            date: unixTime,
            title: _this.newTask.title,
            status: "Planning",
            color: "primary",
            team: [{
              name: "Lopohis",
              avatar: "/static/avatars/user-28.jpg"
            }]
          };

          _this.projectTaskData[tasks].push(newProjectTask);

          _this.newTask.date = "";
          _this.newTask.title = "";
          _this.snackbar = true;
          _this.snackbarMessage = "Task Added Successfully";
        }, 1500);
      }
    },
    expand: function expand(selected) {
      this.selectedProject = selected;
    },
    collapse: function collapse() {
      this.selectedProject = '';
    },
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(date * 1000).format('DD MMM YYYY');
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_DoughnutChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/DoughnutChart */ "./resources/js/components/Charts/DoughnutChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    DoughnutChart: _Charts_DoughnutChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Weather.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Weather.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Constants_AppConfig__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/AppConfig */ "./resources/js/constants/AppConfig.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      props: ['userCity'],
      city: this.userCity ? this.userCity : "Mohali",
      weatherDetails: null,
      temp: '',
      weatherDescription: '',
      date: '',
      weatherIcon: ''
    };
  },
  mounted: function mounted() {
    var _this = this;

    var appid = Constants_AppConfig__WEBPACK_IMPORTED_MODULE_1__["default"].weatherApiId; // Your api id

    var apikey = Constants_AppConfig__WEBPACK_IMPORTED_MODULE_1__["default"].weatherApiKey; // Your apikey

    this.$http.get('https://api.openweathermap.org/data/2.5/forecast/daily?q=' + this.city + '&cnt=5&units=metric&mode=json&appid=' + appid + '&apikey=' + apikey).then(function (response) {
      _this.weatherDetails = response.data;
      _this.city = _this.weatherDetails.city.name;
      _this.temp = _this.weatherDetails.list[0].temp.day;
      _this.date = _this.weatherDetails.list[0].temp.dt;
      _this.weatherDescription = _this.weatherDetails.list[0].weather[0].description;
      _this.weatherIcon = _this.getIcon(_this.weatherDetails.list[0].weather[0].id);
    }, function (response) {
      console.log(response);
    });
  },
  methods: {
    // function to get today weather icon
    getIcon: function getIcon(id) {
      if (id >= 200 && id < 300) {
        return 'wi wi-night-showers';
      } else if (id >= 300 && id < 500) {
        return 'wi day-sleet';
      } else if (id >= 500 && id < 600) {
        return 'wi wi-night-showers';
      } else if (id >= 600 && id < 700) {
        return 'wi wi-day-snow';
      } else if (id >= 700 && id < 800) {
        return 'wi wi-day-fog';
      } else if (id === 800) {
        return 'wi wi-day-sunny';
      } else if (id >= 801 && id < 803) {
        return 'wi wi-night-partly-cloudy';
      } else if (id >= 802 && id < 900) {
        return 'wi wi-day-cloudy';
      } else if (id === 905 || id >= 951 && id <= 956) {
        return 'wi wi-day-windy';
      } else if (id >= 900 && id < 1000) {
        return 'wi wi-night-showers';
      }
    },
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(date).format('dddd MM YYYY');
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Saas.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Saas.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartShadow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartShadow */ "./resources/js/components/Charts/LineChartShadow.js");
/* harmony import */ var Components_Charts_AdCampaignPerfomance__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Charts/AdCampaignPerfomance */ "./resources/js/components/Charts/AdCampaignPerfomance.js");
/* harmony import */ var Components_Widgets_CampaignPerformance_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Widgets/CampaignPerformance.vue */ "./resources/js/components/Widgets/CampaignPerformance.vue");
/* harmony import */ var Components_Widgets_UserProfile__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/UserProfile */ "./resources/js/components/Widgets/UserProfile.vue");
/* harmony import */ var Components_Widgets_QuoteOfTheDay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/QuoteOfTheDay */ "./resources/js/components/Widgets/QuoteOfTheDay.vue");
/* harmony import */ var Components_Widgets_SupportRequest__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/SupportRequest */ "./resources/js/components/Widgets/SupportRequest.vue");
/* harmony import */ var Components_Widgets_ActiveUser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/ActiveUser */ "./resources/js/components/Widgets/ActiveUser.vue");
/* harmony import */ var Components_Widgets_Weather__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/Widgets/Weather */ "./resources/js/components/Widgets/Weather.vue");
/* harmony import */ var Components_Widgets_ProjectManagement__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Components/Widgets/ProjectManagement */ "./resources/js/components/Widgets/ProjectManagement.vue");
/* harmony import */ var Components_Widgets_ProjectTaskManagement__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Components/Widgets/ProjectTaskManagement */ "./resources/js/components/Widgets/ProjectTaskManagement.vue");
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./data */ "./resources/js/views/dashboard/data.js");
/* harmony import */ var Views_widgets_data__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Views/widgets/data */ "./resources/js/views/widgets/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // widgets








 // data



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    LineChartShadow: Components_Charts_LineChartShadow__WEBPACK_IMPORTED_MODULE_0__["default"],
    CampaignPerformance: Components_Widgets_CampaignPerformance_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    UserProfile: Components_Widgets_UserProfile__WEBPACK_IMPORTED_MODULE_3__["default"],
    QuoteOfTheDay: Components_Widgets_QuoteOfTheDay__WEBPACK_IMPORTED_MODULE_4__["default"],
    SupportRequest: Components_Widgets_SupportRequest__WEBPACK_IMPORTED_MODULE_5__["default"],
    AdCampaignPerfomance: Components_Charts_AdCampaignPerfomance__WEBPACK_IMPORTED_MODULE_1__["default"],
    ActiveUser: Components_Widgets_ActiveUser__WEBPACK_IMPORTED_MODULE_6__["default"],
    Weather: Components_Widgets_Weather__WEBPACK_IMPORTED_MODULE_7__["default"],
    ProjectManagement: Components_Widgets_ProjectManagement__WEBPACK_IMPORTED_MODULE_8__["default"],
    ProjectTaskManagement: Components_Widgets_ProjectTaskManagement__WEBPACK_IMPORTED_MODULE_9__["default"]
  },
  data: function data() {
    return {
      totalEarnings: _data__WEBPACK_IMPORTED_MODULE_10__["totalEarnings"],
      netProfit: _data__WEBPACK_IMPORTED_MODULE_10__["netProfit"],
      onlineRevenue: _data__WEBPACK_IMPORTED_MODULE_10__["onlineRevenue"],
      totalExpences: _data__WEBPACK_IMPORTED_MODULE_10__["totalExpences"],
      adCampaignPerfomanceData: _data__WEBPACK_IMPORTED_MODULE_10__["adCampaignPerfomanceData"],
      activeUser: Views_widgets_data__WEBPACK_IMPORTED_MODULE_11__["activeUser"],
      defaultSelectedItem: 'All',
      selectItems: ['All', 'Booking Koala', 'Reactify', 'Adminify']
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectManagement.vue?vue&type=template&id=1ee78ae1&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectManagement.vue?vue&type=template&id=1ee78ae1& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "project-management" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "636px" }, attrs: { settings: _vm.settings } },
        [
          _c("div", { staticClass: "v-table__overflow" }, [
            _c(
              "table",
              {
                staticClass:
                  "v-datatable v-table v-datatable--select-all theme--light"
              },
              [
                _c("thead", [
                  _c("tr", [
                    _c("th", [_c("span", [_vm._v("Project Name")])]),
                    _vm._v(" "),
                    _c("th", [_c("span", [_vm._v("Deadline")])]),
                    _vm._v(" "),
                    _c("th", [_c("span", [_vm._v("Status")])]),
                    _vm._v(" "),
                    _c("th", [_c("span", [_vm._v("Team")])])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "tbody",
                  [
                    _vm._l(_vm.projectData, function(data, index) {
                      return [
                        _c("tr", { key: index }, [
                          _c("td", [_vm._v(_vm._s(data.name))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(_vm.moment(data.date)))]),
                          _vm._v(" "),
                          _c(
                            "td",
                            [
                              _c("v-progress-linear", {
                                attrs: {
                                  value: data.progress.value,
                                  height: "5",
                                  color: data.progress.color
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "fs-12 fw-normal grey--text" },
                                [_vm._v(_vm._s(data.status))]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("td", [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "justify-space-between d-custom-flex align-center"
                              },
                              [
                                _c(
                                  "div",
                                  { staticClass: "team-avatar w-100" },
                                  [
                                    _c(
                                      "ul",
                                      {
                                        staticClass:
                                          "d-custom-flex align-items-center pa-0"
                                      },
                                      _vm._l(data.team, function(team) {
                                        return _c("li", { key: team.name }, [
                                          _c(
                                            "div",
                                            [
                                              _c(
                                                "v-tooltip",
                                                {
                                                  attrs: { top: "" },
                                                  scopedSlots: _vm._u(
                                                    [
                                                      {
                                                        key: "activator",
                                                        fn: function(ref) {
                                                          var on = ref.on
                                                          return [
                                                            _c(
                                                              "img",
                                                              _vm._g(
                                                                {
                                                                  staticClass:
                                                                    "img-responsive",
                                                                  attrs: {
                                                                    src:
                                                                      team.avatar,
                                                                    alt:
                                                                      "user images",
                                                                    width: "26",
                                                                    height: "26"
                                                                  }
                                                                },
                                                                on
                                                              )
                                                            )
                                                          ]
                                                        }
                                                      }
                                                    ],
                                                    null,
                                                    true
                                                  )
                                                },
                                                [
                                                  _vm._v(" "),
                                                  _c("span", [
                                                    _vm._v(_vm._s(team.name))
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        ])
                                      }),
                                      0
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                data.collapseStatus
                                  ? _c(
                                      "v-btn",
                                      {
                                        attrs: { text: "", icon: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.expand(data)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass:
                                            "zmdi zmdi-minus-circle font-lg grey--text"
                                        })
                                      ]
                                    )
                                  : _c(
                                      "v-btn",
                                      {
                                        attrs: { text: "", icon: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.expand(data)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass:
                                            "zmdi zmdi-plus-circle font-lg grey--text"
                                        })
                                      ]
                                    )
                              ],
                              1
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        data.collapseStatus
                          ? _c("tr", { key: data.description }, [
                              _c("td", { attrs: { colspan: "5" } }, [
                                _c("div", { staticClass: "pa-2" }, [
                                  _c(
                                    "span",
                                    { staticClass: "fs-12 fw-normal" },
                                    [_vm._v("Description")]
                                  ),
                                  _vm._v(" "),
                                  _c("p", { staticClass: "mb-0" }, [
                                    _vm._v(_vm._s(data.description))
                                  ])
                                ])
                              ])
                            ])
                          : _vm._e()
                      ]
                    })
                  ],
                  2
                )
              ]
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=template&id=1f046674&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=template&id=1f046674& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "622px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            [
              _vm._l(_vm.projectTaskData, function(tasks, taskkey, index) {
                return [
                  _c(
                    "v-list-item",
                    { key: taskkey },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "project-head d-custom-flex justify-space-between align-center"
                        },
                        [
                          _c("h5", { staticClass: "mb-0" }, [
                            _vm._v(_vm._s(taskkey))
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              attrs: { text: "", icon: "" },
                              on: {
                                click: function($event) {
                                  return _vm.expand(tasks)
                                }
                              }
                            },
                            [
                              _c("i", {
                                staticClass:
                                  "zmdi zmdi-plus-circle font-lg grey--text"
                              })
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-list",
                        { staticClass: "project-list-item list-aqua-ripple" },
                        _vm._l(tasks.length, function(items, index) {
                          return _c(
                            "v-list-item",
                            { key: tasks[index].date, attrs: { ripple: "" } },
                            [
                              _c("div", { staticClass: "w-50" }, [
                                _c("h6", { staticClass: "mb-1" }, [
                                  _vm._v(_vm._s(tasks[index].title))
                                ]),
                                _vm._v(" "),
                                _c("span", { staticClass: "fs-12 fw-normal" }, [
                                  _vm._v(_vm._s(_vm.moment(tasks[index].date)))
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "w-50 d-custom-flex justify-space-between align-center badge-wrap"
                                },
                                [
                                  _c(
                                    "span",
                                    {
                                      staticClass: "v-badge",
                                      class: tasks[index].color
                                    },
                                    [_vm._v(_vm._s(tasks[index].status))]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "team-avatar" }, [
                                    _c(
                                      "ul",
                                      {
                                        staticClass:
                                          "d-custom-flex align-items-center pa-0"
                                      },
                                      _vm._l(tasks[index].team, function(team) {
                                        return _c("li", { key: team.name }, [
                                          _c(
                                            "div",
                                            [
                                              _c(
                                                "v-tooltip",
                                                {
                                                  attrs: { top: "" },
                                                  scopedSlots: _vm._u(
                                                    [
                                                      {
                                                        key: "activator",
                                                        fn: function(ref) {
                                                          var on = ref.on
                                                          return [
                                                            _c(
                                                              "img",
                                                              _vm._g(
                                                                {
                                                                  staticClass:
                                                                    "img-responsive",
                                                                  attrs: {
                                                                    src:
                                                                      team.avatar,
                                                                    alt:
                                                                      "user images",
                                                                    width: "26",
                                                                    height: "26"
                                                                  }
                                                                },
                                                                on
                                                              )
                                                            )
                                                          ]
                                                        }
                                                      }
                                                    ],
                                                    null,
                                                    true
                                                  )
                                                },
                                                [
                                                  _vm._v(" "),
                                                  _c("span", [
                                                    _vm._v(_vm._s(team.name))
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        ])
                                      }),
                                      0
                                    )
                                  ])
                                ]
                              )
                            ]
                          )
                        }),
                        1
                      ),
                      _vm._v(" "),
                      _vm.selectedProject == tasks
                        ? [
                            _c(
                              "v-form",
                              {
                                key: index,
                                model: {
                                  value: _vm.valid,
                                  callback: function($$v) {
                                    _vm.valid = $$v
                                  },
                                  expression: "valid"
                                }
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "add-project d-custom-flex justify-space-between align-center"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "w-50 input-wrap" },
                                      [
                                        _c("v-text-field", {
                                          staticClass: "mb-5",
                                          attrs: { label: "Task Name" },
                                          model: {
                                            value: _vm.newTask.title,
                                            callback: function($$v) {
                                              _vm.$set(
                                                _vm.newTask,
                                                "title",
                                                $$v
                                              )
                                            },
                                            expression: "newTask.title"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("v-text-field", {
                                          staticClass: "mb-5",
                                          attrs: {
                                            type: "date",
                                            label: "Due Date"
                                          },
                                          model: {
                                            value: _vm.newTask.date,
                                            callback: function($$v) {
                                              _vm.$set(_vm.newTask, "date", $$v)
                                            },
                                            expression: "newTask.date"
                                          }
                                        })
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "button-wrap" },
                                      [
                                        _c(
                                          "v-btn",
                                          {
                                            staticClass:
                                              "mt-0 mb-5 d-inline-block",
                                            attrs: {
                                              color: "primary",
                                              small: ""
                                            },
                                            on: {
                                              click: function($event) {
                                                return _vm.addTask(taskkey)
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\t\tAdd\n\t\t\t\t\t\t\t\t\t"
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "v-btn",
                                          {
                                            staticClass: "mb-0 mt-2 d-block",
                                            attrs: {
                                              color: "error",
                                              small: ""
                                            },
                                            on: {
                                              click: function($event) {
                                                return _vm.collapse()
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n\t\t\t\t\t\t\t\t\t\tCancel\n\t\t\t\t\t\t\t\t\t"
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ]
                            )
                          ]
                        : _vm._e()
                    ],
                    2
                  )
                ]
              })
            ],
            2
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { top: _vm.y === "top", timeout: _vm.timeout },
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [_vm._v("\n\t\t" + _vm._s(_vm.snackbarMessage) + "\n\t")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "support-widget" },
    [
      _c("doughnut-chart", { attrs: { height: 150 } }),
      _vm._v(" "),
      _c("ul", { staticClass: "list-unstyled" }, [
        _c(
          "li",
          { staticClass: "justify-space-between align-center" },
          [
            _c("p", { staticClass: "mb-0 content-title" }, [
              _vm._v(_vm._s(_vm.$t("message.totalRequest")))
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "badge-wrap" },
              [
                _c(
                  "v-badge",
                  { staticClass: "primary", attrs: { value: false } },
                  [_vm._v("250")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "grey--text" }, [
                  _vm._v("visibility")
                ])
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "justify-space-between align-center" },
          [
            _c("p", { staticClass: "mb-0 content-title" }, [
              _vm._v(_vm._s(_vm.$t("message.new")))
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "badge-wrap" },
              [
                _c(
                  "v-badge",
                  { staticClass: "warning", attrs: { value: false } },
                  [_vm._v("25")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "grey--text" }, [
                  _vm._v("visibility")
                ])
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "li",
          { staticClass: "justify-space-between align-center" },
          [
            _c("p", { staticClass: "mb-0 content-title" }, [
              _vm._v(_vm._s(_vm.$t("message.pending")))
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "badge-wrap" },
              [
                _c(
                  "v-badge",
                  { staticClass: "error", attrs: { value: false } },
                  [_vm._v("125")]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "grey--text" }, [
                  _vm._v("visibility")
                ])
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UserProfile.vue?vue&type=template&id=3be10ec3&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UserProfile.vue?vue&type=template&id=3be10ec3& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "user-profile-widget top-author-wrap" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "author-detail-wrap" }, [
      _vm._m(1),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "d-custom-flex align-center px-3 pb-3" },
        [
          _c(
            "v-btn",
            { staticClass: "ma-0 mr-3", attrs: { color: "primary" } },
            [_vm._v("View Profile")]
          ),
          _vm._v(" "),
          _c("v-btn", { staticClass: "ma-0", attrs: { color: "warning" } }, [
            _vm._v("Send Message")
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm._m(2)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "avatar-wrap mb-50 pos-relative" }, [
      _c("span", { staticClass: "overlay-content" }),
      _vm._v(" "),
      _c("div", { staticClass: "user-info" }, [
        _c("img", {
          staticClass: "img-responsive rounded-circle mr-3",
          attrs: {
            src: "/static/avatars/user-7.jpg",
            alt: "reviwers",
            width: "100",
            height: "100"
          }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "white--text pt-4" }, [
          _c("h5", { staticClass: "mb-0" }, [_vm._v("Phoebe Henderson")]),
          _vm._v(" "),
          _c("span", { staticClass: "fs-14" }, [_vm._v("CEO")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pa-3 authors-info" }, [
      _c("ul", { staticClass: "list-unstyled author-contact-info mb-2" }, [
        _c("li", [
          _c("span", { staticClass: "mr-3" }, [
            _c("i", { staticClass: "zmdi zmdi-phone-msg" })
          ]),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "fs-12 grey--text fw-normal",
              attrs: { href: "tel:123456" }
            },
            [_vm._v("+2347637684")]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c("span", { staticClass: "mr-3" }, [
            _c("i", { staticClass: "zmdi zmdi-email" })
          ]),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "fs-12 grey--text fw-normal",
              attrs: { href: "mailto:joan_parisian@gmail.com" }
            },
            [_vm._v("abc@example.com")]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c("span", { staticClass: "mr-3" }, [
            _c("i", { staticClass: "zmdi zmdi-pin" })
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "fs-12 grey--text fw-normal" }, [
            _vm._v("Mohali")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("ul", { staticClass: "d-custom-flex social-info list-unstyled" }, [
        _c("li", [
          _c(
            "a",
            { staticClass: "facebook", attrs: { href: "www.facebook.com" } },
            [_c("i", { staticClass: "zmdi zmdi-facebook-box" })]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { staticClass: "twitter", attrs: { href: "www.twitter.com" } },
            [_c("i", { staticClass: "zmdi zmdi-twitter-box" })]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { staticClass: "linkedin", attrs: { href: "www.linkedin.com" } },
            [_c("i", { staticClass: "zmdi zmdi-linkedin-box" })]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { staticClass: "instagram", attrs: { href: "www.instagram.com" } },
            [_c("i", { staticClass: "zmdi zmdi-instagram" })]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      {
        staticClass:
          "d-custom-flex list-unstyled footer-content text-center w-100 border-top-1 align-end"
      },
      [
        _c("li", [
          _c("h5", { staticClass: "mb-0" }, [_vm._v("80")]),
          _vm._v(" "),
          _c("span", { staticClass: "fs-12 grey--text fw-normal" }, [
            _vm._v("Articles")
          ])
        ]),
        _vm._v(" "),
        _c("li", [
          _c("h5", { staticClass: "mb-0" }, [_vm._v("150")]),
          _vm._v(" "),
          _c("span", { staticClass: "fs-12 grey--text fw-normal" }, [
            _vm._v("Followers")
          ])
        ]),
        _vm._v(" "),
        _c("li", [
          _c("h5", { staticClass: "mb-0" }, [_vm._v("2k")]),
          _vm._v(" "),
          _c("span", { staticClass: "fs-12 grey--text fw-normal" }, [
            _vm._v("Likes")
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Weather.vue?vue&type=template&id=19eae24e&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Weather.vue?vue&type=template&id=19eae24e& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.weatherDetails != null
    ? _c(
        "div",
        { staticClass: "weather-widget" },
        [
          _c("div", { staticClass: "d-custom-flex weather-head white--text" }, [
            _c("span", { staticClass: "overlay-content" }),
            _vm._v(" "),
            _c("div", { staticClass: "w-70 weather-info" }, [
              _c("div", { staticClass: "mb-5 mr-4" }, [
                _c("h3", { staticClass: "city-name" }, [
                  _vm._v(_vm._s(_vm.city))
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "d-block" }, [
                  _vm._v(_vm._s(_vm.moment(_vm.date)))
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "weather-temp" }, [
                _c("h2", { staticClass: "fw-bold d-inline-block" }, [
                  _vm._v(_vm._s(_vm.temp)),
                  _c("sup", [_vm._v("°C")])
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "text-capitalize d-inline-block" }, [
                  _vm._v(_vm._s(_vm.weatherDescription))
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "align-item-end pt-4 weather-icon" }, [
              _c("span", { staticClass: "font-4x fw-bold" }, [
                _c("i", { class: _vm.weatherIcon })
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "v-list",
            _vm._l(_vm.weatherDetails.list, function(weather, index) {
              return _c("v-list-item", { key: index }, [
                _c("h6", { staticClass: "mb-0 w-20" }, [
                  _vm._v(_vm._s(_vm.city))
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "w-40" }, [
                  _vm._v(_vm._s(_vm.moment(weather.dt * 1000)))
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "w-20" }, [
                  _vm._v(_vm._s(weather.temp.day)),
                  _c("sup", [_vm._v("°C")])
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "w-20" }, [
                  _vm._v(_vm._s(weather.weather[0].main))
                ])
              ])
            }),
            1
          )
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Saas.vue?vue&type=template&id=2c1f86fa&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Saas.vue?vue&type=template&id=2c1f86fa& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.overallTrafficStatus"),
                    reloadable: true,
                    fullScreen: true,
                    closeable: true,
                    colClasses: "col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "d-sm-block d-none pa-4 mb-5 mt-n5" },
                    [
                      _c(
                        "v-row",
                        [
                          _c(
                            "v-col",
                            {
                              staticClass:
                                "d-custom-flex justify-space-between align-end",
                              attrs: { xl: "4", lg: "4", md: "4", sm: "4" }
                            },
                            [
                              _c("div", [
                                _c("p", { staticClass: "mb-0 grey--text" }, [
                                  _vm._v("Online Sources")
                                ]),
                                _vm._v(" "),
                                _c("h3", { staticClass: "mb-0" }, [
                                  _vm._v("3500")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("span", [
                                _c("i", {
                                  staticClass:
                                    "ti-arrow-up font-lg success--text"
                                })
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            {
                              staticClass:
                                "d-custom-flex justify-space-between align-end",
                              attrs: { xl: "4", lg: "4", md: "4", sm: "4" }
                            },
                            [
                              _c("div", [
                                _c("p", { staticClass: "mb-0 grey--text" }, [
                                  _vm._v("Today")
                                ]),
                                _vm._v(" "),
                                _c("h3", { staticClass: "mb-0" }, [
                                  _vm._v("17,020")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("span", [
                                _c("i", {
                                  staticClass:
                                    "ti-arrow-up font-lg success--text"
                                })
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            {
                              staticClass:
                                "d-custom-flex justify-space-between align-end",
                              attrs: { xl: "4", lg: "4", md: "4", sm: "4" }
                            },
                            [
                              _c("div", [
                                _c("p", { staticClass: "mb-0 grey--text" }, [
                                  _vm._v("Last Month")
                                ]),
                                _vm._v(" "),
                                _c("h3", { staticClass: "mb-0" }, [
                                  _vm._v("20.30%")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("span", [
                                _c("i", {
                                  staticClass:
                                    "ti-arrow-down error--text font-lg"
                                })
                              ])
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("ad-campaign-perfomance", {
                    attrs: { height: 300, data: _vm.adCampaignPerfomanceData }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "12", sm: "12", cols: "12" } },
                [
                  _c(
                    "v-row",
                    { staticClass: "border-rad-sm" },
                    [
                      _c(
                        "stats-card-v2",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12",
                            heading: _vm.$t("message.totalSales"),
                            amount: 1435
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "pa-4" },
                            [
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.totalEarnings.data,
                                  lineTension: _vm.totalEarnings.lineTension,
                                  dataLabels: _vm.totalEarnings.labels,
                                  width: 450,
                                  height: 100,
                                  borderColor: _vm.totalEarnings.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "stats-card-v2",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12",
                            heading: _vm.$t("message.netProfit"),
                            amount: 2478
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "pa-4" },
                            [
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.netProfit.data,
                                  lineTension: _vm.netProfit.lineTension,
                                  dataLabels: _vm.netProfit.labels,
                                  width: 450,
                                  height: 100,
                                  borderColor: _vm.netProfit.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "stats-card-v2",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12",
                            heading: _vm.$t("message.tax"),
                            amount: 1200
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "pa-4" },
                            [
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.totalExpences.data,
                                  lineTension: _vm.totalExpences.lineTension,
                                  dataLabels: _vm.totalExpences.labels,
                                  width: 450,
                                  height: 100,
                                  borderColor: _vm.totalExpences.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "stats-card-v2",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12",
                            heading: _vm.$t("message.expenses"),
                            amount: 3478
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "pa-4" },
                            [
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.onlineRevenue.data,
                                  lineTension: _vm.onlineRevenue.lineTension,
                                  dataLabels: _vm.onlineRevenue.labels,
                                  width: 450,
                                  height: 100,
                                  borderColor: _vm.onlineRevenue.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12",
                    heading: _vm.$t("message.supportRequest"),
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    footer: true,
                    fullBlock: true,
                    customClasses: "support-widget-wrap"
                  }
                },
                [
                  _c("support-request"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "justify-space-between footer-flex align-items-center",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "span",
                        {
                          staticClass:
                            "grey--text d-custom-flex align-items-center"
                        },
                        [
                          _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v("Updated 10 min ago")
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-0",
                          attrs: { color: "primary", small: "" }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.assignNow")))]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12",
                    fullBlock: true
                  }
                },
                [_c("user-profile")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-12 col-12 col-sm-12",
                    fullBlock: true,
                    customClasses: "grayish-blue white--text"
                  }
                },
                [_c("quote-of-the-day")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses:
                      "col-xl-4 col-lg-4 col-md-6 col-12 col-sm-6 col-12",
                    fullBlock: true
                  }
                },
                [_c("active-user", { attrs: { data: _vm.activeUser } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-12 col-sm-6",
                    fullBlock: true
                  }
                },
                [_c("weather")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.campaignPerformance"),
                    colClasses: "col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    fullBlock: true,
                    customClasses: "campaign-performance"
                  }
                },
                [_c("campaign-performance")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12",
                    heading: _vm.$t("message.projectManagement"),
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    fullBlock: true,
                    footer: true
                  }
                },
                [
                  _c("project-management"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "justify-space-between footer-flex align-items-center",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "span",
                        {
                          staticClass:
                            "grey--text d-custom-flex align-items-center"
                        },
                        [
                          _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v("Updated 10 min ago")
                          ])
                        ]
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12",
                    heading: _vm.$t("message.projectTaskManagement"),
                    customClasses: "project-task-management",
                    closeable: true,
                    fullScreen: true,
                    reloadable: true,
                    fullBlock: true,
                    footer: true
                  }
                },
                [
                  _c("project-task-management"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "justify-space-between d-sm-flex align-items-center",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "w-40 mb-sm-0 mb-2 d-custom-flex align-center justify-space-between"
                        },
                        [
                          _c(
                            "p",
                            {
                              staticClass:
                                "grey-text fs-12 fw-normal w-50 mb-0 mr-2"
                            },
                            [_vm._v("Select Project")]
                          ),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              items: _vm.selectItems,
                              label: "Select Project",
                              solo: ""
                            },
                            model: {
                              value: _vm.defaultSelectedItem,
                              callback: function($$v) {
                                _vm.defaultSelectedItem = $$v
                              },
                              expression: "defaultSelectedItem"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass:
                            "grey--text d-custom-flex align-items-center"
                        },
                        [
                          _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v("Updated 10 min ago")
                          ])
                        ]
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/index.js":
/*!***********************************!*\
  !*** ./resources/js/api/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: 'https://reactify.theironnetwork.org/data/'
}));

/***/ }),

/***/ "./resources/js/components/Charts/DoughnutChart.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/Charts/DoughnutChart.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Doughnut Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Doughnut"],
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      options: {
        legend: {
          display: false
        },
        cutoutPercentage: 60,
        padding: 10
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Total Request', 'New', 'Pending'],
      datasets: [{
        data: [250, 25, 125],
        backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger],
        hoverBackgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.danger]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartShadow.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/Charts/LineChartShadow.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Line Chart Shadow


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    enableShadow: {
      type: Boolean,
      "default": true
    },
    shadowColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    dataSet: {
      type: Array,
      "default": function _default() {
        return [10, 30, 39, 65, 85, 10, 10];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0.4;
      }
    },
    dataLabels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F'];
      }
    },
    borderWidth: {
      type: Number,
      "default": function _default() {
        return 3;
      }
    },
    gradientColor1: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    gradientColor2: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    enableGradient: {
      type: Boolean,
      "default": function _default() {
        return true;
      }
    },
    borderColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    }
  },
  data: function data() {
    return {
      gradient1: null,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false,
              drawTicks: false
            }
          }],
          xAxes: [{
            ticks: {
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltip: {
          enabled: true
        }
      }
    };
  },
  mounted: function mounted() {
    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].shadowColor;
        ctx.shadowBlur = 8;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 10;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    var gradientColor = ' ';

    if (this.enableGradient) {
      this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 140, 0);
      this.gradient1.addColorStop(0, this.gradientColor1);
      this.gradient1.addColorStop(1, this.gradientColor2);
      gradientColor = this.gradient1;
    } else {
      gradientColor = this.borderColor;
    }

    this.renderChart({
      labels: this.dataLabels,
      datasets: [{
        label: 'My First dataset',
        data: this.dataSet,
        barPercentage: 0.8,
        lineTension: this.lineTension,
        borderColor: gradientColor,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        pointHoverBorderWidth: 0,
        borderWidth: this.borderWidth,
        fill: false
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectManagement.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectManagement.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectManagement_vue_vue_type_template_id_1ee78ae1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectManagement.vue?vue&type=template&id=1ee78ae1& */ "./resources/js/components/Widgets/ProjectManagement.vue?vue&type=template&id=1ee78ae1&");
/* harmony import */ var _ProjectManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectManagement.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProjectManagement.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectManagement_vue_vue_type_template_id_1ee78ae1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectManagement_vue_vue_type_template_id_1ee78ae1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProjectManagement.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectManagement.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectManagement.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectManagement.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectManagement.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectManagement.vue?vue&type=template&id=1ee78ae1&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectManagement.vue?vue&type=template&id=1ee78ae1& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectManagement_vue_vue_type_template_id_1ee78ae1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectManagement.vue?vue&type=template&id=1ee78ae1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectManagement.vue?vue&type=template&id=1ee78ae1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectManagement_vue_vue_type_template_id_1ee78ae1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectManagement_vue_vue_type_template_id_1ee78ae1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ProjectTaskManagement.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectTaskManagement.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectTaskManagement_vue_vue_type_template_id_1f046674___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectTaskManagement.vue?vue&type=template&id=1f046674& */ "./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=template&id=1f046674&");
/* harmony import */ var _ProjectTaskManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectTaskManagement.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectTaskManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectTaskManagement_vue_vue_type_template_id_1f046674___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectTaskManagement_vue_vue_type_template_id_1f046674___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProjectTaskManagement.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectTaskManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectTaskManagement.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectTaskManagement_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=template&id=1f046674&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=template&id=1f046674& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectTaskManagement_vue_vue_type_template_id_1f046674___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectTaskManagement.vue?vue&type=template&id=1f046674& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectTaskManagement.vue?vue&type=template&id=1f046674&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectTaskManagement_vue_vue_type_template_id_1f046674___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectTaskManagement_vue_vue_type_template_id_1f046674___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequest.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequest.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SupportRequest.vue?vue&type=template&id=11de476b& */ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&");
/* harmony import */ var _SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SupportRequest.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SupportRequest.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportRequest.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportRequest.vue?vue&type=template&id=11de476b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequest.vue?vue&type=template&id=11de476b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequest_vue_vue_type_template_id_11de476b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/UserProfile.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/UserProfile.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserProfile_vue_vue_type_template_id_3be10ec3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserProfile.vue?vue&type=template&id=3be10ec3& */ "./resources/js/components/Widgets/UserProfile.vue?vue&type=template&id=3be10ec3&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _UserProfile_vue_vue_type_template_id_3be10ec3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserProfile_vue_vue_type_template_id_3be10ec3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/UserProfile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/UserProfile.vue?vue&type=template&id=3be10ec3&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/UserProfile.vue?vue&type=template&id=3be10ec3& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserProfile_vue_vue_type_template_id_3be10ec3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserProfile.vue?vue&type=template&id=3be10ec3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UserProfile.vue?vue&type=template&id=3be10ec3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserProfile_vue_vue_type_template_id_3be10ec3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserProfile_vue_vue_type_template_id_3be10ec3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Weather.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/Widgets/Weather.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Weather_vue_vue_type_template_id_19eae24e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Weather.vue?vue&type=template&id=19eae24e& */ "./resources/js/components/Widgets/Weather.vue?vue&type=template&id=19eae24e&");
/* harmony import */ var _Weather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Weather.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Weather.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Weather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Weather_vue_vue_type_template_id_19eae24e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Weather_vue_vue_type_template_id_19eae24e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Weather.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Weather.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/Widgets/Weather.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Weather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Weather.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Weather.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Weather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Weather.vue?vue&type=template&id=19eae24e&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Weather.vue?vue&type=template&id=19eae24e& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Weather_vue_vue_type_template_id_19eae24e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Weather.vue?vue&type=template&id=19eae24e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Weather.vue?vue&type=template&id=19eae24e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Weather_vue_vue_type_template_id_19eae24e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Weather_vue_vue_type_template_id_19eae24e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/Saas.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/dashboard/Saas.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Saas_vue_vue_type_template_id_2c1f86fa___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Saas.vue?vue&type=template&id=2c1f86fa& */ "./resources/js/views/dashboard/Saas.vue?vue&type=template&id=2c1f86fa&");
/* harmony import */ var _Saas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Saas.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/Saas.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Saas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Saas_vue_vue_type_template_id_2c1f86fa___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Saas_vue_vue_type_template_id_2c1f86fa___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/Saas.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/Saas.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/views/dashboard/Saas.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Saas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Saas.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Saas.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Saas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/Saas.vue?vue&type=template&id=2c1f86fa&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/dashboard/Saas.vue?vue&type=template&id=2c1f86fa& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Saas_vue_vue_type_template_id_2c1f86fa___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Saas.vue?vue&type=template&id=2c1f86fa& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Saas.vue?vue&type=template&id=2c1f86fa&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Saas_vue_vue_type_template_id_2c1f86fa___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Saas_vue_vue_type_template_id_2c1f86fa___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);