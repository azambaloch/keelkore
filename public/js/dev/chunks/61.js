(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[61],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      colOne: ["Its is too amezing component try it !", "You can move these elements between these two containers", 'Moving them anywhere else isn"t quite possible', 'There"s also the possibility of moving elements around in the same container, changing their position'],
      colTwo: ["This is the use case. You only need to specify the containers you want to use", "More interactive use cases lie ahead", "Another message", "Move on upper"]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=template&id=3c223134&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=template&id=3c223134& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "", "py-0": "" } },
        [
          _c(
            "app-card",
            [
              _c(
                "v-row",
                [
                  _c("v-col", { attrs: { cols: "12", sm: "6" } }, [
                    _c("h3", { staticClass: "mb-4" }, [_vm._v("List 1")]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "dragula",
                            rawName: "v-dragula",
                            value: _vm.colOne,
                            expression: "colOne"
                          }
                        ],
                        staticClass: "vue2-dragula-container",
                        attrs: { drake: "first" }
                      },
                      _vm._l(_vm.colOne, function(text) {
                        return _c("div", { key: text }, [
                          _vm._v(_vm._s(text) + " [click me]")
                        ])
                      }),
                      0
                    )
                  ]),
                  _vm._v(" "),
                  _c("v-col", { attrs: { cols: "12", sm: "6" } }, [
                    _c("h3", { staticClass: "mb-4" }, [_vm._v("List 2")]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "dragula",
                            rawName: "v-dragula",
                            value: _vm.colTwo,
                            expression: "colTwo"
                          }
                        ],
                        staticClass: "vue2-dragula-container",
                        attrs: { drake: "first" }
                      },
                      _vm._l(_vm.colTwo, function(text) {
                        return _c("div", { key: text }, [
                          _vm._v(_vm._s(text) + " [click me]")
                        ])
                      }),
                      0
                    )
                  ])
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/drag-drop/Vue2Dragula.vue":
/*!******************************************************!*\
  !*** ./resources/js/views/drag-drop/Vue2Dragula.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Vue2Dragula_vue_vue_type_template_id_3c223134___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Vue2Dragula.vue?vue&type=template&id=3c223134& */ "./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=template&id=3c223134&");
/* harmony import */ var _Vue2Dragula_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Vue2Dragula.vue?vue&type=script&lang=js& */ "./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Vue2Dragula_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Vue2Dragula_vue_vue_type_template_id_3c223134___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Vue2Dragula_vue_vue_type_template_id_3c223134___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/drag-drop/Vue2Dragula.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Vue2Dragula_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Vue2Dragula.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Vue2Dragula_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=template&id=3c223134&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=template&id=3c223134& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Vue2Dragula_vue_vue_type_template_id_3c223134___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Vue2Dragula.vue?vue&type=template&id=3c223134& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/drag-drop/Vue2Dragula.vue?vue&type=template&id=3c223134&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Vue2Dragula_vue_vue_type_template_id_3c223134___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Vue2Dragula_vue_vue_type_template_id_3c223134___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);