(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[106],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/SlideGroups.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/SlideGroups.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      model: null,
      multiple: false,
      mandatory: false,
      showArrows: true,
      prevIcon: false,
      nextIcon: false,
      centerActive: false
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/SlideGroups.vue?vue&type=template&id=8abb6ad2&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/SlideGroups.vue?vue&type=template&id=8abb6ad2& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3" },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.slideGroups"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-30" }, [
                    _c("p", [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-slide-group")]),
                      _vm._v(
                        " component is used to display pseudo paginated information."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-sheet",
                    { staticClass: "mx-auto", attrs: { "max-width": "700" } },
                    [
                      _c(
                        "v-slide-group",
                        { attrs: { multiple: "", "show-arrows": "" } },
                        _vm._l(25, function(n) {
                          return _c("v-slide-item", {
                            key: n,
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var active = ref.active
                                    var toggle = ref.toggle
                                    return [
                                      _c(
                                        "v-btn",
                                        {
                                          staticClass: "mx-2",
                                          attrs: {
                                            "input-value": active,
                                            "active-class":
                                              "purple white--text",
                                            depressed: "",
                                            rounded: ""
                                          },
                                          on: { click: toggle }
                                        },
                                        [
                                          _vm._v(
                                            "\n                Options " +
                                              _vm._s(n) +
                                              "\n              "
                                          )
                                        ]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          })
                        }),
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-row",
                    { attrs: { justify: "space-around" } },
                    [
                      _c("v-switch", {
                        attrs: { label: "Multiple" },
                        model: {
                          value: _vm.multiple,
                          callback: function($$v) {
                            _vm.multiple = $$v
                          },
                          expression: "multiple"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Mandatory" },
                        model: {
                          value: _vm.mandatory,
                          callback: function($$v) {
                            _vm.mandatory = $$v
                          },
                          expression: "mandatory"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Show arrows" },
                        model: {
                          value: _vm.showArrows,
                          callback: function($$v) {
                            _vm.showArrows = $$v
                          },
                          expression: "showArrows"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Custom prev icon" },
                        model: {
                          value: _vm.prevIcon,
                          callback: function($$v) {
                            _vm.prevIcon = $$v
                          },
                          expression: "prevIcon"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Custom next icon" },
                        model: {
                          value: _vm.nextIcon,
                          callback: function($$v) {
                            _vm.nextIcon = $$v
                          },
                          expression: "nextIcon"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Center active item" },
                        model: {
                          value: _vm.centerActive,
                          callback: function($$v) {
                            _vm.centerActive = $$v
                          },
                          expression: "centerActive"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-sheet",
                    {
                      staticClass: "mx-auto",
                      attrs: { elevation: "8", "max-width": "800" }
                    },
                    [
                      _c(
                        "v-slide-group",
                        {
                          staticClass: "pa-4",
                          attrs: {
                            "prev-icon": _vm.prevIcon ? "mdi-minus" : undefined,
                            "next-icon": _vm.nextIcon ? "mdi-plus" : undefined,
                            multiple: _vm.multiple,
                            mandatory: _vm.mandatory,
                            "show-arrows": _vm.showArrows,
                            "center-active": _vm.centerActive
                          },
                          model: {
                            value: _vm.model,
                            callback: function($$v) {
                              _vm.model = $$v
                            },
                            expression: "model"
                          }
                        },
                        _vm._l(15, function(n) {
                          return _c("v-slide-item", {
                            key: n,
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var active = ref.active
                                    var toggle = ref.toggle
                                    return [
                                      _c(
                                        "v-card",
                                        {
                                          staticClass: "ma-4",
                                          attrs: {
                                            color: active
                                              ? "primary"
                                              : "grey lighten-1",
                                            height: "200",
                                            width: "100"
                                          },
                                          on: { click: toggle }
                                        },
                                        [
                                          _c(
                                            "v-row",
                                            {
                                              staticClass: "fill-height",
                                              attrs: {
                                                align: "center",
                                                justify: "center"
                                              }
                                            },
                                            [
                                              _c(
                                                "v-scale-transition",
                                                [
                                                  active
                                                    ? _c("v-icon", {
                                                        attrs: {
                                                          color: "white",
                                                          size: "48"
                                                        },
                                                        domProps: {
                                                          textContent: _vm._s(
                                                            "mdi-close-circle-outline"
                                                          )
                                                        }
                                                      })
                                                    : _vm._e()
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          })
                        }),
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/SlideGroups.vue":
/*!********************************************************!*\
  !*** ./resources/js/views/ui-elements/SlideGroups.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SlideGroups_vue_vue_type_template_id_8abb6ad2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SlideGroups.vue?vue&type=template&id=8abb6ad2& */ "./resources/js/views/ui-elements/SlideGroups.vue?vue&type=template&id=8abb6ad2&");
/* harmony import */ var _SlideGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SlideGroups.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/SlideGroups.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SlideGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SlideGroups_vue_vue_type_template_id_8abb6ad2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SlideGroups_vue_vue_type_template_id_8abb6ad2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/SlideGroups.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/SlideGroups.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/SlideGroups.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SlideGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SlideGroups.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/SlideGroups.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SlideGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/SlideGroups.vue?vue&type=template&id=8abb6ad2&":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/SlideGroups.vue?vue&type=template&id=8abb6ad2& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SlideGroups_vue_vue_type_template_id_8abb6ad2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SlideGroups.vue?vue&type=template&id=8abb6ad2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/SlideGroups.vue?vue&type=template&id=8abb6ad2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SlideGroups_vue_vue_type_template_id_8abb6ad2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SlideGroups_vue_vue_type_template_id_8abb6ad2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);