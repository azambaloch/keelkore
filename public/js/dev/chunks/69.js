(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[69],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/ProductDetail.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/ProductDetail.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Views_ecommerce_data_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Views/ecommerce/data.js */ "./resources/js/views/ecommerce/data.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      productsData: Views_ecommerce_data_js__WEBPACK_IMPORTED_MODULE_0__["productsData"],
      products: '',
      selectedProduct: '',
      colors: ["Red", "Blue", "Yellow", "Green"],
      sizes: ["28", "30", "32", "34", "36", "38", "40"]
    };
  },
  mounted: function mounted() {
    this.getProductData();
  },
  methods: {
    getProductData: function getProductData() {
      var allItems = this.productsData.men.concat(this.productsData.women, this.productsData.accessories, this.productsData.gadgets);

      for (var i = 0; i < allItems.length; i++) {
        if (allItems[i].id == this.$route.params.id) {
          this.selectedProduct = {
            availablity: allItems[i].availablity,
            brand: allItems[i].brand,
            category: allItems[i].category,
            category_type: allItems[i].category_type,
            color: allItems[i].color,
            discountPriceValue: allItems[i].discountPriceValue,
            description: allItems[i].description,
            discount_price: allItems[i].discount_price,
            features: allItems[i].features,
            id: allItems[i].id,
            image: allItems[i].image,
            image_gallery: allItems[i].image_gallery,
            name: allItems[i].name,
            popular: allItems[i].popular,
            price: allItems[i].price,
            product_code: allItems[i].product_code,
            quantity: allItems[i].quantity,
            rating: allItems[i].rating,
            status: allItems[i].status,
            tags: allItems[i].tags,
            type: allItems[i].type
          };
        }
      }
    },
    doHover: function doHover(img) {
      this.selectedProduct.image = img;
    },
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/ProductDetail.vue?vue&type=template&id=a39c537c&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/ProductDetail.vue?vue&type=template&id=a39c537c& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "v-add-products v-product-details" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c("page-title-bar"),
      _vm._v(" "),
      _vm.selectedProduct !== null
        ? [
            _c(
              "v-container",
              { attrs: { fluid: "" } },
              [
                _c(
                  "v-row",
                  [
                    _c(
                      "v-col",
                      {
                        staticClass: "mx-auto",
                        attrs: { cols: "12", sm: "10", md: "10" }
                      },
                      [
                        _c(
                          "v-row",
                          [
                            _c(
                              "v-col",
                              { attrs: { cols: "12", md: "6" } },
                              [
                                _c(
                                  "v-row",
                                  { staticClass: "product-images-wrap" },
                                  [
                                    _c(
                                      "v-col",
                                      { attrs: { cols: "2", md: "2" } },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "thumb-wrap ml-auto" },
                                          _vm._l(
                                            _vm.selectedProduct.image_gallery,
                                            function(img, i) {
                                              return _c("v-img", {
                                                key: i,
                                                staticClass: "mb-6",
                                                staticStyle: {
                                                  width: "40px",
                                                  height: "70px"
                                                },
                                                attrs: { src: img },
                                                on: {
                                                  mouseover: function($event) {
                                                    return _vm.doHover(img)
                                                  }
                                                }
                                              })
                                            }
                                          ),
                                          1
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-col",
                                      { attrs: { cols: "10", md: "10" } },
                                      [
                                        _c("v-img", {
                                          staticStyle: { width: "100%" },
                                          attrs: {
                                            src: _vm.selectedProduct.image
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-col",
                              {
                                staticClass: "content-wrap",
                                attrs: { cols: "12", sm: "12", md: "6" }
                              },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "my-4",
                                    attrs: {
                                      to:
                                        "/" +
                                        (_vm.getCurrentAppLayoutHandler() +
                                          "/ecommerce/shop")
                                    }
                                  },
                                  [_vm._v("Go to Products")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Name :")]
                                    ),
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedProduct.name)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Price :")]
                                    ),
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedProduct.price)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Availability :")]
                                    ),
                                    _vm._v(
                                      " " +
                                        _vm._s(_vm.selectedProduct.availablity)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Product Code :")]
                                    ),
                                    _vm._v(
                                      " " +
                                        _vm._s(_vm.selectedProduct.product_code)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Tags :")]
                                    ),
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedProduct.tags)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Description :")]
                                    ),
                                    _vm._v(
                                      " " +
                                        _vm._s(_vm.selectedProduct.description)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "p",
                                  { staticClass: "mb-3 fs-14 text-gray-light" },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "fw-semi-bold text-gray" },
                                      [_vm._v("Features :")]
                                    ),
                                    _vm._v(
                                      " " + _vm._s(_vm.selectedProduct.features)
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "ul",
                                  {
                                    staticClass:
                                      "list-unstyled d-flex flex-wrap"
                                  },
                                  _vm._l(_vm.colors, function(color, i) {
                                    return _c(
                                      "li",
                                      { key: i, staticClass: "mr-3" },
                                      [
                                        _c("v-checkbox", {
                                          attrs: { label: color }
                                        })
                                      ],
                                      1
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _c(
                                  "ul",
                                  {
                                    staticClass:
                                      "list-unstyled d-flex flex-wrap"
                                  },
                                  _vm._l(_vm.sizes, function(size, i) {
                                    return _c(
                                      "li",
                                      { key: i, staticClass: "mr-3" },
                                      [
                                        _c("v-checkbox", {
                                          attrs: { label: size }
                                        })
                                      ],
                                      1
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  staticClass: "mb-4",
                                  attrs: {
                                    value: "5",
                                    type: "number",
                                    label: "Total Products"
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    staticClass: "mr-3",
                                    attrs: { color: "success" }
                                  },
                                  [_vm._v("Buy")]
                                ),
                                _vm._v(" "),
                                _c("v-btn", { attrs: { color: "error" } }, [
                                  _vm._v("Wishlist")
                                ])
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ecommerce/ProductDetail.vue":
/*!********************************************************!*\
  !*** ./resources/js/views/ecommerce/ProductDetail.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductDetail_vue_vue_type_template_id_a39c537c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductDetail.vue?vue&type=template&id=a39c537c& */ "./resources/js/views/ecommerce/ProductDetail.vue?vue&type=template&id=a39c537c&");
/* harmony import */ var _ProductDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductDetail.vue?vue&type=script&lang=js& */ "./resources/js/views/ecommerce/ProductDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductDetail_vue_vue_type_template_id_a39c537c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductDetail_vue_vue_type_template_id_a39c537c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ecommerce/ProductDetail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ecommerce/ProductDetail.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/ecommerce/ProductDetail.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductDetail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/ProductDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ecommerce/ProductDetail.vue?vue&type=template&id=a39c537c&":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/ecommerce/ProductDetail.vue?vue&type=template&id=a39c537c& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetail_vue_vue_type_template_id_a39c537c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductDetail.vue?vue&type=template&id=a39c537c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/ProductDetail.vue?vue&type=template&id=a39c537c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetail_vue_vue_type_template_id_a39c537c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetail_vue_vue_type_template_id_a39c537c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);