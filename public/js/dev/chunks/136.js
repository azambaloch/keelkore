(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[136],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DailySales.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/DailySales.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_SalesChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/SalesChart */ "./resources/js/components/Charts/SalesChart.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // constants


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["label", "chartdata", "labels"],
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"]
    };
  },
  components: {
    SalesChart: _Charts_SalesChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProductSales.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProductSales.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_ProductSalesChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/ProductSalesChart */ "./resources/js/components/Charts/ProductSalesChart.js");
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["height"],
  components: {
    ProductSalesChart: _Charts_ProductSalesChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TrafficChannel.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TrafficChannel.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_HorizontalBarChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/HorizontalBarChart */ "./resources/js/components/Charts/HorizontalBarChart.js");
//
//
//
//
//
//
//
// chart

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["data"],
  components: {
    HorizontalBarChart: _Charts_HorizontalBarChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_SalesAnalysisChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/SalesAnalysisChart */ "./resources/js/components/Charts/SalesAnalysisChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SalesAnalysis: _Charts_SalesAnalysisChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_WeeklySummary__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/WeeklySummary */ "./resources/js/components/Charts/WeeklySummary.js");
/* harmony import */ var Components_Charts_TotalEarnings__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Charts/TotalEarnings */ "./resources/js/components/Charts/TotalEarnings.js");
/* harmony import */ var Components_Widgets_VisitAndSalesStatistics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Widgets/VisitAndSalesStatistics */ "./resources/js/components/Widgets/VisitAndSalesStatistics.vue");
/* harmony import */ var Components_Widgets_ProductSales__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/ProductSales */ "./resources/js/components/Widgets/ProductSales.vue");
/* harmony import */ var Components_Widgets_DailySales__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/DailySales */ "./resources/js/components/Widgets/DailySales.vue");
/* harmony import */ var Components_Widgets_TrafficChannel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/TrafficChannel */ "./resources/js/components/Widgets/TrafficChannel.vue");
/* harmony import */ var Components_Widgets_CampaignPerformance_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/CampaignPerformance.vue */ "./resources/js/components/Widgets/CampaignPerformance.vue");
/* harmony import */ var vue_radial_progress__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-radial-progress */ "./node_modules/vue-radial-progress/src/main.js");
/* harmony import */ var vue_radial_progress__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(vue_radial_progress__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var _data_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../data.js */ "./resources/js/views/widgets/data.js");
/* harmony import */ var Views_dashboard_data__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Views/dashboard/data */ "./resources/js/views/dashboard/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// charts component

 // widgets






 //chart config

 // data



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VisitAndSalesStatistics: Components_Widgets_VisitAndSalesStatistics__WEBPACK_IMPORTED_MODULE_2__["default"],
    RadialProgressBar: vue_radial_progress__WEBPACK_IMPORTED_MODULE_7___default.a,
    WeeklySummary: Components_Charts_WeeklySummary__WEBPACK_IMPORTED_MODULE_0__["default"],
    TotalEarnings: Components_Charts_TotalEarnings__WEBPACK_IMPORTED_MODULE_1__["default"],
    ProductSales: Components_Widgets_ProductSales__WEBPACK_IMPORTED_MODULE_3__["default"],
    DailySales: Components_Widgets_DailySales__WEBPACK_IMPORTED_MODULE_4__["default"],
    TrafficChannel: Components_Widgets_TrafficChannel__WEBPACK_IMPORTED_MODULE_5__["default"],
    CampaignPerformance: Components_Widgets_CampaignPerformance_vue__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    return {
      labels: ["A", "B", "C", "D", "E", "F", "J", "K", "L", "M", "N", "P"],
      totalEarnings: [30, 50, 25, 55, 44, 60, 30, 20, 40, 20, 40, 44],
      onlineRevenue: [30, 50, 25, 55, 44, 60, 30, 20, 40, 20, 40, 44],
      offlineRevenue: [30, 50, 25, 55, 44, 60, 30, 20, 40, 20, 40, 44],
      marketingExpenses: [30, 50, 25, 55, 10, 60, 30, 20, 40, 20, 70, 40],
      newClients: [30, 20, 14, 22, 20, 15, 10, 30, 22, 20, 40, 42],
      dailySales: _data_js__WEBPACK_IMPORTED_MODULE_9__["dailySales"],
      trafficChannel: _data_js__WEBPACK_IMPORTED_MODULE_9__["trafficChannel"],
      devicesShare: Views_dashboard_data__WEBPACK_IMPORTED_MODULE_10__["devicesShare"],
      newsLetterCampaignData: Views_dashboard_data__WEBPACK_IMPORTED_MODULE_10__["newsLetterCampaignData"],
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_8__["ChartConfig"]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DailySales.vue?vue&type=template&id=a997b3c4&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/DailySales.vue?vue&type=template&id=a997b3c4& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "app-flex justify-start align-start" },
        [
          _c("v-icon", { staticClass: "success--text mr-3 font-2x" }, [
            _vm._v("arrow_upward")
          ]),
          _vm._v(" "),
          _vm._m(0)
        ],
        1
      ),
      _vm._v(" "),
      _c("SalesChart", {
        attrs: {
          label: _vm.label,
          chartdata: _vm.chartdata,
          labels: _vm.labels,
          borderColor: _vm.ChartConfig.color.success,
          pointBackgroundColor: _vm.ChartConfig.color.success,
          height: 160,
          pointBorderColor: _vm.ChartConfig.color.white,
          borderWidth: 4
        }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h2", { staticClass: "mb-0" }, [_vm._v("12,255 Today")]),
      _vm._v(" "),
      _c("span", { staticClass: "fs-14 gray--text" }, [
        _vm._v("10% increase from yesterday")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProductSales.vue?vue&type=template&id=324f2f48&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProductSales.vue?vue&type=template&id=324f2f48& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "widget" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("product-sales-chart", { attrs: { height: _vm.height } })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "ladgend-wrapper mb-4" }, [
      _c("p", { staticClass: "mb-0" }, [
        _c("span", { staticClass: "ladgend ladgend-success" }),
        _vm._v(" "),
        _c("span", [_vm._v("iPhone 8")])
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "mb-0" }, [
        _c("span", { staticClass: "ladgend ladgend-warning" }),
        _vm._v(" "),
        _c("span", [_vm._v("Samsung X8")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TrafficChannel.vue?vue&type=template&id=1feb4cf1&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TrafficChannel.vue?vue&type=template&id=1feb4cf1& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("horizontal-bar-chart", {
        attrs: {
          label: _vm.data.label,
          chartdata: _vm.data.chartdata,
          labels: _vm.data.labels,
          height: "200"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "widget" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("sales-analysis", { attrs: { width: 370, height: 300 } })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "ladgend-wrapper mb-4" }, [
      _c("p", { staticClass: "mb-0" }, [
        _c("span", { staticClass: "ladgend ladgend-success" }),
        _vm._v(" "),
        _c("span", [_vm._v("Sales")])
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "mb-0" }, [
        _c("span", { staticClass: "ladgend ladgend-warning" }),
        _vm._v(" "),
        _c("span", [_vm._v("Visits")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=template&id=47cfd79f&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=template&id=47cfd79f& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        {
          attrs: {
            fluid: "",
            "grid-list-xl": "",
            "pt-0": "",
            "pb-0": "",
            "mt-n3": ""
          }
        },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.dailySales"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    footer: true
                  }
                },
                [
                  _c("daily-sales", {
                    attrs: {
                      label: _vm.dailySales.label,
                      chartdata: _vm.dailySales.chartdata,
                      labels: _vm.dailySales.labels
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "justify-space-between footer-flex",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "span",
                        { staticClass: "fs-14" },
                        [
                          _c("v-icon", { staticClass: "mr-1 font-md" }, [
                            _vm._v("autorenew")
                          ]),
                          _vm._v(
                            _vm._s(_vm.$t("message.updated10MinAgo")) +
                              "\n\t\t\t\t\t"
                          )
                        ],
                        1
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.trafficChannel"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    footer: true
                  }
                },
                [
                  _c("traffic-channel", {
                    attrs: { data: _vm.trafficChannel }
                  }),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "justify-space-between footer-flex",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "span",
                        { staticClass: "fs-14" },
                        [
                          _c("v-icon", { staticClass: "mr-1 font-md" }, [
                            _vm._v("autorenew")
                          ]),
                          _vm._v(
                            _vm._s(_vm.$t("message.updated10MinAgo")) +
                              "\n\t\t\t\t\t"
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c("v-btn", { attrs: { color: "primary", small: "" } }, [
                        _vm._v(_vm._s(_vm.$t("message.goToCampaign")))
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.campaignPerformance"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    fullBlock: true,
                    customClasses: "campaign-performance"
                  }
                },
                [_c("campaign-performance")],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl py-0", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-12 col-sm-12",
                    heading: _vm.$t("message.visitAndSalesStatistics"),
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("visit-and-sales-statistics")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.totalEarnings"),
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-12 col-sm-12",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [
                  _c("div", { staticClass: "ladgend-wrapper mb-4" }, [
                    _c("p", { staticClass: "mb-0" }, [
                      _c("span", { staticClass: "ladgend ladgend-success" }),
                      _vm._v(" "),
                      _c("span", [_vm._v("Open Rate")])
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "mb-0" }, [
                      _c("span", { staticClass: "ladgend ladgend-pink" }),
                      _vm._v(" "),
                      _c("span", [_vm._v("Recurring Payments")])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("total-earnings", { attrs: { width: 300, height: 300 } })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "8",
                    lg: "8",
                    md: "8",
                    sm: "12",
                    cols: "12",
                    "b-100": ""
                  }
                },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "dash-card-wrap d-flex flex-wrap overflow-hidden border-rad-sm"
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "dash-card-light" },
                        [
                          _c("p", [
                            _vm._v(_vm._s(_vm.$t("message.averageSteps")))
                          ]),
                          _vm._v(" "),
                          _c("h2", { staticClass: "primary--text" }, [
                            _vm._v("3261")
                          ]),
                          _vm._v(" "),
                          _c(
                            "radial-progress-bar",
                            {
                              staticClass: "mx-auto my-6",
                              attrs: {
                                diameter: 135,
                                "completed-steps": 60,
                                "total-steps": 100,
                                startColor: "#3B7CFF",
                                stopColor: "#1E3C72",
                                strokeWidth: 6,
                                innerStrokeColor: "#EAEAEA"
                              }
                            },
                            [
                              _c("h2", [
                                _c("i", { staticClass: "ti-arrow-up" })
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c("h2", { staticClass: "primary--text" }, [
                            _vm._v("5000")
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(_vm._s(_vm.$t("message.todaysStep")))
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "dash-card-light" },
                        [
                          _c("p", [
                            _vm._v(_vm._s(_vm.$t("message.todaysDistance")))
                          ]),
                          _vm._v(" "),
                          _c("h2", { staticClass: "warning--text" }, [
                            _vm._v("7.6 Km")
                          ]),
                          _vm._v(" "),
                          _c(
                            "radial-progress-bar",
                            {
                              staticClass: "mx-auto my-6",
                              attrs: {
                                diameter: 135,
                                "completed-steps": 40,
                                "total-steps": 100,
                                startColor: "#FD7266",
                                stopColor: "#F7981C",
                                strokeWidth: 6,
                                innerStrokeColor: "#EAEAEA"
                              }
                            },
                            [
                              _c("h2", [
                                _c("i", { staticClass: "ti-arrow-down" })
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c("h2", { staticClass: "warning--text" }, [
                            _vm._v("10 Km")
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(_vm._s(_vm.$t("message.todaysDistance")))
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "dash-card-light" },
                        [
                          _c("p", [_vm._v(_vm._s(_vm.$t("message.calories")))]),
                          _vm._v(" "),
                          _c("h2", { staticClass: "info--text" }, [
                            _vm._v("350")
                          ]),
                          _vm._v(" "),
                          _c(
                            "radial-progress-bar",
                            {
                              staticClass: "mx-auto my-6",
                              attrs: {
                                diameter: 135,
                                "completed-steps": 90,
                                "total-steps": 100,
                                startColor: "#007ADF",
                                stopColor: "#00ecbc",
                                strokeWidth: 6,
                                innerStrokeColor: "#EAEAEA"
                              }
                            },
                            [
                              _c("h2", [
                                _c("i", { staticClass: "ti-arrow-up" })
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c("h2", { staticClass: "info--text" }, [
                            _vm._v("550")
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(_vm._s(_vm.$t("message.todaysGoal")))
                          ])
                        ],
                        1
                      )
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.weeklySummary"),
                    colClasses:
                      "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 b-100",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("weekly-summary", { attrs: { height: 245 } })],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses:
                      "col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 d-xs-full b-100",
                    heading: _vm.$t("message.productSales"),
                    customClasses: "mb-0",
                    contentCustomClass: "overflow-hidden",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("product-sales", { attrs: { height: 150 } })],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/HorizontalBarChart.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/Charts/HorizontalBarChart.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/**
 * Horizontal Bar Chart
 */


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["HorizontalBar"],
  props: ['labels', 'label', 'chartdata', 'height'],
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            gridLines: {
              color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].chartGridColor,
              drawBorder: false
            },
            ticks: {
              fontColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].axesColor,
              min: 0,
              max: 9
            }
          }],
          yAxes: [{
            gridLines: {
              display: false
            },
            ticks: {
              fontColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].axesColor
            }
          }]
        }
      }
    };
  },
  mounted: function mounted() {
    var labels = this.labels,
        label = this.label,
        chartdata = this.chartdata;
    this.renderChart({
      labels: labels,
      datasets: [{
        label: label,
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.success,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.success,
        borderWidth: 1,
        barPercentage: 1.0,
        categoryPercentage: 0.5,
        hoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.success,
        hoverBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.success,
        data: chartdata
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/ProductSalesChart.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/Charts/ProductSalesChart.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  data: function data() {
    return {
      gradient1: null,
      gradient2: null,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false
            },
            ticks: {
              stepSize: 50
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        tooltip: {
          enabled: true
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: true
      }
    };
  },
  mounted: function mounted() {
    this.gradient1 = this.$refs.canvas.getContext("2d").createLinearGradient(0, 0, 0, 400);
    this.gradient2 = this.$refs.canvas.getContext("2d").createLinearGradient(0, 0, 0, 700);
    this.gradient1.addColorStop(0, "rgba(59, 124, 255, 0.8)");
    this.gradient1.addColorStop(1, "#1E3C72");
    this.gradient2.addColorStop(0, "rgba(247, 152, 28, 0.8)");
    this.gradient2.addColorStop(1, "#Fc6373");
    this.renderChart({
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
      datasets: [{
        label: "Data",
        lineTension: 0,
        pointRadius: 0,
        fill: true,
        backgroundColor: this.gradient1,
        borderWidth: 0,
        data: [80, 80, 160, 130, 50, 140, 100]
      }, {
        label: "Data 2",
        lineTension: 0,
        pointRadius: 0,
        fill: true,
        backgroundColor: this.gradient2,
        borderWidth: 0,
        data: [90, 140, 50, 120, 160, 90, 90]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/SalesAnalysisChart.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/Charts/SalesAnalysisChart.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
// Sales Analysis Chart

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  data: function data() {
    return {
      gradient1: null,
      gradient2: null,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false
            },
            ticks: {
              stepSize: 500
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltip: {
          enabled: true
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    var ctx = this.$refs.canvas.getContext('2d');
    var _stroke = ctx.stroke;

    ctx.stroke = function () {
      ctx.save();
      ctx.shadowColor = 'rgba(0,0,0,0.5)';
      ctx.shadowBlur = 10;
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 15;

      _stroke.apply(this, arguments);

      ctx.restore();
    };

    this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 800, 0);
    this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 1000, 0);
    this.gradient1.addColorStop(0, '#1E3C72');
    this.gradient1.addColorStop(1, '#3B7CFF');
    this.gradient2.addColorStop(0, '#F7981C');
    this.gradient2.addColorStop(1, '#F56074');
    this.renderChart({
      labels: ['January 2014', 'January 2015', 'January 2016', 'January 2017'],
      datasets: [{
        label: 'Sales',
        lineTension: 0.5,
        data: [0, 1000, 1250, 1000],
        borderColor: this.gradient1,
        borderWidth: 5,
        pointRadius: 0,
        fill: false
      }, {
        label: 'Visits',
        lineTension: 0.5,
        data: [750, 900, 450, 500],
        borderColor: this.gradient2,
        borderWidth: 5,
        pointBackgroundColor: this.gradient2,
        pointBorderColor: this.gradient2,
        pointHoverBackgroundColor: this.gradient2,
        pointHoverBorderColor: this.gradient2,
        pointBorderWidth: 5,
        pointHoverRadius: 5,
        pointHoverBorderWidth: 1,
        pointRadius: 0,
        fill: false
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/SalesChart.js":
/*!******************************************************!*\
  !*** ./resources/js/components/Charts/SalesChart.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/**
 * Sales Chart Component
 */
 // Main Component

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: ['label', 'chartdata', 'labels', 'borderColor', 'pointBackgroundColor', 'height', 'pointBorderColor', 'borderWidth'],
  data: function data() {
    return {
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true,
            ticks: {
              min: 0
            },
            gridLines: {
              display: true,
              drawBorder: false
            }
          }],
          yAxes: [{
            display: false,
            ticks: {
              suggestedMin: 0,
              beginAtZero: true
            }
          }]
        }
      }
    };
  },
  mounted: function mounted() {
    var label = this.label,
        chartdata = this.chartdata,
        labels = this.labels,
        borderColor = this.borderColor,
        borderWidth = this.borderWidth,
        pointBorderColor = this.pointBorderColor,
        pointBackgroundColor = this.pointBackgroundColor;
    this.renderChart({
      labels: labels,
      datasets: [{
        label: label,
        fill: false,
        lineTension: 0,
        fillOpacity: 0.3,
        borderColor: borderColor,
        borderWidth: borderWidth,
        pointBorderColor: pointBorderColor,
        pointBackgroundColor: pointBackgroundColor,
        pointBorderWidth: 3,
        pointRadius: 6,
        pointHoverBackgroundColor: pointBackgroundColor,
        pointHoverBorderColor: borderColor,
        pointHoverBorderWidth: 4,
        pointHoverRadius: 7,
        data: chartdata
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/TotalEarnings.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/Charts/TotalEarnings.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  data: function data() {
    return {
      gradient1: null,
      gradient2: null,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false
            },
            ticks: {
              stepSize: 500
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    var ctx = this.$refs.canvas.getContext('2d');
    var _stroke = ctx.stroke;

    ctx.stroke = function () {
      ctx.save();
      ctx.shadowColor = 'rgba(0,0,0,0.6)';
      ctx.shadowBlur = 22;
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 15;

      _stroke.apply(this, arguments);

      ctx.restore();
    };

    this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(1000, 0, 100, 0);
    this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(1000, 0, 100, 0);
    this.gradient1.addColorStop(0, '#1E3C72');
    this.gradient1.addColorStop(1, '#3B7CFF');
    this.gradient2.addColorStop(0, '#F7981C');
    this.gradient2.addColorStop(1, '#F56074');
    this.renderChart({
      labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      datasets: [{
        label: 'Open Rate',
        lineTension: 0.4,
        borderColor: this.gradient2,
        pointBorderColor: this.gradient2,
        pointBorderWidth: 2,
        pointRadius: 7,
        fill: false,
        pointBackgroundColor: '#FFF',
        borderWidth: 3,
        data: [2200, 2100, 1950, 1600, 800, 350, 250, 650, 1200, 1900, 2900, 3100]
      }, {
        label: 'Recurring Payments',
        lineTension: 0.4,
        borderColor: this.gradient1,
        pointBorderColor: this.gradient1,
        pointBorderWidth: 2,
        pointRadius: 7,
        fill: false,
        fillColor: 'Black',
        pointBackgroundColor: '#FFF',
        borderWidth: 3,
        data: [500, 1100, 1600, 2000, 2500, 2600, 2300, 1900, 1500, 1100, 750, 850]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/WeeklySummary.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/Charts/WeeklySummary.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
// Weekly Summary Widget

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  data: function data() {
    return {
      gradient1: null,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        maxBarThickness: 0,
        scales: {
          yAxes: [{
            ticks: {
              stepSize: 5
            },
            gridLines: {
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        },
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 250);
    this.gradient1.addColorStop(0, '#3B7CFF');
    this.gradient1.addColorStop(1, '#1E3C72');
    this.renderChart({
      labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      datasets: [{
        label: 'Series A',
        barPercentage: 0.4,
        backgroundColor: this.gradient1,
        hoverBackgroundColor: this.gradient1,
        data: [12, 7, 14, 9, 14, 9, 17]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/DailySales.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/DailySales.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DailySales_vue_vue_type_template_id_a997b3c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DailySales.vue?vue&type=template&id=a997b3c4& */ "./resources/js/components/Widgets/DailySales.vue?vue&type=template&id=a997b3c4&");
/* harmony import */ var _DailySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DailySales.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/DailySales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DailySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DailySales_vue_vue_type_template_id_a997b3c4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DailySales_vue_vue_type_template_id_a997b3c4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/DailySales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/DailySales.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/DailySales.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DailySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DailySales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DailySales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DailySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/DailySales.vue?vue&type=template&id=a997b3c4&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/DailySales.vue?vue&type=template&id=a997b3c4& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DailySales_vue_vue_type_template_id_a997b3c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DailySales.vue?vue&type=template&id=a997b3c4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DailySales.vue?vue&type=template&id=a997b3c4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DailySales_vue_vue_type_template_id_a997b3c4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DailySales_vue_vue_type_template_id_a997b3c4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ProductSales.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/ProductSales.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductSales_vue_vue_type_template_id_324f2f48___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductSales.vue?vue&type=template&id=324f2f48& */ "./resources/js/components/Widgets/ProductSales.vue?vue&type=template&id=324f2f48&");
/* harmony import */ var _ProductSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductSales.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProductSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductSales_vue_vue_type_template_id_324f2f48___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductSales_vue_vue_type_template_id_324f2f48___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProductSales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProductSales.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProductSales.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductSales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProductSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProductSales.vue?vue&type=template&id=324f2f48&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProductSales.vue?vue&type=template&id=324f2f48& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductSales_vue_vue_type_template_id_324f2f48___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductSales.vue?vue&type=template&id=324f2f48& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProductSales.vue?vue&type=template&id=324f2f48&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductSales_vue_vue_type_template_id_324f2f48___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductSales_vue_vue_type_template_id_324f2f48___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TrafficChannel.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/TrafficChannel.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TrafficChannel_vue_vue_type_template_id_1feb4cf1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TrafficChannel.vue?vue&type=template&id=1feb4cf1& */ "./resources/js/components/Widgets/TrafficChannel.vue?vue&type=template&id=1feb4cf1&");
/* harmony import */ var _TrafficChannel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TrafficChannel.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TrafficChannel.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TrafficChannel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TrafficChannel_vue_vue_type_template_id_1feb4cf1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TrafficChannel_vue_vue_type_template_id_1feb4cf1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TrafficChannel.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TrafficChannel.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TrafficChannel.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TrafficChannel.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TrafficChannel.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TrafficChannel.vue?vue&type=template&id=1feb4cf1&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TrafficChannel.vue?vue&type=template&id=1feb4cf1& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannel_vue_vue_type_template_id_1feb4cf1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TrafficChannel.vue?vue&type=template&id=1feb4cf1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TrafficChannel.vue?vue&type=template&id=1feb4cf1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannel_vue_vue_type_template_id_1feb4cf1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannel_vue_vue_type_template_id_1feb4cf1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/VisitAndSalesStatistics.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/components/Widgets/VisitAndSalesStatistics.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VisitAndSalesStatistics_vue_vue_type_template_id_825e72f0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0& */ "./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0&");
/* harmony import */ var _VisitAndSalesStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VisitAndSalesStatistics.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VisitAndSalesStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VisitAndSalesStatistics_vue_vue_type_template_id_825e72f0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VisitAndSalesStatistics_vue_vue_type_template_id_825e72f0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/VisitAndSalesStatistics.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitAndSalesStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./VisitAndSalesStatistics.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitAndSalesStatistics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitAndSalesStatistics_vue_vue_type_template_id_825e72f0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitAndSalesStatistics.vue?vue&type=template&id=825e72f0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitAndSalesStatistics_vue_vue_type_template_id_825e72f0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitAndSalesStatistics_vue_vue_type_template_id_825e72f0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/widgets/chart-widgets/ChartWidgets.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/views/widgets/chart-widgets/ChartWidgets.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ChartWidgets_vue_vue_type_template_id_47cfd79f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ChartWidgets.vue?vue&type=template&id=47cfd79f& */ "./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=template&id=47cfd79f&");
/* harmony import */ var _ChartWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChartWidgets.vue?vue&type=script&lang=js& */ "./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ChartWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ChartWidgets_vue_vue_type_template_id_47cfd79f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ChartWidgets_vue_vue_type_template_id_47cfd79f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/widgets/chart-widgets/ChartWidgets.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChartWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ChartWidgets.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChartWidgets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=template&id=47cfd79f&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=template&id=47cfd79f& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChartWidgets_vue_vue_type_template_id_47cfd79f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ChartWidgets.vue?vue&type=template&id=47cfd79f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/widgets/chart-widgets/ChartWidgets.vue?vue&type=template&id=47cfd79f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChartWidgets_vue_vue_type_template_id_47cfd79f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChartWidgets_vue_vue_type_template_id_47cfd79f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/widgets/data.js":
/*!********************************************!*\
  !*** ./resources/js/views/widgets/data.js ***!
  \********************************************/
/*! exports provided: dailySales, trafficChannel, spaceUsed, activeUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailySales", function() { return dailySales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "trafficChannel", function() { return trafficChannel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "spaceUsed", function() { return spaceUsed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "activeUser", function() { return activeUser; });
/* harmony import */ var _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../constants/chart-config */ "./resources/js/constants/chart-config.js");
 // Daily Sales

var dailySales = {
  label: 'Daily Sales',
  chartdata: [100, 200, 125, 250, 200, 150, 200],
  labels: ['9', '10', '11', '12', '13', '14', '15']
}; //Traffic Channel

var trafficChannel = {
  label: 'Direct User',
  labels: ['Direct User', 'Referral', 'Facebook', 'Google', 'Instagram'],
  chartdata: [8.5, 6.75, 5.5, 7, 4.75]
}; // Space Used

var spaceUsed = {
  chartData: {
    labels: ['Space Used', 'Space Left'],
    datasets: [{
      data: [275, 100],
      backgroundColor: [_constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger, _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning],
      hoverBackgroundColor: [_constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger, _constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
    }]
  }
}; // Active User

var activeUser = [{
  id: 1,
  flag: 'icons8-usa',
  countryName: 'United States',
  userCount: 150,
  userPercent: 20,
  status: 1
}, {
  id: 2,
  flag: 'icons8-hungary',
  countryName: 'Hungary',
  userCount: 180,
  userPercent: -5,
  status: 0
}, {
  id: 3,
  flag: 'icons8-france',
  countryName: 'France',
  userCount: 86,
  userPercent: 20,
  status: 1
}, {
  id: 4,
  flag: 'icons8-japan',
  countryName: 'Japan',
  userCount: 243,
  userPercent: 20,
  status: 1
}, {
  id: 5,
  flag: 'icons8-china',
  countryName: 'China',
  userCount: 155,
  userPercent: 20,
  status: 0
}, {
  id: 6,
  flag: 'ru',
  countryName: 'Russia',
  userCount: 155,
  userPercent: 20,
  status: 0
}];

/***/ })

}]);