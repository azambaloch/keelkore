(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[99],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      items: [{
        icon: 'mdi-inbox',
        text: 'Inbox'
      }, {
        icon: 'mdi-star',
        text: 'Star'
      }, {
        icon: 'mdi-send',
        text: 'Send'
      }, {
        icon: 'mdi-email-open',
        text: 'Drafts'
      }],
      model: 1,
      item: {
        icon: 'mdi-wifi',
        text: 'Wifi'
      },
      multiple: false,
      mandatory: false,
      flat: false,
      dense: false,
      count: 4
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=template&id=50b08f7a&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=template&id=50b08f7a& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.listItemGroups"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-list",
                    [
                      _c(
                        "v-list-item-group",
                        {
                          model: {
                            value: _vm.model,
                            callback: function($$v) {
                              _vm.model = $$v
                            },
                            expression: "model"
                          }
                        },
                        _vm._l(_vm.items, function(item, i) {
                          return _c(
                            "v-list-item",
                            { key: i },
                            [
                              _c(
                                "v-list-item-icon",
                                [
                                  _c("v-icon", {
                                    domProps: { textContent: _vm._s(item.icon) }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-list-item-content",
                                [
                                  _c("v-list-item-title", {
                                    domProps: { textContent: _vm._s(item.text) }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        }),
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-row",
                    { attrs: { justify: "space-around" } },
                    [
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Multiple" },
                        model: {
                          value: _vm.multiple,
                          callback: function($$v) {
                            _vm.multiple = $$v
                          },
                          expression: "multiple"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Mandatory" },
                        model: {
                          value: _vm.mandatory,
                          callback: function($$v) {
                            _vm.mandatory = $$v
                          },
                          expression: "mandatory"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Flat" },
                        model: {
                          value: _vm.flat,
                          callback: function($$v) {
                            _vm.flat = $$v
                          },
                          expression: "flat"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Dense" },
                        model: {
                          value: _vm.dense,
                          callback: function($$v) {
                            _vm.dense = $$v
                          },
                          expression: "dense"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c("v-slider", {
                            attrs: {
                              min: "0",
                              max: "25",
                              label: "Items count"
                            },
                            model: {
                              value: _vm.count,
                              callback: function($$v) {
                                _vm.count = $$v
                              },
                              expression: "count"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card",
                    { staticClass: "mx-auto", attrs: { "max-width": "400" } },
                    [
                      _c(
                        "v-list",
                        { attrs: { flat: _vm.flat, dense: _vm.dense } },
                        [
                          _c(
                            "v-list-item-group",
                            {
                              attrs: {
                                multiple: _vm.multiple,
                                mandatory: _vm.mandatory,
                                color: "indigo"
                              },
                              model: {
                                value: _vm.model,
                                callback: function($$v) {
                                  _vm.model = $$v
                                },
                                expression: "model"
                              }
                            },
                            _vm._l(_vm.count, function(i) {
                              return _c(
                                "v-list-item",
                                { key: i },
                                [
                                  _c(
                                    "v-list-item-icon",
                                    [
                                      _c("v-icon", {
                                        domProps: {
                                          textContent: _vm._s(_vm.item.icon)
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item-content",
                                    [
                                      _c("v-list-item-title", {
                                        domProps: {
                                          textContent: _vm._s(_vm.item.text)
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            }),
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/ListItemGroups.vue":
/*!***********************************************************!*\
  !*** ./resources/js/views/ui-elements/ListItemGroups.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListItemGroups_vue_vue_type_template_id_50b08f7a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListItemGroups.vue?vue&type=template&id=50b08f7a& */ "./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=template&id=50b08f7a&");
/* harmony import */ var _ListItemGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListItemGroups.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListItemGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListItemGroups_vue_vue_type_template_id_50b08f7a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListItemGroups_vue_vue_type_template_id_50b08f7a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/ListItemGroups.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListItemGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ListItemGroups.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListItemGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=template&id=50b08f7a&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=template&id=50b08f7a& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListItemGroups_vue_vue_type_template_id_50b08f7a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ListItemGroups.vue?vue&type=template&id=50b08f7a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ListItemGroups.vue?vue&type=template&id=50b08f7a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListItemGroups_vue_vue_type_template_id_50b08f7a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListItemGroups_vue_vue_type_template_id_50b08f7a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);