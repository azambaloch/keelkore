(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[107],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Slider.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Slider.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      value1: 0,
      value2: 0,
      value3: 0,
      media: 0,
      alarm: 0,
      red: 64,
      green: 128,
      blue: 0,
      ex1: {
        label: "color",
        val: 25,
        color: "orange darken-3"
      },
      ex2: {
        label: "track-color",
        val: 75,
        color: "green lighten-1"
      },
      ex3: {
        label: "thumb-color",
        val: 50,
        color: "red"
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Slider.vue?vue&type=template&id=42b1a31f&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Slider.vue?vue&type=template&id=42b1a31f& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.continuous"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-slider", {
                    attrs: { "hide-details": "", step: "0" },
                    model: {
                      value: _vm.value1,
                      callback: function($$v) {
                        _vm.value1 = $$v
                      },
                      expression: "value1"
                    }
                  }),
                  _vm._v(" "),
                  _c("v-slider", {
                    attrs: { "hide-details": "", step: "0", disabled: "" },
                    model: {
                      value: _vm.value2,
                      callback: function($$v) {
                        _vm.value2 = $$v
                      },
                      expression: "value2"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.discrete"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-slider", {
                    attrs: {
                      "hide-details": "",
                      "thumb-label": "",
                      step: "10",
                      ticks: ""
                    },
                    model: {
                      value: _vm.value3,
                      callback: function($$v) {
                        _vm.value3 = $$v
                      },
                      expression: "value3"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.customColors"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-slider", {
                    attrs: { label: _vm.ex1.label, color: _vm.ex1.color },
                    model: {
                      value: _vm.ex1.val,
                      callback: function($$v) {
                        _vm.$set(_vm.ex1, "val", $$v)
                      },
                      expression: "ex1.val"
                    }
                  }),
                  _vm._v(" "),
                  _c("v-slider", {
                    attrs: {
                      label: _vm.ex2.label,
                      "track-color": _vm.ex2.color
                    },
                    model: {
                      value: _vm.ex2.val,
                      callback: function($$v) {
                        _vm.$set(_vm.ex2, "val", $$v)
                      },
                      expression: "ex2.val"
                    }
                  }),
                  _vm._v(" "),
                  _c("v-slider", {
                    attrs: {
                      label: _vm.ex3.label,
                      "thumb-color": _vm.ex3.color,
                      "thumb-label": ""
                    },
                    model: {
                      value: _vm.ex3.val,
                      callback: function($$v) {
                        _vm.$set(_vm.ex3, "val", $$v)
                      },
                      expression: "ex3.val"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.editableNumericValue"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-responsive", {
                    style: {
                      background:
                        "rgb(" +
                        _vm.red +
                        ", " +
                        _vm.green +
                        ", " +
                        _vm.blue +
                        ")"
                    },
                    attrs: { height: "100px" }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "9" } },
                        [
                          _c("v-slider", {
                            attrs: { "hide-details": "", label: "R", max: 255 },
                            model: {
                              value: _vm.red,
                              callback: function($$v) {
                                _vm.red = $$v
                              },
                              expression: "red"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "3" } },
                        [
                          _c("v-text-field", {
                            attrs: { "hide-details": "", type: "number" },
                            model: {
                              value: _vm.red,
                              callback: function($$v) {
                                _vm.red = $$v
                              },
                              expression: "red"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "9" } },
                        [
                          _c("v-slider", {
                            attrs: { "hide-details": "", label: "G", max: 255 },
                            model: {
                              value: _vm.green,
                              callback: function($$v) {
                                _vm.green = $$v
                              },
                              expression: "green"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "3" } },
                        [
                          _c("v-text-field", {
                            attrs: { "hide-details": "", type: "number" },
                            model: {
                              value: _vm.green,
                              callback: function($$v) {
                                _vm.green = $$v
                              },
                              expression: "green"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "9" } },
                        [
                          _c("v-slider", {
                            attrs: { "hide-details": "", label: "B", max: 255 },
                            model: {
                              value: _vm.blue,
                              callback: function($$v) {
                                _vm.blue = $$v
                              },
                              expression: "blue"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "3" } },
                        [
                          _c("v-text-field", {
                            attrs: { "hide-details": "", type: "number" },
                            model: {
                              value: _vm.blue,
                              callback: function($$v) {
                                _vm.blue = $$v
                              },
                              expression: "blue"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.icons"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "4" } },
                        [_c("v-subheader", [_vm._v("Media volume")])],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "8" } },
                        [
                          _c("v-slider", {
                            attrs: {
                              "hide-details": "",
                              "prepend-icon": "volume_up"
                            },
                            model: {
                              value: _vm.media,
                              callback: function($$v) {
                                _vm.media = $$v
                              },
                              expression: "media"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "4" } },
                        [_c("v-subheader", [_vm._v("Alarm volume")])],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "8" } },
                        [
                          _c("v-slider", {
                            attrs: {
                              "hide-details": "",
                              "append-icon": "alarm"
                            },
                            model: {
                              value: _vm.alarm,
                              callback: function($$v) {
                                _vm.alarm = $$v
                              },
                              expression: "alarm"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Slider.vue":
/*!***************************************************!*\
  !*** ./resources/js/views/ui-elements/Slider.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Slider_vue_vue_type_template_id_42b1a31f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Slider.vue?vue&type=template&id=42b1a31f& */ "./resources/js/views/ui-elements/Slider.vue?vue&type=template&id=42b1a31f&");
/* harmony import */ var _Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Slider.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Slider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Slider_vue_vue_type_template_id_42b1a31f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Slider_vue_vue_type_template_id_42b1a31f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Slider.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Slider.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Slider.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Slider.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Slider.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Slider.vue?vue&type=template&id=42b1a31f&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Slider.vue?vue&type=template&id=42b1a31f& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_42b1a31f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Slider.vue?vue&type=template&id=42b1a31f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Slider.vue?vue&type=template&id=42b1a31f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_42b1a31f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_42b1a31f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);