(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[91],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ChipGroups.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/ChipGroups.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      column: true,
      mandatory: false,
      multiple: true,
      tags: ['Work', 'Home Improvement', 'Vacation', 'Food', 'Drawers', 'Shopping', 'Art', 'Tech', 'Science', 'Computer', 'Creative Writing'],
      tags1: ['Work', 'Home Improvement', 'Vacation', 'Food', 'Drawers', 'Shopping', 'Art', 'Tech', 'Science', 'Computer', 'Creative Writing']
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ChipGroups.vue?vue&type=template&id=22ab50fe&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/ChipGroups.vue?vue&type=template&id=22ab50fe& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3" },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.chips"),
                    customClasses: "mb-30 mx-auto",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-chip-group")]),
                      _vm._v(" supercharges the "),
                      _c("code", [_vm._v("v-chip")]),
                      _vm._v(
                        " component by providing groupable\n\t\t\t\t\t\tfunctionality."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card",
                    {
                      staticClass: "mx-auto pa-4",
                      attrs: { "max-width": "500" }
                    },
                    [
                      _c(
                        "v-chip-group",
                        {
                          attrs: {
                            multiple: "",
                            column: "",
                            "active-class": "primary--text"
                          }
                        },
                        _vm._l(_vm.tags1, function(tag) {
                          return _c("v-chip", { key: tag }, [
                            _vm._v(
                              "\n\t\t\t\t\t\t\t" +
                                _vm._s(tag) +
                                "\n\t\t\t\t\t\t"
                            )
                          ])
                        }),
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-btn",
                    { staticClass: "mr-6", attrs: { icon: "" } },
                    [_c("v-icon", [_vm._v("mdi-chevron-right")])],
                    1
                  ),
                  _vm._v(" "),
                  _c("span", { staticClass: "subheading" }, [
                    _vm._v("Filter your files")
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    { attrs: { justify: "space-around" } },
                    [
                      _c("v-switch", {
                        attrs: { label: "Column" },
                        model: {
                          value: _vm.column,
                          callback: function($$v) {
                            _vm.column = $$v
                          },
                          expression: "column"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Mandatory" },
                        model: {
                          value: _vm.mandatory,
                          callback: function($$v) {
                            _vm.mandatory = $$v
                          },
                          expression: "mandatory"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Multiple" },
                        model: {
                          value: _vm.multiple,
                          callback: function($$v) {
                            _vm.multiple = $$v
                          },
                          expression: "multiple"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("span", { staticClass: "subheading" }, [
                    _vm._v("Filter by Tags")
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card",
                    {
                      staticClass: "mx-auto pa-4",
                      attrs: { "max-width": "500" }
                    },
                    [
                      _c(
                        "v-chip-group",
                        {
                          attrs: {
                            multiple: _vm.multiple,
                            mandatory: _vm.mandatory,
                            column: _vm.column,
                            "active-class": "primary--text"
                          }
                        },
                        _vm._l(_vm.tags, function(tag) {
                          return _c("v-chip", { key: tag }, [
                            _vm._v(
                              "\n\t\t\t\t\t\t\t" +
                                _vm._s(tag) +
                                "\n\t\t\t\t\t\t"
                            )
                          ])
                        }),
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/ChipGroups.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/ui-elements/ChipGroups.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ChipGroups_vue_vue_type_template_id_22ab50fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ChipGroups.vue?vue&type=template&id=22ab50fe& */ "./resources/js/views/ui-elements/ChipGroups.vue?vue&type=template&id=22ab50fe&");
/* harmony import */ var _ChipGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChipGroups.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/ChipGroups.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ChipGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ChipGroups_vue_vue_type_template_id_22ab50fe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ChipGroups_vue_vue_type_template_id_22ab50fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/ChipGroups.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/ChipGroups.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/ChipGroups.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChipGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ChipGroups.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ChipGroups.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChipGroups_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/ChipGroups.vue?vue&type=template&id=22ab50fe&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/ChipGroups.vue?vue&type=template&id=22ab50fe& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChipGroups_vue_vue_type_template_id_22ab50fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ChipGroups.vue?vue&type=template&id=22ab50fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/ChipGroups.vue?vue&type=template&id=22ab50fe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChipGroups_vue_vue_type_template_id_22ab50fe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChipGroups_vue_vue_type_template_id_22ab50fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);