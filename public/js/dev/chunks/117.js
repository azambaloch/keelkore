(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[117],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/timelines/SmallDots.vue?vue&type=template&id=3ce40125&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/timelines/SmallDots.vue?vue&type=template&id=3ce40125& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "hover-wrapper" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3" },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v(
                        "Easily alternate styles to provide a unique design."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-timeline",
                    [
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "purple lighten-2",
                            "fill-dot": "",
                            right: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "purple lighten-2" },
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-3 white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-magnify\n                           "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 1")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-row",
                                    [
                                      _c("v-col", { attrs: { xs: "10" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit.\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "v-col",
                                        { attrs: { xs: "2" } },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { size: "64" } },
                                            [_vm._v("mdi-calendar-text")]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "amber lighten-1",
                            "fill-dot": "",
                            left: "",
                            small: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "amber lighten-1 justify-end" },
                                [
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 mr-3 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 2")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-home-outline\n                           "
                                      )
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-row",
                                    [
                                      _c("v-col", { attrs: { xs: "8" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit.\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("v-col", { attrs: { xs: "4" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus.\n                              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "cyan lighten-1",
                            "fill-dot": "",
                            right: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "cyan lighten-1" },
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-3 white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-email-outline\n                           "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 3")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-row",
                                    _vm._l(3, function(n) {
                                      return _c(
                                        "v-col",
                                        { key: n, attrs: { xs4: "" } },
                                        [
                                          _vm._v(
                                            "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus no nam oblique.\n                              "
                                          )
                                        ]
                                      )
                                    }),
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "red lighten-1",
                            "fill-dot": "",
                            left: "",
                            small: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "red lighten-1 justify-end" },
                                [
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 mr-3 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 4")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-account-multiple-outline\n                           "
                                      )
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-row",
                                    [
                                      _c(
                                        "v-col",
                                        { attrs: { xs: "2" } },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { size: "64" } },
                                            [_vm._v("mdi-server-network")]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("v-col", { attrs: { xs: "10" } }, [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit, an vim zril disputando voluptatibus.\n                              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-timeline-item",
                        {
                          attrs: {
                            color: "green lighten-1",
                            "fill-dot": "",
                            right: ""
                          }
                        },
                        [
                          _c(
                            "v-card",
                            [
                              _c(
                                "v-card-title",
                                { staticClass: "green lighten-1" },
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-3 white--text",
                                      attrs: { size: "42" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                              mdi-phone-in-talk\n                           "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "h2",
                                    {
                                      staticClass:
                                        "display-1 white--text font-weight-light"
                                    },
                                    [_vm._v("Title 5")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-container",
                                [
                                  _c(
                                    "v-row",
                                    [
                                      _c("v-col", [
                                        _vm._v(
                                          "\n                                 Lorem ipsum dolor sit amet, no nam oblique veritus. Commune scaevola imperdiet nec ut, sed euismod convenire principes at. Est et nobis iisque percipit, an vim zril disputando voluptatibus, vix an salutandi sententiae.\n                              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/timelines/SmallDots.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/timelines/SmallDots.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SmallDots_vue_vue_type_template_id_3ce40125___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SmallDots.vue?vue&type=template&id=3ce40125& */ "./resources/js/views/timelines/SmallDots.vue?vue&type=template&id=3ce40125&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _SmallDots_vue_vue_type_template_id_3ce40125___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SmallDots_vue_vue_type_template_id_3ce40125___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/timelines/SmallDots.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/timelines/SmallDots.vue?vue&type=template&id=3ce40125&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/timelines/SmallDots.vue?vue&type=template&id=3ce40125& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SmallDots_vue_vue_type_template_id_3ce40125___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SmallDots.vue?vue&type=template&id=3ce40125& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/timelines/SmallDots.vue?vue&type=template&id=3ce40125&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SmallDots_vue_vue_type_template_id_3ce40125___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SmallDots_vue_vue_type_template_id_3ce40125___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);