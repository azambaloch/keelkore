(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[138],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Chat.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Chat.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      chats: [{
        id: 1,
        body: "A small river named Duden flows by their place and supplies it with the necessary",
        isAdmin: false,
        userName: "Max Wells",
        time: "5 Min Ago"
      }, {
        id: 2,
        body: "You better believe it! ",
        isAdmin: true,
        adminName: "Andre Adkins",
        time: "4 Min Ago"
      }, {
        id: 3,
        body: "A small river named Duden flows by their place and supplies it with the necessary",
        isAdmin: false,
        userName: "Max Wells",
        time: "3 Min Ago"
      }, {
        id: 4,
        body: "Lorem ipsum dolor sit ametcon",
        isAdmin: true,
        adminName: "Andre Adkins",
        time: "1 Min Ago"
      }],
      settings: {
        maxScrollbarLength: 160
      },
      newMessage: "",
      typing: false,
      randomMessages: ["How are you?", "We are glad to know", "How can I help you?", "We are happy to help you"]
    };
  },
  updated: function updated() {
    if (this.newMessage == '') {
      this.scrollToEnd();
    }
  },
  methods: {
    sendMessage: function sendMessage() {
      var _this = this;

      if (this.newMessage !== "") {
        var newMessage = {
          id: new Date().getTime(),
          body: this.newMessage,
          avatar: "/static/avatars/user-20.jpg",
          isAdmin: true,
          adminName: "Andre Adkins",
          time: "Just Now"
        };
        this.chats.push(newMessage);
        this.newMessage = "";
        this.scrollToEnd();
        setTimeout(function () {
          _this.typing = true;

          _this.getReply();
        }, 3000);
      }
    },
    getReply: function getReply() {
      var _this2 = this;

      var randomMessage = Math.floor(Math.random() * this.randomMessages.length);
      var reply = {
        id: new Date().getTime(),
        body: this.randomMessages[randomMessage],
        avatar: "/static/avatars/user-14.jpg",
        isAdmin: false,
        userName: "Max Wells",
        time: "Just Now"
      };
      setTimeout(function () {
        _this2.typing = false;
        setTimeout(function () {
          _this2.chats.push(reply);
        }, 200);
      }, 3000);
    },
    scrollToEnd: function scrollToEnd() {
      var container = document.getElementById("chat-scroll");

      if (container !== null) {
        var scrollHeight = container.scrollHeight;
        container.scrollTop = scrollHeight;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Chat.vue?vue&type=template&id=21bf86c3&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Chat.vue?vue&type=template&id=21bf86c3& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      attrs: {
        colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
        heading: _vm.$t("message.chat"),
        closeable: true,
        reloadable: true,
        fullScreen: true,
        footer: true,
        fullBlock: true,
        customClasses: "chat-widget-wrap crm-chat-wrap"
      }
    },
    [
      _c(
        "div",
        { staticClass: "chat-widget" },
        [
          _c(
            "vue-perfect-scrollbar",
            {
              staticStyle: { height: "375px" },
              attrs: { id: "chat-scroll", settings: _vm.settings }
            },
            [
              _c(
                "div",
                { staticClass: "chat-body" },
                [
                  _vm._l(_vm.chats, function(chat, index) {
                    return [
                      _c(
                        "div",
                        {
                          key: index,
                          staticClass: "chat-block mb-2",
                          class: { "flex-row-reverse": chat.isAdmin }
                        },
                        [
                          chat.isAdmin
                            ? [
                                _c("img", {
                                  staticClass: "rounded-circle ml-3 mt-3",
                                  attrs: {
                                    src: "/static/avatars/user-20.jpg",
                                    alt: "user-profile",
                                    width: "40",
                                    height: "40"
                                  }
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "chat-bubble-wrap" }, [
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "fs-12 fw-normal grey--text text-right d-block mb-1"
                                    },
                                    [_vm._v(_vm._s(chat.adminName))]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "chat-bubble odd grey lighten-4"
                                    },
                                    [_vm._v(_vm._s(chat.body))]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "fs-12 grey--text text-left d-block mt-1 fw-normal"
                                    },
                                    [_vm._v(_vm._s(chat.time))]
                                  )
                                ])
                              ]
                            : [
                                _c(
                                  "div",
                                  { staticClass: "mr-3 mt-3 pos-relative" },
                                  [
                                    _c("img", {
                                      staticClass: "rounded-circle",
                                      attrs: {
                                        src: "/static/avatars/user-14.jpg",
                                        alt: "user-profile",
                                        width: "40",
                                        height: "40"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("span", {
                                      staticClass:
                                        "v-badge success rounded floating"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "chat-bubble-wrap" }, [
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "fs-12 grey--text d-block mb-1 fw-normal"
                                    },
                                    [_vm._v(_vm._s(chat.userName))]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "chat-bubble even primary lighten-4"
                                    },
                                    [_vm._v(_vm._s(chat.body))]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "fs-12 grey--text text-right d-block mt-1 fw-normal"
                                    },
                                    [_vm._v(_vm._s(chat.time))]
                                  )
                                ])
                              ]
                        ],
                        2
                      )
                    ]
                  }),
                  _vm._v(" "),
                  _vm.typing
                    ? _c("div", { staticClass: "chat-block mb-2" }, [
                        _c("img", {
                          staticClass: "rounded-circle mr-3 mt-3",
                          attrs: {
                            src: "/static/avatars/user-14.jpg",
                            alt: "user-profile",
                            width: "40",
                            height: "40"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "chat-bubble even primary lighten-4 animated bounce"
                          },
                          [_vm._v("typing....")]
                        )
                      ])
                    : _vm._e()
                ],
                2
              )
            ]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "send-message-box",
          attrs: { slot: "footer" },
          slot: "footer"
        },
        [
          _c(
            "div",
            { staticClass: "d-custom-flex" },
            [
              _c("v-text-field", {
                attrs: {
                  id: "sendMessage",
                  name: "send-message",
                  solo: "",
                  label: "Send Message"
                },
                on: {
                  keyup: function($event) {
                    if (
                      !$event.type.indexOf("key") &&
                      _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                    ) {
                      return null
                    }
                    return _vm.sendMessage.apply(null, arguments)
                  }
                },
                model: {
                  value: _vm.newMessage,
                  callback: function($$v) {
                    _vm.newMessage = $$v
                  },
                  expression: "newMessage"
                }
              }),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  staticClass: "ml-3 px-5",
                  attrs: { dark: "", large: "", color: "primary" },
                  on: { click: _vm.sendMessage }
                },
                [_vm._v("\n               Send\n            ")]
              )
            ],
            1
          )
        ]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Widgets/Chat.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/Widgets/Chat.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Chat_vue_vue_type_template_id_21bf86c3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Chat.vue?vue&type=template&id=21bf86c3& */ "./resources/js/components/Widgets/Chat.vue?vue&type=template&id=21bf86c3&");
/* harmony import */ var _Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Chat.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Chat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Chat_vue_vue_type_template_id_21bf86c3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Chat_vue_vue_type_template_id_21bf86c3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Chat.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Chat.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/Widgets/Chat.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Chat.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Chat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Chat.vue?vue&type=template&id=21bf86c3&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/Chat.vue?vue&type=template&id=21bf86c3& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_template_id_21bf86c3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Chat.vue?vue&type=template&id=21bf86c3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Chat.vue?vue&type=template&id=21bf86c3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_template_id_21bf86c3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_template_id_21bf86c3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);