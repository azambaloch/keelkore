(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[109],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Tabs.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Tabs.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      tab: null,
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      icons: false,
      centered: false,
      grow: false,
      vertical: false,
      prevIcon: false,
      nextIcon: false,
      right: false,
      tabs: 3
    };
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Tabs.vue?vue&type=template&id=3abece88&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Tabs.vue?vue&type=template&id=3abece88& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.usage"),
                    colClasses: "col-12 col-md-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-6" }, [
                    _c("p", { staticClass: "mb-0" }, [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-tabs")]),
                      _vm._v(
                        " component is a styled extension of v-item-group. It provides an easy to use interface for organizing groups of content."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-tabs",
                    [
                      _c("v-tab", [_vm._v("Item One")]),
                      _vm._v(" "),
                      _c("v-tab", [_vm._v("Item Two")]),
                      _vm._v(" "),
                      _c("v-tab", [_vm._v("Item Three")])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    colClasses: "col-12 col-md-12"
                  }
                },
                [
                  _c(
                    "v-row",
                    { attrs: { justify: "space-around" } },
                    [
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Text + icons" },
                        model: {
                          value: _vm.icons,
                          callback: function($$v) {
                            _vm.icons = $$v
                          },
                          expression: "icons"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Centered", disabled: _vm.vertical },
                        model: {
                          value: _vm.centered,
                          callback: function($$v) {
                            _vm.centered = $$v
                          },
                          expression: "centered"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Grow" },
                        model: {
                          value: _vm.grow,
                          callback: function($$v) {
                            _vm.grow = $$v
                          },
                          expression: "grow"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Vertical" },
                        model: {
                          value: _vm.vertical,
                          callback: function($$v) {
                            _vm.vertical = $$v
                          },
                          expression: "vertical"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        staticClass: "mx-2",
                        attrs: { label: "Right" },
                        model: {
                          value: _vm.right,
                          callback: function($$v) {
                            _vm.right = $$v
                          },
                          expression: "right"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c("v-slider", {
                            attrs: {
                              min: "0",
                              max: "10",
                              label: "Tabs number"
                            },
                            model: {
                              value: _vm.tabs,
                              callback: function($$v) {
                                _vm.tabs = $$v
                              },
                              expression: "tabs"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tabs",
                    {
                      staticClass: "elevation-2",
                      attrs: {
                        centered: _vm.centered,
                        grow: _vm.grow,
                        vertical: _vm.vertical,
                        right: _vm.right,
                        "prev-icon": _vm.prevIcon
                          ? "mdi-arrow-left-bold-box-outline"
                          : undefined,
                        "next-icon": _vm.nextIcon
                          ? "mdi-arrow-right-bold-box-outline"
                          : undefined,
                        "icons-and-text": _vm.icons
                      },
                      model: {
                        value: _vm.tab,
                        callback: function($$v) {
                          _vm.tab = $$v
                        },
                        expression: "tab"
                      }
                    },
                    [
                      _c("v-tabs-slider"),
                      _vm._v(" "),
                      _vm._l(_vm.tabs, function(i) {
                        return _c(
                          "v-tab",
                          { key: i, attrs: { href: "#tab-" + i } },
                          [
                            _vm._v(
                              "\n\t\t\t\t\t\tTab " + _vm._s(i) + "\n\t\t\t\t\t"
                            ),
                            _vm.icons
                              ? _c("v-icon", [_vm._v("mdi-phone")])
                              : _vm._e()
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _vm._l(_vm.tabs, function(i) {
                        return _c(
                          "v-tab-item",
                          { key: i, attrs: { value: "tab-" + i } },
                          [
                            _c(
                              "v-card",
                              { attrs: { flat: "", tile: "" } },
                              [_c("v-card-text", [_vm._v(_vm._s(_vm.text))])],
                              1
                            )
                          ],
                          1
                        )
                      })
                    ],
                    2
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Tabs.vue":
/*!*************************************************!*\
  !*** ./resources/js/views/ui-elements/Tabs.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Tabs_vue_vue_type_template_id_3abece88___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Tabs.vue?vue&type=template&id=3abece88& */ "./resources/js/views/ui-elements/Tabs.vue?vue&type=template&id=3abece88&");
/* harmony import */ var _Tabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Tabs.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Tabs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Tabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Tabs_vue_vue_type_template_id_3abece88___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Tabs_vue_vue_type_template_id_3abece88___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Tabs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Tabs.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Tabs.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Tabs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Tabs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tabs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Tabs.vue?vue&type=template&id=3abece88&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Tabs.vue?vue&type=template&id=3abece88& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tabs_vue_vue_type_template_id_3abece88___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Tabs.vue?vue&type=template&id=3abece88& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Tabs.vue?vue&type=template&id=3abece88&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tabs_vue_vue_type_template_id_3abece88___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tabs_vue_vue_type_template_id_3abece88___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);