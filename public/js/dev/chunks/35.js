(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["heading", "viewers", "colClasses", "customClasses", "trade", "icon", "color"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExpandableTable.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExpandableTable.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartV6__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartV6 */ "./resources/js/components/Charts/LineChartV6.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import CourseData from "Views/course/dasRta.js";



/* harmony default export */ __webpack_exports__["default"] = ({
  // props:['details'],
  components: {
    LineChartV6: Components_Charts_LineChartV6__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      panel: 0,
      selectedBtn: "yearly",
      marketCapDetails: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_2__["marketCapDetails"],
      disabled: false,
      readonly: false,
      yearly: [10, 65, 40, 150, 40, 85, 30],
      monthly: [0, 65, 40, 0, 40, 85, 0],
      weekly: [60, 25, 150, 60, 100, 5, 60]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__["getCurrentAppLayout"])(this.$router);
    },
    showYearly: function showYearly() {
      this.selectedBtn = "yearly"; // console.log(this.selectedBtn);
    },
    showMonthly: function showMonthly() {
      this.selectedBtn = "monthly"; // console.log(this.selectedBtn);
    },
    showWeekly: function showWeekly() {
      this.selectedBtn = "weekly"; // console.log(this.selectedBtn);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/MarketCap.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crypto/MarketCap.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_StatsCardV8_StatsCardV8__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/StatsCardV8/StatsCardV8 */ "./resources/js/components/StatsCardV8/StatsCardV8.vue");
/* harmony import */ var Components_Widgets_ExpandableTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Widgets/ExpandableTable */ "./resources/js/components/Widgets/ExpandableTable.vue");
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    StatsCardV8: Components_StatsCardV8_StatsCardV8__WEBPACK_IMPORTED_MODULE_0__["default"],
    ExpandableTable: Components_Widgets_ExpandableTable__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      marketCapDetails: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_2__["marketCapDetails"],
      cardData: [{
        name: "Bitcoin",
        viewers: "+41",
        trade: "30",
        icon: "cc BTC-alt",
        color: "primary"
      }, {
        name: "Ethereum",
        viewers: "+4381",
        trade: "60",
        icon: "cc ETH",
        color: "success"
      }, {
        name: "Litecoin",
        viewers: "+2611",
        trade: "80",
        icon: "cc LTC",
        color: "warning"
      }, {
        name: "Zcash",
        viewers: "+611",
        trade: "40",
        icon: "cc ZEC",
        color: "error"
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=template&id=7969df18&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=template&id=7969df18& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-col", { class: _vm.colClasses }, [
    _c(
      "div",
      { staticClass: "app-card white--text", class: _vm.color },
      [
        _c("v-card-text", { staticClass: "wallet-stats-card" }, [
          _c("div", [
            _c(
              "div",
              {
                staticClass:
                  "mb-2 d-custom-flex justify-start align-items-center"
              },
              [
                _c("span", { staticClass: "font-md d-inline-block" }, [
                  _c("i", {
                    staticClass: "mr-2 white--text font-lg",
                    class: _vm.icon,
                    attrs: { title: "BCN" }
                  })
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "d-inline-block font-md" }, [
                  _vm._v(_vm._s(_vm.heading))
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "d-custom-flex justify-space-between align-items-start mb-4"
              },
              [
                _c("span", [_vm._v(_vm._s(_vm.viewers))]),
                _vm._v(" "),
                _c("span", [
                  _vm._v(
                    _vm._s(_vm.$t("message.trade")) +
                      " : " +
                      _vm._s(_vm.trade) +
                      "%"
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "mb-2" },
              [
                _c("v-progress-linear", {
                  staticClass: "my-1",
                  attrs: { value: _vm.trade, height: "3", color: "white" }
                })
              ],
              1
            )
          ])
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-expansion-panels",
        {
          model: {
            value: _vm.panel,
            callback: function($$v) {
              _vm.panel = $$v
            },
            expression: "panel"
          }
        },
        _vm._l(_vm.marketCapDetails, function(details, index) {
          return _c(
            "v-expansion-panel",
            { key: index, attrs: { id: index } },
            [
              _c("v-expansion-panel-header", [
                _c("p", [_vm._v(_vm._s(details.sr_no))]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(details.currency))]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(details.market_cap))]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(details.price))]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(details.volume))]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(details.change))]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(details.more))])
              ]),
              _vm._v(" "),
              _c(
                "v-expansion-panel-content",
                [
                  _c(
                    "v-card",
                    { staticClass: "marketcap-chart-wrap" },
                    [
                      _c(
                        "div",
                        { staticClass: "mb-3" },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "error mr-3",
                              on: {
                                click: function($event) {
                                  return _vm.showYearly()
                                }
                              }
                            },
                            [_vm._v("Yearly")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              staticClass: "success mr-3",
                              on: {
                                click: function($event) {
                                  return _vm.showMonthly()
                                }
                              }
                            },
                            [_vm._v("Monthly")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              staticClass: "primary",
                              on: {
                                click: function($event) {
                                  return _vm.showWeekly()
                                }
                              }
                            },
                            [_vm._v("Weekly")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("line-chart-v6", {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.selectedBtn == "yearly",
                            expression: "selectedBtn == 'yearly'"
                          }
                        ],
                        staticStyle: { width: "100%", height: "400px" },
                        attrs: { dataSet: [10, 65, 40, 150, 40, 85, 30] }
                      }),
                      _vm._v(" "),
                      _c("line-chart-v6", {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.selectedBtn == "monthly",
                            expression: "selectedBtn == 'monthly'"
                          }
                        ],
                        staticStyle: { width: "100%", height: "400px" },
                        attrs: { dataSet: [0, 65, 40, 0, 40, 85, 0] }
                      }),
                      _vm._v(" "),
                      _c("line-chart-v6", {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.selectedBtn == "weekly",
                            expression: "selectedBtn == 'weekly'"
                          }
                        ],
                        staticStyle: { width: "100%", height: "400px" },
                        attrs: { dataSet: [60, 25, 150, 60, 100, 5, 60] }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        }),
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/MarketCap.vue?vue&type=template&id=039ae707&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crypto/MarketCap.vue?vue&type=template&id=039ae707& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        {
          staticClass: "grid-list-xl pt-0 mktcap-container-wrap",
          attrs: { fluid: "" }
        },
        [
          _c("crypto-slider"),
          _vm._v(" "),
          _c(
            "v-row",
            { staticClass: "border-rad-sm overflow-hidden crypto-status-card" },
            [
              _vm._l(_vm.cardData, function(data, index) {
                return [
                  _c("stats-card-v8", {
                    key: index,
                    attrs: {
                      colClasses: "col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12",
                      heading: data.name,
                      viewers: data.viewers,
                      trade: data.trade,
                      icon: data.icon,
                      color: data.color
                    }
                  })
                ]
              })
            ],
            2
          ),
          _vm._v(" "),
          _c("div", { staticClass: "mrkcap-scroll-wrap" }, [
            _c("div", { staticClass: "mktcap-wrapper" }, [
              _c(
                "div",
                { staticClass: "mktcap-resp-scroll" },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                            customClasses: "mb-0 crypto-card-wrap"
                          }
                        },
                        [
                          _c(
                            "div",
                            [
                              _c(
                                "v-container",
                                {
                                  staticClass: "grid-list-md",
                                  attrs: { fluid: "" }
                                },
                                [
                                  _c(
                                    "v-row",
                                    {
                                      staticClass:
                                        "d-flex align-items-center w-100"
                                    },
                                    [
                                      _c("div", { staticClass: "pa-4" }, [
                                        _vm._v(
                                          "\n                                 S.No.\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "pa-4" }, [
                                        _vm._v(
                                          "\n                                 Currency\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "pa-4" }, [
                                        _vm._v(
                                          "\n                                 Market Cap\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "pa-4" }, [
                                        _vm._v(
                                          "\n                                 Price\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "pa-4" }, [
                                        _vm._v(
                                          "\n                                 Volume\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "pa-4" }, [
                                        _vm._v(
                                          "\n                                 Change(24hr)\n                              "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "d-flex justify-end" },
                                        [
                                          _vm._v(
                                            "\n                                 More\n                              "
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                            customClasses: "mb-0"
                          }
                        },
                        [_c("expandable-table")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/LineChartV6.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/Charts/LineChartV6.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// News Letter Campaign Widget


var lineTension = lineTension;
var borderWidth = 3;
var pointRadius = 7;
var pointBorderWidth = 2;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: ["dataSet"],
  data: function data() {
    return {
      options: {
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Y-Axis'
            },
            gridLines: {
              display: true,
              drawBorder: false
            },
            ticks: {
              stepSize: 50,
              display: true
            }
          }],
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'x-Axes'
            },
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              display: true
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    // console.log(this.dataSet+ "test");
    this.renderChart({
      labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
      datasets: [{
        label: 'Data',
        lineTension: lineTension,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.dataSet
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/StatsCardV8/StatsCardV8.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/StatsCardV8/StatsCardV8.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatsCardV8_vue_vue_type_template_id_7969df18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatsCardV8.vue?vue&type=template&id=7969df18& */ "./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=template&id=7969df18&");
/* harmony import */ var _StatsCardV8_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatsCardV8.vue?vue&type=script&lang=js& */ "./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StatsCardV8_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StatsCardV8_vue_vue_type_template_id_7969df18___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StatsCardV8_vue_vue_type_template_id_7969df18___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StatsCardV8/StatsCardV8.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV8_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV8.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV8_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=template&id=7969df18&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=template&id=7969df18& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV8_vue_vue_type_template_id_7969df18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV8.vue?vue&type=template&id=7969df18& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV8/StatsCardV8.vue?vue&type=template&id=7969df18&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV8_vue_vue_type_template_id_7969df18___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV8_vue_vue_type_template_id_7969df18___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ExpandableTable.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/Widgets/ExpandableTable.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExpandableTable_vue_vue_type_template_id_6895941f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true& */ "./resources/js/components/Widgets/ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true&");
/* harmony import */ var _ExpandableTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpandableTable.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ExpandableTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExpandableTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExpandableTable_vue_vue_type_template_id_6895941f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExpandableTable_vue_vue_type_template_id_6895941f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6895941f",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ExpandableTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ExpandableTable.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExpandableTable.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExpandableTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExpandableTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExpandableTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExpandableTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true& ***!
  \********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExpandableTable_vue_vue_type_template_id_6895941f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExpandableTable.vue?vue&type=template&id=6895941f&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExpandableTable_vue_vue_type_template_id_6895941f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExpandableTable_vue_vue_type_template_id_6895941f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/crypto/MarketCap.vue":
/*!*************************************************!*\
  !*** ./resources/js/views/crypto/MarketCap.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MarketCap_vue_vue_type_template_id_039ae707___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MarketCap.vue?vue&type=template&id=039ae707& */ "./resources/js/views/crypto/MarketCap.vue?vue&type=template&id=039ae707&");
/* harmony import */ var _MarketCap_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MarketCap.vue?vue&type=script&lang=js& */ "./resources/js/views/crypto/MarketCap.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MarketCap_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MarketCap_vue_vue_type_template_id_039ae707___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MarketCap_vue_vue_type_template_id_039ae707___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/crypto/MarketCap.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/crypto/MarketCap.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/crypto/MarketCap.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MarketCap_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./MarketCap.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/MarketCap.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MarketCap_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/crypto/MarketCap.vue?vue&type=template&id=039ae707&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/crypto/MarketCap.vue?vue&type=template&id=039ae707& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MarketCap_vue_vue_type_template_id_039ae707___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./MarketCap.vue?vue&type=template&id=039ae707& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/MarketCap.vue?vue&type=template&id=039ae707&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MarketCap_vue_vue_type_template_id_039ae707___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MarketCap_vue_vue_type_template_id_039ae707___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);