(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[68],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/EditDetail.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/EditDetail.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Views_ecommerce_data_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Views/ecommerce/data.js */ "./resources/js/views/ecommerce/data.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      productsData: Views_ecommerce_data_js__WEBPACK_IMPORTED_MODULE_0__["productsData"],
      products: '',
      selectedProduct: '',
      colors: ["Red", "Blue", "Yellow", "Green"],
      sizes: ["28", "30", "32", "34", "36", "38", "40"]
    };
  },
  mounted: function mounted() {
    this.getProductData();
  },
  methods: {
    getProductData: function getProductData() {
      var allItems = this.productsData.men.concat(this.productsData.women, this.productsData.accessories, this.productsData.gadgets);

      for (var i = 0; i < allItems.length; i++) {
        if (allItems[i].id == this.$route.params.id) {
          this.selectedProduct = {
            availablity: allItems[i].availablity,
            brand: allItems[i].brand,
            category: allItems[i].category,
            category_type: allItems[i].category_type,
            color: allItems[i].color,
            discountPriceValue: allItems[i].discountPriceValue,
            description: allItems[i].description,
            discount_price: allItems[i].discount_price,
            features: allItems[i].features,
            id: allItems[i].id,
            image: allItems[i].image,
            image_gallery: allItems[i].image_gallery,
            name: allItems[i].name,
            popular: allItems[i].popular,
            price: allItems[i].price,
            product_code: allItems[i].product_code,
            quantity: allItems[i].quantity,
            rating: allItems[i].rating,
            status: allItems[i].status,
            tags: allItems[i].tags,
            type: allItems[i].type
          };
        }
      }
    },
    doHover: function doHover(img) {
      // console.log(img)
      this.selectedProduct.image = img;
    },
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/EditDetail.vue?vue&type=template&id=41f76ac9&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/EditDetail.vue?vue&type=template&id=41f76ac9& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "v-edit-products" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c("page-title-bar"),
      _vm._v(" "),
      _vm.selectedProduct !== null
        ? [
            _c(
              "v-container",
              { attrs: { fluid: "" } },
              [
                _c(
                  "v-row",
                  [
                    _c(
                      "v-col",
                      {
                        staticClass: "mx-auto",
                        attrs: { cols: "12", sm: "10", md: "10" }
                      },
                      [
                        _c(
                          "v-row",
                          [
                            _c(
                              "v-col",
                              { attrs: { cols: "12", md: "6" } },
                              [
                                _c(
                                  "v-row",
                                  { staticClass: "product-images-wrap" },
                                  [
                                    _c(
                                      "v-col",
                                      { attrs: { cols: "2", md: "2" } },
                                      _vm._l(
                                        _vm.selectedProduct.image_gallery,
                                        function(img, i) {
                                          return _c(
                                            "div",
                                            {
                                              key: i,
                                              staticClass: "thumb-wrap ml-auto",
                                              attrs: { for: "upload" }
                                            },
                                            [
                                              _c("v-img", {
                                                staticStyle: { height: "70px" },
                                                attrs: { src: img },
                                                on: {
                                                  mouseover: function($event) {
                                                    return _vm.doHover(img)
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "edit-btn d-flex justify-center align-items-center"
                                                },
                                                [
                                                  _c(
                                                    "v-icon",
                                                    { attrs: { dark: "" } },
                                                    [_vm._v("edit")]
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c("input", {
                                                staticClass: "upload-img",
                                                attrs: {
                                                  type: "file",
                                                  id: "upload",
                                                  accept: "image/*"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        }
                                      ),
                                      0
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-col",
                                      { attrs: { cols: "10", md: "10" } },
                                      [
                                        _c("v-img", {
                                          staticStyle: { width: "100%" },
                                          attrs: {
                                            src: _vm.selectedProduct.image
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-col",
                              {
                                staticClass: "content-wrap",
                                attrs: { cols: "12", sm: "12", md: "6" }
                              },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "pt-4",
                                    attrs: {
                                      to:
                                        "/" +
                                        (_vm.getCurrentAppLayoutHandler() +
                                          "/ecommerce/edit-product")
                                    }
                                  },
                                  [_vm._v("\n\t\t\t\t\t\t\t\tGo to Products")]
                                ),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  staticClass: "name-input",
                                  attrs: {
                                    "prepend-icon": "edit",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.name,
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedProduct, "name", $$v)
                                    },
                                    expression: "selectedProduct.name"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  staticClass: "price-input",
                                  attrs: {
                                    "prepend-icon": "edit",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.price,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.selectedProduct,
                                        "price",
                                        $$v
                                      )
                                    },
                                    expression: "selectedProduct.price"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  attrs: {
                                    "prepend-icon": "edit",
                                    label: "Availablity",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.availablity,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.selectedProduct,
                                        "availablity",
                                        $$v
                                      )
                                    },
                                    expression: "selectedProduct.availablity"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  attrs: {
                                    "prepend-icon": "edit",
                                    label: "Product Code :",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.product_code,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.selectedProduct,
                                        "product_code",
                                        $$v
                                      )
                                    },
                                    expression: "selectedProduct.product_code"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  attrs: {
                                    "prepend-icon": "edit",
                                    label: "Tags :",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.tags,
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedProduct, "tags", $$v)
                                    },
                                    expression: "selectedProduct.tags"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  attrs: {
                                    "prepend-icon": "edit",
                                    label: "Description",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.description,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.selectedProduct,
                                        "description",
                                        $$v
                                      )
                                    },
                                    expression: "selectedProduct.description"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  attrs: {
                                    "prepend-icon": "edit",
                                    label: "Features points",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.selectedProduct.features,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.selectedProduct,
                                        "features",
                                        $$v
                                      )
                                    },
                                    expression: "selectedProduct.features"
                                  }
                                }),
                                _vm._v(" "),
                                _c("v-text-field", {
                                  attrs: {
                                    "prepend-icon": "edit",
                                    value: "5",
                                    type: "number",
                                    label: "Total Products"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ecommerce/EditDetail.vue":
/*!*****************************************************!*\
  !*** ./resources/js/views/ecommerce/EditDetail.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditDetail_vue_vue_type_template_id_41f76ac9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditDetail.vue?vue&type=template&id=41f76ac9& */ "./resources/js/views/ecommerce/EditDetail.vue?vue&type=template&id=41f76ac9&");
/* harmony import */ var _EditDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditDetail.vue?vue&type=script&lang=js& */ "./resources/js/views/ecommerce/EditDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditDetail_vue_vue_type_template_id_41f76ac9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditDetail_vue_vue_type_template_id_41f76ac9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ecommerce/EditDetail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ecommerce/EditDetail.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/ecommerce/EditDetail.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditDetail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/EditDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ecommerce/EditDetail.vue?vue&type=template&id=41f76ac9&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/ecommerce/EditDetail.vue?vue&type=template&id=41f76ac9& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditDetail_vue_vue_type_template_id_41f76ac9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditDetail.vue?vue&type=template&id=41f76ac9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/EditDetail.vue?vue&type=template&id=41f76ac9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditDetail_vue_vue_type_template_id_41f76ac9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditDetail_vue_vue_type_template_id_41f76ac9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);