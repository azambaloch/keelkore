(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_CampaignBarChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/CampaignBarChart */ "./resources/js/components/Charts/CampaignBarChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      yesterday: {
        labels: ["12:00 AM", "4:00 AM", "8:00 AM", "12:00 PM", "16:00 PM"],
        websiteViews: [600, 900, 660, 750, 800],
        emailSubscription: [400, 550, 400, 400, 450]
      },
      last5Days: {
        labels: ["Mon", "Tue", "Wed", "Thur", "Fri"],
        websiteViews: [600, 900, 725, 1000, 460],
        emailSubscription: [400, 700, 500, 625, 400]
      },
      last1Month: {
        labels: ["1-5", "6-10", "11-15", "16-20", "21-25"],
        websiteViews: [800, 700, 725, 600, 900],
        emailSubscription: [700, 600, 400, 400, 500]
      },
      last5Months: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May"],
        websiteViews: [1000, 700, 725, 625, 600],
        emailSubscription: [800, 450, 550, 500, 450]
      },
      defaultSelectedItem: 'Yesterday',
      chartOptions: ["Yesterday", "Last Week", "Last Month", "Last 6 Months"]
    };
  },
  components: {
    CampaignBarChart: _Charts_CampaignBarChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=template&id=0df75b36&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=template&id=0df75b36& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "sales-chart-wrap d-custom-flex justify-space-between" },
    [
      _c("campaign-bar-chart", {
        staticClass: "pa-3",
        attrs: {
          labels: _vm.yesterday.labels,
          websiteViews: _vm.yesterday.websiteViews,
          emailSubscription: _vm.yesterday.emailSubscription,
          height: 210
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "app-footer" }, [
        _c(
          "div",
          [
            _c("v-select", {
              attrs: { items: _vm.chartOptions, label: "Select", solo: "" },
              model: {
                value: _vm.defaultSelectedItem,
                callback: function($$v) {
                  _vm.defaultSelectedItem = $$v
                },
                expression: "defaultSelectedItem"
              }
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/CampaignBarChart.js":
/*!************************************************************!*\
  !*** ./resources/js/components/Charts/CampaignBarChart.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../constants/chart-config */ "./resources/js/constants/chart-config.js");
// Bar Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  props: ['websiteViews', 'labels', 'emailSubscription'],
  data: function data() {
    return {
      ChartConfig: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      options: {
        legend: {
          display: false
        },
        tooltips: {
          titleSpacing: 6,
          cornerRadius: 5
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: false,
              color: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].chartGridColor
            },
            ticks: {
              fontColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].axesColor
            }
          }],
          yAxes: [{
            gridLines: {
              color: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].chartGridColor,
              drawBorder: false
            },
            ticks: {
              fontColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].axesColor,
              min: 100,
              max: 1000
            }
          }]
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: this.labels,
      datasets: [{
        label: 'Website view',
        backgroundColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        borderColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        borderWidth: 1,
        barPercentage: 1.3,
        categoryPercentage: 0.5,
        hoverBackgroundColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        hoverBorderColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        data: this.websiteViews
      }, {
        label: 'Email Subscription',
        backgroundColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning,
        borderColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning,
        borderWidth: 1,
        barPercentage: 1.3,
        categoryPercentage: 0.5,
        hoverBackgroundColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning,
        hoverBorderColor: _constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning,
        data: this.emailSubscription
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/CampaignPerformance.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/Widgets/CampaignPerformance.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CampaignPerformance_vue_vue_type_template_id_0df75b36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CampaignPerformance.vue?vue&type=template&id=0df75b36& */ "./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=template&id=0df75b36&");
/* harmony import */ var _CampaignPerformance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CampaignPerformance.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CampaignPerformance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CampaignPerformance_vue_vue_type_template_id_0df75b36___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CampaignPerformance_vue_vue_type_template_id_0df75b36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/CampaignPerformance.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CampaignPerformance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CampaignPerformance.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CampaignPerformance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=template&id=0df75b36&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=template&id=0df75b36& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CampaignPerformance_vue_vue_type_template_id_0df75b36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CampaignPerformance.vue?vue&type=template&id=0df75b36& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CampaignPerformance.vue?vue&type=template&id=0df75b36&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CampaignPerformance_vue_vue_type_template_id_0df75b36___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CampaignPerformance_vue_vue_type_template_id_0df75b36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/dashboard/data.js":
/*!**********************************************!*\
  !*** ./resources/js/views/dashboard/data.js ***!
  \**********************************************/
/*! exports provided: newClients, recurringClients, bounceRates, pageViews, newsLetterCampaignData, newsLetterCampaignData2, newsLetterCampaignData3, earnedToday, itemsSold, salesAndEarning, totalEarnings, netProfit, totalExpences, onlineRevenue, adCampaignPerfomanceData, profitShare, devicesShare, subscribers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newClients", function() { return newClients; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recurringClients", function() { return recurringClients; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bounceRates", function() { return bounceRates; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pageViews", function() { return pageViews; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newsLetterCampaignData", function() { return newsLetterCampaignData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newsLetterCampaignData2", function() { return newsLetterCampaignData2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newsLetterCampaignData3", function() { return newsLetterCampaignData3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "earnedToday", function() { return earnedToday; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "itemsSold", function() { return itemsSold; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "salesAndEarning", function() { return salesAndEarning; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "totalEarnings", function() { return totalEarnings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "netProfit", function() { return netProfit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "totalExpences", function() { return totalExpences; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onlineRevenue", function() { return onlineRevenue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "adCampaignPerfomanceData", function() { return adCampaignPerfomanceData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "profitShare", function() { return profitShare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "devicesShare", function() { return devicesShare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "subscribers", function() { return subscribers; });
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
 // New clients

var newClients = {
  title: 'message.newClients',
  value: '35,455',
  data: [12, 10, 4, 15, 16, 10, 20, 12],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary
}; // Recuring clients

var recurringClients = {
  title: 'message.recurringClients',
  value: '12,110',
  data: [10, 6, 10, 3, 15, 10, 15, 16],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info
}; // Bounce Rate

var bounceRates = {
  title: 'message.bounceRate',
  value: '8,855',
  data: [12, 12, 12, 18, 8, 17, 8, 12],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger
}; // Page Views

var pageViews = {
  title: 'message.pageViews',
  value: '9,123',
  data: [10, 6, 10, 3, 15, 10, 15, 16],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning
}; // news letter campaign data

var newsLetterCampaignData = {
  label1: 'Data 1',
  label2: 'Data 2',
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  data1: [45, 42, 51, 19, 50, 30, 22, 20, 45, 35, 40, 37],
  data2: [32, 28, 40, 38, 32, 42, 39, 15, 9, 17, 20, 41]
}; // news letter campaign data

var newsLetterCampaignData2 = {
  data1: [19, 21, 18, 20, 23, 16, 18, 30],
  data2: [10, 8, 14, 11, 10, 12, 10, 0]
}; // news letter campaign data for news dashboard

var newsLetterCampaignData3 = {
  label1: 'Campaign 1',
  label2: 'Campaign 2',
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  data1: [50, 45, 22, 18, 25, 5, 35, 20, 45, 22, 30, 70, 40],
  data2: [40, 30, 60, 30, 35, 50, 10, 30, 25, 28, 55, 65, 80]
}; // earned Today

var earnedToday = {
  color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary,
  label: 'Earn',
  data: [75, 60, 50, 35, 90, 35, 75, 20, 10, 20, 40, 5, 30, 75, 40, 90, 35, 20, 40, 30, 50, 35, 20, 75],
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
}; // items sold

var itemsSold = {
  color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info,
  label: 'Sold',
  data: [75, 60, 50, 35, 90, 35, 75, 20, 10, 20, 40, 5, 30, 75, 40, 90, 35, 20, 40, 30, 50, 35, 20, 75],
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
}; // sales and earning

var salesAndEarning = {
  data: [70, 55, 80, 50, 60, 75, 40, 55, 45, 50, 80, 90, 30],
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
  lineTension: 0,
  color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary
}; // total earning

var totalEarnings = {
  data: [15, 30, 10, 25, 10, 35],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger,
  lineTension: 0.5
}; // net profit

var netProfit = {
  data: [15, 35, 10, 25, 5, 35],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info,
  lineTension: 0.5
}; // total expenses

var totalExpences = {
  data: [30, 10, 25, 14, 35, 35],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success,
  lineTension: 0.5
}; // online revenue

var onlineRevenue = {
  data: [5, 25, 10, 35, 15, 25],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning,
  lineTension: 0.5
}; // ad campaign perfomance data

var adCampaignPerfomanceData = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  lineChartData: {
    label: 'Sales',
    data: [60, 45, 55, 75, 40, 55, 45, 50, 40, 50, 30, 50],
    color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info
  },
  barChartData: {
    label: 'Views',
    data: [80, 55, 80, 75, 50, 50, 40, 50, 50, 50, 70, 50, 10],
    color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].lineChartAxesColor
  },
  barChartData2: {
    label: 'Earned',
    data: [70, 45, 70, 65, 40, 40, 30, 40, 40, 40, 60, 40, 10],
    color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary
  }
}; // profit share

var profitShare = {
  labels: ['Support Tickets', 'Business Events', 'Others'],
  data: [300, 50, 20],
  backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
}; // devices share

var devicesShare = {
  labels: ['Website', 'iOS Devices', 'Android Devices'],
  data: [300, 50, 20],
  backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
}; // subscribers

var subscribers = {
  labels: ['Website', 'iOS Devices', 'Android Devices'],
  data: [280, 30, 60],
  backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
};

/***/ })

}]);