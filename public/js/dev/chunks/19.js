(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[19],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_echarts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-echarts */ "./node_modules/vue-echarts/components/ECharts.vue");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! echarts/lib/chart/pie */ "./node_modules/echarts/lib/chart/pie.js");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ECharts: vue_echarts__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      pie: {
        backgroundColor: "transparent",
        avoidLabelOverlap: false,
        color: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.danger, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.warning, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.success],
        series: [{
          name: "Product Sales",
          type: "pie",
          radius: "40%",
          center: ["50%", "50%"],
          data: [{
            value: 700,
            name: "Product A"
          }, {
            value: 300,
            name: "Product B"
          }, {
            value: 247,
            name: "Product C"
          }, {
            value: 379,
            name: "Product E"
          }],
          label: {
            normal: {
              textStyle: {
                color: "rgba(0, 0, 0, 1)"
              }
            }
          },
          itemStyle: {
            normal: {
              shadowColor: "rgba(0, 0, 0, 0.5)"
            }
          },
          animationType: "scale",
          animationEasing: "elasticOut",
          animationDelay: function animationDelay() {
            return Math.random() * 20;
          }
        }]
      },
      media: [{
        // query: {
        //   maxWidth: 650
        // },
        option: {
          legend: {
            display: true,
            right: 10,
            top: '15%',
            orient: 'vertical'
          },
          series: [{
            radius: [20, '50%'],
            center: ['50%', '50%']
          }, {
            radius: [30, '50%'],
            center: ['50%', '75%']
          }]
        }
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      currencyDefault: 'Bitcoin',
      currency: ['Bitcoin', 'Ethereum', 'EOS', 'Litecoin'],
      paymentMethods: ['Debit Card', 'PayPal', 'Bank Transfer', 'Credit Cards'],
      paymentDefault: "Debit Card"
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyOrSell.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BuyOrSell.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartV4__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartV4 */ "./resources/js/components/Charts/LineChartV4.js");
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    LineChartV4: Components_Charts_LineChartV4__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      buySellChartData: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_1__["buySellChartData"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      active: null,
      currOne: 'Bitcoin',
      currTwo: 'Ethereum',
      currency: ['Bitcoin', 'Ethereum', 'EOS', 'Litecoin']
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Views/crypto/data.js */ "./resources/js/views/crypto/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      recentTrade: Views_crypto_data_js__WEBPACK_IMPORTED_MODULE_0__["recentTrade"],
      loader: false,
      invoice: [],
      headers: [{
        text: "Currency",
        sortable: false,
        value: "currency"
      }, {
        text: "Status",
        sortable: false,
        value: "status"
      }, {
        text: "Price",
        sortable: false,
        value: "price"
      }, {
        text: "Total($)",
        sortable: false,
        value: "total($)"
      }],
      active: null
    };
  },
  mounted: function mounted() {},
  methods: {
    next: function next() {
      var active = parseInt(this.active);
      this.active = active < 2 ? active + 1 : 0;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      showPassword: false,
      currencyDefault: 'Bitcoin',
      currency: ['Bitcoin', 'Ethereum', 'EOS', 'Litecoin']
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      showPassword: false,
      currencyDefault: 'Bitcoin',
      currency: ['Bitcoin', 'Ethereum', 'EOS', 'Litecoin']
    };
  },
  mounted: function mounted() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WalletAddress.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/WalletAddress.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Trade.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crypto/Trade.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Widgets_WalletAddress__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Widgets/WalletAddress */ "./resources/js/components/Widgets/WalletAddress.vue");
/* harmony import */ var Components_Widgets_BuyCryptocurrency__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Widgets/BuyCryptocurrency */ "./resources/js/components/Widgets/BuyCryptocurrency.vue");
/* harmony import */ var Components_Widgets_SellCryptocurrency__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Widgets/SellCryptocurrency */ "./resources/js/components/Widgets/SellCryptocurrency.vue");
/* harmony import */ var Components_Widgets_TransferCryptocurrency__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/TransferCryptocurrency */ "./resources/js/components/Widgets/TransferCryptocurrency.vue");
/* harmony import */ var Components_Widgets_ExchangeRate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/ExchangeRate */ "./resources/js/components/Widgets/ExchangeRate.vue");
/* harmony import */ var Components_Widgets_RecentTrades__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/RecentTrades */ "./resources/js/components/Widgets/RecentTrades.vue");
/* harmony import */ var Components_Widgets_BuyOrSell__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/BuyOrSell */ "./resources/js/components/Widgets/BuyOrSell.vue");
/* harmony import */ var Components_Charts_PieChartWithLegend__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/Charts/PieChartWithLegend */ "./resources/js/components/Charts/PieChartWithLegend.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    WalletAddress: Components_Widgets_WalletAddress__WEBPACK_IMPORTED_MODULE_0__["default"],
    BuyCryptocurrency: Components_Widgets_BuyCryptocurrency__WEBPACK_IMPORTED_MODULE_1__["default"],
    SellCryptocurrency: Components_Widgets_SellCryptocurrency__WEBPACK_IMPORTED_MODULE_2__["default"],
    TransferCryptocurrency: Components_Widgets_TransferCryptocurrency__WEBPACK_IMPORTED_MODULE_3__["default"],
    ExchangeRate: Components_Widgets_ExchangeRate__WEBPACK_IMPORTED_MODULE_4__["default"],
    RecentTrades: Components_Widgets_RecentTrades__WEBPACK_IMPORTED_MODULE_5__["default"],
    BuyOrSell: Components_Widgets_BuyOrSell__WEBPACK_IMPORTED_MODULE_6__["default"],
    ExpenditureStats: Components_Charts_PieChartWithLegend__WEBPACK_IMPORTED_MODULE_7__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/*#salesChart > :first-child{\n  width: 100% !important; \n}\n\n#salesChart > :first-child :first-child{\n  width: 100% !important; \n}*/\n\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("ECharts", {
        staticStyle: { width: "100%", height: "350px" },
        attrs: { options: _vm.pie, id: "salesChart" }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            attach: "",
            label: "Select",
            "prepend-icon": "zmdi zmdi-money-box",
            outlined: ""
          },
          model: {
            value: _vm.currencyDefault,
            callback: function($$v) {
              _vm.currencyDefault = $$v
            },
            expression: "currencyDefault"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-select", {
          attrs: {
            items: _vm.paymentMethods,
            attach: "",
            label: "Select",
            "prepend-icon": "zmdi zmdi-card",
            outlined: ""
          },
          model: {
            value: _vm.paymentDefault,
            callback: function($$v) {
              _vm.paymentDefault = $$v
            },
            expression: "paymentDefault"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            label: "Select Amount",
            value: "200",
            type: "number",
            min: "1",
            outlined: ""
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "Wallet Address",
            "prepend-inner-icon": "cc BTC-alt mr-2",
            value: "AXB35H24ISDJHCISDT"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "d-inline-flex align-items-center" }, [
      _c(
        "div",
        [_c("v-btn", { staticClass: "primary ml-0" }, [_vm._v("Purchase")])],
        1
      ),
      _vm._v(" "),
      _vm._m(1)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mb-2" }, [
      _c("h5", { staticClass: "success--text fw-normal mb-0" }, [
        _vm._v("Total amount is 200 $")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text fw-normal" }, [
        _vm._v("Transaction successfull")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyOrSell.vue?vue&type=template&id=22163220&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BuyOrSell.vue?vue&type=template&id=22163220& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "mb-4" },
      [
        _c("v-btn", { staticClass: "ml-0 ma-2", attrs: { color: "primary" } }, [
          _vm._v("This Month")
        ]),
        _vm._v(" "),
        _c("v-btn", { staticClass: "ma-2", attrs: { color: "error" } }, [
          _vm._v("This Year")
        ]),
        _vm._v(" "),
        _c("v-btn", { staticClass: "ma-2", attrs: { color: "info" } }, [
          _vm._v("Over all")
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("line-chart-v4", {
          attrs: {
            width: 650,
            height: 400,
            dataSet: _vm.buySellChartData.dataAndLabel,
            labels: _vm.buySellChartData.buySellChartLabel,
            chartColorsData: _vm.buySellChartData.buySellChartColors,
            label: _vm.buySellChartData.dataAndLabel
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "mb-6" },
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            "menu-props": "auto",
            label: "Select",
            "hide-details": "",
            "prepend-icon": "zmdi zmdi-money-box",
            "single-line": "",
            outlined: ""
          },
          model: {
            value: _vm.currOne,
            callback: function($$v) {
              _vm.currOne = $$v
            },
            expression: "currOne"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "mb-6" },
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            "menu-props": "auto",
            label: "Select",
            "hide-details": "",
            "prepend-icon": "zmdi zmdi-money-box",
            "single-line": "",
            outlined: ""
          },
          model: {
            value: _vm.currTwo,
            callback: function($$v) {
              _vm.currTwo = $$v
            },
            expression: "currTwo"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(3)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("label", [_vm._v("Choose Currency")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center mb-6" }, [
      _c("i", { staticClass: "ti-exchange-vertical font-2x" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("label", [_vm._v("Choose Currency")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text fw-normal" }, [
        _vm._v("1 BTC = 0.45373 ETC")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("v-card", { attrs: { flat: "" } }, [
        _c(
          "div",
          { staticClass: "table-responsive" },
          [
            _c("app-section-loader", { attrs: { status: _vm.loader } }),
            _vm._v(" "),
            _c("v-data-table", {
              staticClass: "mb-4",
              attrs: {
                headers: _vm.headers,
                items: _vm.recentTrade,
                "hide-default-footer": ""
              },
              scopedSlots: _vm._u([
                {
                  key: "item",
                  fn: function(ref) {
                    var item = ref.item
                    return [
                      _c("tr", [
                        _c("td", [_c("i", { class: item.currencyIcon })]),
                        _vm._v(" "),
                        _c("td", { class: item.statusClass }, [
                          _vm._v(_vm._s(item.status))
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.price))]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(item.total))])
                      ])
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("div", { staticClass: "stats-summary mb-2" }, [
              _c("div", [
                _vm._v("\n               Overall Profit:\n               "),
                _c("span", { staticClass: "success--text" }, [
                  _c("i", { staticClass: "zmdi zmdi-plus" }),
                  _vm._v(" \n                  $35237\n               ")
                ])
              ]),
              _vm._v(" "),
              _c("div", [
                _vm._v("\n               Overall Loss:\n               "),
                _c("span", { staticClass: "error--text" }, [
                  _c("i", { staticClass: "zmdi zmdi-minus" }),
                  _vm._v("\n                  $20\n               ")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("v-btn", { staticClass: "primary m-0" }, [
              _vm._v("Download CSV")
            ])
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=template&id=08c13822&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=template&id=08c13822& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            attach: "",
            label: "Choose Currency",
            "prepend-icon": "zmdi zmdi-money-box",
            outlined: ""
          },
          model: {
            value: _vm.currencyDefault,
            callback: function($$v) {
              _vm.currencyDefault = $$v
            },
            expression: "currencyDefault"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            label: "Select Amount",
            value: "201",
            type: "number",
            min: "1",
            outlined: ""
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            "append-icon": _vm.showPassword ? "visibility" : "visibility_off",
            type: _vm.showPassword ? "text" : "password",
            label: "Password",
            "prepend-icon": "ti-shield",
            outlined: ""
          },
          on: {
            "click:append": function($event) {
              _vm.showPassword = !_vm.showPassword
            }
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "Wallet Address",
            "prepend-inner-icon": "cc BTC-alt mr-2",
            value: "AXB35H24ISDJHCISDT"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "d-inline-flex align-items-center" }, [
      _c(
        "div",
        { staticClass: "mr-2" },
        [_c("v-btn", { staticClass: "primary ml-0" }, [_vm._v("Sell")])],
        1
      ),
      _vm._v(" "),
      _vm._m(1)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text mb-2 fw-normal" }, [
        _vm._v("Your Account will be credited with 200 $")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text fw-normal" }, [
        _vm._v("Transaction successfull")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=template&id=21e69270&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=template&id=21e69270& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      [
        _c("v-select", {
          attrs: {
            items: _vm.currency,
            attach: "",
            label: "Choose Currency",
            "prepend-icon": "zmdi zmdi-money-box",
            outlined: ""
          },
          model: {
            value: _vm.currencyDefault,
            callback: function($$v) {
              _vm.currencyDefault = $$v
            },
            expression: "currencyDefault"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            label: "Select Amount",
            value: "201",
            type: "number",
            outlined: "",
            min: "1"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            "append-icon": _vm.showPassword ? "visibility" : "visibility_off",
            type: _vm.showPassword ? "text" : "password",
            label: "Password",
            "prepend-icon": "ti-shield",
            outlined: ""
          },
          on: {
            "click:append": function($event) {
              _vm.showPassword = !_vm.showPassword
            }
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "Send To",
            "prepend-inner-icon": "cc BTC-alt mr-2"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "d-inline-flex align-items-center" }, [
      _c(
        "div",
        { staticClass: "mr-2" },
        [_c("v-btn", { staticClass: "primary ml-0" }, [_vm._v("Transfer")])],
        1
      ),
      _vm._v(" "),
      _vm._m(1)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text mb-2 fw-normal" }, [
        _vm._v("Total amount transfered 200 $")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text fw-normal" }, [
        _vm._v("Transaction successfull")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WalletAddress.vue?vue&type=template&id=a6d6d940&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/WalletAddress.vue?vue&type=template&id=a6d6d940& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "Bitcoin",
            "prepend-inner-icon": "cc BTC-alt mr-2",
            value: "AXB35H24ISDJHCISDT"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "Ethereum",
            "prepend-inner-icon": "cc ETH-alt mr-2",
            value: "YXB35H24ISDJHCISDT"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "Litecoin",
            "prepend-inner-icon": "cc LTC-alt mr-2",
            value: "TXB35H24ISDJHCISDT"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("v-text-field", {
          attrs: {
            outlined: "",
            label: "ZCash",
            "prepend-inner-icon": "cc ZEC-alt mr-2",
            value: "XXB35H24ISDJHCISDT"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      [_c("v-btn", { staticClass: "primary ml-0" }, [_vm._v("Go to Wallet")])],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h5", { staticClass: "success--text mb-2 fw-normal" }, [
        _vm._v("Total amount in all wallet is 200 $")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Trade.vue?vue&type=template&id=216ddbf5&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crypto/Trade.vue?vue&type=template&id=216ddbf5& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0", attrs: { fluid: "" } },
        [
          _c("crypto-slider"),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.walletAddress"),
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("wallet-address")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.buyCryptocurrency"),
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("buy-cryptocurrency")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.expenditureStats"),
                    colClasses: "col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("expenditure-stats")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.sellCryptocurrency"),
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("sell-cryptocurrency")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.transferCryptocurrency"),
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("transfer-cryptocurrency")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.exchangeRate"),
                    colClasses: "col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("exchange-rate")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.buyOrSell"),
                    colClasses: "col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("buy-or-sell")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.recentTrades"),
                    colClasses: "col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("recent-trades")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/LineChartV4.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/Charts/LineChartV4.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// News Letter Campaign Widget


var lineTension = lineTension;
var borderWidth = 3;
var pointRadius = 2;
var pointBorderWidth = 2;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    dataSet: {
      type: Array
    },
    label: {
      type: Array,
      "default": function _default() {
        return 'Subscribed';
      }
    },
    labels: {
      type: Array
    },
    chartColorsData: {
      type: Array
    }
  },
  data: function data() {
    return {
      gradient1: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
      gradient2: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: true
            },
            ticks: {
              stepSize: 25,
              // padding: 5
              display: true
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              // padding: 10
              display: true
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    // console.log("this.dataSet="+this.dataSet)
    // console.log("this.label="+this.label)
    this.renderChart({
      labels: this.labels,
      datasets: [{
        label: this.label[0].label,
        lineTension: lineTension,
        borderColor: this.chartColorsData[0].borderColor,
        pointBorderColor: this.chartColorsData[0].borderColor,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.dataSet[0].data
      }, {
        label: this.label[1].label,
        lineTension: lineTension,
        borderColor: this.chartColorsData[1].borderColor,
        pointBorderColor: this.chartColorsData[1].borderColor,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.dataSet[1].data
      }, {
        label: this.label[2].label,
        lineTension: lineTension,
        borderColor: this.chartColorsData[2].borderColor,
        pointBorderColor: this.chartColorsData[2].borderColor,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.dataSet[2].data
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& */ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&");
/* harmony import */ var _PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PieChartWithLegend.vue?vue&type=script&lang=js& */ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& */ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "c2d66ef8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Charts/PieChartWithLegend.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BuyCryptocurrency.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/BuyCryptocurrency.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BuyCryptocurrency_vue_vue_type_template_id_bc0a1ec6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6& */ "./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6&");
/* harmony import */ var _BuyCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BuyCryptocurrency.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BuyCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BuyCryptocurrency_vue_vue_type_template_id_bc0a1ec6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BuyCryptocurrency_vue_vue_type_template_id_bc0a1ec6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BuyCryptocurrency.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BuyCryptocurrency.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyCryptocurrency_vue_vue_type_template_id_bc0a1ec6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyCryptocurrency.vue?vue&type=template&id=bc0a1ec6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyCryptocurrency_vue_vue_type_template_id_bc0a1ec6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyCryptocurrency_vue_vue_type_template_id_bc0a1ec6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BuyOrSell.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/BuyOrSell.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BuyOrSell_vue_vue_type_template_id_22163220___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BuyOrSell.vue?vue&type=template&id=22163220& */ "./resources/js/components/Widgets/BuyOrSell.vue?vue&type=template&id=22163220&");
/* harmony import */ var _BuyOrSell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BuyOrSell.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/BuyOrSell.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BuyOrSell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BuyOrSell_vue_vue_type_template_id_22163220___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BuyOrSell_vue_vue_type_template_id_22163220___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BuyOrSell.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BuyOrSell.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Widgets/BuyOrSell.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyOrSell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BuyOrSell.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyOrSell.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyOrSell_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/BuyOrSell.vue?vue&type=template&id=22163220&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BuyOrSell.vue?vue&type=template&id=22163220& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyOrSell_vue_vue_type_template_id_22163220___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BuyOrSell.vue?vue&type=template&id=22163220& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BuyOrSell.vue?vue&type=template&id=22163220&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyOrSell_vue_vue_type_template_id_22163220___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BuyOrSell_vue_vue_type_template_id_22163220___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeRate.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeRate.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExchangeRate.vue?vue&type=template&id=4d9ad88e& */ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&");
/* harmony import */ var _ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExchangeRate.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ExchangeRate.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExchangeRate.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExchangeRate.vue?vue&type=template&id=4d9ad88e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ExchangeRate.vue?vue&type=template&id=4d9ad88e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExchangeRate_vue_vue_type_template_id_4d9ad88e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/RecentTrades.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/RecentTrades.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecentTrades.vue?vue&type=template&id=2ea9e535& */ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&");
/* harmony import */ var _RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecentTrades.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/RecentTrades.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentTrades.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentTrades.vue?vue&type=template&id=2ea9e535& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentTrades.vue?vue&type=template&id=2ea9e535&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentTrades_vue_vue_type_template_id_2ea9e535___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SellCryptocurrency.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/Widgets/SellCryptocurrency.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SellCryptocurrency_vue_vue_type_template_id_08c13822___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SellCryptocurrency.vue?vue&type=template&id=08c13822& */ "./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=template&id=08c13822&");
/* harmony import */ var _SellCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SellCryptocurrency.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SellCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SellCryptocurrency_vue_vue_type_template_id_08c13822___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SellCryptocurrency_vue_vue_type_template_id_08c13822___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SellCryptocurrency.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SellCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SellCryptocurrency.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SellCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=template&id=08c13822&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=template&id=08c13822& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SellCryptocurrency_vue_vue_type_template_id_08c13822___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SellCryptocurrency.vue?vue&type=template&id=08c13822& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SellCryptocurrency.vue?vue&type=template&id=08c13822&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SellCryptocurrency_vue_vue_type_template_id_08c13822___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SellCryptocurrency_vue_vue_type_template_id_08c13822___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TransferCryptocurrency.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/Widgets/TransferCryptocurrency.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TransferCryptocurrency_vue_vue_type_template_id_21e69270___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TransferCryptocurrency.vue?vue&type=template&id=21e69270& */ "./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=template&id=21e69270&");
/* harmony import */ var _TransferCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TransferCryptocurrency.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TransferCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TransferCryptocurrency_vue_vue_type_template_id_21e69270___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TransferCryptocurrency_vue_vue_type_template_id_21e69270___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TransferCryptocurrency.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TransferCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TransferCryptocurrency.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TransferCryptocurrency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=template&id=21e69270&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=template&id=21e69270& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TransferCryptocurrency_vue_vue_type_template_id_21e69270___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TransferCryptocurrency.vue?vue&type=template&id=21e69270& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TransferCryptocurrency.vue?vue&type=template&id=21e69270&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TransferCryptocurrency_vue_vue_type_template_id_21e69270___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TransferCryptocurrency_vue_vue_type_template_id_21e69270___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/WalletAddress.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/WalletAddress.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _WalletAddress_vue_vue_type_template_id_a6d6d940___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WalletAddress.vue?vue&type=template&id=a6d6d940& */ "./resources/js/components/Widgets/WalletAddress.vue?vue&type=template&id=a6d6d940&");
/* harmony import */ var _WalletAddress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WalletAddress.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/WalletAddress.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _WalletAddress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _WalletAddress_vue_vue_type_template_id_a6d6d940___WEBPACK_IMPORTED_MODULE_0__["render"],
  _WalletAddress_vue_vue_type_template_id_a6d6d940___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/WalletAddress.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/WalletAddress.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/WalletAddress.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_WalletAddress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./WalletAddress.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WalletAddress.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_WalletAddress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/WalletAddress.vue?vue&type=template&id=a6d6d940&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/WalletAddress.vue?vue&type=template&id=a6d6d940& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WalletAddress_vue_vue_type_template_id_a6d6d940___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./WalletAddress.vue?vue&type=template&id=a6d6d940& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WalletAddress.vue?vue&type=template&id=a6d6d940&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WalletAddress_vue_vue_type_template_id_a6d6d940___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WalletAddress_vue_vue_type_template_id_a6d6d940___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/crypto/Trade.vue":
/*!*********************************************!*\
  !*** ./resources/js/views/crypto/Trade.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Trade_vue_vue_type_template_id_216ddbf5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Trade.vue?vue&type=template&id=216ddbf5& */ "./resources/js/views/crypto/Trade.vue?vue&type=template&id=216ddbf5&");
/* harmony import */ var _Trade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Trade.vue?vue&type=script&lang=js& */ "./resources/js/views/crypto/Trade.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Trade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Trade_vue_vue_type_template_id_216ddbf5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Trade_vue_vue_type_template_id_216ddbf5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/crypto/Trade.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/crypto/Trade.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/crypto/Trade.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Trade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Trade.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Trade.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Trade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/crypto/Trade.vue?vue&type=template&id=216ddbf5&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/crypto/Trade.vue?vue&type=template&id=216ddbf5& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Trade_vue_vue_type_template_id_216ddbf5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Trade.vue?vue&type=template&id=216ddbf5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crypto/Trade.vue?vue&type=template&id=216ddbf5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Trade_vue_vue_type_template_id_216ddbf5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Trade_vue_vue_type_template_id_216ddbf5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);