(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[20],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_echarts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-echarts */ "./node_modules/vue-echarts/components/ECharts.vue");
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! echarts/lib/chart/bar */ "./node_modules/echarts/lib/chart/bar.js");
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "buyers-stats",
  components: {
    ECharts: vue_echarts__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      bar: {
        tooltip: {
          trigger: "axis"
        },
        color: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.info],
        legend: {
          data: ["Series A"]
        },
        xAxis: {
          type: "category",
          boundaryGap: true,
          data: ["Project 1", "Project 2", "Project 3", "Project 4"]
        },
        yAxis: {
          type: "value",
          axisLabel: {
            formatter: "{value}"
          }
        },
        series: [{
          name: "Series A",
          type: "bar",
          data: [400, 700, 1400, 900]
        }]
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_echarts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-echarts */ "./node_modules/vue-echarts/components/ECharts.vue");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! echarts/lib/chart/pie */ "./node_modules/echarts/lib/chart/pie.js");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ECharts: vue_echarts__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      pie: {
        backgroundColor: "transparent",
        avoidLabelOverlap: false,
        color: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.danger, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.warning, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.success],
        series: [{
          name: "Product Sales",
          type: "pie",
          radius: "40%",
          center: ["50%", "50%"],
          data: [{
            value: 700,
            name: "Product A"
          }, {
            value: 300,
            name: "Product B"
          }, {
            value: 247,
            name: "Product C"
          }, {
            value: 379,
            name: "Product E"
          }],
          label: {
            normal: {
              textStyle: {
                color: "rgba(0, 0, 0, 1)"
              }
            }
          },
          itemStyle: {
            normal: {
              shadowColor: "rgba(0, 0, 0, 0.5)"
            }
          },
          animationType: "scale",
          animationEasing: "elasticOut",
          animationDelay: function animationDelay() {
            return Math.random() * 20;
          }
        }]
      },
      media: [{
        // query: {
        //   maxWidth: 650
        // },
        option: {
          legend: {
            display: true,
            right: 10,
            top: '15%',
            orient: 'vertical'
          },
          series: [{
            radius: [20, '50%'],
            center: ['50%', '50%']
          }, {
            radius: [30, '50%'],
            center: ['50%', '75%']
          }]
        }
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["title", "viewer", "trade", "dataSet", "icon", "color", "labels", "extraClass", "bgColor"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NotificationsCard.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/NotificationsCard.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      notifications: [{
        color: "#5d92f4",
        content: "Site goes is down for 6 hours due to maintainance and bug fixing.Please Check"
      }, {
        color: "#00d0bd",
        content: "New users from March is promoted as special benefit under promotional offer of 30%."
      }, {
        color: "#ff3739",
        content: "Bug detected from the development team at the cart module of Fashion store."
      }],
      settings: {
        maxScrollbarLength: 150
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/OngoingProjects.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/OngoingProjects.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      ongoingProjects: [{
        id: '1',
        supervisor: 'John Gena',
        duration: '3 Weeks',
        netWorth: '$2364378',
        email: 'support@theironnetwork.org',
        phone: '+01 3456 25378'
      }],
      valueDeterminate: 30,
      settings: {
        maxScrollbarLength: 160
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import api from "Api";
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      progressStatus: [{
        id: '1',
        status: 'Completed',
        btnStatus: "success",
        valueDeterminate: 60
      }, {
        id: '2',
        status: 'Pending',
        btnStatus: "error",
        valueDeterminate: 40
      }, {
        id: '3',
        status: 'Ongoing',
        btnStatus: "warning",
        valueDeterminate: 20
      }, {
        id: '4',
        status: 'Completed',
        btnStatus: "info",
        valueDeterminate: 90
      }],
      settings: {
        maxScrollbarLength: 160
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import api from "Api";
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: false,
      upcomingEvents: [{
        title: 'Marketing Seminar',
        dateAndCity: '28th April, Mumbai',
        label: 'Email'
      }, {
        title: 'Strategy Planning',
        dateAndCity: '22th May, Delhi',
        label: 'Phone'
      }, {
        title: 'Hiring Personals',
        dateAndCity: '29th May, Delhi',
        label: 'Skype'
      }, {
        title: 'Training',
        dateAndCity: '30th May, Delhi',
        label: 'Email'
      }],
      settings: {
        maxScrollbarLength: 60
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/Dashboard.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crm/Dashboard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Widgets_UpcomingEvents__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components//Widgets/UpcomingEvents */ "./resources/js/components/Widgets/UpcomingEvents.vue");
/* harmony import */ var Components_Widgets_OngoingProjects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components//Widgets/OngoingProjects */ "./resources/js/components/Widgets/OngoingProjects.vue");
/* harmony import */ var Components_Widgets_ProjectStatusV2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components//Widgets/ProjectStatusV2 */ "./resources/js/components/Widgets/ProjectStatusV2.vue");
/* harmony import */ var Components_Widgets_Chat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/Chat */ "./resources/js/components/Widgets/Chat.vue");
/* harmony import */ var Components_Widgets_NotificationsCard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/NotificationsCard */ "./resources/js/components/Widgets/NotificationsCard.vue");
/* harmony import */ var Components_Widgets_TabsAndTable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/TabsAndTable */ "./resources/js/components/Widgets/TabsAndTable.vue");
/* harmony import */ var Components_Charts_LineChartV3__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Charts/LineChartV3 */ "./resources/js/components/Charts/LineChartV3.js");
/* harmony import */ var Components_StatsCardV6_StatsCardV6__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/StatsCardV6/StatsCardV6 */ "./resources/js/components/StatsCardV6/StatsCardV6.vue");
/* harmony import */ var Components_Charts_PieChartWithLegend__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Components/Charts/PieChartWithLegend */ "./resources/js/components/Charts/PieChartWithLegend.vue");
/* harmony import */ var Components_Charts_ColumnChartWithImages__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Components/Charts/ColumnChartWithImages */ "./resources/js/components/Charts/ColumnChartWithImages.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//










/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    UpcomingEvents: Components_Widgets_UpcomingEvents__WEBPACK_IMPORTED_MODULE_0__["default"],
    OngoingProjects: Components_Widgets_OngoingProjects__WEBPACK_IMPORTED_MODULE_1__["default"],
    ProjectStatusV2: Components_Widgets_ProjectStatusV2__WEBPACK_IMPORTED_MODULE_2__["default"],
    Chat: Components_Widgets_Chat__WEBPACK_IMPORTED_MODULE_3__["default"],
    NotificationsCard: Components_Widgets_NotificationsCard__WEBPACK_IMPORTED_MODULE_4__["default"],
    TabsAndTable: Components_Widgets_TabsAndTable__WEBPACK_IMPORTED_MODULE_5__["default"],
    LineChartV3: Components_Charts_LineChartV3__WEBPACK_IMPORTED_MODULE_6__["default"],
    StatsCardV6: Components_StatsCardV6_StatsCardV6__WEBPACK_IMPORTED_MODULE_7__["default"],
    Sales: Components_Charts_PieChartWithLegend__WEBPACK_IMPORTED_MODULE_8__["default"],
    ProjectStatusV3: Components_Charts_ColumnChartWithImages__WEBPACK_IMPORTED_MODULE_9__["default"]
  },
  data: function data() {
    return {
      blog: {
        id: 3,
        thumbnail: "/static/img/blog-3.jpg",
        title: "lorem ipsum is simply dummy text",
        body: "Consectetur adipisicing elit. Ullam expedita, necessitatibus sit exercitationem aut quo quos inventore similique nulla minima distinctio illo iste dignissimos vero nostrum, magni pariatur delectus natus.",
        date: "1-jun-2018"
      },
      visitors: {
        card_color: "primary",
        icon: "zmdi zmdi-account-add",
        title: "Visitors",
        viewer: "+ 41",
        trade: "30",
        chartLabel: ['A', 'B', 'C', 'D', 'E'],
        label: "Visitors",
        bgColor: "primary",
        chartData: [30, 5, 26, 10, 30]
      },
      revenue: {
        card_color: "success",
        icon: "zmdi zmdi-money-box",
        title: "Revenue",
        viewer: "+ 4381",
        trade: "60",
        chartLabel: ['A', 'B', 'C', 'D', 'E'],
        label: "Revenue",
        chartData: [1, 26, 8, 22, 1]
      },
      sales: {
        card_color: "warning",
        icon: "zmdi zmdi-shopping-cart",
        title: "Sales",
        viewer: "+ 2611",
        trade: "80",
        chartLabel: ['A', 'B', 'C', 'D', 'E'],
        label: "Sales",
        chartData: [30, 5, 26, 10, 30]
      },
      deals: {
        card_color: "error",
        icon: "ti-thumb-up",
        title: "Deals",
        viewer: "+ 611",
        trade: "40",
        chartLabel: ['A', 'B', 'C', 'D', 'E'],
        label: "Deals",
        chartData: [1, 26, 8, 22, 1]
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n#barChart[data-v-1482b8b7] > :first-child{\n  width: 100% !important;\n}\n#barChart[data-v-1482b8b7] > :first-child :first-child{\n  width: 100% !important;\n}\n\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/*#salesChart > :first-child{\n  width: 100% !important; \n}\n\n#salesChart > :first-child :first-child{\n  width: 100% !important; \n}*/\n\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("ECharts", {
    staticStyle: { width: "100%", height: "400px" },
    attrs: { options: _vm.bar, id: "barChart" }
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("ECharts", {
        staticStyle: { width: "100%", height: "350px" },
        attrs: { options: _vm.pie, id: "salesChart" }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=template&id=500c17d8&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=template&id=500c17d8& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { attrs: { customClasses: "magazine-stats-card crm-stats-card" } },
    [
      _c("div", { staticClass: "pa-6 white--text", class: _vm.bgColor }, [
        _c("div", { staticClass: "d-custom-flex align-items-center mb-2" }, [
          _c("span", { staticClass: "d-inline-block font-2x mr-2" }, [
            _c("i", { class: _vm.icon })
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "d-inline-block font-lg fw-bold" }, [
            _vm._v(_vm._s(_vm.title))
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "d-custom-flex align-items-center justify-space-between"
          },
          [_vm._t("default")],
          2
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "d-custom-flex align-items-center justify-space-between"
          },
          [
            _c("span", [_vm._v(_vm._s(_vm.viewer))]),
            _vm._v(" "),
            _c("span", [
              _vm._v(
                _vm._s(_vm.$t("message.trade")) +
                  " : " +
                  _vm._s(_vm.trade) +
                  " %"
              )
            ])
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NotificationsCard.vue?vue&type=template&id=7953b8bd&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/NotificationsCard.vue?vue&type=template&id=7953b8bd& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "notification-card-content" },
    _vm._l(_vm.notifications, function(notification, index) {
      return _c(
        "div",
        { key: index },
        [
          _c(
            "v-card",
            { attrs: { color: notification.color, dark: "" } },
            [
              _c("v-card-text", { staticClass: "v-input__prepend-outer" }, [
                _vm._v("\n\t\t\t\t" + _vm._s(notification.content) + "\n\t\t\t")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("br")
        ],
        1
      )
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/OngoingProjects.vue?vue&type=template&id=0a745a7a&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/OngoingProjects.vue?vue&type=template&id=0a745a7a& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "310px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            { staticClass: "card-list top-selling ongoing-projects" },
            _vm._l(_vm.ongoingProjects, function(project, index) {
              return _c(
                "v-list-item",
                { key: index, attrs: { ripple: "" } },
                [
                  _c(
                    "v-list-item-content",
                    [
                      _c(
                        "v-list-item-subtitle",
                        [
                          _c("h4", { staticClass: "mb-4" }, [
                            _vm._v("Project " + _vm._s(project.id))
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "mb-4" }, [
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center" },
                              [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-inline-block mb-2 fw-bold w-50"
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-account-circle v-input__prepend-outer"
                                    }),
                                    _vm._v(
                                      " \n                           " +
                                        _vm._s(_vm.$t("message.supervisor")) +
                                        " :\n                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "mb-2 w-50 text-truncate" },
                                  [_vm._v(_vm._s(project.supervisor))]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center" },
                              [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-inline-block mb-2 fw-bold w-50"
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-time v-input__prepend-outer"
                                    }),
                                    _vm._v(
                                      " \n                           " +
                                        _vm._s(_vm.$t("message.duration")) +
                                        " :\n                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "mb-2 w-50 text-truncate" },
                                  [_vm._v(_vm._s(project.duration))]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center" },
                              [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-inline-block mb-2 fw-bold w-50"
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-money v-input__prepend-outer"
                                    }),
                                    _vm._v(
                                      " \n                           " +
                                        _vm._s(_vm.$t("message.netWorth")) +
                                        " :\n                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "mb-2 w-50 text-truncate" },
                                  [_vm._v(_vm._s(project.netWorth))]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center" },
                              [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-inline-block mb-2 fw-bold w-50"
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-email v-input__prepend-outer"
                                    }),
                                    _vm._v(
                                      " \n                           " +
                                        _vm._s(_vm.$t("message.email")) +
                                        " : \n                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "mb-2 w-50 text-truncate" },
                                  [_vm._v(_vm._s(project.email))]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center" },
                              [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-inline-block mb-2 fw-bold w-50"
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-phone v-input__prepend-outer"
                                    }),
                                    _vm._v(
                                      " \n                           " +
                                        _vm._s(_vm.$t("message.phone")) +
                                        " :\n                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "mb-2 w-50 text-truncate" },
                                  [_vm._v(_vm._s(project.phone))]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "d-custom-flex justify-space-between align-items-center mb-4"
                            },
                            [
                              _c("span", { staticClass: "fw-bold" }, [
                                _vm._v("Progress :")
                              ]),
                              _vm._v(" "),
                              _c("span", [
                                _vm._v(_vm._s(_vm.valueDeterminate) + "%")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c("v-progress-linear", {
                            staticClass: "my-0",
                            attrs: { color: "primary", height: "7" },
                            model: {
                              value: _vm.valueDeterminate,
                              callback: function($$v) {
                                _vm.valueDeterminate = $$v
                              },
                              expression: "valueDeterminate"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            }),
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=template&id=4dc584ac&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=template&id=4dc584ac& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "310px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            { staticClass: "card-list top-selling project-status" },
            _vm._l(_vm.progressStatus, function(project, index) {
              return _c(
                "v-list-item",
                { key: index, attrs: { ripple: "" } },
                [
                  _c(
                    "v-list-item-content",
                    { staticClass: "py-1" },
                    [
                      _c(
                        "v-list-item-subtitle",
                        [
                          _c(
                            "div",
                            {
                              staticClass:
                                "d-custom-flex justify-space-between align-items-center mb-4"
                            },
                            [
                              _c("div", [
                                _c("span", { staticClass: "fw-bold" }, [
                                  _vm._v("Project " + _vm._s(project.id))
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                [
                                  _c(
                                    "v-badge",
                                    {
                                      class: project.btnStatus,
                                      attrs: { value: false }
                                    },
                                    [_vm._v(_vm._s(project.status))]
                                  )
                                ],
                                1
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("v-progress-linear", {
                            staticClass: "my-0",
                            attrs: { color: "primary", height: "7" },
                            model: {
                              value: project.valueDeterminate,
                              callback: function($$v) {
                                _vm.$set(project, "valueDeterminate", $$v)
                              },
                              expression: "project.valueDeterminate"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            }),
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=template&id=50f9db00&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=template&id=50f9db00& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "310px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            { staticClass: "card-list top-selling" },
            _vm._l(_vm.upcomingEvents, function(events, index) {
              return _c(
                "v-list-item",
                { key: index, attrs: { ripple: "" } },
                [
                  _c(
                    "v-list-item-content",
                    { staticClass: "py-1" },
                    [
                      _c("v-list-item-subtitle", [
                        _c("h5", { staticClass: "mb-2" }, [
                          _vm._v(_vm._s(events.title))
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "mb-0 fs-12 grey--text" }, [
                          _vm._v(_vm._s(events.dateAndCity))
                        ])
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-badge",
                    {
                      staticClass: "info text-capitalize",
                      attrs: { value: false }
                    },
                    [_vm._v(_vm._s(events.label))]
                  )
                ],
                1
              )
            }),
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/Dashboard.vue?vue&type=template&id=4172d0d8&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/crm/Dashboard.vue?vue&type=template&id=4172d0d8& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            {
              staticClass: "border-rad-sm overflow-hidden crm-stats-card-wrap"
            },
            [
              _c(
                "stats-card-v6",
                {
                  staticClass:
                    "flex col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12",
                  attrs: {
                    title: _vm.visitors.title,
                    viewer: _vm.visitors.viewer,
                    icon: _vm.visitors.icon,
                    trade: _vm.visitors.trade,
                    bgColor: _vm.visitors.card_color
                  }
                },
                [
                  _c("line-chart-v3", {
                    style: {
                      height: "80px",
                      width: "100%",
                      position: "relative"
                    },
                    attrs: {
                      label: _vm.visitors.label,
                      dataSet: _vm.visitors.chartData,
                      labels: _vm.visitors.chartLabel
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "stats-card-v6",
                {
                  staticClass:
                    "flex col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12",
                  attrs: {
                    title: _vm.revenue.title,
                    viewer: _vm.revenue.viewer,
                    trade: _vm.revenue.trade,
                    icon: _vm.revenue.icon,
                    bgColor: _vm.revenue.card_color
                  }
                },
                [
                  _c("line-chart-v3", {
                    style: {
                      height: "80px",
                      width: "100%",
                      position: "relative"
                    },
                    attrs: {
                      label: _vm.revenue.label,
                      dataSet: _vm.revenue.chartData,
                      labels: _vm.revenue.chartLabel
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "stats-card-v6",
                {
                  staticClass:
                    "flex col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12",
                  attrs: {
                    title: _vm.sales.title,
                    viewer: _vm.sales.viewer,
                    trade: _vm.sales.trade,
                    icon: _vm.sales.icon,
                    bgColor: _vm.sales.card_color
                  }
                },
                [
                  _c("line-chart-v3", {
                    style: {
                      height: "80px",
                      width: "100%",
                      position: "relative"
                    },
                    attrs: {
                      label: _vm.sales.label,
                      dataSet: _vm.sales.chartData,
                      labels: _vm.sales.chartLabel
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "stats-card-v6",
                {
                  staticClass:
                    "flex col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12",
                  attrs: {
                    title: _vm.deals.title,
                    viewer: _vm.deals.viewer,
                    trade: _vm.deals.trade,
                    icon: _vm.deals.icon,
                    bgColor: _vm.deals.card_color
                  }
                },
                [
                  _c("line-chart-v3", {
                    style: {
                      height: "80px",
                      width: "100%",
                      position: "relative"
                    },
                    attrs: {
                      label: _vm.deals.label,
                      dataSet: _vm.deals.chartData,
                      labels: _vm.deals.chartLabel
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.projectStatus"),
                    colClasses: "col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullBlock: true,
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("project-status-v3")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.sales"),
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12",
                    customClasses: "mb-0",
                    contentCustomClass: "text-center",
                    fullBlock: true,
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("sales")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.upcomingEvents"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullBlock: true,
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("upcoming-events")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.ongoingProjects"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullBlock: true,
                    fullScreen: true,
                    reloadable: true,
                    closeable: true
                  }
                },
                [_c("ongoing-projects")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.projectStatus"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12",
                    customClasses: "mb-0",
                    fullBlock: true,
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    footer: false
                  }
                },
                [_c("project-status-v2")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.notifications"),
                    colClasses: "col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12",
                    customClasses: "mb-0 notification-card-wrap",
                    fullBlock: true,
                    fullScreen: true,
                    reloadable: true,
                    closeable: true,
                    footer: true
                  }
                },
                [
                  _c("notifications-card"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "text-center",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-0",
                          attrs: { color: "white", large: "" }
                        },
                        [_vm._v("View All Notifications")]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticClass: "pa-0",
                  attrs: { xl: "8", lg: "8", md: "7", sm: "12", cols: "12" }
                },
                [_c("chat")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses:
                      "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12",
                    customClasses: "mb-0"
                  }
                },
                [_c("tabs-and-table")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/ColumnChartWithImages.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/Charts/ColumnChartWithImages.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ColumnChartWithImages_vue_vue_type_template_id_1482b8b7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true& */ "./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true&");
/* harmony import */ var _ColumnChartWithImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ColumnChartWithImages.vue?vue&type=script&lang=js& */ "./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ColumnChartWithImages_vue_vue_type_style_index_0_id_1482b8b7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css& */ "./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ColumnChartWithImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ColumnChartWithImages_vue_vue_type_template_id_1482b8b7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ColumnChartWithImages_vue_vue_type_template_id_1482b8b7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1482b8b7",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Charts/ColumnChartWithImages.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColumnChartWithImages.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css& ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_style_index_0_id_1482b8b7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=style&index=0&id=1482b8b7&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_style_index_0_id_1482b8b7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_style_index_0_id_1482b8b7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_style_index_0_id_1482b8b7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_style_index_0_id_1482b8b7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_template_id_1482b8b7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/ColumnChartWithImages.vue?vue&type=template&id=1482b8b7&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_template_id_1482b8b7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ColumnChartWithImages_vue_vue_type_template_id_1482b8b7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Charts/LineChartV3.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/Charts/LineChartV3.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// News Letter Campaign Widget


var lineTension = 0.1;
var borderWidth = 3;
var pointRadius = 6;
var pointBorderWidth = 2;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    label: {
      type: String,
      "default": function _default() {
        return "abc";
      }
    },
    dataSet: {
      type: Array,
      "default": function _default() {
        return [30, 5, 26, 10, 30];
      }
    },
    label1: {
      type: String,
      "default": function _default() {
        return 'Subscribed';
      }
    },
    labels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E'];
      }
    }
  },
  data: function data() {
    return {
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              display: false
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              display: false
            }
          }]
        },
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    var self = this;
    this.renderChart({
      labels: this.labels,
      datasets: [{
        label: self.label,
        lineTension: lineTension,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.dataSet
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& */ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&");
/* harmony import */ var _PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PieChartWithLegend.vue?vue&type=script&lang=js& */ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& */ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "c2d66ef8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Charts/PieChartWithLegend.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=style&index=0&id=c2d66ef8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_style_index_0_id_c2d66ef8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Charts/PieChartWithLegend.vue?vue&type=template&id=c2d66ef8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PieChartWithLegend_vue_vue_type_template_id_c2d66ef8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/StatsCardV6/StatsCardV6.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/StatsCardV6/StatsCardV6.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatsCardV6_vue_vue_type_template_id_500c17d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatsCardV6.vue?vue&type=template&id=500c17d8& */ "./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=template&id=500c17d8&");
/* harmony import */ var _StatsCardV6_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatsCardV6.vue?vue&type=script&lang=js& */ "./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StatsCardV6_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StatsCardV6_vue_vue_type_template_id_500c17d8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StatsCardV6_vue_vue_type_template_id_500c17d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StatsCardV6/StatsCardV6.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV6_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV6.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV6_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=template&id=500c17d8&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=template&id=500c17d8& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV6_vue_vue_type_template_id_500c17d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV6.vue?vue&type=template&id=500c17d8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV6/StatsCardV6.vue?vue&type=template&id=500c17d8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV6_vue_vue_type_template_id_500c17d8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV6_vue_vue_type_template_id_500c17d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/NotificationsCard.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/NotificationsCard.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NotificationsCard_vue_vue_type_template_id_7953b8bd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NotificationsCard.vue?vue&type=template&id=7953b8bd& */ "./resources/js/components/Widgets/NotificationsCard.vue?vue&type=template&id=7953b8bd&");
/* harmony import */ var _NotificationsCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NotificationsCard.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/NotificationsCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _NotificationsCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NotificationsCard_vue_vue_type_template_id_7953b8bd___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NotificationsCard_vue_vue_type_template_id_7953b8bd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/NotificationsCard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/NotificationsCard.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/NotificationsCard.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./NotificationsCard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NotificationsCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/NotificationsCard.vue?vue&type=template&id=7953b8bd&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/NotificationsCard.vue?vue&type=template&id=7953b8bd& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsCard_vue_vue_type_template_id_7953b8bd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./NotificationsCard.vue?vue&type=template&id=7953b8bd& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NotificationsCard.vue?vue&type=template&id=7953b8bd&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsCard_vue_vue_type_template_id_7953b8bd___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsCard_vue_vue_type_template_id_7953b8bd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/OngoingProjects.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/Widgets/OngoingProjects.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OngoingProjects_vue_vue_type_template_id_0a745a7a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OngoingProjects.vue?vue&type=template&id=0a745a7a& */ "./resources/js/components/Widgets/OngoingProjects.vue?vue&type=template&id=0a745a7a&");
/* harmony import */ var _OngoingProjects_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OngoingProjects.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/OngoingProjects.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OngoingProjects_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OngoingProjects_vue_vue_type_template_id_0a745a7a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OngoingProjects_vue_vue_type_template_id_0a745a7a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/OngoingProjects.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/OngoingProjects.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/OngoingProjects.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OngoingProjects_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./OngoingProjects.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/OngoingProjects.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OngoingProjects_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/OngoingProjects.vue?vue&type=template&id=0a745a7a&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/OngoingProjects.vue?vue&type=template&id=0a745a7a& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OngoingProjects_vue_vue_type_template_id_0a745a7a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./OngoingProjects.vue?vue&type=template&id=0a745a7a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/OngoingProjects.vue?vue&type=template&id=0a745a7a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OngoingProjects_vue_vue_type_template_id_0a745a7a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OngoingProjects_vue_vue_type_template_id_0a745a7a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ProjectStatusV2.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectStatusV2.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectStatusV2_vue_vue_type_template_id_4dc584ac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectStatusV2.vue?vue&type=template&id=4dc584ac& */ "./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=template&id=4dc584ac&");
/* harmony import */ var _ProjectStatusV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectStatusV2.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectStatusV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectStatusV2_vue_vue_type_template_id_4dc584ac___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectStatusV2_vue_vue_type_template_id_4dc584ac___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProjectStatusV2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatusV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectStatusV2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatusV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=template&id=4dc584ac&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=template&id=4dc584ac& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatusV2_vue_vue_type_template_id_4dc584ac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectStatusV2.vue?vue&type=template&id=4dc584ac& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatusV2.vue?vue&type=template&id=4dc584ac&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatusV2_vue_vue_type_template_id_4dc584ac___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatusV2_vue_vue_type_template_id_4dc584ac___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/UpcomingEvents.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/UpcomingEvents.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UpcomingEvents_vue_vue_type_template_id_50f9db00___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UpcomingEvents.vue?vue&type=template&id=50f9db00& */ "./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=template&id=50f9db00&");
/* harmony import */ var _UpcomingEvents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UpcomingEvents.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UpcomingEvents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UpcomingEvents_vue_vue_type_template_id_50f9db00___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UpcomingEvents_vue_vue_type_template_id_50f9db00___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/UpcomingEvents.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpcomingEvents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./UpcomingEvents.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UpcomingEvents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=template&id=50f9db00&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=template&id=50f9db00& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UpcomingEvents_vue_vue_type_template_id_50f9db00___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UpcomingEvents.vue?vue&type=template&id=50f9db00& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UpcomingEvents.vue?vue&type=template&id=50f9db00&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UpcomingEvents_vue_vue_type_template_id_50f9db00___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UpcomingEvents_vue_vue_type_template_id_50f9db00___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/crm/Dashboard.vue":
/*!**********************************************!*\
  !*** ./resources/js/views/crm/Dashboard.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_4172d0d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=4172d0d8& */ "./resources/js/views/crm/Dashboard.vue?vue&type=template&id=4172d0d8&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/views/crm/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_4172d0d8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_4172d0d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/crm/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/crm/Dashboard.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/crm/Dashboard.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/crm/Dashboard.vue?vue&type=template&id=4172d0d8&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/crm/Dashboard.vue?vue&type=template&id=4172d0d8& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_4172d0d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=4172d0d8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/crm/Dashboard.vue?vue&type=template&id=4172d0d8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_4172d0d8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_4172d0d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);