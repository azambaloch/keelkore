(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"],
      dialog: false
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CoursesDetail.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CoursesDetail.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data */ "./resources/js/views/courses/data.js");
/* harmony import */ var _CourseWidgets_CourseDetailLearn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseWidgets/CourseDetailLearn */ "./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue");
/* harmony import */ var _CourseWidgets_CourseDetailBanner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CourseWidgets/CourseDetailBanner */ "./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue");
/* harmony import */ var _CourseWidgets_CourseDetailDescription__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./CourseWidgets/CourseDetailDescription */ "./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue");
/* harmony import */ var _CourseWidgets_CourseDetailOverview__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./CourseWidgets/CourseDetailOverview */ "./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue");
/* harmony import */ var _CourseWidgets_CourseDetailInstructor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CourseWidgets/CourseDetailInstructor */ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue");
/* harmony import */ var _CourseWidgets_CourseCard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./CourseWidgets/CourseCard */ "./resources/js/views/courses/CourseWidgets/CourseCard.vue");
/* harmony import */ var _CourseWidgets_CourseDetailBilling__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./CourseWidgets/CourseDetailBilling */ "./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  },
  components: {
    CourseDetailBanner: _CourseWidgets_CourseDetailBanner__WEBPACK_IMPORTED_MODULE_2__["default"],
    CourseDetailLearn: _CourseWidgets_CourseDetailLearn__WEBPACK_IMPORTED_MODULE_1__["default"],
    CourseDetailDesciption: _CourseWidgets_CourseDetailDescription__WEBPACK_IMPORTED_MODULE_3__["default"],
    CourseDetailOverview: _CourseWidgets_CourseDetailOverview__WEBPACK_IMPORTED_MODULE_4__["default"],
    CourseDetailInstructor: _CourseWidgets_CourseDetailInstructor__WEBPACK_IMPORTED_MODULE_5__["default"],
    CourseCard: _CourseWidgets_CourseCard__WEBPACK_IMPORTED_MODULE_6__["default"],
    CourseDetailBilling: _CourseWidgets_CourseDetailBilling__WEBPACK_IMPORTED_MODULE_7__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.image-wrap[data-v-feb1d8cc]{\n\twidth:130px;\n}\n.instructor-meta[data-v-feb1d8cc]{\n\twidth:calc(100% - 130px);\n}\n@media (max-width: 600px){\n.instructor-meta[data-v-feb1d8cc]{\n\t\twidth:100%;\n}\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=template&id=b4b645ae&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=template&id=b4b645ae& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "banner-image-wrap banner-detail grey darken-4 ma-0" },
    [
      _c("v-container", { staticClass: "grid-list-xl", attrs: { fluid: "" } }, [
        _c(
          "div",
          { staticClass: "banner-content-wrap" },
          [
            _c(
              "v-row",
              { staticClass: "align-center justify-center fill-height" },
              [
                _c(
                  "v-col",
                  { attrs: { cols: "12", md: "12", lg: "12", xl: "12" } },
                  [
                    _c(
                      "v-row",
                      {
                        staticClass: "align-center justify-center fill-height"
                      },
                      [
                        _c(
                          "v-col",
                          {
                            attrs: {
                              cols: "12",
                              sm: "6",
                              md: "7",
                              lg: "7",
                              xl: "8"
                            }
                          },
                          [
                            _c("h2", { staticClass: "white--text" }, [
                              _vm._v(
                                _vm._s(_vm.CourseData.courseDetail.heading)
                              )
                            ]),
                            _vm._v(" "),
                            _c("h4", { staticClass: "white--text" }, [
                              _vm._v(
                                _vm._s(_vm.CourseData.courseDetail.content)
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "meta-info" }, [
                              _c("p", { staticClass: "white--text" }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "bestseller-tag d-inline-block mr-4 mb-1"
                                  },
                                  [_vm._v(_vm._s(_vm.$t("message.bestseller")))]
                                ),
                                _vm._v(" "),
                                _c("span", {
                                  staticClass: "rating-wrap d-inline-block"
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass: "student-count d-inline-block"
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticClass: "count font-weight-bold" },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.CourseData.courseDetail
                                              .bestSeller
                                          )
                                        )
                                      ]
                                    ),
                                    _vm._v(" students enrolled")
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "white--text" }, [
                                _c("span", { staticClass: "white--text" }, [
                                  _vm._v("Created By ")
                                ]),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "instructor-name d-inline-block"
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "white--text",
                                        attrs: { href: "#" }
                                      },
                                      [
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              _vm.CourseData.courseDetail
                                                .createdBy
                                            ) +
                                            " "
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass: "updated-time d-inline-block"
                                  },
                                  [
                                    _vm._v(
                                      " Last Updated " +
                                        _vm._s(
                                          _vm.CourseData.courseDetail
                                            .lastUpdates
                                        )
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "white--text" }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "language layout align-start ma-0"
                                  },
                                  [
                                    _c("v-icon", { staticClass: "cmr-8" }, [
                                      _vm._v("chat_bubble_outline")
                                    ]),
                                    _vm._v(
                                      _vm._s(
                                        _vm.CourseData.courseDetail.language
                                      )
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "v-col",
                          {
                            attrs: {
                              cols: "12",
                              sm: "6",
                              md: "5",
                              lg: "4",
                              xl: "3"
                            }
                          },
                          [
                            _c(
                              "app-card",
                              { attrs: { contentCustomClass: "pa-4" } },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "video-wrap overlay-wrap d-inline-flex"
                                  },
                                  [
                                    _c("img", {
                                      staticClass: "banner-video w-100",
                                      attrs: {
                                        src: "/static/img/about2.png",
                                        width: "230",
                                        height: "235",
                                        alt: "video"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "overlay-content layout align-center justify-center"
                                      },
                                      [
                                        _c(
                                          "v-dialog",
                                          {
                                            attrs: {
                                              width: "500",
                                              height: "300"
                                            },
                                            scopedSlots: _vm._u([
                                              {
                                                key: "activator",
                                                fn: function(ref) {
                                                  var on = ref.on
                                                  return [
                                                    _c(
                                                      "v-btn",
                                                      _vm._g(
                                                        { attrs: { icon: "" } },
                                                        on
                                                      ),
                                                      [
                                                        _c("v-icon", [
                                                          _vm._v(
                                                            "play_circle_filled"
                                                          )
                                                        ])
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                }
                                              }
                                            ]),
                                            model: {
                                              value: _vm.dialog,
                                              callback: function($$v) {
                                                _vm.dialog = $$v
                                              },
                                              expression: "dialog"
                                            }
                                          },
                                          [
                                            _vm._v(" "),
                                            _c("iframe", {
                                              attrs: {
                                                src:
                                                  _vm.CourseData.courseDetail
                                                    .demoVideoUrl,
                                                frameborder: "0",
                                                allowfullscreen: ""
                                              }
                                            })
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=template&id=ee81aa64&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=template&id=ee81aa64& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { staticClass: "course-detail-billing-wrap cpb-24" },
    [
      _c("div", { staticClass: "price-wrap" }, [
        _c("span", { staticClass: "cmr-8 discount-price d-inline-block" }, [
          _vm._v(
            "$" +
              _vm._s(
                (_vm.CourseData.courseDetail.billingDetails.totalPrice *
                  (100 -
                    _vm.CourseData.courseDetail.billingDetails
                      .discountPercent)) /
                  100
              )
          )
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "cmr-8 base-price d-inline-block" }, [
          _c("del", [
            _vm._v(
              "$" +
                _vm._s(_vm.CourseData.courseDetail.billingDetails.totalPrice)
            )
          ])
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "discount-value d-inline-block" }, [
          _vm._v(
            _vm._s(_vm.CourseData.courseDetail.billingDetails.discountPercent) +
              " % off"
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "p",
        { staticClass: "price-duration error--text " },
        [
          _c("v-icon", { staticClass: "cmr-8" }, [_vm._v("timer")]),
          _vm._v(" "),
          _c("span", { staticClass: "font-weight-bold cmr-8" }, [
            _vm._v(
              " " +
                _vm._s(
                  _vm.CourseData.courseDetail.billingDetails.discountTime
                ) +
                " "
            )
          ]),
          _vm._v(" left at this price!\n   ")
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "button-wrap" },
        [
          _c(
            "v-btn",
            {
              staticClass: "error pa-4",
              attrs: {
                to:
                  "/" + (_vm.getCurrentAppLayoutHandler() + "/courses/sign-in"),
                block: ""
              }
            },
            [_vm._v(_vm._s(_vm.$t("message.addToCart")) + "\n      ")]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "text-center" }, [
            _vm._v(
              _vm._s(_vm.CourseData.courseDetail.billingDetails.guarntee) +
                "day money back garuntee"
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "incentives" },
        [
          _c("h4", [_vm._v("Includes")]),
          _vm._v(" "),
          _vm._l(_vm.CourseData.courseDetail.billingDetails.includes, function(
            feature,
            key
          ) {
            return _c(
              "v-list",
              { key: key, staticClass: "incentive-list" },
              [
                _c(
                  "v-list-item",
                  [
                    _c(
                      "v-list-item-action",
                      { staticClass: "my-0 mr-1" },
                      [_c("v-icon", [_vm._v(_vm._s(feature.icon))])],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-list-item-content",
                      { staticClass: "py-0" },
                      [
                        _c("v-list-item-title", [
                          _c("p", { staticClass: "mb-0" }, [
                            _vm._v(_vm._s(feature.feature))
                          ])
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          })
        ],
        2
      ),
      _vm._v(" "),
      _c("div", { staticClass: "coupon-available text-center" }, [
        _c(
          "a",
          { staticClass: "accent-text", attrs: { href: "javascript:void(0)" } },
          [_vm._v("Have a coupon?")]
        )
      ]),
      _vm._v(" "),
      _c("v-divider"),
      _vm._v(" "),
      _c("div", { staticClass: "coupon-available text-center px-3" }, [
        _c(
          "a",
          { staticClass: "accent-text", attrs: { href: "javascript:void(0)" } },
          [_vm._v("Share")]
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=template&id=3443ce4f&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=template&id=3443ce4f& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      staticClass: "detail-dec-wrap",
      attrs: {
        colClasses: "col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12",
        heading: _vm.$t("message.description"),
        contentCustomClass: "pt-0"
      }
    },
    [
      _c("p", { staticClass: "font-weight-bold mrgn-b-sm" }, [
        _vm._v(_vm._s(_vm.CourseData.courseDetail.description.title))
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(_vm._s(_vm.CourseData.courseDetail.description.content))
      ]),
      _vm._v(" "),
      _c(
        "ul",
        _vm._l(_vm.CourseData.courseDetail.description.features, function(
          feature,
          key
        ) {
          return _c("li", { key: key }, [_vm._v(_vm._s(feature))])
        }),
        0
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.CourseData.courseDetail
    ? _c(
        "app-card",
        {
          staticClass: "about-instructor",
          attrs: {
            heading: _vm.$t("message.aboutInstructor"),
            colClasses: "col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12",
            contentCustomClass: "pt-0"
          }
        },
        [
          _c(
            "div",
            { staticClass: "d-flex align-center justify-start mx-0 mt-0 mb-4" },
            [
              _c("div", { staticClass: "image-wrap mb-2" }, [
                _c("img", {
                  attrs: {
                    src: _vm.CourseData.courseDetail.instructorInformation.image
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "instructor-meta" },
                [
                  _c("h4", [
                    _vm._v(
                      _vm._s(
                        _vm.CourseData.courseDetail.instructorInformation.name
                      )
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(
                    _vm.CourseData.courseDetail.instructorInformation.features,
                    function(feature, key) {
                      return _c(
                        "v-list",
                        { key: key, staticClass: "incentive-list" },
                        [
                          _c(
                            "v-list-item",
                            { staticClass: "d-custom-flex" },
                            [
                              _c(
                                "v-list-item-action",
                                { staticClass: "my-0 mr-1" },
                                [_c("v-icon", [_vm._v(_vm._s(feature.icon))])],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-list-item-content",
                                { staticClass: "py-0" },
                                [
                                  _c("v-list-item-title", [
                                    _c("p", { staticClass: "mb-0" }, [
                                      _vm._v(_vm._s(feature.feature))
                                    ])
                                  ])
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    }
                  )
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              _vm._s(_vm.CourseData.courseDetail.instructorInformation.content)
            )
          ])
        ]
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=template&id=115edcd7&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=template&id=115edcd7& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      staticClass: "course-info-wrap",
      attrs: {
        heading: _vm.$t("message.whatYoWillLearn"),
        colClasses: "col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12",
        contentCustomClass: "pt-0"
      }
    },
    [
      _c("div", { staticClass: "course-info-box" }, [
        _c(
          "ul",
          { staticClass: "info-box-list" },
          _vm._l(_vm.CourseData.courseDetail.learn, function(info, key) {
            return _c("li", { key: key }, [
              _c("i", { staticClass: "material-icons" }, [_vm._v("check")]),
              _c("span", [_vm._v(_vm._s(info))])
            ])
          }),
          0
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 courses-overview"
    },
    [
      _c(
        "v-expansion-panels",
        _vm._l(_vm.CourseData.courseDetail.topics, function(list, key) {
          return _c(
            "v-expansion-panel",
            { key: key },
            [
              _c("v-expansion-panel-header", [_vm._v(_vm._s(list.name))]),
              _vm._v(" "),
              _c(
                "v-expansion-panel-content",
                _vm._l(list.courseDetail, function(details, key) {
                  return _c(
                    "v-list",
                    { key: key },
                    [
                      _c(
                        "v-list-item",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c(
                            "v-list-item-action",
                            [_c("v-icon", [_vm._v("play_circle_filled")])],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-list-item-content",
                            [
                              _c("v-list-item-title", {}, [
                                _vm._v(
                                  "\n\t\t\t\t\t\t\t\t" +
                                    _vm._s(details.name) +
                                    "\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t"
                                )
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("v-list-item-action", [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "ma-0 d-flex justify-space-between align-center"
                              },
                              [
                                _c("div", { staticClass: "pr-5" }, [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "fs-12 fw-normal",
                                      attrs: { href: "#" }
                                    },
                                    [_vm._v("Preview")]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "pl-5" }, [
                                  _c(
                                    "span",
                                    { staticClass: "fs-12 fw-normal" },
                                    [_vm._v(_vm._s(details.time))]
                                  )
                                ])
                              ]
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("v-divider")
                    ],
                    1
                  )
                }),
                1
              )
            ],
            1
          )
        }),
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CoursesDetail.vue?vue&type=template&id=42209f83&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CoursesDetail.vue?vue&type=template&id=42209f83& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "courses-detail-wrap" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl", attrs: { fluid: "" } },
        [
          _c("course-detail-banner"),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "course-detail" },
            [
              _c(
                "v-row",
                { staticClass: "justify-center align-center" },
                [
                  _c(
                    "v-col",
                    {
                      attrs: {
                        cols: "12",
                        sm: "12",
                        md: "12",
                        lg: "12",
                        xl: "12"
                      }
                    },
                    [
                      _c(
                        "v-row",
                        [
                          _c(
                            "v-col",
                            {
                              attrs: {
                                cols: "12",
                                sm: "12",
                                md: "8",
                                lg: "9",
                                xl: "9"
                              }
                            },
                            [
                              _c(
                                "v-row",
                                [
                                  _c("course-detail-learn"),
                                  _vm._v(" "),
                                  _c("course-detail-desciption"),
                                  _vm._v(" "),
                                  _c("course-detail-overview"),
                                  _vm._v(" "),
                                  _c("course-detail-instructor"),
                                  _vm._v(" "),
                                  _c(
                                    "app-card",
                                    {
                                      attrs: {
                                        colClasses:
                                          "col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12",
                                        customClasses: "more-courses-grid",
                                        contentCustomClass: "layout pt-0",
                                        heading: _vm.$t(
                                          "message.moreCoursesFromJamesColt"
                                        )
                                      }
                                    },
                                    [
                                      _c(
                                        "v-row",
                                        {
                                          staticClass:
                                            "align-start justify-start mx-0 mb-0"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "course-item-wrap" },
                                            [
                                              _c("course-card", {
                                                attrs: {
                                                  height: 200,
                                                  width: 335,
                                                  data:
                                                    _vm.CourseData.courseDetail
                                                      .moreCourses,
                                                  colxl: 4,
                                                  collg: 4,
                                                  colmd: 4,
                                                  colsm: 6,
                                                  cols: 12
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            {
                              staticClass: "course-sidebar",
                              attrs: {
                                cols: "12",
                                sm: "12",
                                md: "4",
                                lg: "3",
                                xl: "3"
                              }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "custom-height-auto" },
                                [
                                  _c("course-detail-billing"),
                                  _vm._v(" "),
                                  _c(
                                    "app-card",
                                    {
                                      attrs: {
                                        contentCustomClass: "pt-0",
                                        heading: "Contrary to popular belief ?"
                                      }
                                    },
                                    [
                                      _c("p", [
                                        _vm._v(
                                          "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "a",
                                        {
                                          staticClass:
                                            "font-weight-bold primary-text",
                                          attrs: { href: "#" }
                                        },
                                        [_vm._v("Nulla eu augue !")]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue":
/*!*************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseDetailBanner_vue_vue_type_template_id_b4b645ae___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseDetailBanner.vue?vue&type=template&id=b4b645ae& */ "./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=template&id=b4b645ae&");
/* harmony import */ var _CourseDetailBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseDetailBanner.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CourseDetailBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseDetailBanner_vue_vue_type_template_id_b4b645ae___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseDetailBanner_vue_vue_type_template_id_b4b645ae___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailBanner.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBanner_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=template&id=b4b645ae&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=template&id=b4b645ae& ***!
  \********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBanner_vue_vue_type_template_id_b4b645ae___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailBanner.vue?vue&type=template&id=b4b645ae& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBanner.vue?vue&type=template&id=b4b645ae&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBanner_vue_vue_type_template_id_b4b645ae___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBanner_vue_vue_type_template_id_b4b645ae___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseDetailBilling_vue_vue_type_template_id_ee81aa64___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseDetailBilling.vue?vue&type=template&id=ee81aa64& */ "./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=template&id=ee81aa64&");
/* harmony import */ var _CourseDetailBilling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseDetailBilling.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CourseDetailBilling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseDetailBilling_vue_vue_type_template_id_ee81aa64___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseDetailBilling_vue_vue_type_template_id_ee81aa64___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBilling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailBilling.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBilling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=template&id=ee81aa64&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=template&id=ee81aa64& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBilling_vue_vue_type_template_id_ee81aa64___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailBilling.vue?vue&type=template&id=ee81aa64& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailBilling.vue?vue&type=template&id=ee81aa64&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBilling_vue_vue_type_template_id_ee81aa64___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailBilling_vue_vue_type_template_id_ee81aa64___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue":
/*!******************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseDetailDescription_vue_vue_type_template_id_3443ce4f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseDetailDescription.vue?vue&type=template&id=3443ce4f& */ "./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=template&id=3443ce4f&");
/* harmony import */ var _CourseDetailDescription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseDetailDescription.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CourseDetailDescription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseDetailDescription_vue_vue_type_template_id_3443ce4f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseDetailDescription_vue_vue_type_template_id_3443ce4f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailDescription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailDescription.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailDescription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=template&id=3443ce4f&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=template&id=3443ce4f& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailDescription_vue_vue_type_template_id_3443ce4f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailDescription.vue?vue&type=template&id=3443ce4f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailDescription.vue?vue&type=template&id=3443ce4f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailDescription_vue_vue_type_template_id_3443ce4f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailDescription_vue_vue_type_template_id_3443ce4f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseDetailInstructor_vue_vue_type_template_id_feb1d8cc_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true& */ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true&");
/* harmony import */ var _CourseDetailInstructor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseDetailInstructor.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CourseDetailInstructor_vue_vue_type_style_index_0_id_feb1d8cc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css& */ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CourseDetailInstructor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseDetailInstructor_vue_vue_type_template_id_feb1d8cc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseDetailInstructor_vue_vue_type_template_id_feb1d8cc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "feb1d8cc",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailInstructor.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css&":
/*!**************************************************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_style_index_0_id_feb1d8cc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=style&index=0&id=feb1d8cc&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_style_index_0_id_feb1d8cc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_style_index_0_id_feb1d8cc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_style_index_0_id_feb1d8cc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_style_index_0_id_feb1d8cc_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true& ***!
  \************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_template_id_feb1d8cc_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailInstructor.vue?vue&type=template&id=feb1d8cc&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_template_id_feb1d8cc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailInstructor_vue_vue_type_template_id_feb1d8cc_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue":
/*!************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseDetailLearn_vue_vue_type_template_id_115edcd7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseDetailLearn.vue?vue&type=template&id=115edcd7& */ "./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=template&id=115edcd7&");
/* harmony import */ var _CourseDetailLearn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseDetailLearn.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CourseDetailLearn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseDetailLearn_vue_vue_type_template_id_115edcd7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseDetailLearn_vue_vue_type_template_id_115edcd7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailLearn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailLearn.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailLearn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=template&id=115edcd7&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=template&id=115edcd7& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailLearn_vue_vue_type_template_id_115edcd7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailLearn.vue?vue&type=template&id=115edcd7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailLearn.vue?vue&type=template&id=115edcd7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailLearn_vue_vue_type_template_id_115edcd7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailLearn_vue_vue_type_template_id_115edcd7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue":
/*!***************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseDetailOverview_vue_vue_type_template_id_e668d094_class_custom_flex___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex& */ "./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex&");
/* harmony import */ var _CourseDetailOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseDetailOverview.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CourseDetailOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseDetailOverview_vue_vue_type_template_id_e668d094_class_custom_flex___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseDetailOverview_vue_vue_type_template_id_e668d094_class_custom_flex___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailOverview.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex&":
/*!****************************************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex& ***!
  \****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailOverview_vue_vue_type_template_id_e668d094_class_custom_flex___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseDetailOverview.vue?vue&type=template&id=e668d094&class=custom-flex&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailOverview_vue_vue_type_template_id_e668d094_class_custom_flex___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseDetailOverview_vue_vue_type_template_id_e668d094_class_custom_flex___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/CoursesDetail.vue":
/*!******************************************************!*\
  !*** ./resources/js/views/courses/CoursesDetail.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CoursesDetail_vue_vue_type_template_id_42209f83___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CoursesDetail.vue?vue&type=template&id=42209f83& */ "./resources/js/views/courses/CoursesDetail.vue?vue&type=template&id=42209f83&");
/* harmony import */ var _CoursesDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CoursesDetail.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CoursesDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CoursesDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CoursesDetail_vue_vue_type_template_id_42209f83___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CoursesDetail_vue_vue_type_template_id_42209f83___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CoursesDetail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CoursesDetail.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/courses/CoursesDetail.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CoursesDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CoursesDetail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CoursesDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CoursesDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CoursesDetail.vue?vue&type=template&id=42209f83&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/courses/CoursesDetail.vue?vue&type=template&id=42209f83& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CoursesDetail_vue_vue_type_template_id_42209f83___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CoursesDetail.vue?vue&type=template&id=42209f83& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CoursesDetail.vue?vue&type=template&id=42209f83&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CoursesDetail_vue_vue_type_template_id_42209f83___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CoursesDetail_vue_vue_type_template_id_42209f83___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);