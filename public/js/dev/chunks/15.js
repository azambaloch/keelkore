(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[15],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["icon", "title", "value", "color"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartV2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartV2 */ "./resources/js/components/Charts/LineChartV2.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["title", "value", "body", "status", "data", "color", "labels"],
  components: {
    LineChartv2: Components_Charts_LineChartV2__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* eslint-disable */
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      activitiesInWorld: [{
        id: 1,
        iso: "icons8-austria",
        value: "42",
        name: "Austria - 4100"
      }, {
        id: 2,
        iso: "icons8-japan",
        value: "31",
        name: "Japan-2300"
      }, {
        id: 3,
        iso: "icons8-usa",
        value: "93",
        name: "USA-2300"
      }, {
        id: 4,
        iso: "icons8-china",
        value: "15",
        name: "China-800"
      }, {
        id: 5,
        iso: "icons8-france",
        value: "38",
        name: "France-3521"
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartWithArea */ "./resources/js/components/Charts/LineChartWithArea.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44"],
      bandwidthData: [40, 10, 30, 40, 60, 70, 60, 40, 80, 25, 44, 59, 20, 52, 40, 80, 45, 68, 40, 38, 85, 64, 25, 45, 65, 95, 40, 10, 30, 40, 60, 70, 60, 40, 80, 25, 14, 18, 22, 65, 85, 75, 95, 40],
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"]
    };
  },
  components: {
    LineChartWithArea: Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_DeviceSeprations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/DeviceSeprations */ "./resources/js/components/Charts/DeviceSeprations.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    DeviceSeprations: Components_Charts_DeviceSeprations__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/HotKeywords.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/HotKeywords.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      headers: [{
        text: "Keyword Name",
        sortable: false,
        value: "keywordName"
      }, {
        text: "Unique Visitors",
        value: "uniqueVisitors",
        sortable: false
      }, {
        text: "Visit duraton",
        value: "visitduraton",
        sortable: false
      }],
      keywords: [],
      settings: {
        maxScrollbarLength: 150
      }
    };
  },
  mounted: function mounted() {
    this.getKeywords();
  },
  methods: {
    getKeywords: function getKeywords() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/hotKeywords.js").then(function (response) {
        _this.loader = false;
        _this.keywords = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Notifications.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Notifications.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      notifications: [{
        id: 1,
        body: "New invoice received",
        label: "Admin",
        time: "1 Hour Ago",
        icon: "notifications_active",
        iconColor: "warning"
      }, {
        id: 2,
        body: "New Feedback from Aric",
        label: "Admin",
        time: "1 Hour Ago",
        icon: "format_quote",
        iconColor: "primary"
      }, {
        id: 3,
        body: "DB overloaded 80% ",
        label: "Action",
        time: "1 Hour Ago",
        icon: "notifications_active",
        iconColor: "info"
      }, {
        id: 4,
        body: "System error - Check",
        label: "Error",
        time: "1 Hour Ago",
        icon: "warning",
        iconColor: "error"
      }, {
        id: 5,
        body: "System Memory Full",
        label: "Timeout",
        time: "1 Hour Ago",
        icon: "access_alarm",
        iconColor: "warning"
      }, {
        id: 6,
        body: "Production server up",
        label: "System",
        time: "1 Hour Ago",
        icon: "access_alarm",
        iconColor: "primary"
      }, {
        id: 7,
        body: "Clear server cache for more space",
        label: "Timeout",
        time: "1 Hour Ago",
        icon: "access_alarm",
        iconColor: "warning"
      }],
      settings: {
        maxScrollbarLength: 150
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      androidUsers: 70,
      iosUsers: 50
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerLoad.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ServerLoad.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_echarts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-echarts */ "./node_modules/vue-echarts/components/ECharts.vue");
/* harmony import */ var echarts_lib_chart_gauge__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! echarts/lib/chart/gauge */ "./node_modules/echarts/lib/chart/gauge.js");
/* harmony import */ var echarts_lib_chart_gauge__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_gauge__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["styles"],
  name: "buyers-stats",
  components: {
    ECharts: vue_echarts__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      gauge: {
        tooltip: {
          formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
          feature: {
            restore: {},
            saveAsImage: {}
          }
        },
        series: [{
          name: 'Server Load',
          type: 'gauge',
          startAngle: 225,
          endAngle: -45,
          detail: {
            formatter: '{value}%',
            fontSize: 22
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: [[0.4, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.primary], [0.8, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.info], [1, Constants_chart_config__WEBPACK_IMPORTED_MODULE_3__["ChartConfig"].color.warning]],
              width: 20
            }
          },
          data: [{
            value: 50,
            name: 'Server Load'
          }],
          radius: 130
        }]
      }
    };
  },
  mounted: function mounted() {
    this.gaugeMove();
  },
  methods: {
    gaugeMove: function gaugeMove() {
      var self = this;
      setInterval(function () {
        self.gauge.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;
      }, 2000);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerStatus.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ServerStatus.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      serverStatus: [{
        id: 1,
        name: "CPU Usage (25/35 Cpus)",
        value: 40
      }, {
        id: 2,
        name: "Domain (2/5 Used)",
        value: 66
      }, {
        id: 3,
        name: "Database (90/100)",
        value: 90
      }, {
        id: 4,
        name: "Email Account (25/50 Used)",
        value: 50
      }, {
        id: 5,
        name: "Disk Usage(120/250GB)",
        value: 55
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportTickets.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportTickets.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      supportTickets: null,
      settings: {
        maxScrollbarLength: 100
      }
    };
  },
  mounted: function mounted() {
    this.getTickets();
  },
  methods: {
    getTickets: function getTickets() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/supportTickets.js").then(function (response) {
        _this.loader = false;
        _this.supportTickets = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TotalDownloading.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TotalDownloading.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartWithArea */ "./resources/js/components/Charts/LineChartWithArea.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44"],
      bandwidthData: [40, 10, 30, 40, 60, 70, 60, 40, 80, 25, 44, 59, 20, 52, 40, 80, 45, 68, 40, 38, 85, 64, 25, 45, 65, 95, 40, 10, 30, 40, 60, 70, 60, 40, 80, 25, 14, 18, 22, 65, 85, 75, 95, 40],
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"]
    };
  },
  components: {
    LineChartWithArea: Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersStat.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UsersStat.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      maleUsers: 70,
      femaleUsers: 50
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_VisitorsStackedAreaChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/VisitorsStackedAreaChart */ "./resources/js/components/Charts/VisitorsStackedAreaChart.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VisitorsStackedAreaChart: Components_Charts_VisitorsStackedAreaChart__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/WebAnalytics.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/WebAnalytics.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Widgets_VisitorsAreaChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Widgets/VisitorsAreaChart */ "./resources/js/components/Widgets/VisitorsAreaChart.vue");
/* harmony import */ var Components_Widgets_TrafficChannelV2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Widgets/TrafficChannelV2 */ "./resources/js/components/Widgets/TrafficChannelV2.vue");
/* harmony import */ var Components_Widgets_BrowserStatics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Widgets/BrowserStatics */ "./resources/js/components/Widgets/BrowserStatics.vue");
/* harmony import */ var Components_Widgets_ServerLoad__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/ServerLoad */ "./resources/js/components/Widgets/ServerLoad.vue");
/* harmony import */ var Components_Widgets_BandwidthUsageV2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/BandwidthUsageV2 */ "./resources/js/components/Widgets/BandwidthUsageV2.vue");
/* harmony import */ var Components_Widgets_TotalDownloading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/TotalDownloading */ "./resources/js/components/Widgets/TotalDownloading.vue");
/* harmony import */ var Components_Widgets_Notifications__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/Notifications */ "./resources/js/components/Widgets/Notifications.vue");
/* harmony import */ var Components_Widgets_SupportTickets__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/Widgets/SupportTickets */ "./resources/js/components/Widgets/SupportTickets.vue");
/* harmony import */ var Components_Widgets_HotKeywords__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Components/Widgets/HotKeywords */ "./resources/js/components/Widgets/HotKeywords.vue");
/* harmony import */ var Components_Widgets_UsersStat__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Components/Widgets/UsersStat */ "./resources/js/components/Widgets/UsersStat.vue");
/* harmony import */ var Components_Widgets_PlatformUsersStat__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Components/Widgets/PlatformUsersStat */ "./resources/js/components/Widgets/PlatformUsersStat.vue");
/* harmony import */ var Components_Widgets_ServerStatus__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Components/Widgets/ServerStatus */ "./resources/js/components/Widgets/ServerStatus.vue");
/* harmony import */ var Components_Widgets_24x7CustomerSupport__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Components/Widgets/24x7CustomerSupport */ "./resources/js/components/Widgets/24x7CustomerSupport.vue");
/* harmony import */ var Components_Widgets_PromoWidget__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! Components/Widgets/PromoWidget */ "./resources/js/components/Widgets/PromoWidget.vue");
/* harmony import */ var Components_Widgets_ActivityAroundWorld__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! Components/Widgets/ActivityAroundWorld */ "./resources/js/components/Widgets/ActivityAroundWorld.vue");
/* harmony import */ var Components_Widgets_DeviceSeprations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! Components/Widgets/DeviceSeprations */ "./resources/js/components/Widgets/DeviceSeprations.vue");
/* harmony import */ var Components_StatsCardV3_StatsCardV3__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! Components/StatsCardV3/StatsCardV3 */ "./resources/js/components/StatsCardV3/StatsCardV3.vue");
/* harmony import */ var Components_StatsCardV4_StatsCardV4__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! Components/StatsCardV4/StatsCardV4 */ "./resources/js/components/StatsCardV4/StatsCardV4.vue");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//















 // components


 // chart config


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    VisitorsAreaChart: Components_Widgets_VisitorsAreaChart__WEBPACK_IMPORTED_MODULE_0__["default"],
    TrafficChannelV2: Components_Widgets_TrafficChannelV2__WEBPACK_IMPORTED_MODULE_1__["default"],
    BrowserStatics: Components_Widgets_BrowserStatics__WEBPACK_IMPORTED_MODULE_2__["default"],
    ServerLoad: Components_Widgets_ServerLoad__WEBPACK_IMPORTED_MODULE_3__["default"],
    BandwidthUsageV2: Components_Widgets_BandwidthUsageV2__WEBPACK_IMPORTED_MODULE_4__["default"],
    TotalDownloading: Components_Widgets_TotalDownloading__WEBPACK_IMPORTED_MODULE_5__["default"],
    Notifications: Components_Widgets_Notifications__WEBPACK_IMPORTED_MODULE_6__["default"],
    SupportTickets: Components_Widgets_SupportTickets__WEBPACK_IMPORTED_MODULE_7__["default"],
    HotKeywords: Components_Widgets_HotKeywords__WEBPACK_IMPORTED_MODULE_8__["default"],
    UsersStat: Components_Widgets_UsersStat__WEBPACK_IMPORTED_MODULE_9__["default"],
    PlatformUsersStat: Components_Widgets_PlatformUsersStat__WEBPACK_IMPORTED_MODULE_10__["default"],
    ServerStatus: Components_Widgets_ServerStatus__WEBPACK_IMPORTED_MODULE_11__["default"],
    CustomerSupportService: Components_Widgets_24x7CustomerSupport__WEBPACK_IMPORTED_MODULE_12__["default"],
    PromoWidget: Components_Widgets_PromoWidget__WEBPACK_IMPORTED_MODULE_13__["default"],
    StatsCardV3: Components_StatsCardV3_StatsCardV3__WEBPACK_IMPORTED_MODULE_16__["default"],
    ActivityAroundWorld: Components_Widgets_ActivityAroundWorld__WEBPACK_IMPORTED_MODULE_14__["default"],
    DeviceSeparations: Components_Widgets_DeviceSeprations__WEBPACK_IMPORTED_MODULE_15__["default"],
    StatsCardV4: Components_StatsCardV4_StatsCardV4__WEBPACK_IMPORTED_MODULE_17__["default"]
  },
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_18__["ChartConfig"]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=template&id=345bd8e4&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=template&id=345bd8e4& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("app-card", [
    _c("div", { staticClass: "d-custom-flex justify-space-between" }, [
      _c("div", [
        _c("h2", [_vm._v(_vm._s(_vm.value))]),
        _vm._v(" "),
        _c("p", { staticClass: "mb-0 fs-12 fw-normal grey--text" }, [
          _vm._v(_vm._s(_vm.title))
        ])
      ]),
      _vm._v(" "),
      _c("div", [
        _c("span", { staticClass: "icon-style" }, [
          _c("i", { class: "material-icons font-2x " + _vm.color + "--text" }, [
            _vm._v(_vm._s(_vm.icon))
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=template&id=368a0910&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=template&id=368a0910& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("app-card", [
    _c(
      "div",
      {
        staticClass:
          "d-custom-flex justify-space-between align-items-center pa-2"
      },
      [
        _c("div", { staticClass: "w-50" }, [
          _c("p", { staticClass: "mb-2" }, [_vm._v(_vm._s(_vm.title))]),
          _vm._v(" "),
          _c("h4", { staticClass: "mb-2" }, [
            _vm._v(_vm._s(_vm.value) + " "),
            _c("i", {
              class: [
                {
                  "ti-arrow-up success--text ml-2": _vm.status === 1,
                  "ti-arrow-down warning--text ml-2": _vm.status === 0
                }
              ]
            })
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "fs-12 fw-normal grey--text mb-0" }, [
            _vm._v(_vm._s(_vm.body))
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "w-50" },
          [
            _c("line-chartv2", {
              attrs: {
                height: 60,
                data: _vm.data,
                labels: _vm.labels,
                color: _vm.color
              }
            })
          ],
          1
        )
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/24x7CustomerSupport.vue?vue&type=template&id=725880d5&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/24x7CustomerSupport.vue?vue&type=template&id=725880d5& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "promo-widget" },
    [
      _c(
        "app-card",
        { attrs: { customClasses: "border-primary" } },
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "3",
                    lg: "3",
                    md: "2",
                    sm: "3",
                    cols: "4",
                    "b-50": ""
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "d-flex justify-center align-center h-100" },
                    [
                      _c("img", {
                        staticClass: "img-responsive",
                        attrs: {
                          alt: "comment",
                          src: "/static/img/Comments.png"
                        }
                      })
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "9",
                    lg: "9",
                    md: "10",
                    sm: "9",
                    cols: "8",
                    "b-50": ""
                  }
                },
                [
                  _c(
                    "div",
                    [
                      _c("h4", [_vm._v("24x7 Customer Support")]),
                      _vm._v(" "),
                      _c("p", { staticClass: "fs-12 fw-normal grey--text" }, [
                        _vm._v(
                          "Far far away, behind the word mountains, far from the countries\n\t\t\t\t\t\tVokalia and Consonantia. Even the all-powerful Pointing has no control about the blind texts.Far\n\t\t\t\t\t\tfar away, behind the word mountains, far from the countries Vokalia and Consonantia."
                        )
                      ]),
                      _vm._v(" "),
                      _c("v-btn", { attrs: { color: "primary" } }, [
                        _vm._v(_vm._s(_vm.$t("message.letsGetInTouch")))
                      ])
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=template&id=47fd44ca&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=template&id=47fd44ca& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "activity-around-world" },
    [
      _c(
        "v-row",
        [
          _c("v-col", { attrs: { xl: "12", lg: "12", md: "12", cols: "12" } }, [
            _c(
              "ul",
              { staticClass: "list-unstyled pl-0 country-progress" },
              _vm._l(_vm.activitiesInWorld, function(activitiy) {
                return _c(
                  "li",
                  {
                    key: activitiy.id,
                    staticClass: "d-flex align-center py-4"
                  },
                  [
                    _c("img", {
                      staticClass: "img-responsive mr-4",
                      attrs: {
                        width: "45",
                        height: "30",
                        src: "/static/flag-icons/" + activitiy.iso + ".png"
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "w-100" },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "mb-2 d-flex justify-space-between align-center"
                          },
                          [
                            _c("h6", { staticClass: "mb-0" }, [
                              _vm._v(_vm._s(activitiy.name))
                            ]),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "fs-12 fw-normal grey--text" },
                              [_vm._v(_vm._s(activitiy.value) + "%")]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("v-progress-linear", {
                          staticClass: "ma-0",
                          attrs: { value: activitiy.value, height: "4" }
                        })
                      ],
                      1
                    )
                  ]
                )
              }),
              0
            )
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=template&id=c12f5212&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=template&id=c12f5212& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "bandwidth-widget" },
    [
      _c("div", { staticClass: "mb-4 pl-5" }, [
        _c("h2", [_vm._v(_vm._s(_vm.$t("message.bandwidthUsage")))]),
        _vm._v(" "),
        _c("p", { staticClass: "fs-14 mb-0 fw-light" }, [_vm._v("March 2018")])
      ]),
      _vm._v(" "),
      _c("line-chart-with-area", {
        attrs: {
          dataSet: _vm.bandwidthData,
          dataLabels: _vm.labels,
          lineTension: 0.5,
          height: 105,
          color: _vm.ChartConfig.color.info,
          enableXAxesLine: false
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BrowserStatics.vue?vue&type=template&id=1b5a86b0&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BrowserStatics.vue?vue&type=template&id=1b5a86b0& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "browser-statics-wrapper" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "v-list",
        [
          _c("v-list-item", [
            _c("div", [
              _c("img", {
                staticClass: "mr-3",
                attrs: {
                  src: "/static/img/firefox_icon.png",
                  alt: "firefox_icon",
                  height: "20",
                  width: "20"
                }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Firefox")])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "justify-space-between" }, [
              _c("span", { staticClass: "fw-light" }, [_vm._v("8,250")]),
              _vm._v(" "),
              _c("h6", { staticClass: "primary--text" }, [_vm._v("+85%")])
            ])
          ]),
          _vm._v(" "),
          _c("v-list-item", [
            _c("div", [
              _c("img", {
                staticClass: "mr-3",
                attrs: {
                  src: "/static/img/safari_icon.png",
                  alt: "safari_icon",
                  height: "20",
                  width: "20"
                }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Safari")])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "justify-space-between" }, [
              _c("span", { staticClass: "fw-light" }, [_vm._v("1,255")]),
              _vm._v(" "),
              _c("h6", { staticClass: "primary--text" }, [_vm._v("-53%")])
            ])
          ]),
          _vm._v(" "),
          _c("v-list-item", [
            _c("div", [
              _c("img", {
                staticClass: "mr-3",
                attrs: {
                  src: "/static/img/chrome_icon.png",
                  alt: "chrome_icon",
                  height: "20",
                  width: "20"
                }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Chrome")])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "justify-space-between" }, [
              _c("span", { staticClass: "fw-light" }, [_vm._v("3,050")]),
              _vm._v(" "),
              _c("h6", { staticClass: "primary--text" }, [_vm._v("-20%")])
            ])
          ]),
          _vm._v(" "),
          _c("v-list-item", [
            _c("div", [
              _c("img", {
                staticClass: "mr-3",
                attrs: {
                  src: "/static/img/opera_icon.png",
                  alt: "opera_icon",
                  height: "20",
                  width: "20"
                }
              }),
              _vm._v(" "),
              _c("h6", [_vm._v("Opera")])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "justify-space-between" }, [
              _c("span", { staticClass: "fw-light" }, [_vm._v("3,650")]),
              _vm._v(" "),
              _c("h6", { staticClass: "primary--text" }, [_vm._v("+34%")])
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "grayish-blue d-custom-flex justify-space-between pa-5" },
      [
        _c("div", { staticClass: "white--text" }, [
          _c("h1", { staticClass: "mb-0" }, [_vm._v("23,488")]),
          _vm._v(" "),
          _c("p", { staticClass: "mb-0 fs-14" }, [_vm._v("Active Users")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "pr-4" }, [
          _c("i", { staticClass: "zmdi font-3x success--text zmdi-accounts" })
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=template&id=59a3d2d3&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=template&id=59a3d2d3& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "pos-relative mb-5" },
      [
        _c("device-seprations", { attrs: { height: 180 } }),
        _vm._v(" "),
        _vm._m(0)
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(1)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "overlay-content d-custom-flex justify-center align-items-center"
      },
      [
        _c("span", { staticClass: "grey--text font-2x fw-semi-bold" }, [
          _vm._v("2500")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-custom-flex px-4 justify-space-between align-items-center"
      },
      [
        _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
          _c("span", { staticClass: "v-badge rounded primary mr-2" }),
          _vm._v(" "),
          _c("span", [_vm._v("Computer")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
          _c("span", { staticClass: "v-badge rounded info mr-2" }),
          _vm._v(" "),
          _c("span", [_vm._v("Mobile")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
          _c("span", { staticClass: "v-badge rounded warning mr-2" }),
          _vm._v(" "),
          _c("span", [_vm._v("Tablets")])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/HotKeywords.vue?vue&type=template&id=2227cc08&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/HotKeywords.vue?vue&type=template&id=2227cc08& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "hot-keywords" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "584px" }, attrs: { settings: _vm.settings } },
        [
          _c("v-data-table", {
            attrs: {
              headers: _vm.headers,
              items: _vm.keywords,
              "hide-default-footer": ""
            },
            scopedSlots: _vm._u([
              {
                key: "item",
                fn: function(ref) {
                  var item = ref.item
                  return [
                    _c("tr", [
                      _c("td", [_vm._v(_vm._s(item.name))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(item.visitors))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(item.visitDuration))])
                    ])
                  ]
                }
              }
            ])
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Notifications.vue?vue&type=template&id=7f0ea40d&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Notifications.vue?vue&type=template&id=7f0ea40d& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "vue-perfect-scrollbar",
    { staticStyle: { height: "410px" }, attrs: { settings: _vm.settings } },
    [
      _c(
        "v-list",
        { staticClass: "notification-wrap", attrs: { "two-line": "" } },
        [
          _vm._l(_vm.notifications, function(item) {
            return [
              _c(
                "v-list-item",
                { key: item.id, attrs: { ripple: "" } },
                [
                  _c(
                    "v-list-item-avatar",
                    [
                      _c("v-icon", { attrs: { color: item.iconColor } }, [
                        _vm._v(_vm._s(item.icon))
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-list-item-content",
                    [
                      _c(
                        "v-list-item-subtitle",
                        { staticClass: "grey--text fs-12 fw-normal" },
                        [_vm._v(_vm._s(item.label))]
                      ),
                      _vm._v(" "),
                      _c("v-list-item-title", [
                        _c("h6", { staticClass: "mb-0" }, [
                          _vm._v(_vm._s(item.body))
                        ])
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-list-item-action",
                    [
                      _c("v-list-item-action-text", [_vm._v(_vm._s(item.time))])
                    ],
                    1
                  )
                ],
                1
              )
            ]
          })
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=template&id=2f859524&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=template&id=2f859524& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { attrs: { customClasses: "platform-user-stats" } },
    [
      _c("p", { staticClass: "grey--text fs-12 fw-normal mb-2" }, [
        _vm._v(_vm._s(_vm.$t("message.androidUsers")))
      ]),
      _vm._v(" "),
      _c("h4", [_vm._v("4250(56%)")]),
      _vm._v(" "),
      _c("v-progress-linear", {
        staticClass: "user1",
        attrs: { height: "6", color: "info" },
        model: {
          value: _vm.androidUsers,
          callback: function($$v) {
            _vm.androidUsers = $$v
          },
          expression: "androidUsers"
        }
      }),
      _vm._v(" "),
      _c("p", { staticClass: "grey--text fs-12 fw-normal mb-2" }, [
        _vm._v(_vm._s(_vm.$t("message.iOSUsers")))
      ]),
      _vm._v(" "),
      _c("h4", [_vm._v("3250(44%)")]),
      _vm._v(" "),
      _c("v-progress-linear", {
        staticClass: "user2",
        attrs: { color: "error", height: "6" },
        model: {
          value: _vm.iosUsers,
          callback: function($$v) {
            _vm.iosUsers = $$v
          },
          expression: "iosUsers"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/PromoWidget.vue?vue&type=template&id=490af918&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/PromoWidget.vue?vue&type=template&id=490af918& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "promo-widget" },
    [
      _c(
        "app-card",
        { attrs: { customClasses: "border-info" } },
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "3",
                    lg: "3",
                    md: "2",
                    sm: "3",
                    cols: "4",
                    "b-50": ""
                  }
                },
                [
                  _c(
                    "div",
                    { staticClass: "d-flex justify-center align-center h-100" },
                    [
                      _c("img", {
                        staticClass: "img-responsive",
                        attrs: {
                          alt: "transform",
                          src: "/static/img/Freetransform.png"
                        }
                      })
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "9",
                    lg: "9",
                    md: "10",
                    sm: "9",
                    cols: "8",
                    "b-50": ""
                  }
                },
                [
                  _c(
                    "div",
                    [
                      _c("h4", [_vm._v("Pixel Perfect Design")]),
                      _vm._v(" "),
                      _c("p", { staticClass: "fs-12 grey--text" }, [
                        _vm._v(
                          "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia. Even the all-powerful Pointing has no control about the blind texts.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia."
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "fs-12 fw-normal",
                          attrs: { color: "info" }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.purchaseVuely")))]
                      )
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerLoad.vue?vue&type=template&id=b0e69118&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ServerLoad.vue?vue&type=template&id=b0e69118& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "pos-relative h-100 overflow-hidden server-load-widget" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "div",
        [
          _c("ECharts", {
            staticStyle: { width: "100%", height: "260px" },
            attrs: { options: _vm.gauge, autoresize: "", id: "barChart" }
          })
        ],
        1
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-custom-flex justify-space-between align-items-center mb-4"
      },
      [
        _c("div", [
          _c("span", { staticClass: "fs-12 fw-normal grey--text" }, [
            _vm._v("Space")
          ]),
          _vm._v(" "),
          _c("h5", { staticClass: "mb-0" }, [
            _c("span", { staticClass: "primary--text" }, [_vm._v("98GB ")]),
            _c("span", { staticClass: "info--text" }, [_vm._v("/ 124GB")])
          ])
        ]),
        _vm._v(" "),
        _c("div", [
          _c("span", { staticClass: "fs-12 fw-normal grey--text" }, [
            _vm._v("Bandwidth")
          ]),
          _vm._v(" "),
          _c("h5", { staticClass: "mb-0" }, [
            _c("span", { staticClass: "error--text" }, [_vm._v("512MB ")]),
            _c("span", { staticClass: "info--text" }, [_vm._v("/ 1024MB")])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerStatus.vue?vue&type=template&id=51d67700&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ServerStatus.vue?vue&type=template&id=51d67700& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "server-status-widget" }, [
    _c(
      "ul",
      { staticClass: "list-unstyled pl-0" },
      _vm._l(_vm.serverStatus, function(server) {
        return _c(
          "li",
          { key: server.id },
          [
            _c("div", { staticClass: "d-custom-flex justify-space-between" }, [
              _c("h6", { staticClass: "mb-0" }, [_vm._v(_vm._s(server.name))]),
              _vm._v(" "),
              _c("span", { staticClass: "grey--text fs-12 fw-normal" }, [
                _vm._v(_vm._s(server.value) + "%")
              ])
            ]),
            _vm._v(" "),
            _c("v-progress-linear", {
              attrs: { height: "6", value: server.value }
            })
          ],
          1
        )
      }),
      0
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportTickets.vue?vue&type=template&id=382b407a&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportTickets.vue?vue&type=template&id=382b407a& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "support-ticket-wrap" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "411px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            [
              _vm._l(_vm.supportTickets, function(tickets) {
                return [
                  _c(
                    "div",
                    { key: tickets.userName, staticClass: "listing" },
                    [
                      _c(
                        "v-list-item",
                        [
                          _c("v-list-item-avatar", [
                            _c("img", {
                              staticClass: "img-responsive",
                              attrs: { src: tickets.avatar, alt: "user image" }
                            })
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-list-item-content",
                            [
                              _c("v-list-item-subtitle", [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "d-custom-flex align-items-center mb-2"
                                  },
                                  [
                                    _c(
                                      "h5",
                                      { staticClass: "fs-14 mb-0 mr-2" },
                                      [_vm._v(_vm._s(tickets.userName))]
                                    ),
                                    _vm._v(" "),
                                    tickets.status === 1
                                      ? _c(
                                          "v-badge",
                                          {
                                            staticClass: "primary",
                                            attrs: { value: false }
                                          },
                                          [_vm._v("Open")]
                                        )
                                      : _c(
                                          "v-badge",
                                          {
                                            staticClass: "warning",
                                            attrs: { value: false }
                                          },
                                          [_vm._v("Closed")]
                                        )
                                  ],
                                  1
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "p",
                                {
                                  key: tickets.id,
                                  staticClass: "fs-14 mb-0 grey--text"
                                },
                                [
                                  _vm._v(
                                    "            \n\t\t\t\t\t\t\t\t" +
                                      _vm._s(tickets.body) +
                                      "\n\t\t\t\t\t\t\t"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "d-custom-flex" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "mr-3 ripple",
                                    attrs: { href: "javascript:void(0)" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "zmdi zmdi-eye primary--text"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "mr-3",
                                    attrs: { href: "javascript:void(0)" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-edit success--text"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "mr-3",
                                    attrs: { href: "javascript:void(0)" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "zmdi zmdi-delete error--text"
                                    })
                                  ]
                                )
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("v-list-item-action", { staticClass: "w-20" }, [
                            _c(
                              "span",
                              { staticClass: "fs-12 grey--text fw-normal" },
                              [_vm._v(_vm._s(tickets.date))]
                            )
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              })
            ],
            2
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TotalDownloading.vue?vue&type=template&id=29030c61&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TotalDownloading.vue?vue&type=template&id=29030c61& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "bandwidth-widget" },
    [
      _c("div", { staticClass: "pl-4 mb-4" }, [
        _c("h2", [_vm._v(_vm._s(_vm.$t("message.totalDownloading")))]),
        _vm._v(" "),
        _c("p", { staticClass: "fs-14 mb-0 fw-light" }, [_vm._v("March 2018")])
      ]),
      _vm._v(" "),
      _c("line-chart-with-area", {
        attrs: {
          dataSet: _vm.bandwidthData,
          dataLabels: _vm.labels,
          lineTension: 0.5,
          height: 105,
          color: _vm.ChartConfig.color.warning,
          enableXAxesLine: false
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TrafficChannelV2.vue?vue&type=template&id=12af774d&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TrafficChannelV2.vue?vue&type=template&id=12af774d& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-list",
    { staticClass: "traffic-channel-widget" },
    [
      _c("v-list-item", [
        _c("p", [_vm._v("Direct")]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("span", [_vm._v("55%")]),
            _vm._v(" "),
            _c("v-progress-linear", {
              attrs: { value: "55", height: "7", color: "primary" }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("v-list-item", [
        _c("p", [_vm._v("Referral")]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("span", [_vm._v("78%")]),
            _vm._v(" "),
            _c("v-progress-linear", {
              attrs: { value: "78", height: "7", color: "primary" }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("v-list-item", [
        _c("p", [_vm._v("Facebook")]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("span", [_vm._v("68%")]),
            _vm._v(" "),
            _c("v-progress-linear", {
              attrs: { value: "68", height: "7", color: "primary" }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("v-list-item", [
        _c("p", [_vm._v("Google")]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("span", [_vm._v("23%")]),
            _vm._v(" "),
            _c("v-progress-linear", {
              attrs: { value: "23", height: "7", color: "primary" }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("v-list-item", [
        _c("p", [_vm._v("Instagram")]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("span", [_vm._v("57%")]),
            _vm._v(" "),
            _c("v-progress-linear", {
              attrs: { value: "57", height: "7", color: "primary" }
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersStat.vue?vue&type=template&id=26063821&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UsersStat.vue?vue&type=template&id=26063821& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    { attrs: { customClasses: "user-stats-wrap" } },
    [
      _c("p", { staticClass: "grey--text fs-12 mb-2 fw-normal" }, [
        _vm._v(_vm._s(_vm.$t("message.maleUsers")))
      ]),
      _vm._v(" "),
      _c("h4", [_vm._v("4250(56%)")]),
      _vm._v(" "),
      _c("v-progress-linear", {
        staticClass: "user1",
        attrs: { height: "6" },
        model: {
          value: _vm.maleUsers,
          callback: function($$v) {
            _vm.maleUsers = $$v
          },
          expression: "maleUsers"
        }
      }),
      _vm._v(" "),
      _c("p", { staticClass: "grey--text fs-12 mb-2 fw-normal" }, [
        _vm._v(_vm._s(_vm.$t("message.femaleUsers")))
      ]),
      _vm._v(" "),
      _c("h4", [_vm._v("3250(44%)")]),
      _vm._v(" "),
      _c("v-progress-linear", {
        staticClass: "user2",
        attrs: { color: "warning", height: "6" },
        model: {
          value: _vm.femaleUsers,
          callback: function($$v) {
            _vm.femaleUsers = $$v
          },
          expression: "femaleUsers"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "visitor-area-chart" }, [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "pos-relative" },
      [_c("visitors-stacked-area-chart", { attrs: { height: 220 } })],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "d-custom-flex justify-space-between mb-5 label-wrap" },
      [
        _c("h4", [_vm._v("Visitors")]),
        _vm._v(" "),
        _c("div", { staticClass: "d-custom-flex justify-space-between w-30" }, [
          _c("div", { staticClass: "w-50" }, [
            _c("h4", { staticClass: "info--text mb-0" }, [_vm._v("$ 35,455")]),
            _vm._v(" "),
            _c("p", { staticClass: "fs-12 grey--text mb-0 fw-normal" }, [
              _vm._v("International Visior")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "w-50" }, [
            _c("h4", { staticClass: "primary--text mb-0" }, [
              _vm._v("$ 35,455")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "fs-12 grey--text mb-0 fw-normal" }, [
              _vm._v("Domestic Visior")
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/WebAnalytics.vue?vue&type=template&id=c774c8e8&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/WebAnalytics.vue?vue&type=template&id=c774c8e8& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-container",
        { attrs: { fluid: "", "pb-0": "" } },
        [_c("visitors-area-chart")],
        1
      ),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-row",
            { staticClass: "stats-card-v4" },
            [
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "4",
                    lg: "4",
                    md: "4",
                    sm: "4",
                    cols: "12",
                    "b-50": ""
                  }
                },
                [
                  _c("stats-card-v4", {
                    attrs: {
                      title: _vm.$t("message.totalPageViews"),
                      value: "35,455",
                      body: "+5% from yesterday",
                      status: 1,
                      data: [20, 15, 12, 6, 18, 17, 20, 2],
                      labels: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "May",
                        "Jun",
                        "Jul",
                        "Aug"
                      ],
                      color: _vm.ChartConfig.color.info
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "4",
                    lg: "4",
                    md: "4",
                    sm: "4",
                    cols: "12",
                    "b-50": ""
                  }
                },
                [
                  _c("stats-card-v4", {
                    attrs: {
                      title: _vm.$t("message.totalEarnings"),
                      value: "$4,500",
                      body: "-2% from yesterday",
                      status: 0,
                      data: [15, 20, 6, 9, 18, 17, 20, 2],
                      labels: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "May",
                        "Jun",
                        "Jul",
                        "Aug"
                      ],
                      color: _vm.ChartConfig.color.warning
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  attrs: {
                    xl: "4",
                    lg: "4",
                    md: "4",
                    sm: "4",
                    cols: "12",
                    "b-50": ""
                  }
                },
                [
                  _c("stats-card-v4", {
                    attrs: {
                      title: _vm.$t("message.impressions"),
                      value: "45,000",
                      body: "+5% from yesterday",
                      status: 1,
                      data: [1, 15, 12, 6, 18, 17, 20, 22],
                      labels: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "May",
                        "Jun",
                        "Jul",
                        "Aug"
                      ],
                      color: _vm.ChartConfig.color.danger
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.activityAroundWorld"),
                    fullScreen: true,
                    closeable: true,
                    reloadable: true,
                    colClasses: "col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12",
                    contentCustomClass: "pt-0"
                  }
                },
                [_c("activity-around-world")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.deviceSeparations"),
                    fullScreen: true,
                    closeable: true,
                    reloadable: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12",
                    customClasses: "device-seperation-wrap"
                  }
                },
                [_c("device-separations")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.browserStatics"),
                    fullBlock: true,
                    fullScreen: true,
                    closeable: true,
                    reloadable: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12"
                  }
                },
                [_c("browser-statics")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.trafficChannel"),
                    fullScreen: true,
                    closeable: true,
                    reloadable: true,
                    footer: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                    contentCustomClass: "pt-0"
                  }
                },
                [
                  _c("traffic-channel-v2"),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass:
                        "grey--text d-custom-flex align-items-center justify-content-end",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                      _vm._v(" "),
                      _c("span", { staticClass: "fs-12 fw-normal" }, [
                        _vm._v(_vm._s(_vm.$t("message.updated10MinAgo")))
                      ])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.serverLoad"),
                    fullScreen: true,
                    closeable: true,
                    reloadable: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                    contentCustomClass: "pt-0 h-100"
                  }
                },
                [
                  _c("server-load", {
                    attrs: { styles: "width: 100%; height: 275px;" }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12",
                    contentCustomClass: "pb-0 px-0"
                  }
                },
                [_c("bandwidth-usage-v2")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    contentCustomClass: "pb-0 px-0",
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12"
                  }
                },
                [_c("total-downloading")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.notifications"),
                    fullBlock: true,
                    closeable: true,
                    reloadable: true,
                    fullScreen: true,
                    footer: true,
                    colClasses: "col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12"
                  }
                },
                [
                  _c("notifications"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "d-custom-flex justify-space-between",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c("v-btn", { attrs: { small: "", color: "primary" } }, [
                        _vm._v("View All")
                      ]),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass:
                            "grey--text d-custom-flex align-items-center"
                        },
                        [
                          _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v("Updated 10 min ago")
                          ])
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.supportTickets"),
                    fullBlock: true,
                    closeable: true,
                    reloadable: true,
                    fullScreen: true,
                    footer: true,
                    colClasses: "col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12"
                  }
                },
                [
                  _c("support-tickets"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "d-custom-flex justify-space-between",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c("v-btn", { attrs: { small: "", color: "primary" } }, [
                        _vm._v("View All")
                      ]),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass:
                            "grey--text d-custom-flex align-items-center"
                        },
                        [
                          _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v("Updated 10 min ago")
                          ])
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  staticClass: "stats-widget-v3",
                  attrs: { xl: "3", lg: "3", md: "6", sm: "12", cols: "6" }
                },
                [
                  _c("stats-card-v3", {
                    attrs: {
                      icon: "bookmark",
                      title: _vm.$t("message.totalBookmarked"),
                      value: "25,700",
                      color: "primary"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticClass: "stats-widget-v3",
                  attrs: { xl: "3", lg: "3", md: "6", sm: "6", cols: "6" }
                },
                [
                  _c("stats-card-v3", {
                    attrs: {
                      icon: "cloud_download",
                      title: _vm.$t("message.itemsDownloads"),
                      value: "14,200",
                      color: "success"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticClass: "stats-widget-v3",
                  attrs: { xl: "3", lg: "3", md: "6", sm: "6", cols: "6" }
                },
                [
                  _c("stats-card-v3", {
                    attrs: {
                      icon: "cloud_upload",
                      title: _vm.$t("message.itemsUploaded"),
                      value: "5,400",
                      color: "error"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticClass: "stats-widget-v3",
                  attrs: { xl: "3", lg: "3", md: "6", sm: "6", cols: "6" }
                },
                [
                  _c("stats-card-v3", {
                    attrs: {
                      icon: "insert_chart",
                      title: _vm.$t("message.totalAnalytics"),
                      value: "1,288",
                      color: "info"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.hotKeywords"),
                    fullBlock: true,
                    closeable: true,
                    reloadable: true,
                    fullScreen: true,
                    footer: true,
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12"
                  }
                },
                [
                  _c("hot-keywords"),
                  _vm._v(" "),
                  _c("div", { attrs: { slot: "footer" }, slot: "footer" }, [
                    _c(
                      "span",
                      {
                        staticClass:
                          "grey--text d-custom-flex align-items-center"
                      },
                      [
                        _c("i", { staticClass: "zmdi zmdi-replay mr-2" }),
                        _vm._v(" "),
                        _c("span", { staticClass: "fs-12 fw-normal" }, [
                          _vm._v("Updated 10 min ago")
                        ])
                      ]
                    )
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "6", cols: "12" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        {
                          staticClass: "d-block",
                          attrs: { xl: "6", lg: "6", md: "6", cols: "12" }
                        },
                        [_c("users-stat")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticClass: "d-block",
                          attrs: { xl: "6", lg: "6", md: "6", cols: "12" }
                        },
                        [_c("platform-users-stat")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            heading: _vm.$t("message.serverStatus"),
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-12 d-xs-half-block w-full",
                            closeable: true,
                            reloadable: true,
                            fullScreen: true,
                            footer: true,
                            contentCustomClass: "py-0"
                          }
                        },
                        [
                          _c("server-status"),
                          _vm._v(" "),
                          _c(
                            "div",
                            { attrs: { slot: "footer" }, slot: "footer" },
                            [
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "grey--text d-custom-flex align-items-center"
                                },
                                [
                                  _c("i", {
                                    staticClass: "zmdi zmdi-replay mr-2"
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    { staticClass: "fs-12 fw-normal" },
                                    [_vm._v("Updated 10 min ago")]
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "6", cols: "12" } },
                [_c("customer-support-service")],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "6", cols: "12" } },
                [_c("promo-widget")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/index.js":
/*!***********************************!*\
  !*** ./resources/js/api/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: 'https://reactify.theironnetwork.org/data/'
}));

/***/ }),

/***/ "./resources/js/components/Charts/DeviceSeprations.js":
/*!************************************************************!*\
  !*** ./resources/js/components/Charts/DeviceSeprations.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Doughnut Chart


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Doughnut"],
  data: function data() {
    return {
      options: {
        cutoutPercentage: 70,
        legend: {
          display: false,
          position: 'bottom'
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Computer', 'Mobile', 'Tablets'],
      datasets: [{
        data: [250, 100, 70],
        backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning],
        borderWidth: [0, 0, 0],
        hoverBackgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info, Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.warning]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartV2.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/Charts/LineChartV2.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Line Chart Widget


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    data: {
      type: Array,
      "default": function _default() {
        return [10, 30, 30, 60, 75, 10];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0.6;
      }
    },
    labels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F'];
      }
    },
    color: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info;
      }
    },
    enableShadow: {
      type: Boolean,
      "default": false
    }
  },
  data: function data() {
    return {
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false,
              drawTicks: false
            },
            display: false
          }],
          xAxes: [{
            ticks: {
              display: false,
              beginAtZero: true
            },
            gridLines: {
              display: true,
              drawBorder: false
            },
            display: false
          }]
        },
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].shadowColor;
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 6;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    this.renderChart({
      labels: this.labels,
      datasets: [{
        label: 'My First dataset',
        data: this.data,
        lineTension: this.lineTension,
        borderColor: this.color,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        pointHoverBorderWidth: 0,
        borderWidth: 2,
        fill: false
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartWithArea.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/Charts/LineChartWithArea.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");



/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    dataSet: {
      type: Array,
      "default": function _default() {
        return [85, 50, 80, 90, 10, 5, 15, 60, 30, 20, 50];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0;
      }
    },
    dataLabels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
      }
    },
    enableXAxesLine: {
      type: Boolean,
      "default": true
    },
    color: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    gridLines: {
      type: Boolean,
      "default": false
    }
  },
  data: function data() {
    return {
      gradient: null,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              display: false
            },
            display: false
          }],
          xAxes: [{
            gridLines: {
              display: this.gridLines,
              drawBorder: false
            },
            ticks: {
              display: this.enableXAxesLine
            },
            display: this.enableXAxesLine
          }]
        },
        tooltip: {
          enabled: true
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: true
      }
    };
  },
  mounted: function mounted() {
    this.gradient = this.$refs.canvas.getContext("2d").createLinearGradient(0, 280, 0, 0);
    this.gradient.addColorStop(0, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])('#FFF', 0.1));
    this.gradient.addColorStop(1, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(this.color, 0.4));
    this.renderChart({
      labels: this.dataLabels,
      datasets: [{
        label: "Data",
        lineTension: this.lineTension,
        pointRadius: 0,
        fill: true,
        backgroundColor: this.gradient,
        borderColor: this.color,
        data: this.dataSet,
        borderWidth: 3
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/VisitorsStackedAreaChart.js":
/*!********************************************************************!*\
  !*** ./resources/js/components/Charts/VisitorsStackedAreaChart.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
// Line Chart Widget



/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  data: function data() {
    return {
      gradient1: null,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false,
              drawTicks: false
            }
          }],
          xAxes: [{
            ticks: {
              display: true,
              beginAtZero: true
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    var ctx = this.$refs.canvas.getContext('2d');
    var _stroke = ctx.stroke;

    ctx.stroke = function () {
      ctx.save();
      ctx.shadowColor = 'rgba(0,0,0,0.5)';
      ctx.shadowBlur = 8;
      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 4;

      _stroke.apply(this, arguments);

      ctx.restore();
    }; // let gradientColor = ' '


    this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 110, 0, 0);
    this.gradient1.addColorStop(0, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white, 0.2));
    this.gradient1.addColorStop(0.5, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info, 0.2));
    this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 110, 0, 0);
    this.gradient2.addColorStop(0, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white, 0.1));
    this.gradient2.addColorStop(0.5, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary, 0.1));
    this.renderChart({
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', "Nov", "Dec"],
      datasets: [{
        label: 'Data 1',
        data: [30, 70, 40, 50, 20, 30, 40, 50, 20, 50, 70, 30],
        lineTension: 0.5,
        backgroundColor: this.gradient2,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        pointHoverBorderWidth: 0,
        borderWidth: 2,
        fill: true
      }, {
        label: 'Data 2',
        data: [45, 65, 50, 20, 30, 25, 20, 15, 55, 45, 70, 45],
        lineTension: 0.5,
        backgroundColor: this.gradient1,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        pointHoverBorderWidth: 0,
        borderWidth: 2,
        fill: true
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/StatsCardV3/StatsCardV3.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/StatsCardV3/StatsCardV3.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatsCardV3_vue_vue_type_template_id_345bd8e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatsCardV3.vue?vue&type=template&id=345bd8e4& */ "./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=template&id=345bd8e4&");
/* harmony import */ var _StatsCardV3_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatsCardV3.vue?vue&type=script&lang=js& */ "./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StatsCardV3_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StatsCardV3_vue_vue_type_template_id_345bd8e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StatsCardV3_vue_vue_type_template_id_345bd8e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StatsCardV3/StatsCardV3.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV3_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV3.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV3_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=template&id=345bd8e4&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=template&id=345bd8e4& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV3_vue_vue_type_template_id_345bd8e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV3.vue?vue&type=template&id=345bd8e4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV3/StatsCardV3.vue?vue&type=template&id=345bd8e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV3_vue_vue_type_template_id_345bd8e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV3_vue_vue_type_template_id_345bd8e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/StatsCardV4/StatsCardV4.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/StatsCardV4/StatsCardV4.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatsCardV4_vue_vue_type_template_id_368a0910___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatsCardV4.vue?vue&type=template&id=368a0910& */ "./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=template&id=368a0910&");
/* harmony import */ var _StatsCardV4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatsCardV4.vue?vue&type=script&lang=js& */ "./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StatsCardV4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StatsCardV4_vue_vue_type_template_id_368a0910___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StatsCardV4_vue_vue_type_template_id_368a0910___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StatsCardV4/StatsCardV4.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV4.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV4_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=template&id=368a0910&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=template&id=368a0910& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV4_vue_vue_type_template_id_368a0910___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatsCardV4.vue?vue&type=template&id=368a0910& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StatsCardV4/StatsCardV4.vue?vue&type=template&id=368a0910&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV4_vue_vue_type_template_id_368a0910___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatsCardV4_vue_vue_type_template_id_368a0910___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/24x7CustomerSupport.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/Widgets/24x7CustomerSupport.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _24x7CustomerSupport_vue_vue_type_template_id_725880d5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./24x7CustomerSupport.vue?vue&type=template&id=725880d5& */ "./resources/js/components/Widgets/24x7CustomerSupport.vue?vue&type=template&id=725880d5&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _24x7CustomerSupport_vue_vue_type_template_id_725880d5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _24x7CustomerSupport_vue_vue_type_template_id_725880d5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/24x7CustomerSupport.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/24x7CustomerSupport.vue?vue&type=template&id=725880d5&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/24x7CustomerSupport.vue?vue&type=template&id=725880d5& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_24x7CustomerSupport_vue_vue_type_template_id_725880d5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./24x7CustomerSupport.vue?vue&type=template&id=725880d5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/24x7CustomerSupport.vue?vue&type=template&id=725880d5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_24x7CustomerSupport_vue_vue_type_template_id_725880d5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_24x7CustomerSupport_vue_vue_type_template_id_725880d5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ActivityAroundWorld.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/Widgets/ActivityAroundWorld.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ActivityAroundWorld_vue_vue_type_template_id_47fd44ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActivityAroundWorld.vue?vue&type=template&id=47fd44ca& */ "./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=template&id=47fd44ca&");
/* harmony import */ var _ActivityAroundWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ActivityAroundWorld.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ActivityAroundWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ActivityAroundWorld_vue_vue_type_template_id_47fd44ca___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ActivityAroundWorld_vue_vue_type_template_id_47fd44ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ActivityAroundWorld.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivityAroundWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ActivityAroundWorld.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivityAroundWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=template&id=47fd44ca&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=template&id=47fd44ca& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivityAroundWorld_vue_vue_type_template_id_47fd44ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ActivityAroundWorld.vue?vue&type=template&id=47fd44ca& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ActivityAroundWorld.vue?vue&type=template&id=47fd44ca&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivityAroundWorld_vue_vue_type_template_id_47fd44ca___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivityAroundWorld_vue_vue_type_template_id_47fd44ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BandwidthUsageV2.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Widgets/BandwidthUsageV2.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BandwidthUsageV2_vue_vue_type_template_id_c12f5212___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BandwidthUsageV2.vue?vue&type=template&id=c12f5212& */ "./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=template&id=c12f5212&");
/* harmony import */ var _BandwidthUsageV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BandwidthUsageV2.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BandwidthUsageV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BandwidthUsageV2_vue_vue_type_template_id_c12f5212___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BandwidthUsageV2_vue_vue_type_template_id_c12f5212___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BandwidthUsageV2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BandwidthUsageV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BandwidthUsageV2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BandwidthUsageV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=template&id=c12f5212&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=template&id=c12f5212& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BandwidthUsageV2_vue_vue_type_template_id_c12f5212___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BandwidthUsageV2.vue?vue&type=template&id=c12f5212& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BandwidthUsageV2.vue?vue&type=template&id=c12f5212&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BandwidthUsageV2_vue_vue_type_template_id_c12f5212___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BandwidthUsageV2_vue_vue_type_template_id_c12f5212___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BrowserStatics.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/BrowserStatics.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BrowserStatics_vue_vue_type_template_id_1b5a86b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BrowserStatics.vue?vue&type=template&id=1b5a86b0& */ "./resources/js/components/Widgets/BrowserStatics.vue?vue&type=template&id=1b5a86b0&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _BrowserStatics_vue_vue_type_template_id_1b5a86b0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BrowserStatics_vue_vue_type_template_id_1b5a86b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BrowserStatics.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BrowserStatics.vue?vue&type=template&id=1b5a86b0&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BrowserStatics.vue?vue&type=template&id=1b5a86b0& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BrowserStatics_vue_vue_type_template_id_1b5a86b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BrowserStatics.vue?vue&type=template&id=1b5a86b0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BrowserStatics.vue?vue&type=template&id=1b5a86b0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BrowserStatics_vue_vue_type_template_id_1b5a86b0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BrowserStatics_vue_vue_type_template_id_1b5a86b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/DeviceSeprations.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Widgets/DeviceSeprations.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DeviceSeprations_vue_vue_type_template_id_59a3d2d3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DeviceSeprations.vue?vue&type=template&id=59a3d2d3& */ "./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=template&id=59a3d2d3&");
/* harmony import */ var _DeviceSeprations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DeviceSeprations.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DeviceSeprations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DeviceSeprations_vue_vue_type_template_id_59a3d2d3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DeviceSeprations_vue_vue_type_template_id_59a3d2d3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/DeviceSeprations.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceSeprations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DeviceSeprations.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceSeprations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=template&id=59a3d2d3&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=template&id=59a3d2d3& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceSeprations_vue_vue_type_template_id_59a3d2d3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DeviceSeprations.vue?vue&type=template&id=59a3d2d3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceSeprations.vue?vue&type=template&id=59a3d2d3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceSeprations_vue_vue_type_template_id_59a3d2d3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceSeprations_vue_vue_type_template_id_59a3d2d3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/HotKeywords.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/HotKeywords.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HotKeywords_vue_vue_type_template_id_2227cc08___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HotKeywords.vue?vue&type=template&id=2227cc08& */ "./resources/js/components/Widgets/HotKeywords.vue?vue&type=template&id=2227cc08&");
/* harmony import */ var _HotKeywords_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HotKeywords.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/HotKeywords.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HotKeywords_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HotKeywords_vue_vue_type_template_id_2227cc08___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HotKeywords_vue_vue_type_template_id_2227cc08___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/HotKeywords.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/HotKeywords.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Widgets/HotKeywords.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HotKeywords_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./HotKeywords.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/HotKeywords.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HotKeywords_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/HotKeywords.vue?vue&type=template&id=2227cc08&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/HotKeywords.vue?vue&type=template&id=2227cc08& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HotKeywords_vue_vue_type_template_id_2227cc08___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./HotKeywords.vue?vue&type=template&id=2227cc08& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/HotKeywords.vue?vue&type=template&id=2227cc08&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HotKeywords_vue_vue_type_template_id_2227cc08___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HotKeywords_vue_vue_type_template_id_2227cc08___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Notifications.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/Notifications.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Notifications_vue_vue_type_template_id_7f0ea40d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Notifications.vue?vue&type=template&id=7f0ea40d& */ "./resources/js/components/Widgets/Notifications.vue?vue&type=template&id=7f0ea40d&");
/* harmony import */ var _Notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Notifications.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Notifications.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Notifications_vue_vue_type_template_id_7f0ea40d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Notifications_vue_vue_type_template_id_7f0ea40d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Notifications.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Notifications.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Notifications.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Notifications.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Notifications.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Notifications.vue?vue&type=template&id=7f0ea40d&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Notifications.vue?vue&type=template&id=7f0ea40d& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Notifications_vue_vue_type_template_id_7f0ea40d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Notifications.vue?vue&type=template&id=7f0ea40d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Notifications.vue?vue&type=template&id=7f0ea40d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Notifications_vue_vue_type_template_id_7f0ea40d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Notifications_vue_vue_type_template_id_7f0ea40d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/PlatformUsersStat.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/PlatformUsersStat.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PlatformUsersStat_vue_vue_type_template_id_2f859524___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PlatformUsersStat.vue?vue&type=template&id=2f859524& */ "./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=template&id=2f859524&");
/* harmony import */ var _PlatformUsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PlatformUsersStat.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PlatformUsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PlatformUsersStat_vue_vue_type_template_id_2f859524___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PlatformUsersStat_vue_vue_type_template_id_2f859524___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/PlatformUsersStat.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlatformUsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PlatformUsersStat.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlatformUsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=template&id=2f859524&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=template&id=2f859524& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlatformUsersStat_vue_vue_type_template_id_2f859524___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PlatformUsersStat.vue?vue&type=template&id=2f859524& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/PlatformUsersStat.vue?vue&type=template&id=2f859524&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlatformUsersStat_vue_vue_type_template_id_2f859524___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlatformUsersStat_vue_vue_type_template_id_2f859524___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/PromoWidget.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/PromoWidget.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PromoWidget_vue_vue_type_template_id_490af918___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PromoWidget.vue?vue&type=template&id=490af918& */ "./resources/js/components/Widgets/PromoWidget.vue?vue&type=template&id=490af918&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _PromoWidget_vue_vue_type_template_id_490af918___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PromoWidget_vue_vue_type_template_id_490af918___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/PromoWidget.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/PromoWidget.vue?vue&type=template&id=490af918&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/PromoWidget.vue?vue&type=template&id=490af918& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PromoWidget_vue_vue_type_template_id_490af918___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PromoWidget.vue?vue&type=template&id=490af918& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/PromoWidget.vue?vue&type=template&id=490af918&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PromoWidget_vue_vue_type_template_id_490af918___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PromoWidget_vue_vue_type_template_id_490af918___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ServerLoad.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/ServerLoad.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ServerLoad_vue_vue_type_template_id_b0e69118___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ServerLoad.vue?vue&type=template&id=b0e69118& */ "./resources/js/components/Widgets/ServerLoad.vue?vue&type=template&id=b0e69118&");
/* harmony import */ var _ServerLoad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ServerLoad.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ServerLoad.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ServerLoad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ServerLoad_vue_vue_type_template_id_b0e69118___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ServerLoad_vue_vue_type_template_id_b0e69118___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ServerLoad.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ServerLoad.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ServerLoad.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerLoad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ServerLoad.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerLoad.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerLoad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ServerLoad.vue?vue&type=template&id=b0e69118&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ServerLoad.vue?vue&type=template&id=b0e69118& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerLoad_vue_vue_type_template_id_b0e69118___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ServerLoad.vue?vue&type=template&id=b0e69118& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerLoad.vue?vue&type=template&id=b0e69118&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerLoad_vue_vue_type_template_id_b0e69118___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerLoad_vue_vue_type_template_id_b0e69118___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ServerStatus.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/ServerStatus.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ServerStatus_vue_vue_type_template_id_51d67700___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ServerStatus.vue?vue&type=template&id=51d67700& */ "./resources/js/components/Widgets/ServerStatus.vue?vue&type=template&id=51d67700&");
/* harmony import */ var _ServerStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ServerStatus.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ServerStatus.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ServerStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ServerStatus_vue_vue_type_template_id_51d67700___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ServerStatus_vue_vue_type_template_id_51d67700___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ServerStatus.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ServerStatus.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ServerStatus.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ServerStatus.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerStatus.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ServerStatus.vue?vue&type=template&id=51d67700&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ServerStatus.vue?vue&type=template&id=51d67700& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerStatus_vue_vue_type_template_id_51d67700___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ServerStatus.vue?vue&type=template&id=51d67700& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ServerStatus.vue?vue&type=template&id=51d67700&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerStatus_vue_vue_type_template_id_51d67700___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ServerStatus_vue_vue_type_template_id_51d67700___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SupportTickets.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportTickets.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SupportTickets_vue_vue_type_template_id_382b407a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SupportTickets.vue?vue&type=template&id=382b407a& */ "./resources/js/components/Widgets/SupportTickets.vue?vue&type=template&id=382b407a&");
/* harmony import */ var _SupportTickets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SupportTickets.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SupportTickets.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SupportTickets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SupportTickets_vue_vue_type_template_id_382b407a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SupportTickets_vue_vue_type_template_id_382b407a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SupportTickets.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SupportTickets.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportTickets.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportTickets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportTickets.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportTickets.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportTickets_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SupportTickets.vue?vue&type=template&id=382b407a&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportTickets.vue?vue&type=template&id=382b407a& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportTickets_vue_vue_type_template_id_382b407a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportTickets.vue?vue&type=template&id=382b407a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportTickets.vue?vue&type=template&id=382b407a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportTickets_vue_vue_type_template_id_382b407a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportTickets_vue_vue_type_template_id_382b407a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TotalDownloading.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Widgets/TotalDownloading.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TotalDownloading_vue_vue_type_template_id_29030c61___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TotalDownloading.vue?vue&type=template&id=29030c61& */ "./resources/js/components/Widgets/TotalDownloading.vue?vue&type=template&id=29030c61&");
/* harmony import */ var _TotalDownloading_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TotalDownloading.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TotalDownloading.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TotalDownloading_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TotalDownloading_vue_vue_type_template_id_29030c61___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TotalDownloading_vue_vue_type_template_id_29030c61___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TotalDownloading.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TotalDownloading.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TotalDownloading.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalDownloading_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TotalDownloading.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TotalDownloading.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalDownloading_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TotalDownloading.vue?vue&type=template&id=29030c61&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TotalDownloading.vue?vue&type=template&id=29030c61& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalDownloading_vue_vue_type_template_id_29030c61___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TotalDownloading.vue?vue&type=template&id=29030c61& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TotalDownloading.vue?vue&type=template&id=29030c61&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalDownloading_vue_vue_type_template_id_29030c61___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalDownloading_vue_vue_type_template_id_29030c61___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TrafficChannelV2.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Widgets/TrafficChannelV2.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TrafficChannelV2_vue_vue_type_template_id_12af774d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TrafficChannelV2.vue?vue&type=template&id=12af774d& */ "./resources/js/components/Widgets/TrafficChannelV2.vue?vue&type=template&id=12af774d&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _TrafficChannelV2_vue_vue_type_template_id_12af774d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TrafficChannelV2_vue_vue_type_template_id_12af774d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TrafficChannelV2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TrafficChannelV2.vue?vue&type=template&id=12af774d&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TrafficChannelV2.vue?vue&type=template&id=12af774d& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannelV2_vue_vue_type_template_id_12af774d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TrafficChannelV2.vue?vue&type=template&id=12af774d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TrafficChannelV2.vue?vue&type=template&id=12af774d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannelV2_vue_vue_type_template_id_12af774d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TrafficChannelV2_vue_vue_type_template_id_12af774d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/UsersStat.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/UsersStat.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UsersStat_vue_vue_type_template_id_26063821___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UsersStat.vue?vue&type=template&id=26063821& */ "./resources/js/components/Widgets/UsersStat.vue?vue&type=template&id=26063821&");
/* harmony import */ var _UsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UsersStat.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/UsersStat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UsersStat_vue_vue_type_template_id_26063821___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UsersStat_vue_vue_type_template_id_26063821___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/UsersStat.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/UsersStat.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Widgets/UsersStat.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./UsersStat.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersStat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersStat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/UsersStat.vue?vue&type=template&id=26063821&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/UsersStat.vue?vue&type=template&id=26063821& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersStat_vue_vue_type_template_id_26063821___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UsersStat.vue?vue&type=template&id=26063821& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersStat.vue?vue&type=template&id=26063821&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersStat_vue_vue_type_template_id_26063821___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersStat_vue_vue_type_template_id_26063821___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/VisitorsAreaChart.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/VisitorsAreaChart.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VisitorsAreaChart_vue_vue_type_template_id_3ae1a9b1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1& */ "./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1&");
/* harmony import */ var _VisitorsAreaChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VisitorsAreaChart.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VisitorsAreaChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VisitorsAreaChart_vue_vue_type_template_id_3ae1a9b1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VisitorsAreaChart_vue_vue_type_template_id_3ae1a9b1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/VisitorsAreaChart.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitorsAreaChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./VisitorsAreaChart.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitorsAreaChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitorsAreaChart_vue_vue_type_template_id_3ae1a9b1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/VisitorsAreaChart.vue?vue&type=template&id=3ae1a9b1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitorsAreaChart_vue_vue_type_template_id_3ae1a9b1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VisitorsAreaChart_vue_vue_type_template_id_3ae1a9b1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/dashboard/WebAnalytics.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/dashboard/WebAnalytics.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _WebAnalytics_vue_vue_type_template_id_c774c8e8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WebAnalytics.vue?vue&type=template&id=c774c8e8& */ "./resources/js/views/dashboard/WebAnalytics.vue?vue&type=template&id=c774c8e8&");
/* harmony import */ var _WebAnalytics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WebAnalytics.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/WebAnalytics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _WebAnalytics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _WebAnalytics_vue_vue_type_template_id_c774c8e8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _WebAnalytics_vue_vue_type_template_id_c774c8e8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/WebAnalytics.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/WebAnalytics.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/dashboard/WebAnalytics.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_WebAnalytics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./WebAnalytics.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/WebAnalytics.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_WebAnalytics_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/WebAnalytics.vue?vue&type=template&id=c774c8e8&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/dashboard/WebAnalytics.vue?vue&type=template&id=c774c8e8& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WebAnalytics_vue_vue_type_template_id_c774c8e8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./WebAnalytics.vue?vue&type=template&id=c774c8e8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/WebAnalytics.vue?vue&type=template&id=c774c8e8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WebAnalytics_vue_vue_type_template_id_c774c8e8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WebAnalytics_vue_vue_type_template_id_c774c8e8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);