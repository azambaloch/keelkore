(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[108],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Snackbar.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Snackbar.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      snackbar: false,
      snackbar2: false,
      y: "top",
      x: null,
      mode: "",
      mode2: "",
      timeout: 6000,
      text: "Hello, I'm a snackbar",
      color: ""
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Snackbar.vue?vue&type=template&id=70be207a&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Snackbar.vue?vue&type=template&id=70be207a& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "custom-flex" },
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "app-card",
            { attrs: { heading: _vm.$t("message.snackbar") } },
            [
              _c("div", { staticClass: "mb-4" }, [
                _c("p", [
                  _vm._v(
                    "The standard snackbar is useful for calling attention to some function that has just happened."
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c("v-col", { attrs: { cols: "12", md: "6" } }, [
                    _c("ul", { staticClass: "list-unstyled" }, [
                      _c(
                        "li",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              value: "left",
                              label: "Left"
                            },
                            model: {
                              value: _vm.x,
                              callback: function($$v) {
                                _vm.x = $$v
                              },
                              expression: "x"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              value: "right",
                              label: "Right"
                            },
                            model: {
                              value: _vm.x,
                              callback: function($$v) {
                                _vm.x = $$v
                              },
                              expression: "x"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              value: "top",
                              label: "Top"
                            },
                            model: {
                              value: _vm.y,
                              callback: function($$v) {
                                _vm.y = $$v
                              },
                              expression: "y"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              value: "bottom",
                              label: "Bottom"
                            },
                            model: {
                              value: _vm.y,
                              callback: function($$v) {
                                _vm.y = $$v
                              },
                              expression: "y"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              value: "multi-line",
                              label: "Multi-line (mobile)"
                            },
                            model: {
                              value: _vm.mode,
                              callback: function($$v) {
                                _vm.mode = $$v
                              },
                              expression: "mode"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              value: "vertical",
                              label: "Vertical (mobile)"
                            },
                            model: {
                              value: _vm.mode,
                              callback: function($$v) {
                                _vm.mode = $$v
                              },
                              expression: "mode"
                            }
                          })
                        ],
                        1
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c("v-text-field", {
                        attrs: { type: "text", label: "Text" },
                        model: {
                          value: _vm.text,
                          callback: function($$v) {
                            _vm.text = $$v
                          },
                          expression: "text"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: { type: "number", label: "Timeout" },
                        model: {
                          value: _vm.timeout,
                          callback: function($$v) {
                            _vm.timeout = _vm._n($$v)
                          },
                          expression: "timeout"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { block: "", color: "primary", large: "" },
                          nativeOn: {
                            click: function($event) {
                              _vm.snackbar = true
                            }
                          }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.showSnackbar")))]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-snackbar",
                {
                  attrs: {
                    timeout: _vm.timeout,
                    top: _vm.y === "top",
                    bottom: _vm.y === "bottom",
                    right: _vm.x === "right",
                    left: _vm.x === "left",
                    "multi-line": _vm.mode === "multi-line",
                    vertical: _vm.mode === "vertical"
                  },
                  model: {
                    value: _vm.snackbar,
                    callback: function($$v) {
                      _vm.snackbar = $$v
                    },
                    expression: "snackbar"
                  }
                },
                [
                  _c("p", { staticClass: "ma-0" }, [_vm._v(_vm._s(_vm.text))]),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "error" },
                      nativeOn: {
                        click: function($event) {
                          _vm.snackbar = false
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("message.close")))]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            { attrs: { heading: _vm.$t("message.contextualSnackbar") } },
            [
              _c("div", { staticClass: "mb-4" }, [
                _c("p", { staticClass: "mb-0" }, [
                  _vm._v(
                    "You can also apply a color to the snackbar to better fit your implementation."
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c(
                        "v-radio-group",
                        {
                          attrs: { row: "" },
                          model: {
                            value: _vm.color,
                            callback: function($$v) {
                              _vm.color = $$v
                            },
                            expression: "color"
                          }
                        },
                        _vm._l(
                          ["success", "info", "error", "cyan darken-2"],
                          function(colorValue, i) {
                            return _c("v-radio", {
                              key: i,
                              attrs: {
                                light: "",
                                value: colorValue,
                                label: colorValue,
                                color: colorValue
                              }
                            })
                          }
                        ),
                        1
                      ),
                      _vm._v(" "),
                      _c("v-checkbox", {
                        attrs: {
                          color: "primary",
                          value: "multi-line",
                          label: "Multi-line (mobile)"
                        },
                        model: {
                          value: _vm.mode2,
                          callback: function($$v) {
                            _vm.mode2 = $$v
                          },
                          expression: "mode2"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-checkbox", {
                        attrs: {
                          color: "primary",
                          value: "vertical",
                          label: "Vertical (mobile)"
                        },
                        model: {
                          value: _vm.mode2,
                          callback: function($$v) {
                            _vm.mode2 = $$v
                          },
                          expression: "mode2"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c("v-text-field", {
                        attrs: { type: "text", label: "Text" },
                        model: {
                          value: _vm.text,
                          callback: function($$v) {
                            _vm.text = $$v
                          },
                          expression: "text"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: { type: "number", label: "Timeout" },
                        model: {
                          value: _vm.timeout,
                          callback: function($$v) {
                            _vm.timeout = _vm._n($$v)
                          },
                          expression: "timeout"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: {
                            block: "",
                            large: "",
                            color: "warning",
                            dark: ""
                          },
                          nativeOn: {
                            click: function($event) {
                              _vm.snackbar2 = true
                            }
                          }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.showSnackbar")))]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-snackbar",
                {
                  attrs: {
                    timeout: _vm.timeout,
                    color: _vm.color,
                    "multi-line": _vm.mode2 === "multi-line",
                    vertical: _vm.mode2 === "vertical"
                  },
                  model: {
                    value: _vm.snackbar2,
                    callback: function($$v) {
                      _vm.snackbar2 = $$v
                    },
                    expression: "snackbar2"
                  }
                },
                [
                  _vm._v("\n\t\t\t\t" + _vm._s(_vm.text) + "\n\t\t\t\t"),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "error" },
                      nativeOn: {
                        click: function($event) {
                          _vm.snackbar2 = false
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("message.close")))]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Snackbar.vue":
/*!*****************************************************!*\
  !*** ./resources/js/views/ui-elements/Snackbar.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Snackbar_vue_vue_type_template_id_70be207a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Snackbar.vue?vue&type=template&id=70be207a& */ "./resources/js/views/ui-elements/Snackbar.vue?vue&type=template&id=70be207a&");
/* harmony import */ var _Snackbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Snackbar.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Snackbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Snackbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Snackbar_vue_vue_type_template_id_70be207a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Snackbar_vue_vue_type_template_id_70be207a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Snackbar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Snackbar.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Snackbar.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Snackbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Snackbar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Snackbar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Snackbar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Snackbar.vue?vue&type=template&id=70be207a&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Snackbar.vue?vue&type=template&id=70be207a& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Snackbar_vue_vue_type_template_id_70be207a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Snackbar.vue?vue&type=template&id=70be207a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Snackbar.vue?vue&type=template&id=70be207a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Snackbar_vue_vue_type_template_id_70be207a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Snackbar_vue_vue_type_template_id_70be207a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);