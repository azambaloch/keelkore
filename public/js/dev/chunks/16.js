(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[16],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CountDown/CountDown.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CountDown/CountDown.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["totalTime"],
  mounted: function mounted() {
    var timeLeftVar = this.secondsToTime(this.seconds);
    this.time = timeLeftVar;
    this.startTimer();
  },
  data: function data() {
    return {
      time: {},
      seconds: this.totalTime ? this.totalTime : 2600,
      timer: 0
    };
  },
  computed: {
    hours: function hours() {
      return this.time.h;
    },
    totalMinutes: function totalMinutes() {
      return this.time.m;
    },
    totalSeconds: function totalSeconds() {
      return this.time.s;
    }
  },
  methods: {
    secondsToTime: function secondsToTime(secs) {
      var hours = Math.floor(secs / (60 * 60));
      var divisor_for_minutes = secs % (60 * 60);
      var minutes = Math.floor(divisor_for_minutes / 60);
      var divisor_for_seconds = divisor_for_minutes % 60;
      var seconds = Math.ceil(divisor_for_seconds);
      var obj = {
        h: hours,
        m: minutes,
        s: seconds
      };
      return obj;
    },
    startTimer: function startTimer() {
      var _this = this;

      if (this.timer === 0) {
        this.timer = setInterval(function () {
          _this.countDown();
        }, 1000);
      }
    },
    countDown: function countDown() {
      // Remove one second, and call secondsToTime method
      var seconds = this.seconds - 1;
      this.time = this.secondsToTime(seconds);
      this.seconds = seconds; // Check if we're at zero.

      if (seconds == 0) {
        clearInterval(this.timer);
      }
    }
  },
  destroyed: function destroyed() {
    clearInterval(this.timer);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      dialog: false,
      content: "",
      editorOption: {
        modules: {
          toolbar: [[{
            header: [1, 2, 3, 4, 5, 6, false]
          }], ["bold", "italic", "underline", "strike"], ["blockquote", "code-block"], ["link", "image"]]
        }
      }
    };
  },
  methods: {
    open: function open() {
      this.dialog = true;
    },
    closeDialog: function closeDialog() {
      this.dialog = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Activity.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Activity.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    this.getActivities();
  },
  methods: {
    getActivities: function getActivities() {
      var _this = this;

      var self = this;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/activities.js").then(function (response) {
        self.loader = false;
        _this.activities = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  data: function data() {
    return {
      activities: []
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// app config

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      employeePayroll: [],
      snackbar: false,
      snackbarMessage: "",
      timeout: 2000,
      y: "top",
      loader: false,
      headers: [{
        text: "Name",
        align: "left",
        sortable: false
      }, {
        text: "Designation",
        sortable: false
      }, {
        text: "Salary",
        sortable: false
      }, {
        text: "Status",
        sortable: false
      }, {
        text: "Actions",
        sortable: false
      }]
    };
  },
  mounted: function mounted() {
    // call getEmployeePayrolls method
    this.getEmployeePayrolls();
  },
  methods: {
    // get data from API
    getEmployeePayrolls: function getEmployeePayrolls() {
      this.loader = true;
      var self = this;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("employeePayrols.js").then(function (response) {
        self.loader = false;
        self.employeePayroll = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    },
    // update employee status
    updateEmployeeStatus: function updateEmployeeStatus(emplyoee) {
      var _this = this;

      this.loader = true;
      setTimeout(function () {
        _this.loader = false;
        emplyoee.status = 1;

        var index = _this.employeePayroll.indexOf(emplyoee);

        _this.employeePayroll[index] = emplyoee;
        _this.snackbar = true;
        _this.snackbarMessage = "Employee Payroll Updated Successfully";
      }, 1500);
    },
    // confirmation dialog to delete
    onDeleteEmployePayroll: function onDeleteEmployePayroll(employee) {
      this.$refs.deleteConfirmationDialog.openDialog();
      this.selectEmployeePayroll = employee;
    },
    // delete email if cofirmation is true
    deleteEmployeePayroll: function deleteEmployeePayroll() {
      var _this2 = this;

      this.$refs.deleteConfirmationDialog.close();
      this.loader = true;
      var deletedEmplyoee = this.employeePayroll;
      var index = deletedEmplyoee.indexOf(this.selectEmployeePayroll);
      setTimeout(function () {
        _this2.loader = false;
        _this2.selectEmployeePayroll = null;

        _this2.employeePayroll.splice(index, 1);

        _this2.snackbar = true;
        _this2.snackbarMessage = "Employee Payroll Deleted Successfully";
      }, 1500);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EventCalendar.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/EventCalendar.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      selectedTab: 0,
      birthdays: [{
        id: 1,
        dob: "23 May 2019",
        year: 30,
        userName: "Callie Carpenter",
        userAvatar: "/static/avatars/user-1.jpg"
      }, {
        id: 2,
        dob: "12 June 2018",
        year: 30,
        userName: "Charlotte Mclaughlin",
        userAvatar: "/static/avatars/user-2.jpg"
      }, {
        id: 3,
        dob: "14 June 2018",
        year: 30,
        userName: "Tasha Tyler",
        userAvatar: "/static/avatars/user-3.jpg"
      }, {
        id: 4,
        dob: "24 August 2019",
        year: 22,
        userName: "Eugene Rios",
        userAvatar: "/static/avatars/user-4.jpg"
      }, {
        id: 5,
        dob: "1 September 2019",
        year: 20,
        userName: "Jarred Bell",
        userAvatar: "/static/avatars/user-5.jpg"
      }],
      events: {
        "Jun 2018": [{
          id: 1,
          name: "Comapany Aniversery Celebrartion",
          month: "Jun",
          date: "14",
          place: "St. 128 New York",
          "class": "info",
          time: "8:00 AM - 10:00 AM"
        }, {
          id: 2,
          name: "Night Dinner With Micke",
          month: "Jun",
          date: "15",
          place: "GT Raod, Near IT Park New York",
          "class": "danger",
          time: "8:00 AM - 10:00 AM"
        }, {
          id: 3,
          name: "Suspendisse luctus est at tincidunt tristique",
          month: "Jun",
          date: "10",
          place: "GT Raod, Near IT Park New York",
          "class": "warning",
          time: "8:00 AM - 10:00 AM"
        }, {
          id: 4,
          name: "Nunc pellentesque velit orci, nec mattis",
          month: "Jun",
          date: "8",
          place: "GT Raod, Near IT Park New York",
          "class": "primary",
          time: "8:00 AM - 10:00 AM"
        }, {
          id: 5,
          name: "Aliquam vehicula nisi enim, sed tincidunt leo",
          month: "Jun",
          date: "5",
          place: "GT Raod, Near IT Park New York",
          "class": "success",
          time: "8:00 AM - 10:00 AM"
        }]
      },
      settings: {
        maxScrollbarLength: 260
      },
      selectedEventForReschedule: {
        key: "",
        data: null
      }
    };
  },
  methods: {
    onTabChange: function onTabChange(value) {
      this.selectedTab = value;
    },
    onDeleteEvent: function onDeleteEvent(eventName, key) {
      var indexOfEvent = this.events[key].indexOf(eventName);
      this.events[key].splice(indexOfEvent, 1);
    },
    onRescheduleEvent: function onRescheduleEvent(eventName, key) {
      console.log(eventName + key);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Mailbox.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Mailbox.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_DialogBox_ComposeEmail__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/DialogBox/ComposeEmail */ "./resources/js/components/DialogBox/ComposeEmail.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var allEmails = [{
  id: 1,
  from: {
    avatar: "/static/avatars/user-1.jpg",
    name: "Precious Munoz",
    email: "Precious@example.com"
  },
  type: "inbox",
  subject: "Praesent fringilla arcu vel metus vehicula sagittis",
  date: "7 Mar 2018",
  starred: false
}, {
  id: 2,
  from: {
    avatar: "/static/avatars/user-2.jpg",
    name: "Anita Barker",
    email: "Anita@example.com"
  },
  type: "inbox",
  subject: "Mauris congue ipsum eros",
  date: "10 Mar 2018",
  starred: true
}, {
  id: 3,
  to: {
    avatar: "/static/avatars/user-3.jpg",
    name: "Alexus Ellis",
    email: "Alexus@example.com"
  },
  type: "sent",
  subject: "Quisque eu justo sed nisl facilisis fermentum",
  date: "11 Mar 2018",
  starred: false
}, {
  id: 4,
  to: {
    avatar: "/static/avatars/user-4.jpg",
    name: "Connor Griffith",
    email: "Connor@example.com"
  },
  type: "sent",
  subject: "Maecenas volutpat tortor non maximus suscipit",
  date: "11 Mar 2018",
  starred: false
}, {
  id: 5,
  to: {
    avatar: "/static/avatars/user-5.jpg",
    name: "Alonzo Owens",
    email: "Alonzo@example.com"
  },
  type: "draft",
  subject: "Lorem ipsum is simply dummy text",
  date: "11 Mar 2018"
}, {
  id: 6,
  from: {
    avatar: "/static/avatars/user-6.jpg",
    name: "Holly Bowen",
    email: "Holly@example.com"
  },
  type: "inbox",
  subject: "Sed vestibulum sagittis lorem, non pharetra sapien finibus",
  date: "15 Mar 2018",
  starred: false
}, {
  id: 7,
  from: {
    avatar: "/static/avatars/user-7.jpg",
    name: "Tylor Vasquez",
    email: "Tylor@example.com"
  },
  type: "inbox",
  subject: " Quisque euismod est eget nulla commodo",
  date: "17 Mar 2018",
  starred: false
}, {
  id: 8,
  from: {
    avatar: "/static/avatars/user-8.jpg",
    name: "Teresa Cortez",
    email: "Teresa@example.com"
  },
  type: "inbox",
  subject: "Nullam sit amet volutpat ex, sed porttitor",
  date: "17 Mar 2018",
  starred: false
}];
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      allEmails: allEmails,
      emails: allEmails.filter(function (email) {
        return email.type === "inbox";
      })
    };
  },
  components: {
    ComposeEmailDialog: Components_DialogBox_ComposeEmail__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    deleteEmail: function deleteEmail(selectedEmail) {
      var indexOfSelectedEmailInAllEmails = this.allEmails.indexOf(selectedEmail);
      var indexOfSelectedEmailInCurrentEmails = this.emails.indexOf(selectedEmail);
      this.allEmails.splice(indexOfSelectedEmailInAllEmails, 1);
      this.emails.splice(indexOfSelectedEmailInCurrentEmails, 1);
    },
    onChangeTab: function onChangeTab(value) {
      switch (value) {
        case 0:
          this.emails = this.allEmails.filter(function (email) {
            return email.type === "inbox";
          });
          break;

        case 1:
          this.emails = this.allEmails.filter(function (email) {
            return email.type === "sent";
          });
          break;

        case 2:
          this.emails = this.allEmails.filter(function (email) {
            return email.type === "draft";
          });
          break;

        default:
          break;
      }
    },
    markAsStar: function markAsStar(email) {
      var indexOfEmailInCurrentEmails = this.emails.indexOf(email);
      this.emails[indexOfEmailInCurrentEmails].starred = !email.starred;
    },
    onComposeEmail: function onComposeEmail() {
      this.$refs.composeEmailDialog.open();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CountDown_CountDown__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../CountDown/CountDown */ "./resources/js/components/CountDown/CountDown.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// components

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    CountDown: _CountDown_CountDown__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProfitShare.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProfitShare.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_DoughnutChartV2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/DoughnutChartV2 */ "./resources/js/components/Charts/DoughnutChartV2.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var _views_dashboard_data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../views/dashboard/data */ "./resources/js/views/dashboard/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // constants

 //data


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    DoughnutChartV2: _Charts_DoughnutChartV2__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      profitShare: _views_dashboard_data__WEBPACK_IMPORTED_MODULE_2__["profitShare"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatus.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectStatus.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      settings: {
        maxScrollbarLength: 100
      },
      selected: [],
      headers: [{
        text: "Select",
        align: "left",
        sortable: false,
        value: "select"
      }, {
        text: "Project Name",
        align: "left",
        sortable: false,
        value: "productName"
      }, {
        text: "Priority",
        align: "left",
        sortable: false,
        value: "priority"
      }, {
        text: "Status",
        align: "left",
        sortable: false,
        value: "status"
      }, {
        text: "Team",
        align: "center",
        sortable: false,
        value: "team"
      }],
      items: []
    };
  },
  mounted: function mounted() {
    this.getProjects();
  },
  methods: {
    toggleAll: function toggleAll() {
      if (this.selected.length) this.selected = [];else this.selected = this.items.slice();
    },
    getProjects: function getProjects() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/projectStatus.js").then(function (response) {
        _this.loader = false;
        _this.items = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TodayWeather.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TodayWeather.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
 // function to get today weather icon

function getIcon(id) {
  if (id >= 200 && id < 300) {
    return "wi wi-night-showers";
  } else if (id >= 300 && id < 500) {
    return "wi day-sleet";
  } else if (id >= 500 && id < 600) {
    return "wi wi-night-showers";
  } else if (id >= 600 && id < 700) {
    return "wi wi-day-snow";
  } else if (id >= 700 && id < 800) {
    return "wi wi-day-fog";
  } else if (id === 800) {
    return "wi wi-day-sunny";
  } else if (id >= 801 && id < 803) {
    return "wi wi-night-partly-cloudy";
  } else if (id >= 802 && id < 900) {
    return "wi wi-day-cloudy";
  } else if (id === 905 || id >= 951 && id <= 956) {
    return "wi wi-day-windy";
  } else if (id >= 900 && id < 1000) {
    return "wi wi-night-showers";
  }
}

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      city: "",
      temp: "",
      weatherDescription: "",
      weatherIcon: "",
      currentTime: moment__WEBPACK_IMPORTED_MODULE_0___default()().format("dddd DD MMMM")
    };
  },
  mounted: function mounted() {
    var _this = this;

    var appid = "b1b15e88fa797225412429c1c50c122a1"; // Your api id

    var apikey = "69b72ed255ce5efad910bd946685883a"; // Your apikey

    var city = "Mohali"; // city name

    this.$http.get("https://api.openweathermap.org/data/2.5/weather?q=" + city + "&cnt=6&units=metric&mode=json&appid=" + appid + "&apikey=" + apikey).then(function (response) {
      _this.city = response.data.name;
      _this.temp = response.data.main.temp_max;
      _this.weatherDescription = response.data.weather[0].description;
      _this.weatherIcon = getIcon(response.data.weather[0].id);
    }, function (response) {
      console.log(response);
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopAuthors.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TopAuthors.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  computed: {
    slickOptions: function slickOptions() {
      return {
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        dots: false
      };
    }
  },
  mounted: function mounted() {
    this.getAuthors();
  },
  methods: {
    getAuthors: function getAuthors() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_1__["default"].get("vuely/topAuthors.js").then(function (response) {
        _this.authors = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  },
  data: function data() {
    return {
      authors: null
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersList.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UsersList.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      usersList: "",
      settings: {
        maxScrollbarLength: 160
      },
      selectDeletedUser: null,
      editUser: null,
      addUserModal: true,
      dialog2: false,
      addNewUser: {
        customer_name: "",
        customer_email: "",
        photo_url: "/static/avatars/user-8.jpg"
      },
      reload: false,
      snackbar: false,
      snackbarMessage: "",
      timeout: 2000,
      y: "top"
    };
  },
  // call getusers method
  mounted: function mounted() {
    this.getUsers();
  },
  methods: {
    // get user details from an API
    getUsers: function getUsers() {
      this.reload = true;
      var self = this;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("newCustomers.js").then(function (response) {
        self.reload = false;
        self.usersList = response.data;
      })["catch"](function (error) {
        console.log("error" + error);
      });
    },
    // function to add a user in a user list
    addUsers: function addUsers() {
      var _this = this;

      if (this.addNewUser.customer_name !== "") {
        this.dialog2 = false;
        this.reload = true;
        setTimeout(function () {
          _this.reload = false;

          _this.usersList.push(_objectSpread(_objectSpread({}, _this.addNewUser), {}, {
            customer_id: new Date().getTime()
          }));

          _this.snackbar = true;
          _this.snackbarMessage = "User Added Successfully";
          _this.addNewUser.customer_name = "";
          _this.addNewUser.customer_email = "";
        }, 1500);
      }
    },
    // to edit user
    onEditUser: function onEditUser(user) {
      this.addUserModal = false;
      this.dialog2 = true;
      this.editUser = user;
    },
    // to open dialog for update user details
    updateUserModal: function updateUserModal() {
      this.addUserModal = true;
    },
    // to update user list
    updateUser: function updateUser() {
      var _this2 = this;

      this.dialog2 = false;
      this.reload = true;
      setTimeout(function () {
        _this2.reload = false;
        _this2.snackbar = true;

        for (var i = 0; i < _this2.usersList.length; i++) {
          var user = _this2.usersList;

          if (user[i].customer_id === _this2.editUser.customer_id) {
            _this2.usersList[i] = _this2.editUser;
          }
        }

        _this2.snackbarMessage = "User Updated Successfully";
      }, 1500);
    },
    // confirmation box to delete user
    onDeleteUser: function onDeleteUser(user) {
      this.$refs.deleteConfirmationDialog.openDialog();
      this.selectDeletedUser = user;
    },
    // delete user
    deleteUser: function deleteUser() {
      var _this3 = this;

      this.$refs.deleteConfirmationDialog.close();
      this.reload = true;
      var newUser = this.usersList;
      var index = newUser.indexOf(this.selectDeletedUser);
      setTimeout(function () {
        _this3.reload = false;
        _this3.selectDeletedUser = null;

        _this3.usersList.splice(index, 1);

        _this3.snackbar = true;
        _this3.snackbarMessage = "User Deleted Successfully";
      }, 1500);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Agency.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Agency.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_BarChart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/BarChart */ "./resources/js/components/Charts/BarChart.js");
/* harmony import */ var Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Charts/LineChartWithArea */ "./resources/js/components/Charts/LineChartWithArea.js");
/* harmony import */ var Components_Charts_LineChartShadow__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Charts/LineChartShadow */ "./resources/js/components/Charts/LineChartShadow.js");
/* harmony import */ var Components_Charts_AdCampaignPerfomance__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Charts/AdCampaignPerfomance */ "./resources/js/components/Charts/AdCampaignPerfomance.js");
/* harmony import */ var Components_Charts_NewsLetterCampaign__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Charts/NewsLetterCampaign */ "./resources/js/components/Charts/NewsLetterCampaign.js");
/* harmony import */ var Components_Widgets_ToDoList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/ToDoList */ "./resources/js/components/Widgets/ToDoList.vue");
/* harmony import */ var Components_Widgets_ProjectStatus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/ProjectStatus */ "./resources/js/components/Widgets/ProjectStatus.vue");
/* harmony import */ var Components_Widgets_UsersList__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/Widgets/UsersList */ "./resources/js/components/Widgets/UsersList.vue");
/* harmony import */ var Components_Widgets_RecentSales__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Components/Widgets/RecentSales */ "./resources/js/components/Widgets/RecentSales.vue");
/* harmony import */ var Components_Widgets_QuoteOfTheDay__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Components/Widgets/QuoteOfTheDay */ "./resources/js/components/Widgets/QuoteOfTheDay.vue");
/* harmony import */ var Components_Widgets_NewOrderCountdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Components/Widgets/NewOrderCountdown */ "./resources/js/components/Widgets/NewOrderCountdown.vue");
/* harmony import */ var Components_Widgets_Followers__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Components/Widgets/Followers */ "./resources/js/components/Widgets/Followers.vue");
/* harmony import */ var Components_Widgets_BookingInfo__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Components/Widgets/BookingInfo */ "./resources/js/components/Widgets/BookingInfo.vue");
/* harmony import */ var Components_Widgets_EmployeePayroll__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! Components/Widgets/EmployeePayroll */ "./resources/js/components/Widgets/EmployeePayroll.vue");
/* harmony import */ var Components_Widgets_Mailbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! Components/Widgets/Mailbox */ "./resources/js/components/Widgets/Mailbox.vue");
/* harmony import */ var Components_Widgets_ActiveUser__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! Components/Widgets/ActiveUser */ "./resources/js/components/Widgets/ActiveUser.vue");
/* harmony import */ var Components_Widgets_TopAuthors__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! Components/Widgets/TopAuthors */ "./resources/js/components/Widgets/TopAuthors.vue");
/* harmony import */ var Components_Widgets_Chat__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! Components/Widgets/Chat */ "./resources/js/components/Widgets/Chat.vue");
/* harmony import */ var Components_Widgets_EventCalendar__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! Components/Widgets/EventCalendar */ "./resources/js/components/Widgets/EventCalendar.vue");
/* harmony import */ var Components_Widgets_Activity__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! Components/Widgets/Activity */ "./resources/js/components/Widgets/Activity.vue");
/* harmony import */ var Components_Widgets_TodayWeather__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! Components/Widgets/TodayWeather */ "./resources/js/components/Widgets/TodayWeather.vue");
/* harmony import */ var Components_Widgets_ProfitShare__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! Components/Widgets/ProfitShare */ "./resources/js/components/Widgets/ProfitShare.vue");
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./data */ "./resources/js/views/dashboard/data.js");
/* harmony import */ var Views_widgets_data__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! Views/widgets/data */ "./resources/js/views/widgets/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




 // widgets

















 // data



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BarChart: Components_Charts_BarChart__WEBPACK_IMPORTED_MODULE_0__["default"],
    LineChartWithArea: Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_1__["default"],
    LineChartShadow: Components_Charts_LineChartShadow__WEBPACK_IMPORTED_MODULE_2__["default"],
    TodoList: Components_Widgets_ToDoList__WEBPACK_IMPORTED_MODULE_5__["default"],
    ProjectStatus: Components_Widgets_ProjectStatus__WEBPACK_IMPORTED_MODULE_6__["default"],
    UsersList: Components_Widgets_UsersList__WEBPACK_IMPORTED_MODULE_7__["default"],
    RecentSales: Components_Widgets_RecentSales__WEBPACK_IMPORTED_MODULE_8__["default"],
    QuoteOfTheDay: Components_Widgets_QuoteOfTheDay__WEBPACK_IMPORTED_MODULE_9__["default"],
    NewOrderCountdown: Components_Widgets_NewOrderCountdown__WEBPACK_IMPORTED_MODULE_10__["default"],
    Followers: Components_Widgets_Followers__WEBPACK_IMPORTED_MODULE_11__["default"],
    BookingInfo: Components_Widgets_BookingInfo__WEBPACK_IMPORTED_MODULE_12__["default"],
    EmployeePayroll: Components_Widgets_EmployeePayroll__WEBPACK_IMPORTED_MODULE_13__["default"],
    AdCampaignPerfomance: Components_Charts_AdCampaignPerfomance__WEBPACK_IMPORTED_MODULE_3__["default"],
    Mailbox: Components_Widgets_Mailbox__WEBPACK_IMPORTED_MODULE_14__["default"],
    ActiveUser: Components_Widgets_ActiveUser__WEBPACK_IMPORTED_MODULE_15__["default"],
    TopAuthors: Components_Widgets_TopAuthors__WEBPACK_IMPORTED_MODULE_16__["default"],
    Chat: Components_Widgets_Chat__WEBPACK_IMPORTED_MODULE_17__["default"],
    EventCalendar: Components_Widgets_EventCalendar__WEBPACK_IMPORTED_MODULE_18__["default"],
    Activity: Components_Widgets_Activity__WEBPACK_IMPORTED_MODULE_19__["default"],
    NewsLetterCampaign: Components_Charts_NewsLetterCampaign__WEBPACK_IMPORTED_MODULE_4__["default"],
    TodayWeather: Components_Widgets_TodayWeather__WEBPACK_IMPORTED_MODULE_20__["default"],
    ProfitShare: Components_Widgets_ProfitShare__WEBPACK_IMPORTED_MODULE_21__["default"]
  },
  data: function data() {
    return {
      earnedToday: _data__WEBPACK_IMPORTED_MODULE_22__["earnedToday"],
      itemsSold: _data__WEBPACK_IMPORTED_MODULE_22__["itemsSold"],
      salesAndEarning: _data__WEBPACK_IMPORTED_MODULE_22__["salesAndEarning"],
      totalEarnings: _data__WEBPACK_IMPORTED_MODULE_22__["totalEarnings"],
      netProfit: _data__WEBPACK_IMPORTED_MODULE_22__["netProfit"],
      onlineRevenue: _data__WEBPACK_IMPORTED_MODULE_22__["onlineRevenue"],
      totalExpences: _data__WEBPACK_IMPORTED_MODULE_22__["totalExpences"],
      adCampaignPerfomanceData: _data__WEBPACK_IMPORTED_MODULE_22__["adCampaignPerfomanceData"],
      activeUser: Views_widgets_data__WEBPACK_IMPORTED_MODULE_23__["activeUser"]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CountDown/CountDown.vue?vue&type=template&id=43c0b458&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/CountDown/CountDown.vue?vue&type=template&id=43c0b458& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h3", { staticClass: "mb-0" }, [
      _vm._v(
        _vm._s(_vm.totalMinutes) +
          " : " +
          _vm._s(
            _vm.totalSeconds < 10 ? "0" + _vm.totalSeconds : _vm.totalSeconds
          )
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=template&id=4441cda1&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=template&id=4441cda1& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-dialog",
    {
      attrs: { persistent: "", "max-width": "600px", "max-height": "100%" },
      model: {
        value: _vm.dialog,
        callback: function($$v) {
          _vm.dialog = $$v
        },
        expression: "dialog"
      }
    },
    [
      _c(
        "v-card",
        [
          _c("v-card-title", [
            _c("span", { staticClass: "headline" }, [_vm._v("Compose Email")])
          ]),
          _vm._v(" "),
          _c(
            "v-card-text",
            [
              _c("v-text-field", { attrs: { label: "Email", required: "" } }),
              _vm._v(" "),
              _c("v-text-field", { attrs: { label: "Subject" } }),
              _vm._v(" "),
              _c("quill-editor", {
                ref: "myQuillEditor",
                attrs: { options: _vm.editorOption },
                model: {
                  value: _vm.content,
                  callback: function($$v) {
                    _vm.content = $$v
                  },
                  expression: "content"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-card-actions",
            { staticClass: "px-5 pb-5 mx-1" },
            [
              _c("v-spacer"),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { color: "error" },
                  nativeOn: {
                    click: function($event) {
                      return _vm.closeDialog.apply(null, arguments)
                    }
                  }
                },
                [_vm._v("Close")]
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { color: "primary" },
                  nativeOn: {
                    click: function($event) {
                      return _vm.closeDialog.apply(null, arguments)
                    }
                  }
                },
                [_vm._v("Compose")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Activity.vue?vue&type=template&id=31e0dd0c&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Activity.vue?vue&type=template&id=31e0dd0c& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      attrs: {
        heading: _vm.$t("message.activity"),
        closeable: true,
        reloadable: true,
        fullScreen: true,
        colClasses: "activity-widget-wrap pa-0"
      }
    },
    [
      _vm.activities.length > 0
        ? _c(
            "div",
            { staticClass: "activity-widget" },
            [
              _c(
                "vue-perfect-scrollbar",
                {
                  staticClass: "activity-scroll",
                  staticStyle: { height: "433px" }
                },
                [
                  _c(
                    "ul",
                    { staticClass: "list-unstyled px-3" },
                    _vm._l(_vm.activities, function(activity) {
                      return _c(
                        "li",
                        { key: activity.id },
                        [
                          _c("v-badge", {
                            staticClass: "primary floating rounded",
                            class: activity.badgeClass,
                            attrs: { value: false }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "activity-time" }, [
                            _vm._v(_vm._s(activity.date))
                          ]),
                          _vm._v(" "),
                          _c("p", [_vm._v(_vm._s(activity.name))])
                        ],
                        1
                      )
                    }),
                    0
                  )
                ]
              )
            ],
            1
          )
        : _vm._e()
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BookingInfo.vue?vue&type=template&id=75bce38c&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BookingInfo.vue?vue&type=template&id=75bce38c& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("ul", { staticClass: "p-0 list-group-flush list-unstyled" }, [
    _c("li", { staticClass: "d-custom-flex align-center py-2 px-4" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "w-50 d-custom-flex justify-space-between align-center"
        },
        [
          _c("span", { staticClass: "fs-12 fw-normal" }, [_vm._v("1450")]),
          _vm._v(" "),
          _c(
            "v-btn",
            { staticClass: "my-2", attrs: { small: "", icon: "" } },
            [_c("v-icon", [_vm._v("more_horiz")])],
            1
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("li", { staticClass: "d-custom-flex align-center py-2 px-4" }, [
      _vm._m(1),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "w-50 d-custom-flex justify-space-between align-center"
        },
        [
          _c("span", { staticClass: "fs-12 fw-normal" }, [_vm._v("1000")]),
          _vm._v(" "),
          _c(
            "v-btn",
            { staticClass: "my-2", attrs: { small: "", icon: "" } },
            [_c("v-icon", [_vm._v("more_horiz")])],
            1
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("li", { staticClass: "d-custom-flex align-center py-2 px-4" }, [
      _vm._m(2),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "w-50 d-custom-flex justify-space-between align-center"
        },
        [
          _c("span", { staticClass: "fs-12 fw-normal" }, [_vm._v("450")]),
          _vm._v(" "),
          _c(
            "v-btn",
            { staticClass: "my-2", attrs: { small: "", icon: "" } },
            [_c("v-icon", [_vm._v("more_horiz")])],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "span",
      { staticClass: "w-50 d-custom-flex align-items-center" },
      [
        _c("i", {
          staticClass: "zmdi zmdi-file-plus mr-4 primary--text font-lg"
        }),
        _vm._v(" "),
        _c("span", { staticClass: "fs-12 fw-normal" }, [_vm._v("Booking")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "span",
      { staticClass: "w-50 d-custom-flex align-items-center" },
      [
        _c("i", {
          staticClass: "zmdi zmdi-assignment-check mr-4 success--text font-lg"
        }),
        _vm._v(" "),
        _c("span", { staticClass: "fs-12 fw-normal" }, [_vm._v("Confirmed")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "span",
      { staticClass: "w-50 d-custom-flex align-items-center" },
      [
        _c("i", { staticClass: "zmdi zmdi-time mr-4 error--text font-lg" }),
        _vm._v(" "),
        _c("span", { staticClass: "fs-12 fw-normal" }, [_vm._v("Pending")])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=template&id=4385befc&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=template&id=4385befc& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "employee-Payroll-table" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.employeePayroll,
          "hide-default-footer": ""
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c(
                    "td",
                    { staticClass: "d-custom-flex align-items-center" },
                    [
                      _c("img", {
                        staticClass: "img-responsive rounded-circle mr-3",
                        attrs: {
                          width: "30",
                          height: "30",
                          src: item.employeeAvatar
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "fs-14 text-truncate" }, [
                        _vm._v(_vm._s(item.employeeName))
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.designation))]),
                  _vm._v(" "),
                  _c("td", [_vm._v("$ " + _vm._s(item.salary))]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      item.status == 1
                        ? _c(
                            "v-badge",
                            { staticClass: "success", attrs: { value: false } },
                            [_vm._v("Done")]
                          )
                        : _c(
                            "v-badge",
                            { staticClass: "warning", attrs: { value: false } },
                            [_vm._v("Pending")]
                          )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      item.status !== 1
                        ? _c(
                            "v-btn",
                            {
                              staticClass: "ma-0",
                              attrs: { icon: "", small: "" },
                              on: {
                                click: function($event) {
                                  return _vm.updateEmployeeStatus(item)
                                }
                              }
                            },
                            [
                              _c("v-icon", { attrs: { color: "grey" } }, [
                                _vm._v("check")
                              ])
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "ma-0",
                          attrs: { icon: "", small: "" },
                          on: {
                            click: function($event) {
                              return _vm.onDeleteEmployePayroll(item)
                            }
                          }
                        },
                        [
                          _c("v-icon", { attrs: { color: "grey" } }, [
                            _vm._v("clear")
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { top: _vm.y === "top", timeout: _vm.timeout },
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [_vm._v("\n         " + _vm._s(_vm.snackbarMessage) + "\n      ")]
      ),
      _vm._v(" "),
      _c("delete-confirmation-dialog", {
        ref: "deleteConfirmationDialog",
        attrs: {
          heading: "Are You Sure You Want To Delete?",
          message:
            "Are You Sure You Want To Delete This Emplyoee Payroll Permanently?"
        },
        on: { onConfirm: _vm.deleteEmployeePayroll }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EventCalendar.vue?vue&type=template&id=30f89f1d&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/EventCalendar.vue?vue&type=template&id=30f89f1d& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("app-card", { attrs: { fullBlock: true } }, [
    _c("div", { staticClass: "app-event-calendar" }, [
      _c("div", { staticClass: "event-tab-wrapper" }, [
        _c(
          "div",
          { staticClass: "event-heading-wrap primary darken-1 white--text" },
          [
            _c("span", { staticClass: "font-2x fw-semi-bold" }, [
              _vm._v("23 May 2018")
            ]),
            _vm._v(" "),
            _c("p", [_vm._v("Wednesday")]),
            _vm._v(" "),
            _c("ul", { staticClass: "list-unstyled" }, [
              _c(
                "li",
                {
                  staticClass: "event-tab",
                  class: { active: _vm.selectedTab === 0 },
                  on: {
                    click: function($event) {
                      return _vm.onTabChange(0)
                    }
                  }
                },
                [
                  _c("a", { attrs: { href: "javascript:void(0)" } }, [
                    _vm._v(_vm._s(_vm.$t("message.birthdays")))
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "li",
                {
                  staticClass: "event-tab",
                  class: { active: _vm.selectedTab === 1 },
                  on: {
                    click: function($event) {
                      return _vm.onTabChange(1)
                    }
                  }
                },
                [
                  _c("a", { attrs: { href: "javascript:void(0)" } }, [
                    _vm._v(_vm._s(_vm.$t("message.events")))
                  ])
                ]
              )
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "tab-content-wrap" },
        [
          _c(
            "vue-perfect-scrollbar",
            {
              staticStyle: { height: "370px" },
              attrs: { settings: _vm.settings }
            },
            [
              _vm.selectedTab === 0
                ? _c(
                    "v-list",
                    {
                      staticClass: "event-user-list list-aqua-ripple",
                      attrs: { "two-line": "" }
                    },
                    [
                      _vm._l(_vm.birthdays, function(birthday) {
                        return [
                          _c(
                            "v-list-item",
                            { key: birthday.id, attrs: { ripple: "" } },
                            [
                              _c("v-list-item-avatar", [
                                _c("img", {
                                  staticClass: "img-responsive",
                                  attrs: { src: birthday.userAvatar }
                                })
                              ]),
                              _vm._v(" "),
                              _c("v-list-item-content", [
                                _c("h6", { staticClass: "mb-1" }, [
                                  _vm._v(_vm._s(birthday.userName))
                                ]),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "grey--text fs-12 fw-normal" },
                                  [
                                    _vm._v(
                                      "Dob: " +
                                        _vm._s(birthday.dob) +
                                        " / " +
                                        _vm._s(birthday.year) +
                                        " Year"
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "v-list-item-action",
                                [
                                  _c(
                                    "v-btn",
                                    { attrs: { small: "", color: "primary" } },
                                    [_vm._v(_vm._s(_vm.$t("message.message")))]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      })
                    ],
                    2
                  )
                : _c(
                    "v-list",
                    { attrs: { "two-line": "", subheader: "" } },
                    [
                      _vm._l(_vm.events, function(eventData, key, index) {
                        return [
                          _c(
                            "div",
                            { key: index },
                            [
                              _c(
                                "v-subheader",
                                { staticClass: "grey--text fw-normal" },
                                [_vm._v(_vm._s(key))]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.events[key], function(event, index) {
                                return [
                                  _c(
                                    "div",
                                    { key: index },
                                    [
                                      _c(
                                        "v-list-item",
                                        {
                                          class:
                                            event.class +
                                            "-border app-event-item py-1"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "px-4 mr-4 text-center"
                                            },
                                            [
                                              _c(
                                                "h2",
                                                { staticClass: "mb-0" },
                                                [_vm._v(_vm._s(event.date))]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "grey--text fs-12 fw-normal"
                                                },
                                                [_vm._v(_vm._s(event.month))]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("v-list-item-content", [
                                            _c("h6", { staticClass: "mb-2" }, [
                                              _vm._v(_vm._s(event.name))
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "fs-12 grey--text fw-normal"
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  { staticClass: "mr-2" },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "zmdi zmdi-time"
                                                    }),
                                                    _vm._v(
                                                      " " + _vm._s(event.time)
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c("span", [
                                                  _c("i", {
                                                    staticClass: "zmdi zmdi-pin"
                                                  }),
                                                  _vm._v(
                                                    " " + _vm._s(event.place)
                                                  )
                                                ])
                                              ]
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "v-list-item-action",
                                            [
                                              _c(
                                                "v-list-item-action-text",
                                                {
                                                  staticClass: "d-custom-flex"
                                                },
                                                [
                                                  _c(
                                                    "v-btn",
                                                    {
                                                      staticClass: "ma-1",
                                                      attrs: {
                                                        small: "",
                                                        icon: ""
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.onRescheduleEvent(
                                                            event,
                                                            key
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-icon",
                                                        {
                                                          staticClass:
                                                            "primary--text font-md"
                                                        },
                                                        [_vm._v("edit")]
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-btn",
                                                    {
                                                      staticClass: "ma-1",
                                                      attrs: {
                                                        small: "",
                                                        icon: ""
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.onDeleteEvent(
                                                            event,
                                                            key
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "v-icon",
                                                        {
                                                          staticClass:
                                                            "error--text font-md"
                                                        },
                                                        [_vm._v("delete")]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ]
                      })
                    ],
                    2
                  )
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Followers.vue?vue&type=template&id=97ac398c&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Followers.vue?vue&type=template&id=97ac398c& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "followers-wrap" }, [
    _c("div", { staticClass: "app-flex justify-space-between mb-4" }, [
      _c("h4", [_vm._v(_vm._s(_vm.$t("message.followers")))]),
      _vm._v(" "),
      _vm._m(0)
    ]),
    _vm._v(" "),
    _vm._m(1)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "fs-12 mb-0 success--text" }, [
      _vm._v("Trending : 29%\n          "),
      _c("i", { staticClass: "ti-arrow-up ml-2" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("ul", { staticClass: "list-unstyled fs-12 fw-normal" }, [
        _c("li", { staticClass: "d-flex justify-space-between mb-4" }, [
          _c("span", { staticClass: "fw-semi-bold" }, [_vm._v("Last Week")]),
          _vm._v(" "),
          _c("span", [_vm._v("5400")]),
          _vm._v(" "),
          _c(
            "span",
            { staticClass: "text-right success--text fs-12 fw-normal" },
            [
              _vm._v("\n                20% "),
              _c("i", { staticClass: "ti-arrow-up ml-2" })
            ]
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "d-flex justify-space-between" }, [
          _c("span", { staticClass: "fw-semi-bold" }, [_vm._v("This Week")]),
          _vm._v(" "),
          _c("span", [_vm._v("2400")]),
          _vm._v(" "),
          _c(
            "span",
            { staticClass: "text-right error--text fs-12 fw-normal" },
            [
              _vm._v("\n                -5% "),
              _c("i", { staticClass: "ti-arrow-down ml-2" })
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Mailbox.vue?vue&type=template&id=efe100ce&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Mailbox.vue?vue&type=template&id=efe100ce& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      attrs: {
        heading: _vm.$t("message.mailbox"),
        fullBlock: true,
        withTabs: true,
        footer: true,
        tabs: [
          _vm.$t("message.inbox"),
          _vm.$t("message.sent"),
          _vm.$t("message.draft")
        ],
        customClasses: "mailbox-wrapper"
      },
      on: { onChangeTabCallback: _vm.onChangeTab }
    },
    [
      _vm.emails && _vm.emails.length > 0
        ? _c(
            "v-list",
            { staticClass: "list-aqua-ripple", attrs: { "three-line": "" } },
            [
              _vm._l(_vm.emails, function(mail) {
                return [
                  _c(
                    "v-list-item",
                    { key: mail.id, attrs: { ripple: "" } },
                    [
                      _c("v-list-item-avatar", [
                        mail.type === "inbox"
                          ? _c("img", {
                              staticClass: "img-responsive",
                              attrs: { src: mail.from.avatar }
                            })
                          : _c("img", {
                              staticClass: "img-responsive",
                              attrs: { src: mail.to.avatar }
                            })
                      ]),
                      _vm._v(" "),
                      _c(
                        "v-list-item-content",
                        [
                          mail.type === "inbox"
                            ? _c("div", [
                                _c("h5", { staticClass: "mb-0" }, [
                                  _vm._v(_vm._s(mail.from.name))
                                ]),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "grey--text fs-12 fw-normal" },
                                  [_vm._v(_vm._s(mail.from.email))]
                                )
                              ])
                            : [
                                _c("h5", { staticClass: "mb-0" }, [
                                  _vm._v(_vm._s(mail.to.name))
                                ]),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "grey--text fs-12 fw-normal" },
                                  [_vm._v(_vm._s(mail.to.email))]
                                )
                              ],
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _c("strong", [_vm._v("Subject:")]),
                            _vm._v(" " + _vm._s(mail.subject))
                          ])
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c(
                        "v-list-item-action",
                        [
                          _c("v-list-item-action-text", [
                            _c(
                              "span",
                              {
                                staticClass:
                                  "grey--text fs-12 mb-2 d-block text-right fw-normal"
                              },
                              [_vm._v(_vm._s(mail.date))]
                            ),
                            _vm._v(" "),
                            _c(
                              "ul",
                              {
                                staticClass:
                                  "d-custom-flex list-unstyled icon-wrap"
                              },
                              [
                                _c(
                                  "li",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: mail.type !== "draft",
                                        expression: "mail.type !== 'draft'"
                                      }
                                    ]
                                  },
                                  [
                                    _c(
                                      "v-btn",
                                      { attrs: { small: "", icon: "" } },
                                      [_c("v-icon", [_vm._v("archive")])],
                                      1
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "li",
                                  [
                                    _c(
                                      "v-btn",
                                      {
                                        attrs: { small: "", icon: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.deleteEmail(mail)
                                          }
                                        }
                                      },
                                      [_c("v-icon", [_vm._v("delete")])],
                                      1
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "li",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: mail.type !== "draft",
                                        expression: "mail.type !== 'draft'"
                                      }
                                    ]
                                  },
                                  [
                                    _c(
                                      "v-btn",
                                      {
                                        attrs: { small: "", icon: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.markAsStar(mail)
                                          }
                                        }
                                      },
                                      [
                                        _c("v-icon", [
                                          _vm._v(
                                            _vm._s(
                                              mail.starred
                                                ? "star"
                                                : "star_border"
                                            )
                                          )
                                        ])
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "li",
                                  [
                                    _c(
                                      "v-btn",
                                      { attrs: { small: "", icon: "" } },
                                      [_c("v-icon", [_vm._v("more_horiz")])],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ]
                            )
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              })
            ],
            2
          )
        : _c("div", { staticClass: "pa-4" }, [
            _c("h3", { staticClass: "text-center" }, [
              _vm._v("No Emails Found")
            ])
          ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "action", attrs: { slot: "footer" }, slot: "footer" },
        [
          _c(
            "v-btn",
            {
              attrs: { small: "", color: "error" },
              on: { click: _vm.onComposeEmail }
            },
            [_vm._v(_vm._s(_vm.$t("message.composeNewEmail")))]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("compose-email-dialog", { ref: "composeEmailDialog" })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=template&id=45b18130&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=template&id=45b18130& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "app-block countdown-wrap" }, [
    _c("div", { staticClass: "d-custom-flex" }, [
      _c(
        "div",
        { staticClass: "w-20  mr-3 d-inline-flex justify-center" },
        [
          _c(
            "v-btn",
            {
              staticClass: "ma-0",
              attrs: { fab: "", dark: "", small: "", color: "warning" }
            },
            [
              _c("i", {
                staticClass:
                  "zmdi zmdi-notifications-active animated infinite wobble zmdi-hc-fw font-lg"
              })
            ]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", [
        _c("h6", { staticClass: "mb-0" }, [_vm._v("New order from John")]),
        _vm._v(" "),
        _c(
          "span",
          { staticClass: "fs-12 mb-3 grey--text d-inline-block fw-normal" },
          [_vm._v("Accept or Reject Within")]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "d-custom-flex align-items-center" }, [
          _c(
            "h3",
            {
              staticClass:
                "count-value grey--text rounded d-inline-block text-center mb-0 mr-3"
            },
            [_c("CountDown", { attrs: { time: 5300047000 } })],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "d-inline-flex" },
            [
              _c(
                "v-btn",
                { staticClass: "mr-3", attrs: { icon: "", small: "" } },
                [
                  _c("v-icon", { attrs: { color: "success" } }, [
                    _vm._v("check")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                { attrs: { icon: "", small: "" } },
                [
                  _c("v-icon", { attrs: { color: "error" } }, [_vm._v("close")])
                ],
                1
              )
            ],
            1
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProfitShare.vue?vue&type=template&id=08830e40&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProfitShare.vue?vue&type=template&id=08830e40& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "profit-share-chart d-custom-flex justify-space-between h-100"
    },
    [
      _c(
        "div",
        { staticClass: "mb-2 pos-relative" },
        [
          _c("doughnut-chart-v2", {
            attrs: { height: 275, data: _vm.profitShare }
          }),
          _vm._v(" "),
          _vm._m(0)
        ],
        1
      ),
      _vm._v(" "),
      _vm._m(1)
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "overlay-content d-custom-flex justify-center align-items-center"
      },
      [
        _c("span", { staticClass: "grey--text font-2x fw-semi-bold" }, [
          _vm._v("$2500")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-custom-flex justify-space-between" }, [
      _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
        _c("span", { staticClass: "v-badge primary px-2 py-1" }),
        _vm._v(" "),
        _c("span", { staticClass: "d-block" }, [_vm._v("65%")]),
        _vm._v(" "),
        _c("span", { staticClass: "d-block" }, [_vm._v("Sport Tickets")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
        _c("span", { staticClass: "v-badge success px-2 py-1" }),
        _vm._v(" "),
        _c("span", { staticClass: "d-block" }, [_vm._v("25%")]),
        _vm._v(" "),
        _c("span", { staticClass: "d-block" }, [_vm._v("Business Events")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
        _c("span", { staticClass: "v-badge warning px-2 py-1" }),
        _vm._v(" "),
        _c("span", { staticClass: "d-block" }, [_vm._v("10%")]),
        _vm._v(" "),
        _c("span", { staticClass: "d-block" }, [_vm._v("Others")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatus.vue?vue&type=template&id=25932610&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ProjectStatus.vue?vue&type=template&id=25932610& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "project-status-wrap" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "396px" }, attrs: { settings: _vm.settings } },
        [
          _c("v-data-table", {
            attrs: {
              headers: _vm.headers,
              items: _vm.items,
              "item-key": "productName",
              "hide-default-footer": ""
            },
            scopedSlots: _vm._u([
              {
                key: "item",
                fn: function(ref) {
                  var item = ref.item
                  return [
                    _c("tr", [
                      _c(
                        "td",
                        [
                          _c("v-checkbox", {
                            attrs: { color: "primary", "hide-details": "" }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("td", [
                        _c("span", {}, [_vm._v(_vm._s(item.productName))])
                      ]),
                      _vm._v(" "),
                      _c(
                        "td",
                        [
                          _c(
                            "v-badge",
                            {
                              class: item.labelClasses,
                              attrs: { value: false }
                            },
                            [_vm._v(_vm._s(item.priority))]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "td",
                        [
                          _c("v-progress-linear", {
                            staticClass: "my-1",
                            attrs: {
                              value: item.progressValue,
                              height: "3",
                              color: "primary"
                            }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 fw-normal" }, [
                            _vm._v(_vm._s(item.status))
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "td",
                        {
                          staticClass:
                            "text-center team-avatar team-avatar-auto"
                        },
                        [
                          _c("ul", { staticClass: "list-inline" }, [
                            _c("li", [
                              _c("div", [
                                _c("img", {
                                  staticClass: "img-responsive",
                                  attrs: {
                                    src: "/static/avatars/user-7.jpg",
                                    alt: "user images",
                                    width: "18",
                                    height: "18"
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("div", [
                                _c("img", {
                                  staticClass: "img-responsive",
                                  attrs: {
                                    src: "/static/avatars/user-8.jpg",
                                    alt: "user images",
                                    width: "18",
                                    height: "18"
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("div", [
                                _c("img", {
                                  staticClass: "img-responsive",
                                  attrs: {
                                    src: "/static/avatars/user-9.jpg",
                                    alt: "user images",
                                    width: "18",
                                    height: "18"
                                  }
                                })
                              ])
                            ])
                          ])
                        ]
                      )
                    ])
                  ]
                }
              }
            ]),
            model: {
              value: _vm.selected,
              callback: function($$v) {
                _vm.selected = $$v
              },
              expression: "selected"
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TodayWeather.vue?vue&type=template&id=06fcbf44&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TodayWeather.vue?vue&type=template&id=06fcbf44& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "today-weather d-custom-flex align-items-center mb-4" },
    [
      _c("i", { staticClass: "font-3x mr-4", class: _vm.weatherIcon }),
      _vm._v(" "),
      _c("div", [
        _c("p", { staticClass: "mb-0 fs-12 grey--text fw-normal" }, [
          _vm._v(_vm._s(_vm.city) + ", India")
        ]),
        _vm._v(" "),
        _c("h4", { staticClass: "mb-0 fw-light" }, [_vm._v("36° Cloudy")])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopAuthors.vue?vue&type=template&id=4139de44&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TopAuthors.vue?vue&type=template&id=4139de44& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "primary pos-relative white--text pt-5" }, [
        _c("h5", { staticClass: "mb-0 text-center" }, [
          _vm._v(_vm._s(_vm.$t("message.topAuthors")))
        ])
      ]),
      _vm._v(" "),
      _vm.authors
        ? _c(
            "slick",
            { attrs: { options: _vm.slickOptions } },
            _vm._l(_vm.authors, function(author) {
              return _c(
                "div",
                { key: author.id, staticClass: "author-detail-wrap" },
                [
                  _c(
                    "div",
                    { staticClass: "author-avatar primary pos-relative mb-50" },
                    [
                      _c("div", { staticClass: "avatar-img" }, [
                        _c("img", {
                          staticClass: "img-responsive mx-auto rounded-circle",
                          attrs: {
                            src: author.avatar,
                            width: "87",
                            height: "87"
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "pa-4 authors-info" }, [
                    _c("h5", [_vm._v(_vm._s(author.name))]),
                    _vm._v(" "),
                    _c(
                      "ul",
                      {
                        staticClass:
                          "pl-0 list-unstyled author-contact-info mb-3"
                      },
                      [
                        _c("li", [
                          _c("span", { staticClass: "mr-3" }, [
                            _c("i", { staticClass: "zmdi zmdi-phone-msg" })
                          ]),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              staticClass: "fs-12 grey--text fw-normal",
                              attrs: { href: "tel:123456" }
                            },
                            [_vm._v(_vm._s(author.profileInfo.phone))]
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("span", { staticClass: "mr-3" }, [
                            _c("i", { staticClass: "zmdi zmdi-email" })
                          ]),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              staticClass: "fs-12 grey--text fw-normal",
                              attrs: { href: "mailto:joan_parisian@gmail.com" }
                            },
                            [_vm._v(_vm._s(author.profileInfo.email))]
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("span", { staticClass: "mr-3" }, [
                            _c("i", { staticClass: "zmdi zmdi-pin" })
                          ]),
                          _vm._v(" "),
                          _c("span", { staticClass: "fs-12 grey--text" }, [
                            _vm._v(_vm._s(author.profileInfo.address))
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "ul",
                      {
                        staticClass:
                          "pl-0 d-custom-flex social-info list-unstyled"
                      },
                      [
                        _c("li", [
                          _c(
                            "a",
                            {
                              staticClass: "facebook",
                              attrs: { href: author.socialLinks.facebookUrl }
                            },
                            [_c("i", { staticClass: "zmdi zmdi-facebook-box" })]
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c(
                            "a",
                            {
                              staticClass: "twitter",
                              attrs: { href: author.socialLinks.twitterUrl }
                            },
                            [_c("i", { staticClass: "zmdi zmdi-twitter-box" })]
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c(
                            "a",
                            {
                              staticClass: "linkedin",
                              attrs: { href: author.socialLinks.linkedinUrl }
                            },
                            [_c("i", { staticClass: "zmdi zmdi-linkedin-box" })]
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c(
                            "a",
                            {
                              staticClass: "instagram",
                              attrs: { href: author.socialLinks.instagramUrl }
                            },
                            [_c("i", { staticClass: "zmdi zmdi-instagram" })]
                          )
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "ul",
                    {
                      staticClass:
                        "d-custom-flex list-unstyled footer-content text-center w-100 border-top-1"
                    },
                    [
                      _c("li", [
                        _c("h5", { staticClass: "mb-0" }, [
                          _vm._v(_vm._s(author.metaDetails.articles))
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "fs-12 grey--text fw-normal" },
                          [_vm._v("Articles")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("h5", { staticClass: "mb-0" }, [
                          _vm._v(_vm._s(author.metaDetails.followers))
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "fs-12 grey--text fw-normal" },
                          [_vm._v("Followers")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("h5", { staticClass: "mb-0" }, [
                          _vm._v(_vm._s(author.metaDetails.likes))
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "fs-12 grey--text fw-normal" },
                          [_vm._v("Likes")]
                        )
                      ])
                    ]
                  )
                ]
              )
            }),
            0
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersList.vue?vue&type=template&id=27b0ad6b&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/UsersList.vue?vue&type=template&id=27b0ad6b& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.reload } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        {
          staticStyle: { "max-height": "325px" },
          attrs: { settings: _vm.settings }
        },
        [
          _c(
            "v-list",
            {
              staticClass: "list-aqua-ripple recent-user-wrap",
              attrs: { "two-line": "" }
            },
            _vm._l(_vm.usersList, function(item, index) {
              return _c(
                "v-list-item",
                { key: index, attrs: { ripple: "" } },
                [
                  _c("v-list-item-avatar", [
                    _c("img", {
                      attrs: { src: item.photo_url, alt: "avatar" }
                    }),
                    _vm._v(" "),
                    item.status === 1
                      ? _c("span", {
                          staticClass: "v-badge success rounded floating"
                        })
                      : _c("span", {
                          staticClass: "v-badge grey rounded floating"
                        })
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-list-item-content",
                    [
                      _c("v-list-item-title", [
                        _c("h6", { staticClass: "mb-0" }, [
                          _vm._v(_vm._s(item.customer_name))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("v-list-item-subtitle", [
                        _c(
                          "a",
                          {
                            staticClass: "fs-12 fw-normal",
                            attrs: { href: "javascript:void(0)" }
                          },
                          [_vm._v(_vm._s(item.customer_email))]
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-list-item-action",
                    [
                      _c(
                        "v-menu",
                        {
                          attrs: {
                            origin: "center center",
                            transition: "scale-transition",
                            bottom: ""
                          },
                          scopedSlots: _vm._u(
                            [
                              {
                                key: "activator",
                                fn: function(ref) {
                                  var on = ref.on
                                  return [
                                    _c(
                                      "v-btn",
                                      _vm._g({ attrs: { icon: "" } }, on),
                                      [_c("v-icon", [_vm._v("more_horiz")])],
                                      1
                                    )
                                  ]
                                }
                              }
                            ],
                            null,
                            true
                          )
                        },
                        [
                          _vm._v(" "),
                          _c(
                            "v-list",
                            [
                              _c(
                                "v-list-item",
                                {
                                  on: {
                                    click: function($event) {
                                      return _vm.onEditUser(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-list-item-title", [
                                    _vm._v(_vm._s(_vm.$t("message.edit")))
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-list-item",
                                {
                                  on: {
                                    click: function($event) {
                                      return _vm.onDeleteUser(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-list-item-title", [
                                    _vm._v(_vm._s(_vm.$t("message.delete")))
                                  ])
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            }),
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.reload } }),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { top: _vm.y === "top", timeout: _vm.timeout },
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [_vm._v("\n\t\t" + _vm._s(_vm.snackbarMessage) + "\n\t")]
      ),
      _vm._v(" "),
      _c("delete-confirmation-dialog", {
        ref: "deleteConfirmationDialog",
        attrs: {
          heading: "Are You Sure You Want To Delete?",
          message: "Are you sure you want to delete this member permanently?"
        },
        on: { onConfirm: _vm.deleteUser }
      }),
      _vm._v(" "),
      _c("hr", { staticClass: "ma-0" }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          scopedSlots: _vm._u([
            {
              key: "activator",
              fn: function(ref) {
                var on = ref.on
                return [
                  _c(
                    "v-btn",
                    _vm._g(
                      {
                        staticClass: "ma-4",
                        attrs: { color: "primary", small: "" },
                        on: { click: _vm.updateUserModal }
                      },
                      on
                    ),
                    [_vm._v(_vm._s(_vm.$t("message.addNew")))]
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.dialog2,
            callback: function($$v) {
              _vm.dialog2 = $$v
            },
            expression: "dialog2"
          }
        },
        [
          _vm._v(" "),
          _vm.addUserModal
            ? _c(
                "v-card",
                [
                  _c("v-card-title", [
                    _c("span", { staticClass: "headline" }, [
                      _vm._v(_vm._s(_vm.$t("message.addNewUser")))
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card-text",
                    [
                      _c(
                        "v-container",
                        { staticClass: "grid-list-md pa-0" },
                        [
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { cols: "12" } },
                                [
                                  _c("v-text-field", {
                                    attrs: { label: _vm.$t("message.name") },
                                    model: {
                                      value: _vm.addNewUser.customer_name,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.addNewUser,
                                          "customer_name",
                                          $$v
                                        )
                                      },
                                      expression: "addNewUser.customer_name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("v-col", { attrs: { cols: "12" } }, [
                                _c(
                                  "div",
                                  [
                                    _c("v-text-field", {
                                      attrs: { label: _vm.$t("message.email") },
                                      model: {
                                        value: _vm.addNewUser.customer_email,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.addNewUser,
                                            "customer_email",
                                            $$v
                                          )
                                        },
                                        expression: "addNewUser.customer_email"
                                      }
                                    })
                                  ],
                                  1
                                )
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    { staticClass: "pa-4" },
                    [
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "px-4",
                          attrs: { color: "error" },
                          on: { click: _vm.addUsers }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.add")))]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "px-4",
                          attrs: { color: "primary" },
                          nativeOn: {
                            click: function($event) {
                              _vm.dialog2 = false
                            }
                          }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.close")))]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            : _c(
                "v-card",
                [
                  _c(
                    "v-card-title",
                    [
                      _c("span", { staticClass: "headline" }, [
                        _vm._v(_vm._s(_vm.$t("message.editUser")))
                      ]),
                      _vm._v(" "),
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c("a", { attrs: { href: "javascript:void(0)" } }, [
                        _c(
                          "i",
                          {
                            staticClass: "material-icons",
                            on: {
                              click: function($event) {
                                _vm.dialog2 = false
                              }
                            }
                          },
                          [_vm._v("close")]
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-text",
                    [
                      _c(
                        "v-container",
                        { staticClass: "grid-list-md" },
                        [
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { xs12: "" } },
                                [
                                  _c("v-text-field", {
                                    attrs: { label: _vm.$t("message.name") },
                                    model: {
                                      value: _vm.editUser.customer_name,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.editUser,
                                          "customer_name",
                                          $$v
                                        )
                                      },
                                      expression: "editUser.customer_name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("v-col", { attrs: { xs12: "" } }, [
                                _c(
                                  "div",
                                  [
                                    _c("v-text-field", {
                                      attrs: { label: _vm.$t("message.email") },
                                      model: {
                                        value: _vm.editUser.customer_email,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.editUser,
                                            "customer_email",
                                            $$v
                                          )
                                        },
                                        expression: "editUser.customer_email"
                                      }
                                    })
                                  ],
                                  1
                                )
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "primary" },
                          nativeOn: {
                            click: function($event) {
                              _vm.dialog2 = false
                            }
                          }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.cancel")))]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "error" },
                          on: { click: _vm.updateUser }
                        },
                        [_vm._v(_vm._s(_vm.$t("message.update")))]
                      )
                    ],
                    1
                  )
                ],
                1
              )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Agency.vue?vue&type=template&id=3f69ee02&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Agency.vue?vue&type=template&id=3f69ee02& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-container",
        { attrs: { fluid: "", "grid-list-xl": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "6", sm: "7", cols: "12" } },
                [
                  _c("today-weather"),
                  _vm._v(" "),
                  _c("div", { staticClass: "welcome-message mb-8" }, [
                    _c("h4", [_vm._v("Good morning, Jacqueline.")]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "Here’s what’s happening with your store this week."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "welcome-chart" },
                    [
                      _c(
                        "v-row",
                        [
                          _c(
                            "v-col",
                            {
                              attrs: {
                                xl: "6",
                                lg: "6",
                                md: "6",
                                sm: "6",
                                cols: "12"
                              }
                            },
                            [
                              _c("h2", { staticClass: "mb-0 grey--text" }, [
                                _vm._v("$21,349.29")
                              ]),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "d-block fs-12 mb-4 grey--text fw-normal"
                                },
                                [_vm._v("Earned Today")]
                              ),
                              _vm._v(" "),
                              _c("bar-chart", {
                                attrs: {
                                  height: 110,
                                  dataSets: _vm.earnedToday
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            {
                              attrs: {
                                xl: "6",
                                lg: "6",
                                md: "6",
                                sm: "6",
                                cols: "12"
                              }
                            },
                            [
                              _c("h2", { staticClass: "mb-0 grey--text" }, [
                                _vm._v("15,800")
                              ]),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  staticClass:
                                    "d-block fs-12 mb-4 grey--text fw-normal"
                                },
                                [_vm._v("Items Sold")]
                              ),
                              _vm._v(" "),
                              _c("bar-chart", {
                                attrs: { height: 110, dataSets: _vm.itemsSold }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticClass: "d-sm-block d-none",
                  attrs: { xl: "6", lg: "6", md: "6", cols: "12", sm: "5" }
                },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "d-custom-flex align-items-center justify-center pt-3"
                    },
                    [
                      _c("img", {
                        staticClass: "img-responsive",
                        attrs: {
                          src: "/static/img/agency-welcome.png",
                          height: "300",
                          width: "550",
                          alt: "agency block"
                        }
                      })
                    ]
                  )
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.salesAndEarning"),
                    fullBlock: true,
                    reloadable: true,
                    fullScreen: true,
                    closeable: true,
                    colClasses: "col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12",
                    customClasses: "sales-chart-widget"
                  }
                },
                [
                  _c("div", { staticClass: "pa-4" }, [
                    _c("h2", { staticClass: "mb-0" }, [_vm._v("$35,455")]),
                    _vm._v(" "),
                    _c("p", { staticClass: "fs-12 grey--text fw-normal" }, [
                      _vm._v("Total Sales This Month")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("line-chart-with-area", {
                    attrs: {
                      color: _vm.salesAndEarning.color,
                      dataSet: _vm.salesAndEarning.data,
                      dataLabels: _vm.salesAndEarning.labels,
                      enableXAxesLine: false,
                      height: 115
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "6", sm: "12", cols: "12" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        {
                          staticClass: "col-height-auto",
                          attrs: {
                            xl: "6",
                            lg: "6",
                            md: "6",
                            sm: "6",
                            cols: "12"
                          }
                        },
                        [
                          _c(
                            "app-card",
                            { attrs: { "custom-classes": "mb-5" } },
                            [
                              _c("h6", { staticClass: "mb-0" }, [
                                _vm._v("$2156")
                              ]),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "fs-12 grey--text fw-normal" },
                                [
                                  _vm._v(
                                    _vm._s(_vm.$t("message.totalEarnings"))
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.totalEarnings.data,
                                  lineTension: _vm.totalEarnings.lineTension,
                                  dataLabels: _vm.totalEarnings.labels,
                                  width: 370,
                                  height: 100,
                                  borderColor: _vm.totalEarnings.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "app-card",
                            [
                              _c("h6", { staticClass: "mb-0" }, [
                                _vm._v("$2156")
                              ]),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "fs-12 grey--text fw-normal" },
                                [
                                  _vm._v(
                                    _vm._s(_vm.$t("message.totalExpences"))
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.totalExpences.data,
                                  lineTension: _vm.totalExpences.lineTension,
                                  dataLabels: _vm.totalExpences.labels,
                                  width: 370,
                                  height: 100,
                                  borderColor: _vm.totalExpences.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticClass: "col-height-auto",
                          attrs: {
                            xl: "6",
                            lg: "6",
                            md: "6",
                            sm: "6",
                            cols: "12"
                          }
                        },
                        [
                          _c(
                            "app-card",
                            { attrs: { "custom-classes": "mb-5" } },
                            [
                              _c("h6", { staticClass: "mb-0" }, [
                                _vm._v("$2156")
                              ]),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "fs-12 grey--text fw-normal" },
                                [_vm._v(_vm._s(_vm.$t("message.netProfit")))]
                              ),
                              _vm._v(" "),
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.netProfit.data,
                                  lineTension: _vm.netProfit.lineTension,
                                  dataLabels: _vm.netProfit.labels,
                                  width: 370,
                                  height: 100,
                                  borderColor: _vm.netProfit.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "app-card",
                            [
                              _c("h6", { staticClass: "mb-0" }, [
                                _vm._v("$2156")
                              ]),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: " fs-12 grey--text fw-normal" },
                                [
                                  _vm._v(
                                    _vm._s(_vm.$t("message.onlineRevenue"))
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("line-chart-shadow", {
                                attrs: {
                                  dataSet: _vm.onlineRevenue.data,
                                  lineTension: _vm.onlineRevenue.lineTension,
                                  dataLabels: _vm.onlineRevenue.labels,
                                  width: 370,
                                  height: 100,
                                  borderColor: _vm.onlineRevenue.borderColor,
                                  enableGradient: false
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.newRequest"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-12 col-sm-12",
                    fullBlock: true,
                    reloadable: true,
                    closeable: true,
                    fullScreen: true
                  }
                },
                [_c("users-list")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-8 col-lg-8 col-md-8 col-12 col-sm-12",
                    heading: _vm.$t("message.projectStatus"),
                    fullBlock: true,
                    reloadable: true,
                    closeable: true,
                    fullScreen: true
                  }
                },
                [_c("project-status")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-8 col-lg-8 col-md-7 col-12 col-sm-12",
                    heading: _vm.$t("message.adCampaignPerfomance"),
                    closeable: true,
                    fullScreen: true,
                    reloadable: true,
                    contentCustomClass: "pos-relative d-flex",
                    customClasses: "ad-campaign justify-space-between"
                  }
                },
                [
                  _c("ad-campaign-perfomance", {
                    staticStyle: { width: "100%" },
                    attrs: { height: 450, data: _vm.adCampaignPerfomanceData }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-5 col-12 col-sm-12",
                    fullBlock: true,
                    customClasses: "quote-wrap2"
                  }
                },
                [_c("quote-of-the-day")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-12 col-sm-6",
                    heading: _vm.$t("message.todoList"),
                    fullBlock: true,
                    reloadable: true,
                    closeable: true,
                    fullScreen: true
                  }
                },
                [_c("todo-list")],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.topSellingTheme"),
                    colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                    fullBlock: true,
                    reloadable: true,
                    closeable: true,
                    fullScreen: true,
                    footer: true
                  }
                },
                [
                  _c("recent-sales"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "action",
                      attrs: { slot: "footer" },
                      slot: "footer"
                    },
                    [
                      _c("v-btn", { attrs: { small: "", color: "primary" } }, [
                        _vm._v(_vm._s(_vm.$t("message.viewAll")))
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticClass: "d-xs-full",
                  attrs: { xl: "4", lg: "4", md: "4", sm: "12" }
                },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12"
                          }
                        },
                        [_c("new-order-countdown")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12"
                          }
                        },
                        [_c("followers")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          attrs: {
                            fullBlock: true,
                            customClasses: "booking-info-wrap",
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                          }
                        },
                        [_c("booking-info")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    fullBlock: true,
                    colClasses:
                      "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 col-height-auto active-user-wrap"
                  }
                },
                [_c("active-user", { attrs: { data: _vm.activeUser } })],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.employeePayroll"),
                    withTabs: true,
                    tabs: [
                      _vm.$t("message.lastMonth"),
                      _vm.$t("message.allTime")
                    ],
                    fullBlock: true,
                    colClasses: "col-xl-8 col-lg-8 col-md-8 col-sm-12 d-full"
                  }
                },
                [_c("employee-payroll")],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  staticClass: "col-height-auto",
                  attrs: { xl: "8", lg: "8", md: "8", sm: "12", cols: "12" }
                },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { xl: "12", lg: "12", md: "12", cols: "12" } },
                        [_c("mailbox")],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12",
                            heading: _vm.$t("message.newsletter"),
                            fullScreen: true,
                            reloadable: true,
                            closeable: true
                          }
                        },
                        [
                          _c("news-letter-campaign", {
                            attrs: {
                              height: 350,
                              labels: ["1", "2", "3", "4", "5", "6", "7"],
                              data1: [19, 21, 18, 20, 23, 16, 18, 30],
                              data2: [10, 8, 14, 11, 10, 12, 10, 0]
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "app-card",
                        {
                          staticClass: "top-author-wrap",
                          attrs: {
                            colClasses:
                              "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 col-height-auto",
                            fullBlock: true
                          }
                        },
                        [_c("top-authors")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { xl: "4", lg: "4", md: "4", cols: "12", sm: "12" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "app-card",
                        {
                          attrs: {
                            colClasses:
                              "col-xl-12 col-lg-12 col-md-12 col-12 col-sm-6",
                            heading: _vm.$t("message.profitShare"),
                            closeable: true,
                            fullScreen: true,
                            reloadable: true,
                            customClasses: "profit-share-widget"
                          }
                        },
                        [_c("profit-share")],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          attrs: {
                            xl: "12",
                            lg: "12",
                            md: "12",
                            sm: "6",
                            cols: "12"
                          }
                        },
                        [_c("activity")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  staticClass: "pa-0",
                  attrs: { xl: "6", lg: "6", md: "6", sm: "12", cols: "12" }
                },
                [_c("chat")],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { xl: "6", lg: "6", md: "6", sm: "12", cols: "12" } },
                [_c("event-calendar")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/BarChart.js":
/*!****************************************************!*\
  !*** ./resources/js/components/Charts/BarChart.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");


var options = {
  legend: {
    display: false
  },
  tooltips: {
    titleSpacing: 6,
    cornerRadius: 5
  },
  scales: {
    xAxes: [{
      gridLines: {
        display: false,
        color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].chartGridColor,
        drawBorder: false
      },
      ticks: {
        fontColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].axesColor
      },
      display: false
    }],
    yAxes: [{
      gridLines: {
        display: false,
        color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].chartGridColor,
        drawBorder: false
      },
      ticks: {
        fontColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].axesColor
      },
      display: false
    }]
  }
};
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  props: ['dataSets'],
  mounted: function mounted() {
    var _this$dataSets = this.dataSets,
        color = _this$dataSets.color,
        label = _this$dataSets.label,
        data = _this$dataSets.data,
        labels = _this$dataSets.labels;
    this.renderChart({
      labels: labels,
      datasets: [{
        barPercentage: 2,
        categoryPercentage: 0.275,
        label: label,
        backgroundColor: color,
        borderColor: color,
        borderWidth: 1,
        hoverBackgroundColor: color,
        hoverBorderColor: color,
        data: data
      }]
    }, options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/NewsLetterCampaign.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/Charts/NewsLetterCampaign.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// News Letter Campaign Widget


var lineTension = 0.1;
var borderWidth = 3;
var pointRadius = 6;
var pointBorderWidth = 2;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    data1: {
      type: Array,
      "default": function _default() {
        return [10, 30, 30, 60, 75, 10];
      }
    },
    data2: {
      type: Array,
      "default": function _default() {
        return [10, 30, 30, 60, 75, 10];
      }
    },
    label1: {
      type: String,
      "default": function _default() {
        return 'Subscribed';
      }
    },
    label2: {
      type: String,
      "default": function _default() {
        return 'Unsubscribed';
      }
    },
    labels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F'];
      }
    }
  },
  data: function data() {
    return {
      gradient1: null,
      gradient2: null,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false
            },
            ticks: {
              stepSize: 5,
              padding: 5
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              padding: 10
            }
          }]
        },
        tooltip: {
          enabled: true
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    };
  },
  mounted: function mounted() {
    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].shadowColor;
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 12;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    this.renderChart({
      labels: this.labels,
      datasets: [{
        label: this.label1,
        lineTension: lineTension,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        pointBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.data1
      }, {
        label: this.label2,
        lineTension: lineTension,
        borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info,
        pointBorderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.info,
        pointBorderWidth: pointBorderWidth,
        pointRadius: pointRadius,
        fill: false,
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white,
        borderWidth: borderWidth,
        data: this.data2
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/CountDown/CountDown.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/CountDown/CountDown.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CountDown_vue_vue_type_template_id_43c0b458___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CountDown.vue?vue&type=template&id=43c0b458& */ "./resources/js/components/CountDown/CountDown.vue?vue&type=template&id=43c0b458&");
/* harmony import */ var _CountDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CountDown.vue?vue&type=script&lang=js& */ "./resources/js/components/CountDown/CountDown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CountDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CountDown_vue_vue_type_template_id_43c0b458___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CountDown_vue_vue_type_template_id_43c0b458___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/CountDown/CountDown.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/CountDown/CountDown.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/CountDown/CountDown.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CountDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CountDown.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CountDown/CountDown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CountDown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/CountDown/CountDown.vue?vue&type=template&id=43c0b458&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/CountDown/CountDown.vue?vue&type=template&id=43c0b458& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CountDown_vue_vue_type_template_id_43c0b458___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CountDown.vue?vue&type=template&id=43c0b458& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/CountDown/CountDown.vue?vue&type=template&id=43c0b458&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CountDown_vue_vue_type_template_id_43c0b458___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CountDown_vue_vue_type_template_id_43c0b458___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/DialogBox/ComposeEmail.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/DialogBox/ComposeEmail.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ComposeEmail_vue_vue_type_template_id_4441cda1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ComposeEmail.vue?vue&type=template&id=4441cda1& */ "./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=template&id=4441cda1&");
/* harmony import */ var _ComposeEmail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ComposeEmail.vue?vue&type=script&lang=js& */ "./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ComposeEmail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ComposeEmail_vue_vue_type_template_id_4441cda1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ComposeEmail_vue_vue_type_template_id_4441cda1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/DialogBox/ComposeEmail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComposeEmail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ComposeEmail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ComposeEmail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=template&id=4441cda1&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=template&id=4441cda1& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ComposeEmail_vue_vue_type_template_id_4441cda1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ComposeEmail.vue?vue&type=template&id=4441cda1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/DialogBox/ComposeEmail.vue?vue&type=template&id=4441cda1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ComposeEmail_vue_vue_type_template_id_4441cda1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ComposeEmail_vue_vue_type_template_id_4441cda1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Activity.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/Widgets/Activity.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Activity_vue_vue_type_template_id_31e0dd0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Activity.vue?vue&type=template&id=31e0dd0c& */ "./resources/js/components/Widgets/Activity.vue?vue&type=template&id=31e0dd0c&");
/* harmony import */ var _Activity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Activity.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Activity.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Activity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Activity_vue_vue_type_template_id_31e0dd0c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Activity_vue_vue_type_template_id_31e0dd0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Activity.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Activity.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/Widgets/Activity.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Activity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Activity.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Activity.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Activity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Activity.vue?vue&type=template&id=31e0dd0c&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Activity.vue?vue&type=template&id=31e0dd0c& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Activity_vue_vue_type_template_id_31e0dd0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Activity.vue?vue&type=template&id=31e0dd0c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Activity.vue?vue&type=template&id=31e0dd0c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Activity_vue_vue_type_template_id_31e0dd0c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Activity_vue_vue_type_template_id_31e0dd0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BookingInfo.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/BookingInfo.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BookingInfo_vue_vue_type_template_id_75bce38c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BookingInfo.vue?vue&type=template&id=75bce38c& */ "./resources/js/components/Widgets/BookingInfo.vue?vue&type=template&id=75bce38c&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _BookingInfo_vue_vue_type_template_id_75bce38c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BookingInfo_vue_vue_type_template_id_75bce38c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BookingInfo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BookingInfo.vue?vue&type=template&id=75bce38c&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BookingInfo.vue?vue&type=template&id=75bce38c& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BookingInfo_vue_vue_type_template_id_75bce38c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BookingInfo.vue?vue&type=template&id=75bce38c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BookingInfo.vue?vue&type=template&id=75bce38c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BookingInfo_vue_vue_type_template_id_75bce38c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BookingInfo_vue_vue_type_template_id_75bce38c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/EmployeePayroll.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/Widgets/EmployeePayroll.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmployeePayroll_vue_vue_type_template_id_4385befc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmployeePayroll.vue?vue&type=template&id=4385befc& */ "./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=template&id=4385befc&");
/* harmony import */ var _EmployeePayroll_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmployeePayroll.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EmployeePayroll_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmployeePayroll_vue_vue_type_template_id_4385befc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmployeePayroll_vue_vue_type_template_id_4385befc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/EmployeePayroll.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeePayroll_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeePayroll.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeePayroll_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=template&id=4385befc&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=template&id=4385befc& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeePayroll_vue_vue_type_template_id_4385befc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./EmployeePayroll.vue?vue&type=template&id=4385befc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EmployeePayroll.vue?vue&type=template&id=4385befc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeePayroll_vue_vue_type_template_id_4385befc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmployeePayroll_vue_vue_type_template_id_4385befc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/EventCalendar.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/EventCalendar.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EventCalendar_vue_vue_type_template_id_30f89f1d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EventCalendar.vue?vue&type=template&id=30f89f1d& */ "./resources/js/components/Widgets/EventCalendar.vue?vue&type=template&id=30f89f1d&");
/* harmony import */ var _EventCalendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventCalendar.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/EventCalendar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EventCalendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EventCalendar_vue_vue_type_template_id_30f89f1d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EventCalendar_vue_vue_type_template_id_30f89f1d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/EventCalendar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/EventCalendar.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/EventCalendar.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EventCalendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EventCalendar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EventCalendar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EventCalendar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/EventCalendar.vue?vue&type=template&id=30f89f1d&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/EventCalendar.vue?vue&type=template&id=30f89f1d& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EventCalendar_vue_vue_type_template_id_30f89f1d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./EventCalendar.vue?vue&type=template&id=30f89f1d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/EventCalendar.vue?vue&type=template&id=30f89f1d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EventCalendar_vue_vue_type_template_id_30f89f1d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EventCalendar_vue_vue_type_template_id_30f89f1d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Followers.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/Followers.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Followers_vue_vue_type_template_id_97ac398c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Followers.vue?vue&type=template&id=97ac398c& */ "./resources/js/components/Widgets/Followers.vue?vue&type=template&id=97ac398c&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Followers_vue_vue_type_template_id_97ac398c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Followers_vue_vue_type_template_id_97ac398c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Followers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Followers.vue?vue&type=template&id=97ac398c&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Followers.vue?vue&type=template&id=97ac398c& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Followers_vue_vue_type_template_id_97ac398c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Followers.vue?vue&type=template&id=97ac398c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Followers.vue?vue&type=template&id=97ac398c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Followers_vue_vue_type_template_id_97ac398c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Followers_vue_vue_type_template_id_97ac398c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Mailbox.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/Widgets/Mailbox.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Mailbox_vue_vue_type_template_id_efe100ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Mailbox.vue?vue&type=template&id=efe100ce& */ "./resources/js/components/Widgets/Mailbox.vue?vue&type=template&id=efe100ce&");
/* harmony import */ var _Mailbox_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Mailbox.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Mailbox.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Mailbox_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Mailbox_vue_vue_type_template_id_efe100ce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Mailbox_vue_vue_type_template_id_efe100ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Mailbox.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Mailbox.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/Widgets/Mailbox.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Mailbox_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Mailbox.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Mailbox.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Mailbox_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Mailbox.vue?vue&type=template&id=efe100ce&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Mailbox.vue?vue&type=template&id=efe100ce& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Mailbox_vue_vue_type_template_id_efe100ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Mailbox.vue?vue&type=template&id=efe100ce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Mailbox.vue?vue&type=template&id=efe100ce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Mailbox_vue_vue_type_template_id_efe100ce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Mailbox_vue_vue_type_template_id_efe100ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/NewOrderCountdown.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/Widgets/NewOrderCountdown.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NewOrderCountdown_vue_vue_type_template_id_45b18130___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NewOrderCountdown.vue?vue&type=template&id=45b18130& */ "./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=template&id=45b18130&");
/* harmony import */ var _NewOrderCountdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NewOrderCountdown.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _NewOrderCountdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NewOrderCountdown_vue_vue_type_template_id_45b18130___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NewOrderCountdown_vue_vue_type_template_id_45b18130___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/NewOrderCountdown.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewOrderCountdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./NewOrderCountdown.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewOrderCountdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=template&id=45b18130&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=template&id=45b18130& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewOrderCountdown_vue_vue_type_template_id_45b18130___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./NewOrderCountdown.vue?vue&type=template&id=45b18130& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/NewOrderCountdown.vue?vue&type=template&id=45b18130&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewOrderCountdown_vue_vue_type_template_id_45b18130___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewOrderCountdown_vue_vue_type_template_id_45b18130___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ProfitShare.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/ProfitShare.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProfitShare_vue_vue_type_template_id_08830e40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProfitShare.vue?vue&type=template&id=08830e40& */ "./resources/js/components/Widgets/ProfitShare.vue?vue&type=template&id=08830e40&");
/* harmony import */ var _ProfitShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProfitShare.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProfitShare.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProfitShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProfitShare_vue_vue_type_template_id_08830e40___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProfitShare_vue_vue_type_template_id_08830e40___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProfitShare.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProfitShare.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProfitShare.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfitShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProfitShare.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProfitShare.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfitShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProfitShare.vue?vue&type=template&id=08830e40&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProfitShare.vue?vue&type=template&id=08830e40& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfitShare_vue_vue_type_template_id_08830e40___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProfitShare.vue?vue&type=template&id=08830e40& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProfitShare.vue?vue&type=template&id=08830e40&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfitShare_vue_vue_type_template_id_08830e40___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfitShare_vue_vue_type_template_id_08830e40___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ProjectStatus.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectStatus.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProjectStatus_vue_vue_type_template_id_25932610___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProjectStatus.vue?vue&type=template&id=25932610& */ "./resources/js/components/Widgets/ProjectStatus.vue?vue&type=template&id=25932610&");
/* harmony import */ var _ProjectStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProjectStatus.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ProjectStatus.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProjectStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProjectStatus_vue_vue_type_template_id_25932610___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProjectStatus_vue_vue_type_template_id_25932610___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ProjectStatus.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectStatus.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectStatus.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectStatus.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatus.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatus_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ProjectStatus.vue?vue&type=template&id=25932610&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ProjectStatus.vue?vue&type=template&id=25932610& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatus_vue_vue_type_template_id_25932610___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProjectStatus.vue?vue&type=template&id=25932610& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ProjectStatus.vue?vue&type=template&id=25932610&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatus_vue_vue_type_template_id_25932610___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProjectStatus_vue_vue_type_template_id_25932610___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TodayWeather.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/TodayWeather.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TodayWeather_vue_vue_type_template_id_06fcbf44___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TodayWeather.vue?vue&type=template&id=06fcbf44& */ "./resources/js/components/Widgets/TodayWeather.vue?vue&type=template&id=06fcbf44&");
/* harmony import */ var _TodayWeather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TodayWeather.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TodayWeather.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TodayWeather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TodayWeather_vue_vue_type_template_id_06fcbf44___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TodayWeather_vue_vue_type_template_id_06fcbf44___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TodayWeather.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TodayWeather.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/TodayWeather.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TodayWeather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TodayWeather.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TodayWeather.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TodayWeather_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TodayWeather.vue?vue&type=template&id=06fcbf44&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TodayWeather.vue?vue&type=template&id=06fcbf44& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TodayWeather_vue_vue_type_template_id_06fcbf44___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TodayWeather.vue?vue&type=template&id=06fcbf44& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TodayWeather.vue?vue&type=template&id=06fcbf44&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TodayWeather_vue_vue_type_template_id_06fcbf44___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TodayWeather_vue_vue_type_template_id_06fcbf44___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TopAuthors.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/TopAuthors.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TopAuthors_vue_vue_type_template_id_4139de44___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TopAuthors.vue?vue&type=template&id=4139de44& */ "./resources/js/components/Widgets/TopAuthors.vue?vue&type=template&id=4139de44&");
/* harmony import */ var _TopAuthors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TopAuthors.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TopAuthors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TopAuthors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TopAuthors_vue_vue_type_template_id_4139de44___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TopAuthors_vue_vue_type_template_id_4139de44___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TopAuthors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TopAuthors.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/TopAuthors.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TopAuthors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TopAuthors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopAuthors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TopAuthors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TopAuthors.vue?vue&type=template&id=4139de44&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TopAuthors.vue?vue&type=template&id=4139de44& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopAuthors_vue_vue_type_template_id_4139de44___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TopAuthors.vue?vue&type=template&id=4139de44& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopAuthors.vue?vue&type=template&id=4139de44&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopAuthors_vue_vue_type_template_id_4139de44___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopAuthors_vue_vue_type_template_id_4139de44___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/UsersList.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Widgets/UsersList.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UsersList_vue_vue_type_template_id_27b0ad6b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UsersList.vue?vue&type=template&id=27b0ad6b& */ "./resources/js/components/Widgets/UsersList.vue?vue&type=template&id=27b0ad6b&");
/* harmony import */ var _UsersList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UsersList.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/UsersList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UsersList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UsersList_vue_vue_type_template_id_27b0ad6b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UsersList_vue_vue_type_template_id_27b0ad6b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/UsersList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/UsersList.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Widgets/UsersList.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./UsersList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/UsersList.vue?vue&type=template&id=27b0ad6b&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Widgets/UsersList.vue?vue&type=template&id=27b0ad6b& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersList_vue_vue_type_template_id_27b0ad6b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UsersList.vue?vue&type=template&id=27b0ad6b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/UsersList.vue?vue&type=template&id=27b0ad6b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersList_vue_vue_type_template_id_27b0ad6b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersList_vue_vue_type_template_id_27b0ad6b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/Agency.vue":
/*!*************************************************!*\
  !*** ./resources/js/views/dashboard/Agency.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Agency_vue_vue_type_template_id_3f69ee02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Agency.vue?vue&type=template&id=3f69ee02& */ "./resources/js/views/dashboard/Agency.vue?vue&type=template&id=3f69ee02&");
/* harmony import */ var _Agency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Agency.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/Agency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Agency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Agency_vue_vue_type_template_id_3f69ee02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Agency_vue_vue_type_template_id_3f69ee02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/Agency.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/Agency.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/dashboard/Agency.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Agency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Agency.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Agency.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Agency_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/Agency.vue?vue&type=template&id=3f69ee02&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/dashboard/Agency.vue?vue&type=template&id=3f69ee02& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Agency_vue_vue_type_template_id_3f69ee02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Agency.vue?vue&type=template&id=3f69ee02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Agency.vue?vue&type=template&id=3f69ee02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Agency_vue_vue_type_template_id_3f69ee02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Agency_vue_vue_type_template_id_3f69ee02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);