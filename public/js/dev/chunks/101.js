(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[101],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Overlays.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Overlays.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      overlay1: false,
      absolute: false,
      opacity: 0.46,
      overlay: false,
      zIndex: 5
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Overlays.vue?vue&type=template&id=2d258de1&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Overlays.vue?vue&type=template&id=2d258de1& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3" },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.flatButton"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-overlay")]),
                      _vm._v(
                        " component is used to provide emphasis on a particular element or parts\n\t\t\t\t\t\tof it."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "text-center" },
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { color: "error" },
                          on: {
                            click: function($event) {
                              _vm.overlay1 = !_vm.overlay1
                            }
                          }
                        },
                        [_vm._v("\n\t\t\t\t\t\tShow Overlay\n\t\t\t\t\t")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-overlay",
                        { attrs: { value: _vm.overlay1 } },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { icon: "" },
                              on: {
                                click: function($event) {
                                  _vm.overlay1 = false
                                }
                              }
                            },
                            [_c("v-icon", [_vm._v("mdi-close")])],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-row",
                    { attrs: { justify: "center" } },
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "mt-12",
                              attrs: { color: "primary" },
                              on: {
                                click: function($event) {
                                  _vm.overlay = !_vm.overlay
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\tShow Overlay\n\t\t\t\t\t\t"
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-overlay",
                            {
                              attrs: {
                                absolute: _vm.absolute,
                                opacity: _vm.opacity,
                                value: _vm.overlay,
                                "z-index": _vm.zIndex
                              }
                            },
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "primary" },
                                  on: {
                                    click: function($event) {
                                      _vm.overlay = false
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n\t\t\t\t\t\t\t\tHide Overlay\n\t\t\t\t\t\t\t"
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "overlay-input",
                          attrs: { justify: "center" }
                        },
                        [
                          _c(
                            "v-radio-group",
                            { attrs: { row: "" } },
                            [
                              _c("v-checkbox", {
                                attrs: { label: "Absolute" },
                                model: {
                                  value: _vm.absolute,
                                  callback: function($$v) {
                                    _vm.absolute = $$v
                                  },
                                  expression: "absolute"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-checkbox", {
                                staticClass: "mx-6",
                                attrs: { label: "value" },
                                model: {
                                  value: _vm.overlay,
                                  callback: function($$v) {
                                    _vm.overlay = $$v
                                  },
                                  expression: "overlay"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-text-field", {
                                staticStyle: { width: "125px" },
                                attrs: {
                                  label: "Opacity",
                                  max: "1",
                                  min: "0",
                                  step: ".01",
                                  type: "number"
                                },
                                on: { keydown: false },
                                model: {
                                  value: _vm.opacity,
                                  callback: function($$v) {
                                    _vm.opacity = $$v
                                  },
                                  expression: "opacity"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-text-field", {
                                staticStyle: { width: "125px" },
                                attrs: {
                                  label: "z-index",
                                  max: "9999",
                                  min: "-9999",
                                  step: "1",
                                  type: "number"
                                },
                                on: { keydown: false },
                                model: {
                                  value: _vm.zIndex,
                                  callback: function($$v) {
                                    _vm.zIndex = $$v
                                  },
                                  expression: "zIndex"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Overlays.vue":
/*!*****************************************************!*\
  !*** ./resources/js/views/ui-elements/Overlays.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Overlays_vue_vue_type_template_id_2d258de1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Overlays.vue?vue&type=template&id=2d258de1& */ "./resources/js/views/ui-elements/Overlays.vue?vue&type=template&id=2d258de1&");
/* harmony import */ var _Overlays_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Overlays.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Overlays.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Overlays_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Overlays_vue_vue_type_template_id_2d258de1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Overlays_vue_vue_type_template_id_2d258de1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Overlays.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Overlays.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Overlays.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlays_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Overlays.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Overlays.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlays_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Overlays.vue?vue&type=template&id=2d258de1&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Overlays.vue?vue&type=template&id=2d258de1& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlays_vue_vue_type_template_id_2d258de1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Overlays.vue?vue&type=template&id=2d258de1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Overlays.vue?vue&type=template&id=2d258de1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlays_vue_vue_type_template_id_2d258de1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Overlays_vue_vue_type_template_id_2d258de1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);