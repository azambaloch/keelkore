(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[17],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/AddNewBlog.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/AddNewBlog.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Dropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    return {
      dropzoneOptions: {
        url: "https://httpbin.org/post",
        thumbnailWidth: 150,
        maxFilesize: 0.5,
        headers: {
          "My-Awesome-Header": "header value"
        }
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_0__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["data"],
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_0__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceShare.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/DeviceShare.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_DoughnutChartV2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/DoughnutChartV2 */ "./resources/js/components/Charts/DoughnutChartV2.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var _views_dashboard_data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../views/dashboard/data */ "./resources/js/views/dashboard/data.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // constants

 //data


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    DoughnutChartV2: _Charts_DoughnutChartV2__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      devicesShare: _views_dashboard_data__WEBPACK_IMPORTED_MODULE_2__["devicesShare"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Invoice.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Invoice.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      invoice: [],
      headers: [{
        text: "#",
        sortable: false,
        value: "#"
      }, {
        text: "Buyer",
        sortable: false,
        value: "Buyer"
      }, {
        text: "Date",
        sortable: false,
        value: "Date"
      }, {
        text: "Status",
        sortable: false,
        value: "status"
      }, {
        text: "Amount",
        sortable: false,
        value: "amount"
      }]
    };
  },
  mounted: function mounted() {
    this.getInvoice();
  },
  methods: {
    getInvoice: function getInvoice() {
      var self = this;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/invoice.js").then(function (response) {
        self.loader = false;
        self.invoice = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentOrders.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentOrders.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      selected: [],
      headers: [{
        text: "Select",
        align: "center",
        sortable: false,
        value: "select"
      }, {
        text: "Order Id",
        align: "center",
        sortable: false,
        value: "id"
      }, {
        text: "Product Name",
        align: "center",
        sortable: false,
        value: "productName"
      }, {
        text: "Quantity",
        align: "center",
        sortable: false,
        value: "quantity"
      }, {
        text: "Status",
        align: "center",
        sortable: false,
        value: "status"
      }, {
        text: "Amount",
        align: "center",
        sortable: false,
        value: "amount"
      }, {
        text: "",
        align: "center",
        sortable: false
      }],
      items: []
    };
  },
  mounted: function mounted() {
    this.getShoppingCart();
  },
  methods: {
    toggleAll: function toggleAll() {
      if (this.selected.length) this.selected = [];else this.selected = this.items.slice();
    },
    // on delete cart item
    onDeleteCartItem: function onDeleteCartItem(item) {
      var index = this.items.indexOf(item);
      this.items.splice(index, 1);
    },
    getShoppingCart: function getShoppingCart() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/recentOrders.js").then(function (response) {
        _this.loader = false;
        _this.items = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Reviews.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Reviews.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var vue_star_rating__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-star-rating */ "./node_modules/vue-star-rating/dist/VueStarRating.common.js");
/* harmony import */ var vue_star_rating__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_star_rating__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      reviews: null
    };
  },
  mounted: function mounted() {
    this.getReviews();
  },
  methods: {
    getReviews: function getReviews() {
      var self = this;
      Api__WEBPACK_IMPORTED_MODULE_2__["default"].get("vuely/reviews.js").then(function (response) {
        self.reviews = response.data;
      })["catch"](function (error) {
        console.log("error" + error);
      });
    },
    next: function next() {
      this.$refs.reviewSlider.next();
    },
    prev: function prev() {
      this.$refs.reviewSlider.prev();
    }
  },
  computed: {
    slickOptions: function slickOptions() {
      return {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        rtl: this.$store.getters.rtlLayout
      };
    }
  },
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"],
    StarRating: vue_star_rating__WEBPACK_IMPORTED_MODULE_1___default.a
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SocialFeeds.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SocialFeeds.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["socialIcon", "friends", "type", "feeds"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      supportRequests: [],
      settings: {
        maxScrollbarLength: 160
      }
    };
  },
  mounted: function mounted() {
    this.getSupportRequests();
  },
  methods: {
    getSupportRequests: function getSupportRequests() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/supportRequests.js").then(function (response) {
        _this.loader = false;
        _this.supportRequests = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopSelling.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TopSelling.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      slickOptions: {
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: false,
        rtl: this.$store.getters.rtlLayout
      },
      products: [{
        id: 1,
        name: "Bluetooth Speakers",
        price: 255,
        thumbnail: "/static/img/product-1.png"
      }, {
        id: 2,
        name: "Jbm Headphones",
        price: 340,
        thumbnail: "/static/img/product-2.png"
      }, {
        id: 3,
        name: "AT Jack Radio",
        price: 340,
        thumbnail: "/static/img/product-3.png"
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WeeklySales.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/WeeklySales.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Charts/LineChartWithArea */ "./resources/js/components/Charts/LineChartWithArea.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    LineChartWithArea: _Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ["enableXAxesLine"],
  data: function data() {
    return {
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"],
      weeklySales: [10, 25, 55, 40, 45, 50, 75, 70, 72, 60, 25],
      labels: ["A", "B", "C", "D", "E", "F", "J", "K", "L", "M", "N"]
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Ecommerce.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Ecommerce.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Components_Charts_LineChartShadow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Components/Charts/LineChartShadow */ "./resources/js/components/Charts/LineChartShadow.js");
/* harmony import */ var Components_Charts_SalesChartV2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Components/Charts/SalesChartV2 */ "./resources/js/components/Charts/SalesChartV2.js");
/* harmony import */ var Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Components/Charts/LineChartWithArea */ "./resources/js/components/Charts/LineChartWithArea.js");
/* harmony import */ var Components_Widgets_RecentSales__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Components/Widgets/RecentSales */ "./resources/js/components/Widgets/RecentSales.vue");
/* harmony import */ var Components_Widgets_SupportRequestV2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Components/Widgets/SupportRequestV2 */ "./resources/js/components/Widgets/SupportRequestV2.vue");
/* harmony import */ var Components_Widgets_ToDoList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Components/Widgets/ToDoList */ "./resources/js/components/Widgets/ToDoList.vue");
/* harmony import */ var Components_Widgets_Invoice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Components/Widgets/Invoice */ "./resources/js/components/Widgets/Invoice.vue");
/* harmony import */ var Components_Widgets_RecentOrders__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Components/Widgets/RecentOrders */ "./resources/js/components/Widgets/RecentOrders.vue");
/* harmony import */ var Components_Widgets_WeeklySales_vue__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Components/Widgets/WeeklySales.vue */ "./resources/js/components/Widgets/WeeklySales.vue");
/* harmony import */ var Components_Widgets_Reviews__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Components/Widgets/Reviews */ "./resources/js/components/Widgets/Reviews.vue");
/* harmony import */ var Components_Widgets_SocialFeeds__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Components/Widgets/SocialFeeds */ "./resources/js/components/Widgets/SocialFeeds.vue");
/* harmony import */ var Components_Widgets_TopSelling__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Components/Widgets/TopSelling */ "./resources/js/components/Widgets/TopSelling.vue");
/* harmony import */ var Components_Widgets_AddNewBlog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Components/Widgets/AddNewBlog */ "./resources/js/components/Widgets/AddNewBlog.vue");
/* harmony import */ var Components_Widgets_BlogLayoutOne__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! Components/Widgets/BlogLayoutOne */ "./resources/js/components/Widgets/BlogLayoutOne.vue");
/* harmony import */ var Components_Widgets_BlogLayoutFour__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! Components/Widgets/BlogLayoutFour */ "./resources/js/components/Widgets/BlogLayoutFour.vue");
/* harmony import */ var Components_Widgets_CategorySales__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! Components/Widgets/CategorySales */ "./resources/js/components/Widgets/CategorySales.vue");
/* harmony import */ var Components_Widgets_DeviceShare__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! Components/Widgets/DeviceShare */ "./resources/js/components/Widgets/DeviceShare.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// charts component


 // // widgets

















/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    LineChartShadow: Components_Charts_LineChartShadow__WEBPACK_IMPORTED_MODULE_0__["default"],
    RecentSale: Components_Widgets_RecentSales__WEBPACK_IMPORTED_MODULE_3__["default"],
    SupportRequest: Components_Widgets_SupportRequestV2__WEBPACK_IMPORTED_MODULE_4__["default"],
    Sales: Components_Charts_SalesChartV2__WEBPACK_IMPORTED_MODULE_1__["default"],
    ToDoList: Components_Widgets_ToDoList__WEBPACK_IMPORTED_MODULE_5__["default"],
    Invoice: Components_Widgets_Invoice__WEBPACK_IMPORTED_MODULE_6__["default"],
    RecentOrders: Components_Widgets_RecentOrders__WEBPACK_IMPORTED_MODULE_7__["default"],
    CategorySale: Components_Widgets_CategorySales__WEBPACK_IMPORTED_MODULE_15__["default"],
    LineChartWithArea: Components_Charts_LineChartWithArea__WEBPACK_IMPORTED_MODULE_2__["default"],
    WeeklySales: Components_Widgets_WeeklySales_vue__WEBPACK_IMPORTED_MODULE_8__["default"],
    Reviews: Components_Widgets_Reviews__WEBPACK_IMPORTED_MODULE_9__["default"],
    SocialFeeds: Components_Widgets_SocialFeeds__WEBPACK_IMPORTED_MODULE_10__["default"],
    TopSelling: Components_Widgets_TopSelling__WEBPACK_IMPORTED_MODULE_11__["default"],
    NewPost: Components_Widgets_AddNewBlog__WEBPACK_IMPORTED_MODULE_12__["default"],
    BlogLayoutOne: Components_Widgets_BlogLayoutOne__WEBPACK_IMPORTED_MODULE_13__["default"],
    BlogLayoutFour: Components_Widgets_BlogLayoutFour__WEBPACK_IMPORTED_MODULE_14__["default"],
    DeviceShare: Components_Widgets_DeviceShare__WEBPACK_IMPORTED_MODULE_16__["default"]
  },
  data: function data() {
    return {
      loader: true,
      headers: [{
        text: "ID",
        align: "left",
        sortable: false,
        value: "id"
      }, {
        text: "Scheme Name",
        value: "schemeName"
      }, {
        text: "Address",
        value: "address"
      }, {
        text: "Total Plots",
        value: "TPlots"
      }, {
        text: "Total Bought",
        value: "TBought"
      }, {
        text: "Plots Remaining",
        value: "TRemaining"
      }],
      customers: [{
        text: "Customer Name",
        value: "name"
      }, {
        text: "Project",
        value: "project"
      }, {
        text: "Plot Number",
        value: "plotNum"
      }, {
        text: "Plot Size",
        value: "plotSize"
      }, {
        text: "Total Cost",
        value: "project"
      }, {
        text: "Total Paid",
        value: "plotNum"
      }, {
        text: "Total Remaining",
        value: "plotSize"
      }, {
        text: "Action",
        value: "actions"
      }],
      cuss: [],
      total: 0,
      projectCount: 0,
      partnerCount: 0,
      blog: {
        id: 3,
        thumbnail: "/static/img/blog-3.jpg",
        title: "lorem ipsum is simply dummy text",
        body: "Consectetur adipisicing elit. Ullam expedita, necessitatibus sit exercitationem aut quo quos inventore similique nulla minima distinctio illo iste dignissimos vero nostrum, magni pariatur delectus natus.",
        date: "1-jun-2018"
      },
      ChartConfig: Constants_chart_config__WEBPACK_IMPORTED_MODULE_18__["ChartConfig"],
      labels: ["A", "B", "C", "D", "E", "F", "J", "K", "L", "M", "N", "P"],
      totalEarnings: [30, 50, 25, 55, 44, 60, 30, 20, 40, 20, 40, 44],
      onlineRevenue: [30, 50, 25, 55, 44, 60, 30, 20, 40, 20, 40, 44],
      newCustomers: [30, 50, 25, 55, 44, 60, 30, 20, 40, 20, 40, 44]
    };
  },
  methods: {
    getCustomers: function getCustomers() {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_17___default.a.get("/getCustomers").then(function (_ref) {
        var data = _ref.data;
        _this.cuss = data.customers; //console.log('data', this.cuss)
      });
    },
    customerCount: function customerCount() {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_17___default.a.get("/customerCount").then(function (_ref2) {
        var data = _ref2.data;
        _this2.total = data.total; //console.log('total data', this.total)
      });
    },
    getProjectCount: function getProjectCount() {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_17___default.a.get("/projectCount").then(function (_ref3) {
        var data = _ref3.data;
        _this3.projectCount = data.projectCount; //console.log('project data', this.projectCount)
      });
    },
    getPartnerCount: function getPartnerCount() {
      var _this4 = this;

      axios__WEBPACK_IMPORTED_MODULE_17___default.a.get("/partnerCount").then(function (_ref4) {
        var data = _ref4.data;
        _this4.partnerCount = data.partnerCount; //console.log('project data', this.projectCount)
      });
    },
    generateReport: function generateReport() {
      axios__WEBPACK_IMPORTED_MODULE_17___default.a.get("/printData/" + this.$route.params.id, {
        responseType: "arraybuffer"
      }).then(function (response) {
        console.log(response.data);
        var ext = response.headers["content-type"].split("/").pop();
        var blob = new Blob([response.data], {
          type: "application/pdf"
        }); //  let blob = new Blob([response.data], { type: response.headers["content-type"] });

        var link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        link.download = 'Application Form';
        link.click();
      });
    }
  },
  created: function created() {
    this.getCustomers();
    this.customerCount();
    this.getProjectCount();
    this.getPartnerCount();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/AddNewBlog.vue?vue&type=template&id=77e4622c&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/AddNewBlog.vue?vue&type=template&id=77e4622c& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-form",
    { staticClass: "add-blog-form d-custom-flex justify-space-between" },
    [
      _c(
        "dropzone",
        {
          staticClass: "mb-2",
          attrs: { id: "myVueDropzone", options: _vm.dropzoneOptions }
        },
        [
          _c("input", {
            attrs: { type: "hidden", name: "token", value: "xxx" }
          })
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "add-blog-widget" },
        [
          _c("v-text-field", { attrs: { name: "subject", label: "Subject" } }),
          _vm._v(" "),
          _c("v-textarea", { attrs: { name: "content", label: "Content" } }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mb-2" },
            [
              _c("v-btn", { attrs: { color: "success" } }, [
                _vm._v(_vm._s(_vm.$t("message.publish")))
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=template&id=73a04b5d&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=template&id=73a04b5d& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "blog-four-layout white--text pa-4 d-flex align-end" },
    [
      _c("div", [
        _c(
          "a",
          {
            attrs: {
              href: "/" + (_vm.getCurrentAppLayoutHandler() + "/pages/blog")
            }
          },
          [
            _c("h3", { staticClass: "white--text" }, [
              _vm._v("Where Can You Find Unique Myspace Layouts Nowadays")
            ])
          ]
        ),
        _vm._v(" "),
        _c("span", { staticClass: "white--text fs-14" }, [
          _vm._v("11 Nov 2017 , By: Admin , 5 Comments ")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "d-custom-flex justify-space-between" },
          [
            _c(
              "div",
              [
                _c(
                  "v-btn",
                  { staticClass: "mr-3", attrs: { icon: "" } },
                  [
                    _c("v-icon", { staticClass: "white--text" }, [
                      _vm._v("share")
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-btn",
                  { attrs: { icon: "" } },
                  [
                    _c("v-icon", { staticClass: "white--text" }, [
                      _vm._v("favorite")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { staticClass: "white--text" }, [
                  _vm._v("more_horiz")
                ])
              ],
              1
            )
          ],
          1
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=template&id=a8db4102&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=template&id=a8db4102& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "blog-widget" }, [
    _c("div", { staticClass: "blog-layout-v1" }, [
      _c("a", { attrs: { href: "javascript:;" } }, [
        _c("img", {
          staticClass: "img-responsive mb-4",
          attrs: { src: _vm.data.thumbnail }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "app-card-title pa-0 mb-2" }, [
          _c("h5", [_vm._v(_vm._s(_vm.data.title))])
        ])
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "grey--text fs-12 fw-normal mb-2 d-block" }, [
        _vm._v("Last updated: " + _vm._s(_vm.data.date))
      ]),
      _vm._v(" "),
      _c("p", [_vm._v(_vm._s(_vm.data.body))])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "d-custom-flex justify-space-between" },
      [
        _c(
          "div",
          [
            _c(
              "v-btn",
              { staticClass: "mr-3", attrs: { icon: "" } },
              [_c("v-icon", { attrs: { color: "grey" } }, [_vm._v("share")])],
              1
            ),
            _vm._v(" "),
            _c(
              "v-btn",
              { attrs: { icon: "" } },
              [
                _c("v-icon", { attrs: { color: "grey" } }, [_vm._v("favorite")])
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "v-btn",
          { attrs: { icon: "" } },
          [_c("v-icon", { staticClass: "grey--text" }, [_vm._v("more_horiz")])],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CategorySales.vue?vue&type=template&id=4546725a&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/CategorySales.vue?vue&type=template&id=4546725a& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "category-progress-wrap" }, [
    _c("div", { staticClass: "d-custom-flex" }, [
      _c("span", { staticClass: "mr-3 label" }, [_vm._v("Clothing")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "progress-wrap" },
        [
          _c("span", [_vm._v("75%")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "75", height: "24", color: "info" }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-custom-flex" }, [
      _c("span", { staticClass: "mr-3 label" }, [_vm._v("Gadgets")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "progress-wrap" },
        [
          _c("span", [_vm._v("45%")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "45", height: "24", color: "info" }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-custom-flex" }, [
      _c("span", { staticClass: "mr-3 label" }, [_vm._v("Furniture")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "progress-wrap" },
        [
          _c("span", [_vm._v("83%")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "83", height: "24", color: "info" }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-custom-flex" }, [
      _c("span", { staticClass: "mr-3 label" }, [_vm._v("Wine")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "progress-wrap" },
        [
          _c("span", [_vm._v("15%")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "15", height: "24", color: "info" }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-custom-flex" }, [
      _c("span", { staticClass: "mr-3 label" }, [_vm._v("Toys")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "progress-wrap" },
        [
          _c("span", [_vm._v("58%")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "58", height: "24", color: "info" }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-custom-flex" }, [
      _c("span", { staticClass: "mr-3 label" }, [_vm._v("Nutrition")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "progress-wrap" },
        [
          _c("span", [_vm._v("39%")]),
          _vm._v(" "),
          _c("v-progress-linear", {
            attrs: { value: "39", height: "24", color: "info" }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceShare.vue?vue&type=template&id=1c328d0e&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/DeviceShare.vue?vue&type=template&id=1c328d0e& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "device-share-wrap" }, [
    _c(
      "div",
      { staticClass: "mb-2 pos-relative" },
      [
        _c("doughnut-chart-v2", {
          attrs: { height: 240, data: _vm.devicesShare }
        }),
        _vm._v(" "),
        _vm._m(0)
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(1)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "overlay-content d-custom-flex justify-center align-items-center"
      },
      [
        _c("span", { staticClass: "grey--text font-2x fw-semi-bold" }, [
          _vm._v("$2000")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "hidden-xs-only" }, [
      _c("div", { staticClass: "d-custom-flex widget-footer" }, [
        _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
          _c("span", { staticClass: "v-badge primary px-2 py-1" }),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("65%")]),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("Website")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
          _c("span", { staticClass: "v-badge success px-2 py-1" }),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("25%")]),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("iOS Devices")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "fs-12 fw-normal grey--text" }, [
          _c("span", { staticClass: "v-badge warning px-2 py-1" }),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("10%")]),
          _vm._v(" "),
          _c("span", { staticClass: "d-block" }, [_vm._v("Android Devices")])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Invoice.vue?vue&type=template&id=8fcb54dc&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Invoice.vue?vue&type=template&id=8fcb54dc& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "table-responsive" },
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.invoice,
          "hide-default-footer": ""
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c("td", [_vm._v(_vm._s(item.id))]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-nowrap" }, [
                    _vm._v("\n\t\t\t\t\t" + _vm._s(item.name) + "\n\t\t\t\t")
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.date))]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "v-badge",
                        { class: item.labelClasses, attrs: { value: false } },
                        [_vm._v(_vm._s(item.status))]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.amount))])
                ])
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentOrders.vue?vue&type=template&id=28191cea&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentOrders.vue?vue&type=template&id=28191cea& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.items,
          "item-key": "id",
          "hide-default-footer": ""
        },
        scopedSlots: _vm._u([
          {
            key: "item",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("tr", [
                  _c(
                    "td",
                    [
                      _c("v-checkbox", {
                        attrs: { color: "primary", "hide-details": "" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.id))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.productName))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.quantity))]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "v-badge",
                        { class: item.labelClasses, attrs: { value: false } },
                        [_vm._v(_vm._s(item.status))]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("td", [_vm._v("$ " + _vm._s(item.amount))]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { text: "", icon: "", color: "error" },
                          on: {
                            click: function($event) {
                              return _vm.onDeleteCartItem(item)
                            }
                          }
                        },
                        [_c("v-icon", { staticClass: "zmdi zmdi-delete" })],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]
            }
          }
        ]),
        model: {
          value: _vm.selected,
          callback: function($$v) {
            _vm.selected = $$v
          },
          expression: "selected"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Reviews.vue?vue&type=template&id=3d95df80&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/Reviews.vue?vue&type=template&id=3d95df80& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "app-card" }, [
    _c("div", { staticClass: "app-card-title" }, [
      _c("h3", [_vm._v(_vm._s(_vm.$t("message.reviews")))]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "app-contextual-link slider-dir" },
        [
          _c(
            "v-btn",
            {
              staticClass: "ma-0 left-arrow",
              attrs: { icon: "" },
              on: { click: _vm.prev }
            },
            [_c("i", { staticClass: "ti-arrow-left" })]
          ),
          _vm._v(" "),
          _c(
            "v-btn",
            {
              staticClass: "ma-0 right-arrow",
              attrs: { icon: "" },
              on: { click: _vm.next }
            },
            [_c("i", { staticClass: "ti-arrow-right" })]
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "app-card-content pt-0" }, [
      _c(
        "div",
        { staticClass: "review-widget-wrapper" },
        [
          _vm.reviews
            ? _c(
                "slick",
                { ref: "reviewSlider", attrs: { options: _vm.slickOptions } },
                [
                  _vm._l(_vm.reviews, function(review, index) {
                    return [
                      _c("div", { key: index, staticClass: "review-item" }, [
                        _c(
                          "div",
                          { staticClass: "d-custom-flex align-items-center" },
                          [
                            _c("star-rating", {
                              attrs: {
                                "star-size": 15,
                                rating: review.rating,
                                "show-rating": false,
                                "read-only": ""
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "fs-12 grey--text ml-2" },
                              [_vm._v(_vm._s(review.title))]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("p", { staticClass: "fw-bold fs-12 fw-normal" }, [
                          _vm._v(_vm._s(review.subject))
                        ]),
                        _vm._v(" "),
                        _c("p", { staticClass: "fs-12 grey--text fw-normal" }, [
                          _vm._v(_vm._s(review.description))
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "fs-12 fw-normal",
                            attrs: { href: "javascript:void(0)" }
                          },
                          [_vm._v("Reply")]
                        )
                      ])
                    ]
                  })
                ],
                2
              )
            : _vm._e()
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SocialFeeds.vue?vue&type=template&id=0094118d&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SocialFeeds.vue?vue&type=template&id=0094118d& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { class: _vm.type }, [
    _c(
      "span",
      {
        staticClass:
          "social-icon border d-custom-flex align-items-center rounded-circle"
      },
      [
        _c("v-icon", { attrs: { medium: "" } }, [
          _vm._v(_vm._s(_vm.socialIcon))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c("span", [
      _c("span", { staticClass: "d-block fw-bold" }, [
        _vm._v(_vm._s(_vm.friends))
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "d-block" }, [_vm._v("Friends")])
    ]),
    _vm._v(" "),
    _c("span", [
      _c("span", { staticClass: "d-block fw-bold" }, [
        _vm._v(_vm._s(_vm.feeds))
      ]),
      _vm._v(" "),
      _c("span", { staticClass: "d-block" }, [_vm._v("Feeds")])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=template&id=53cdbb47&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=template&id=53cdbb47& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "404px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            { staticClass: "card-list", attrs: { "two-line": "" } },
            [
              _vm._l(_vm.supportRequests, function(request, index) {
                return [
                  _c(
                    "v-list-item",
                    { key: index },
                    [
                      _c(
                        "v-list-item-content",
                        [
                          _c(
                            "v-list-item-subtitle",
                            [
                              _c("h5", { staticClass: "mb-1" }, [
                                _vm._v(_vm._s(request.title))
                              ]),
                              _vm._v(" "),
                              _c(
                                "v-list-item-action-text",
                                { staticClass: "fw-light" },
                                [
                                  _vm._v(
                                    "\n\t\t\t\t\t\t\t\t\t" +
                                      _vm._s(request.date) +
                                      "\n\t\t\t\t\t\t\t\t"
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "p",
                            { staticClass: "mb-0 fs-12 grey--text fw-normal" },
                            [_vm._v(_vm._s(request.content))]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-list-item-action",
                        [
                          request.status
                            ? _c(
                                "v-badge",
                                {
                                  staticClass: "error",
                                  attrs: { value: false }
                                },
                                [_vm._v("Pending")]
                              )
                            : _c(
                                "v-badge",
                                {
                                  staticClass: "info",
                                  attrs: { value: false }
                                },
                                [_vm._v("Closed")]
                              )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              })
            ],
            2
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopSelling.vue?vue&type=template&id=69402834&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/TopSelling.vue?vue&type=template&id=69402834& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "app-card",
    {
      attrs: {
        heading: _vm.$t("message.topSelling"),
        fullScreen: true,
        reloadable: true,
        closeable: true,
        footer: true
      }
    },
    [
      _c(
        "div",
        { staticClass: "top-selling-widget" },
        [
          _c(
            "slick",
            { attrs: { options: _vm.slickOptions } },
            _vm._l(_vm.products, function(product) {
              return _c("div", { key: product.id }, [
                _c("div", { staticClass: "thumbnail mb-2" }, [
                  _c("img", {
                    staticClass: "img-responsive mx-auto",
                    attrs: {
                      src: product.thumbnail,
                      width: "245",
                      height: "250"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("h4", { staticClass: "text-center fw-bold mb-4" }, [
                  _vm._v(_vm._s(product.name))
                ]),
                _vm._v(" "),
                _c("h5", { staticClass: "primary--text text-center" }, [
                  _vm._v("$" + _vm._s(product.price.toFixed(2)))
                ])
              ])
            }),
            0
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "d-flex", attrs: { slot: "footer" }, slot: "footer" },
        [
          _c("div", { staticClass: "w-50 text-center" }, [
            _c("p", { staticClass: "mb-0 grey--text fs-12 fw-normal" }, [
              _vm._v("Total Sales")
            ]),
            _vm._v(" "),
            _c("h4", { staticClass: "fw-bold" }, [_vm._v("12,550")])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "w-50 text-center border-left-1" }, [
            _c("p", { staticClass: "mb-0 grey--text fs-12 fw-normal" }, [
              _vm._v("Earning")
            ]),
            _vm._v(" "),
            _c("h4", { staticClass: "fw-bold" }, [_vm._v("25,000")])
          ])
        ]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WeeklySales.vue?vue&type=template&id=a975f420&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/WeeklySales.vue?vue&type=template&id=a975f420& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "widget" },
    [
      _c(
        "v-card-text",
        { staticClass: "pa-0" },
        [
          _c("line-chart-with-area", {
            attrs: {
              dataSet: _vm.weeklySales,
              lineTension: 0.5,
              dataLabels: _vm.labels,
              width: 370,
              height: 80,
              enableXAxesLine: _vm.enableXAxesLine,
              color: _vm.ChartConfig.color.primary
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Ecommerce.vue?vue&type=template&id=4caec0b6&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/Ecommerce.vue?vue&type=template&id=4caec0b6& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-container",
        { staticClass: "pt-0 grid-list-xl", attrs: { fluid: "" } },
        [
          _c("section-tooltip", {
            attrs: { title: "DASHBOOARD", tooltip: "Keelkore enterprise" }
          }),
          _vm._v(" "),
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "v-row",
                [
                  _c("stats-card-v2", {
                    attrs: {
                      colClasses: "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                      heading: "Total Customers",
                      amount: this.total
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "stats-card-v2",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12",
                        heading: "Total Schemes",
                        amount: this.projectCount
                      }
                    },
                    [
                      _c(
                        "v-btn",
                        {
                          staticClass: "margin",
                          attrs: {
                            color: "primary",
                            small: "",
                            to: "/default/icons/material"
                          }
                        },
                        [_vm._v("Add Project")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "margin",
                          attrs: {
                            color: "primary",
                            small: "",
                            to: "/default/icons/themify"
                          }
                        },
                        [_vm._v("View Projects")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "stats-card-v2",
                    {
                      attrs: {
                        colClasses:
                          "col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12",
                        heading: "Partners",
                        amount: this.partnerCount
                      }
                    },
                    [
                      _c(
                        "v-btn",
                        {
                          staticClass: "margin",
                          attrs: {
                            color: "primary",
                            small: "",
                            to: "/default/maps/google-maps"
                          }
                        },
                        [_vm._v("Add Partner")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "margin",
                          attrs: {
                            color: "primary",
                            small: "",
                            to: "/default/maps/leaflet-maps"
                          }
                        },
                        [_vm._v("View Partners")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-container",
            { attrs: { fluid: "" } },
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: "Total Customers",
                    fullScreen: false,
                    reloadable: false,
                    closeable: false
                  }
                },
                [
                  _c(
                    "v-btn",
                    {
                      staticClass: "ma-2",
                      attrs: {
                        small: "",
                        to: "/default/forms/form-validation/",
                        outlined: "",
                        color: "success"
                      }
                    },
                    [_vm._v("Add Customer")]
                  ),
                  _vm._v(" "),
                  _c("v-data-table", {
                    attrs: { headers: _vm.customers, items: _vm.cuss },
                    scopedSlots: _vm._u([
                      {
                        key: "item",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c("tr", [
                              _c("td", [_vm._v(_vm._s(item.name))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.project))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.plotNum))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.plotSize))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.totalAmount))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.paidAmount))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.remainingAmount))]),
                              _vm._v(" "),
                              _c(
                                "td",
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "ma-2",
                                      attrs: {
                                        small: "",
                                        to: "/printData/" + item.id,
                                        outlined: "",
                                        color: "success"
                                      }
                                    },
                                    [_vm._v("Form")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "ma-2",
                                      attrs: {
                                        small: "",
                                        to: "/printReceipt/" + item.id,
                                        outlined: "",
                                        color: "success"
                                      }
                                    },
                                    [_vm._v("Receipt")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "ma-2",
                                      attrs: {
                                        small: "",
                                        to:
                                          "/default/dashboard/news/" + item.id,
                                        outlined: "",
                                        color: "success"
                                      }
                                    },
                                    [_vm._v("Payment")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "ma-2",
                                      attrs: {
                                        small: "",
                                        to:
                                          "/default/dashboard/magazine/" +
                                          item.id,
                                        outlined: "",
                                        color: "success"
                                      }
                                    },
                                    [_vm._v("Edit")]
                                  )
                                ],
                                1
                              )
                            ])
                          ]
                        }
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Charts/SalesChartV2.js":
/*!********************************************************!*\
  !*** ./resources/js/components/Charts/SalesChartV2.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Sales Widget


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  data: function data() {
    return {
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            stacked: true,
            ticks: {
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }],
          xAxes: [{
            stacked: true,
            ticks: {
              padding: 10
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        }
      }
    };
  },
  mounted: function mounted() {
    this.renderChart({
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
      datasets: [{
        type: 'bar',
        label: 'Earned',
        barPercentage: 0.4,
        categoryPercentage: 1.3,
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        hoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.primary,
        borderWidth: 0,
        data: [45, 32, 50, 42, 32, 46, 30, 25, 29, 21, 21, 25]
      }, {
        type: 'bar',
        label: 'Views',
        barPercentage: 0.4,
        categoryPercentage: 1.3,
        backgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].lineChartAxesColor,
        hoverBackgroundColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].lineChartAxesColor,
        borderWidth: 0,
        data: [10, 20, 12, 30, 10, 32, 28, 30, 20, 18, 18, 30]
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/AddNewBlog.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/AddNewBlog.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddNewBlog_vue_vue_type_template_id_77e4622c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddNewBlog.vue?vue&type=template&id=77e4622c& */ "./resources/js/components/Widgets/AddNewBlog.vue?vue&type=template&id=77e4622c&");
/* harmony import */ var _AddNewBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddNewBlog.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/AddNewBlog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AddNewBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddNewBlog_vue_vue_type_template_id_77e4622c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddNewBlog_vue_vue_type_template_id_77e4622c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/AddNewBlog.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/AddNewBlog.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/AddNewBlog.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNewBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AddNewBlog.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/AddNewBlog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNewBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/AddNewBlog.vue?vue&type=template&id=77e4622c&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/AddNewBlog.vue?vue&type=template&id=77e4622c& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNewBlog_vue_vue_type_template_id_77e4622c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AddNewBlog.vue?vue&type=template&id=77e4622c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/AddNewBlog.vue?vue&type=template&id=77e4622c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNewBlog_vue_vue_type_template_id_77e4622c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNewBlog_vue_vue_type_template_id_77e4622c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutFour.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutFour.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogLayoutFour_vue_vue_type_template_id_73a04b5d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogLayoutFour.vue?vue&type=template&id=73a04b5d& */ "./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=template&id=73a04b5d&");
/* harmony import */ var _BlogLayoutFour_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlogLayoutFour.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BlogLayoutFour_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BlogLayoutFour_vue_vue_type_template_id_73a04b5d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogLayoutFour_vue_vue_type_template_id_73a04b5d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BlogLayoutFour.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutFour_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogLayoutFour.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutFour_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=template&id=73a04b5d&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=template&id=73a04b5d& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutFour_vue_vue_type_template_id_73a04b5d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogLayoutFour.vue?vue&type=template&id=73a04b5d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutFour.vue?vue&type=template&id=73a04b5d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutFour_vue_vue_type_template_id_73a04b5d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutFour_vue_vue_type_template_id_73a04b5d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutOne.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutOne.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogLayoutOne_vue_vue_type_template_id_a8db4102___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogLayoutOne.vue?vue&type=template&id=a8db4102& */ "./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=template&id=a8db4102&");
/* harmony import */ var _BlogLayoutOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlogLayoutOne.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BlogLayoutOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BlogLayoutOne_vue_vue_type_template_id_a8db4102___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogLayoutOne_vue_vue_type_template_id_a8db4102___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/BlogLayoutOne.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogLayoutOne.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutOne_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=template&id=a8db4102&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=template&id=a8db4102& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutOne_vue_vue_type_template_id_a8db4102___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogLayoutOne.vue?vue&type=template&id=a8db4102& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/BlogLayoutOne.vue?vue&type=template&id=a8db4102&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutOne_vue_vue_type_template_id_a8db4102___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogLayoutOne_vue_vue_type_template_id_a8db4102___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/CategorySales.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Widgets/CategorySales.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CategorySales_vue_vue_type_template_id_4546725a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CategorySales.vue?vue&type=template&id=4546725a& */ "./resources/js/components/Widgets/CategorySales.vue?vue&type=template&id=4546725a&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _CategorySales_vue_vue_type_template_id_4546725a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CategorySales_vue_vue_type_template_id_4546725a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/CategorySales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/CategorySales.vue?vue&type=template&id=4546725a&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/Widgets/CategorySales.vue?vue&type=template&id=4546725a& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CategorySales_vue_vue_type_template_id_4546725a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CategorySales.vue?vue&type=template&id=4546725a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/CategorySales.vue?vue&type=template&id=4546725a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CategorySales_vue_vue_type_template_id_4546725a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CategorySales_vue_vue_type_template_id_4546725a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/DeviceShare.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/DeviceShare.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DeviceShare_vue_vue_type_template_id_1c328d0e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DeviceShare.vue?vue&type=template&id=1c328d0e& */ "./resources/js/components/Widgets/DeviceShare.vue?vue&type=template&id=1c328d0e&");
/* harmony import */ var _DeviceShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DeviceShare.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/DeviceShare.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DeviceShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DeviceShare_vue_vue_type_template_id_1c328d0e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DeviceShare_vue_vue_type_template_id_1c328d0e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/DeviceShare.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/DeviceShare.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Widgets/DeviceShare.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DeviceShare.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceShare.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceShare_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/DeviceShare.vue?vue&type=template&id=1c328d0e&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/DeviceShare.vue?vue&type=template&id=1c328d0e& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceShare_vue_vue_type_template_id_1c328d0e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DeviceShare.vue?vue&type=template&id=1c328d0e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/DeviceShare.vue?vue&type=template&id=1c328d0e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceShare_vue_vue_type_template_id_1c328d0e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DeviceShare_vue_vue_type_template_id_1c328d0e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Invoice.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/Widgets/Invoice.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Invoice_vue_vue_type_template_id_8fcb54dc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Invoice.vue?vue&type=template&id=8fcb54dc& */ "./resources/js/components/Widgets/Invoice.vue?vue&type=template&id=8fcb54dc&");
/* harmony import */ var _Invoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Invoice.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Invoice.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Invoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Invoice_vue_vue_type_template_id_8fcb54dc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Invoice_vue_vue_type_template_id_8fcb54dc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Invoice.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Invoice.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/Widgets/Invoice.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Invoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Invoice.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Invoice.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Invoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Invoice.vue?vue&type=template&id=8fcb54dc&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Invoice.vue?vue&type=template&id=8fcb54dc& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Invoice_vue_vue_type_template_id_8fcb54dc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Invoice.vue?vue&type=template&id=8fcb54dc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Invoice.vue?vue&type=template&id=8fcb54dc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Invoice_vue_vue_type_template_id_8fcb54dc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Invoice_vue_vue_type_template_id_8fcb54dc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/RecentOrders.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Widgets/RecentOrders.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RecentOrders_vue_vue_type_template_id_28191cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecentOrders.vue?vue&type=template&id=28191cea& */ "./resources/js/components/Widgets/RecentOrders.vue?vue&type=template&id=28191cea&");
/* harmony import */ var _RecentOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecentOrders.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/RecentOrders.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RecentOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecentOrders_vue_vue_type_template_id_28191cea___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RecentOrders_vue_vue_type_template_id_28191cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/RecentOrders.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/RecentOrders.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentOrders.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentOrders.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentOrders.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/RecentOrders.vue?vue&type=template&id=28191cea&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentOrders.vue?vue&type=template&id=28191cea& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentOrders_vue_vue_type_template_id_28191cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentOrders.vue?vue&type=template&id=28191cea& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentOrders.vue?vue&type=template&id=28191cea&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentOrders_vue_vue_type_template_id_28191cea___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentOrders_vue_vue_type_template_id_28191cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/Reviews.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/Widgets/Reviews.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Reviews_vue_vue_type_template_id_3d95df80___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Reviews.vue?vue&type=template&id=3d95df80& */ "./resources/js/components/Widgets/Reviews.vue?vue&type=template&id=3d95df80&");
/* harmony import */ var _Reviews_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Reviews.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/Reviews.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Reviews_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Reviews_vue_vue_type_template_id_3d95df80___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Reviews_vue_vue_type_template_id_3d95df80___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/Reviews.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/Reviews.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/Widgets/Reviews.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reviews_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Reviews.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Reviews.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Reviews_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/Reviews.vue?vue&type=template&id=3d95df80&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Widgets/Reviews.vue?vue&type=template&id=3d95df80& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Reviews_vue_vue_type_template_id_3d95df80___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Reviews.vue?vue&type=template&id=3d95df80& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/Reviews.vue?vue&type=template&id=3d95df80&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Reviews_vue_vue_type_template_id_3d95df80___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Reviews_vue_vue_type_template_id_3d95df80___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SocialFeeds.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/SocialFeeds.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SocialFeeds_vue_vue_type_template_id_0094118d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SocialFeeds.vue?vue&type=template&id=0094118d& */ "./resources/js/components/Widgets/SocialFeeds.vue?vue&type=template&id=0094118d&");
/* harmony import */ var _SocialFeeds_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SocialFeeds.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SocialFeeds.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SocialFeeds_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SocialFeeds_vue_vue_type_template_id_0094118d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SocialFeeds_vue_vue_type_template_id_0094118d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SocialFeeds.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SocialFeeds.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Widgets/SocialFeeds.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialFeeds_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SocialFeeds.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SocialFeeds.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialFeeds_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SocialFeeds.vue?vue&type=template&id=0094118d&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SocialFeeds.vue?vue&type=template&id=0094118d& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialFeeds_vue_vue_type_template_id_0094118d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SocialFeeds.vue?vue&type=template&id=0094118d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SocialFeeds.vue?vue&type=template&id=0094118d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialFeeds_vue_vue_type_template_id_0094118d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SocialFeeds_vue_vue_type_template_id_0094118d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequestV2.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequestV2.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SupportRequestV2_vue_vue_type_template_id_53cdbb47___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SupportRequestV2.vue?vue&type=template&id=53cdbb47& */ "./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=template&id=53cdbb47&");
/* harmony import */ var _SupportRequestV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SupportRequestV2.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SupportRequestV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SupportRequestV2_vue_vue_type_template_id_53cdbb47___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SupportRequestV2_vue_vue_type_template_id_53cdbb47___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/SupportRequestV2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequestV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportRequestV2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequestV2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=template&id=53cdbb47&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=template&id=53cdbb47& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequestV2_vue_vue_type_template_id_53cdbb47___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SupportRequestV2.vue?vue&type=template&id=53cdbb47& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/SupportRequestV2.vue?vue&type=template&id=53cdbb47&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequestV2_vue_vue_type_template_id_53cdbb47___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SupportRequestV2_vue_vue_type_template_id_53cdbb47___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/TopSelling.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/Widgets/TopSelling.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TopSelling_vue_vue_type_template_id_69402834___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TopSelling.vue?vue&type=template&id=69402834& */ "./resources/js/components/Widgets/TopSelling.vue?vue&type=template&id=69402834&");
/* harmony import */ var _TopSelling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TopSelling.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/TopSelling.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TopSelling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TopSelling_vue_vue_type_template_id_69402834___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TopSelling_vue_vue_type_template_id_69402834___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/TopSelling.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/TopSelling.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/Widgets/TopSelling.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TopSelling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./TopSelling.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopSelling.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TopSelling_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/TopSelling.vue?vue&type=template&id=69402834&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/TopSelling.vue?vue&type=template&id=69402834& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopSelling_vue_vue_type_template_id_69402834___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TopSelling.vue?vue&type=template&id=69402834& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/TopSelling.vue?vue&type=template&id=69402834&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopSelling_vue_vue_type_template_id_69402834___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopSelling_vue_vue_type_template_id_69402834___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/WeeklySales.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/WeeklySales.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _WeeklySales_vue_vue_type_template_id_a975f420___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WeeklySales.vue?vue&type=template&id=a975f420& */ "./resources/js/components/Widgets/WeeklySales.vue?vue&type=template&id=a975f420&");
/* harmony import */ var _WeeklySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WeeklySales.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/WeeklySales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _WeeklySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _WeeklySales_vue_vue_type_template_id_a975f420___WEBPACK_IMPORTED_MODULE_0__["render"],
  _WeeklySales_vue_vue_type_template_id_a975f420___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/WeeklySales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/WeeklySales.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Widgets/WeeklySales.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_WeeklySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./WeeklySales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WeeklySales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_WeeklySales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/WeeklySales.vue?vue&type=template&id=a975f420&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/WeeklySales.vue?vue&type=template&id=a975f420& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WeeklySales_vue_vue_type_template_id_a975f420___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./WeeklySales.vue?vue&type=template&id=a975f420& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/WeeklySales.vue?vue&type=template&id=a975f420&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WeeklySales_vue_vue_type_template_id_a975f420___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_WeeklySales_vue_vue_type_template_id_a975f420___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/Ecommerce.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/dashboard/Ecommerce.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Ecommerce_vue_vue_type_template_id_4caec0b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Ecommerce.vue?vue&type=template&id=4caec0b6& */ "./resources/js/views/dashboard/Ecommerce.vue?vue&type=template&id=4caec0b6&");
/* harmony import */ var _Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Ecommerce.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/Ecommerce.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Ecommerce_vue_vue_type_template_id_4caec0b6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Ecommerce_vue_vue_type_template_id_4caec0b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/Ecommerce.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/Ecommerce.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/dashboard/Ecommerce.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Ecommerce.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Ecommerce.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/Ecommerce.vue?vue&type=template&id=4caec0b6&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/dashboard/Ecommerce.vue?vue&type=template&id=4caec0b6& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_template_id_4caec0b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Ecommerce.vue?vue&type=template&id=4caec0b6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/Ecommerce.vue?vue&type=template&id=4caec0b6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_template_id_4caec0b6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_template_id_4caec0b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);