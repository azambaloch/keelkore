(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data */ "./resources/js/views/courses/data.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['data', 'cols', 'colsm', 'colmd', 'collg', 'colxl', 'width', 'height'],
  data: function data() {
    return {
      dialog: false,
      CourseData: _data__WEBPACK_IMPORTED_MODULE_0__["default"]
    };
  },
  methods: {
    getCurrentAppLayoutHandler: function getCurrentAppLayoutHandler() {
      return Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_1__["getCurrentAppLayout"])(this.$router);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=template&id=9bd39048&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=template&id=9bd39048& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-container", [
    _c(
      "div",
      { staticClass: "course-card layout row wrap" },
      _vm._l(_vm.data, function(list, key) {
        return _c(
          "app-card",
          {
            key: key,
            attrs: {
              customClasses: "course-item-wrap",
              colClasses:
                "col-" +
                _vm.cols +
                " col-sm-" +
                _vm.colsm +
                " col-md-" +
                _vm.colmd +
                " col-lg-" +
                _vm.collg +
                " col-xl-" +
                _vm.colxl
            }
          },
          [
            _c(
              "div",
              { staticClass: "image-wrap" },
              [
                list.videoDemoStatus == false
                  ? [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c("img", {
                            attrs: { src: list.image, alt: "image" }
                          })
                        ]
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                list.videoDemoStatus == true
                  ? [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to:
                              "/" +
                              (_vm.getCurrentAppLayoutHandler() +
                                "/courses/courses-detail")
                          }
                        },
                        [
                          _c("img", {
                            attrs: { src: list.image, alt: "image" }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-dialog",
                        {
                          attrs: { width: "500" },
                          scopedSlots: _vm._u(
                            [
                              {
                                key: "activator",
                                fn: function(ref) {
                                  var on = ref.on
                                  return [
                                    _c(
                                      "v-btn",
                                      _vm._g({ attrs: { icon: "" } }, on),
                                      [
                                        _c("v-icon", [
                                          _vm._v("play_circle_filled")
                                        ])
                                      ],
                                      1
                                    )
                                  ]
                                }
                              }
                            ],
                            null,
                            true
                          ),
                          model: {
                            value: _vm.dialog,
                            callback: function($$v) {
                              _vm.dialog = $$v
                            },
                            expression: "dialog"
                          }
                        },
                        [
                          _vm._v(" "),
                          _c("iframe", {
                            attrs: {
                              src: list.demoVideoUrl,
                              frameborder: "0",
                              allowfullscreen: ""
                            }
                          })
                        ]
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                list.bestseller == true
                  ? [
                      _c(
                        "span",
                        {
                          staticClass:
                            "best-seller bestseller-tag d-inline-block"
                        },
                        [_vm._v(_vm._s(_vm.$t("message.bestseller")))]
                      )
                    ]
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                attrs: {
                  to:
                    "/" +
                    (_vm.getCurrentAppLayoutHandler() +
                      "/courses/courses-detail")
                }
              },
              [
                _c("h4", { staticClass: "make-ellipse" }, [
                  _vm._v(_vm._s(list.name))
                ])
              ]
            ),
            _vm._v(" "),
            _c("span", { staticClass: "fs-12 mb-4 d-block" }, [
              _vm._v(_vm._s(list.content))
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "rating-text layout row wrap ma-0" },
              [
                _c("v-rating", {
                  attrs: {
                    value: list.rating,
                    color: "warning",
                    "background-color": "warning"
                  }
                }),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(list.rating) + " Stars")])
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "price" }, [
              _c("h4", { attrs: { color: "primary" } }, [
                _vm._v(
                  "$" + _vm._s((list.oldPrice * (100 - list.disount)) / 100)
                )
              ]),
              _vm._v(" "),
              _c("del", [_vm._v("$" + _vm._s(list.oldPrice))])
            ]),
            _vm._v(" "),
            _c(
              "app-card",
              { attrs: { customClasses: "course-hover-item" } },
              [
                _c("div", { staticClass: "header" }, [
                  _c("div", { staticClass: "meta-info" }, [
                    _c("span", { staticClass: "date-info fs-12 fw-normal" }, [
                      _vm._v("Last updated: " + _vm._s(list.lastUpdates))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "card-header" }, [
                    _c("h4", [
                      _c("a", { attrs: { href: "#" } }, [
                        _vm._v(_vm._s(list.name))
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "meta-info mb-1" }, [
                    list.bestseller == true
                      ? _c("div", { staticClass: "mb-1" }, [
                          _c(
                            "span",
                            { staticClass: "category fs-12 fw-normal" },
                            [
                              _c("span", { staticClass: "bestseller-tag" }, [
                                _vm._v("bestseller")
                              ]),
                              _vm._v(" " + _vm._s(list.bestSell))
                            ]
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("span", { staticClass: "meta-info-block" }, [
                      _c(
                        "span",
                        { staticClass: "lectures fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("play_circle_filled")
                          ]),
                          _vm._v(_vm._s(list.lectures) + " lectures")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "durations fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("access_time")
                          ]),
                          _vm._v(_vm._s(list.hours) + " hours")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "durations fs-12 fw-normal" },
                        [
                          _c("v-icon", { staticClass: "cmr-8" }, [
                            _vm._v("show_chart")
                          ]),
                          _vm._v(_vm._s(list.level) + " Levels")
                        ],
                        1
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "short-desc" }, [
                  _c("span", [_vm._v(_vm._s(list.describe))]),
                  _vm._v(" "),
                  _c(
                    "ul",
                    { staticClass: "course-list  my-3" },
                    _vm._l(list.features, function(feature, key) {
                      return _c("li", { key: key }, [_vm._v(_vm._s(feature))])
                    }),
                    0
                  )
                ]),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    staticClass: "error",
                    attrs: {
                      block: "",
                      to:
                        "/" +
                        (_vm.getCurrentAppLayoutHandler() + "/courses/sign-in")
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("message.addToCart")))]
                )
              ],
              1
            )
          ],
          1
        )
      }),
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseCard.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseCard.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CourseCard_vue_vue_type_template_id_9bd39048___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CourseCard.vue?vue&type=template&id=9bd39048& */ "./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=template&id=9bd39048&");
/* harmony import */ var _CourseCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CourseCard.vue?vue&type=script&lang=js& */ "./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CourseCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CourseCard_vue_vue_type_template_id_9bd39048___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CourseCard_vue_vue_type_template_id_9bd39048___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/courses/CourseWidgets/CourseCard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseCard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=template&id=9bd39048&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=template&id=9bd39048& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseCard_vue_vue_type_template_id_9bd39048___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CourseCard.vue?vue&type=template&id=9bd39048& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/courses/CourseWidgets/CourseCard.vue?vue&type=template&id=9bd39048&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseCard_vue_vue_type_template_id_9bd39048___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CourseCard_vue_vue_type_template_id_9bd39048___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/courses/data.js":
/*!********************************************!*\
  !*** ./resources/js/views/courses/data.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  "courses": [{
    "image": "/static/img/course1.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "disount": 50,
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/embed/rbTVvpHF4cU",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 5,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "type": "management",
    "popular": "top",
    "bestseller": true,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/embed/LywZELVl_10",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course1.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "type": "design",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/embed/DKN_vgJeOO8",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "rating": 4,
    "hours": "11.5",
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "type": "develop",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "rating": 4,
    "hours": "11.5",
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "type": "develop",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/embed/-4rLLoNkLEU",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "https://www.youtube.com/embed/rVM3kqHGVrk",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "popular": "top",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown,Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "popular": "new",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course5.jpg",
    "popular": "new",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "John Smith Brown",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course2.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": false,
    "demoVideoUrl": "",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Savanna South",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course3.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/embed/Y4EOOb276gY",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Arthur Parker",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }, {
    "image": "/static/img/course4.jpg",
    "popular": "trending",
    "bestseller": false,
    "videoDemoStatus": true,
    "demoVideoUrl": "https://www.youtube.com/embed/zWtncr2UcbA",
    "name": "Lorem ipsum dolor sit amet sit amet sit amet",
    "content": "Joanna Brew",
    "oldPrice": "900",
    "disount": 50,
    "lastUpdates": "09/2018",
    "bestSell": " C++ | Development",
    "lectures": "483",
    "hours": "11.5",
    "rating": 4,
    "level": "All",
    "describe": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical.",
    "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vivamus non ipsum in nisi porttitor suscipit.", "Aliquam ut dolor in lorem accumsan porta."]
  }],
  "instructors": [{
    "image": "/static/avatars/user-6.jpg",
    "profile": "C++",
    "place": "Iron Network",
    "name": "John Smith Brown",
    "courses": "5",
    "number": "36247",
    "type": "students"
  }, {
    "image": "/static/avatars/user-13.jpg",
    "profile": "Marketing",
    "place": "Iron Network",
    "name": "Jonathan Red",
    "courses": "11",
    "number": "7464",
    "type": "students"
  }, {
    "image": "/static/avatars/user-8.jpg",
    "profile": "Designer",
    "place": "Iron Network",
    "name": "Arthur Parker",
    "courses": "3",
    "number": "36",
    "type": "students"
  }, {
    "image": "/static/avatars/user-35.jpg",
    "profile": "Python",
    "place": "Iron Network",
    "name": "Joanna Brew",
    "courses": "15",
    "number": "150",
    "type": "students"
  }, {
    "image": "/static/avatars/user-31.jpg",
    "profile": "Angular",
    "place": "Iron Network",
    "name": "Savanna South",
    "courses": "19",
    "number": "45793",
    "type": "students"
  }],
  "courseDetail": {
    "heading": "Quisque hendrerit ex quis dui eleifend, dapibus vehicula lacus semper.",
    "content": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
    "bestSeller": "2367532",
    "createdBy": "Sahil Goyal, Girija Bansal by James Brown",
    "lastUpdates": "11/2018",
    "language": "English",
    "demoVideoUrl": "https://www.youtube.com/embed/GKHTSJT9kdM",
    "learn": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Proin sit amet ex in quam vehicula maximus sit amet vel mauris.", "Proin sit amet ex in quam vehicula maximus sit amet vel mauris", "Quisque hendrerit ex quis dui eleifend, dapibus vehicula lacus semper.", "Fusce ut libero consectetur, aliquet velit sed, hendrerit sem.", "Maecenas eget urna sagittis, efficitur arcu posuere, pellentesque metus", "Donec sit amet nisi ac tellus mattis venenatis volutpat sit amet mi"],
    "description": {
      "title": "Integer pharetra mi eu libero convallis ultricies",
      "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "features": ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Donec auctor sapien eget sem blandit pharetra.", "In sed tellus congue, rhoncus mi quis, iaculis magna.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla.", "Vestibulum at mauris semper, tristique nulla et, tristique nulla."]
    },
    "topics": [{
      "name": "Course Overview",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/embed/iY6qSRdMtZQ",
        "name": "Introduction",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/embed/A4coW3B8V2c",
        "name": "Applications",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/embed/1NpisqyBoI0",
        "name": "Installing",
        "time": "11.4"
      }]
    }, {
      "name": "Basic Programming",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/embed/yJMtLhvDfe4",
        "name": "Welcome to Section 2",
        "time": "3.24"
      }, {
        "demoVideoUrl": "https://www.youtube.com/embed/D7MudYw1Fhg",
        "name": "Basics",
        "time": "7.14"
      }, {
        "demoVideoUrl": "https://www.youtube.com/embed/0uAPzJVDVEo",
        "name": "Solo Practice",
        "time": "5.72"
      }]
    }, {
      "name": "Advance Topics",
      "courseDetail": [{
        "demoVideoUrl": "https://www.youtube.com/embed/Q2AOSQmV7w4",
        "name": "Advance Programming",
        "time": "10.66"
      }, {
        "demoVideoUrl": "https://www.youtube.com/embed/LESBbhL1TRw",
        "name": "Difference between Topics",
        "time": "11.4"
      }, {
        "demoVideoUrl": "https://www.youtube.com/embed/prXvKdPeDNU",
        "name": "Wrap Up",
        "time": "7.72"
      }]
    }],
    "instructorInformation": {
      "image": "/static/avatars/user-9.jpg",
      "name": "James Colt",
      "features": [{
        "icon": "video_library",
        "feature": "Lorem ipsum dolor sit amet."
      }, {
        "icon": "book",
        "feature": "Fusce ut libero consectetur."
      }, {
        "icon": "code",
        "feature": "Proin sit amet ex in quam."
      }, {
        "icon": "phone_iphone",
        "feature": "Donec sit amet nisi ac."
      }],
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    },
    "billingDetails": {
      "totalPrice": 1000,
      "discountPercent": 94,
      "discountTime": "2 Day",
      "guarntee": "30",
      "includes": [{
        "icon": "video_library",
        "feature": "Lorem ipsum dolor sit amet."
      }, {
        "icon": "book",
        "feature": "Fusce ut libero consectetur."
      }, {
        "icon": "code",
        "feature": "Proin sit amet ex in quam."
      }, {
        "icon": "phone_iphone",
        "feature": "Donec sit amet nisi ac."
      }]
    },
    "moreCourses": [{
      "image": "/static/img/course1.jpg",
      "bestseller": true,
      "videoDemoStatus": false,
      "demoVideoUrl": "",
      "name": "Lorem ipsum dolor sit amet sit amesdfffffft sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "rating": 5,
      "disount": 50
    }, {
      "image": "/static/img/course2.jpg",
      "bestseller": true,
      "videoDemoStatus": false,
      "demoVideoUrl": "",
      "name": "Lorem ipsum dosdaalor sit amet sit sfddddddamet sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "disount": 50,
      "rating": 5
    }, {
      "image": "/static/img/course3.jpg",
      "bestseller": true,
      "videoDemoStatus": true,
      "demoVideoUrl": "https://www.youtube.com/embed/kfgOX1Uh04M",
      "name": "Loadsrem ipsum dolor sit amet sit amet sit amet",
      "content": "Lorem ipsum",
      "oldPrice": "900",
      "disount": 50,
      "rating": 5
    }]
  }
});

/***/ })

}]);