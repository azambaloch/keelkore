(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[86],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Banners.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Banners.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      sticky: false,
      singleLine: true,
      icon: 'mdi-plus',
      color: undefined,
      iconColor: undefined,
      elevation: 4,
      colors: ['red', 'blue', 'teal lighten-2', 'warning lighten-1', 'orange'],
      icons: ['mdi-access-point-network', 'mdi-plus', 'mdi-minus', 'mdi-network-strength-2-alert', 'mdi-earth']
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Banners.vue?vue&type=template&id=ae0edb0e&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Banners.vue?vue&type=template&id=ae0edb0e& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { attrs: { "grid-list-xl": "", "pt-0": "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.banners"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c("div", { staticClass: "mb-5" }, [
                    _c("p", [
                      _vm._v("The "),
                      _c("code", [_vm._v("v-banner")]),
                      _vm._v(
                        " component is used as middle-interruptive message to user with 1-2\n\t\t\t\t\t\tactions. It comes in 2 variations, single-line and multi-line (implicit)."
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-banner",
                    {
                      attrs: { "single-line": "" },
                      scopedSlots: _vm._u([
                        {
                          key: "actions",
                          fn: function() {
                            return [
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    text: "",
                                    color: "deep-purple accent-4"
                                  }
                                },
                                [_vm._v("\n\t\t\t\t\t\t\tAction\n\t\t\t\t\t\t")]
                              )
                            ]
                          },
                          proxy: true
                        }
                      ])
                    },
                    [
                      _vm._v(
                        "\n\t\t\t\t\tOne line message text string with two actions on tablet / Desktop\n\n\t\t\t\t\t"
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.playground"),
                    customClasses: "mb-30",
                    colClasses: "col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                  }
                },
                [
                  _c(
                    "v-row",
                    {
                      staticClass: "px-3",
                      attrs: { justify: "space-between" }
                    },
                    [
                      _c("v-switch", {
                        attrs: { label: "Sticky" },
                        model: {
                          value: _vm.sticky,
                          callback: function($$v) {
                            _vm.sticky = $$v
                          },
                          expression: "sticky"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-switch", {
                        attrs: { label: "Single-line" },
                        model: {
                          value: _vm.singleLine,
                          callback: function($$v) {
                            _vm.singleLine = $$v
                          },
                          expression: "singleLine"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        staticStyle: { "max-width": "250px" },
                        attrs: {
                          items: _vm.icons,
                          label: "Icon",
                          clearable: ""
                        },
                        model: {
                          value: _vm.icon,
                          callback: function($$v) {
                            _vm.icon = $$v
                          },
                          expression: "icon"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        staticStyle: { "max-width": "250px" },
                        attrs: {
                          items: _vm.colors,
                          label: "Color",
                          clearable: ""
                        },
                        model: {
                          value: _vm.color,
                          callback: function($$v) {
                            _vm.color = $$v
                          },
                          expression: "color"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        staticStyle: { "max-width": "250px" },
                        attrs: {
                          items: _vm.colors,
                          label: "Icon color",
                          clearable: ""
                        },
                        model: {
                          value: _vm.iconColor,
                          callback: function($$v) {
                            _vm.iconColor = $$v
                          },
                          expression: "iconColor"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-slider", {
                        staticStyle: { width: "100%" },
                        attrs: {
                          label: "Elevation",
                          min: "0",
                          max: "24",
                          clearable: ""
                        },
                        model: {
                          value: _vm.elevation,
                          callback: function($$v) {
                            _vm.elevation = $$v
                          },
                          expression: "elevation"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-sheet",
                    {
                      staticClass: "overflow-y-auto",
                      attrs: { "max-height": "400" }
                    },
                    [
                      _c(
                        "v-container",
                        { staticStyle: { height: "1500px" } },
                        [
                          _c(
                            "v-banner",
                            {
                              attrs: {
                                sticky: _vm.sticky,
                                "single-line": _vm.singleLine,
                                icon: _vm.icon,
                                color: _vm.color,
                                "icon-color": _vm.iconColor,
                                elevation: _vm.elevation
                              },
                              scopedSlots: _vm._u([
                                {
                                  key: "actions",
                                  fn: function() {
                                    return [
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: {
                                            text: "",
                                            color: "deep-purple accent-4"
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n\t\t\t\t\t\t\t\t\tAction\n\t\t\t\t\t\t\t\t"
                                          )
                                        ]
                                      )
                                    ]
                                  },
                                  proxy: true
                                }
                              ])
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\tLorem ipsum dolor sit amet consectetur adipisicing elit. Corporis magnam necessitatibus possimus\n\t\t\t\t\t\t\tsapiente laboriosam ducimus atque maxime quibusdam, facilis velit assumenda, quod nisi aliquid\n\t\t\t\t\t\t\tcorrupti maiores doloribus soluta optio blanditiis.\n\t\t\t\t\t\t\tLorem ipsum dolor sit amet consectetur adipisicing elit. Corporis magnam necessitatibus possimus\n\t\t\t\t\t\t\tsapiente laboriosam ducimus atque maxime quibusdam, facilis velit assumenda, quod nisi aliquid\n\t\t\t\t\t\t\tcorrupti maiores doloribus soluta optio blanditiis.\n\t\t\t\t\t\t\tLorem ipsum dolor sit amet consectetur adipisicing elit. Corporis magnam necessitatibus possimus\n\t\t\t\t\t\t\tsapiente laboriosam ducimus atque maxime quibusdam, facilis velit assumenda, quod nisi aliquid\n\t\t\t\t\t\t\tcorrupti maiores doloribus soluta optio blanditiis.\n\n\t\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Banners.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/ui-elements/Banners.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Banners_vue_vue_type_template_id_ae0edb0e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Banners.vue?vue&type=template&id=ae0edb0e& */ "./resources/js/views/ui-elements/Banners.vue?vue&type=template&id=ae0edb0e&");
/* harmony import */ var _Banners_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Banners.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Banners.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Banners_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Banners_vue_vue_type_template_id_ae0edb0e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Banners_vue_vue_type_template_id_ae0edb0e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Banners.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Banners.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Banners.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banners_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Banners.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Banners.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Banners_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Banners.vue?vue&type=template&id=ae0edb0e&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Banners.vue?vue&type=template&id=ae0edb0e& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Banners_vue_vue_type_template_id_ae0edb0e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Banners.vue?vue&type=template&id=ae0edb0e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Banners.vue?vue&type=template&id=ae0edb0e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Banners_vue_vue_type_template_id_ae0edb0e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Banners_vue_vue_type_template_id_ae0edb0e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);