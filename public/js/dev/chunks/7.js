(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentSales.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentSales.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loader: true,
      recentSales: [],
      settings: {
        maxScrollbarLength: 160
      }
    };
  },
  mounted: function mounted() {
    this.getRecentSale();
  },
  methods: {
    getRecentSale: function getRecentSale() {
      var _this = this;

      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/recentSales.js").then(function (response) {
        _this.loader = false;
        _this.recentSales = response.data;
      })["catch"](function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ToDoList.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Api */ "./resources/js/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    this.getTodos();
  },
  data: function data() {
    return {
      menu: false,
      settings: {
        maxScrollbarLength: 100
      },
      toDoLists: null,
      dialog: false,
      selectDeletedCustomer: null,
      newToDo: {
        date: "",
        title: "",
        completed: false
      },
      loader: false,
      snackbar: false,
      snackbarMessage: "",
      timeout: 2000,
      y: "top"
    };
  },
  methods: {
    getTodos: function getTodos() {
      var _this = this;

      this.loader = true;
      Api__WEBPACK_IMPORTED_MODULE_0__["default"].get("vuely/todo.js").then(function (response) {
        _this.loader = false;
        _this.toDoLists = response.data;
      })["catch"](function (error) {
        console.log("error" + error);
      });
    },
    marked: function marked(item) {
      var _this2 = this;

      this.loader = true;
      setTimeout(function () {
        _this2.loader = false;
        _this2.snackbar = true;

        var index = _this2.toDoLists.indexOf(item);

        _this2.toDoLists[index].completed = !item.completed;
        _this2.snackbarMessage = "ToDo Updated Successfully";
      }, 1500);
    },
    addToDo: function addToDo() {
      var _this3 = this;

      if (this.newToDo.title && this.newToDo.date !== "") {
        this.dialog = false;
        this.loader = true;
        setTimeout(function () {
          _this3.loader = false;
          var newToDoDetails = {
            date: _this3.newToDo.date,
            title: _this3.newToDo.title,
            completed: false
          };

          _this3.toDoLists.push(newToDoDetails);

          _this3.newToDo.date = "";
          _this3.newToDo.title = "";
          _this3.snackbar = true;
          _this3.snackbarMessage = "ToDo Added Successfully";
        }, 1500);
      }
    },
    onDeleteToDoList: function onDeleteToDoList(toDoList) {
      this.$refs.deleteConfirmationDialog.openDialog();
      this.selectToDoList = toDoList;
    },
    deleteToDo: function deleteToDo() {
      var _this4 = this;

      this.$refs.deleteConfirmationDialog.close();
      this.loader = true;
      var deletedToDoList = this.toDoLists;
      var index = deletedToDoList.indexOf(this.selectToDoList);
      setTimeout(function () {
        _this4.loader = false;
        _this4.selectToDoList = null;

        _this4.toDoLists.splice(index, 1);

        _this4.snackbar = true;
        _this4.snackbarMessage = "ToDo List Deleted Successfully";
      }, 1500);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.strike {\n\ttext-decoration: line-through;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ToDoList.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentSales.vue?vue&type=template&id=43263556&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/RecentSales.vue?vue&type=template&id=43263556& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "409px" }, attrs: { settings: _vm.settings } },
        [
          _c(
            "v-list",
            { staticClass: "card-list top-selling" },
            _vm._l(_vm.recentSales, function(sale, index) {
              return _c(
                "v-list-item",
                { key: index, attrs: { ripple: "" } },
                [
                  _c("img", {
                    staticClass: "mr-4 img-responsive",
                    attrs: { height: "50", width: "50", src: sale.img }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-list-item-content",
                    [
                      _c("v-list-item-subtitle", [
                        _c("h5", { staticClass: "mb-1" }, [
                          _vm._v(_vm._s(sale.title))
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "mr-3 fs-12 grey--text fw-normal" },
                          [
                            _c("i", { staticClass: "ti-download mr-2" }),
                            _vm._v(_vm._s(sale.sale))
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "mr-3 fs-12 grey--text fw-normal" },
                          [
                            _c("i", { staticClass: "ti-eye mr-2" }),
                            _vm._v(_vm._s(sale.views))
                          ]
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-list-item-action", [
                    _c("h5", { staticClass: "mb-0 fw-bold primary--text" }, [
                      _vm._v(_vm._s(sale.price))
                    ])
                  ])
                ],
                1
              )
            }),
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=template&id=12dff20f&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Widgets/ToDoList.vue?vue&type=template&id=12dff20f& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "vue-perfect-scrollbar",
        { staticStyle: { height: "404px" }, attrs: { settings: _vm.settings } },
        [
          _vm.toDoLists !== null
            ? _c(
                "v-list",
                {
                  staticClass: "card-list todo-list list-aqua-ripple",
                  attrs: { "two-line": "" }
                },
                _vm._l(_vm.toDoLists, function(item, index) {
                  return _c(
                    "v-list-item",
                    { key: index, attrs: { href: "javascript:;", ripple: "" } },
                    [
                      _c(
                        "v-list-item-action",
                        [
                          _c("v-checkbox", {
                            attrs: {
                              color: "primary",
                              "input-value": item.completed
                            },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                return _vm.marked(item)
                              }
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-list-item-content",
                        {
                          on: {
                            click: function($event) {
                              return _vm.marked(item)
                            }
                          }
                        },
                        [
                          _c(
                            "v-list-item-title",
                            { class: { strike: item.completed } },
                            [_c("h5", [_vm._v(_vm._s(item.title))])]
                          ),
                          _vm._v(" "),
                          _c("v-list-item-subtitle", [
                            _c("span", { staticClass: "fs-12 fw-normal" }, [
                              _vm._v(_vm._s(item.date))
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "trash-icon",
                          attrs: {
                            fab: "",
                            dark: "",
                            small: "",
                            color: "error"
                          },
                          on: {
                            click: function($event) {
                              return _vm.onDeleteToDoList(item)
                            }
                          }
                        },
                        [_c("v-icon", { staticClass: "zmdi zmdi-delete" })],
                        1
                      )
                    ],
                    1
                  )
                }),
                1
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("delete-confirmation-dialog", {
        ref: "deleteConfirmationDialog",
        attrs: {
          heading: "Are You Sure You Want To Delete?",
          message: "Are you sure you want to delete this list permanently?"
        },
        on: { onConfirm: _vm.deleteToDo }
      }),
      _vm._v(" "),
      _c("app-section-loader", { attrs: { status: _vm.loader } }),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { top: _vm.y === "top", timeout: _vm.timeout },
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [_vm._v("\n\t\t" + _vm._s(_vm.snackbarMessage) + "\n\t")]
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          scopedSlots: _vm._u([
            {
              key: "activator",
              fn: function(ref) {
                var on = ref.on
                return [
                  _c(
                    "v-btn",
                    _vm._g(
                      {
                        staticClass: "mx-4 my-4",
                        attrs: { color: "primary", small: "" }
                      },
                      on
                    ),
                    [
                      _c("i", { staticClass: "material-icons" }, [
                        _vm._v("add")
                      ]),
                      _vm._v(_vm._s(_vm.$t("message.addNew")) + "\n\t\t\t")
                    ]
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _vm._v(" "),
          _c(
            "v-card",
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v(_vm._s(_vm.$t("message.addToDo")))
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c(
                    "v-container",
                    { staticClass: "grid-list-md" },
                    [
                      _c(
                        "v-row",
                        [
                          _c(
                            "v-col",
                            { attrs: { cols: "12" } },
                            [
                              _c(
                                "v-menu",
                                {
                                  ref: "menu",
                                  attrs: {
                                    "close-on-content-click": false,
                                    transition: "scale-transition",
                                    "offset-y": "",
                                    "nudge-right": 40,
                                    "min-width": "290px",
                                    "return-value": _vm.newToDo.date
                                  },
                                  on: {
                                    "update:returnValue": function($event) {
                                      return _vm.$set(
                                        _vm.newToDo,
                                        "date",
                                        $event
                                      )
                                    },
                                    "update:return-value": function($event) {
                                      return _vm.$set(
                                        _vm.newToDo,
                                        "date",
                                        $event
                                      )
                                    }
                                  },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        return [
                                          _c(
                                            "v-text-field",
                                            _vm._g(
                                              {
                                                attrs: {
                                                  label: _vm.$t(
                                                    "message.scheduleDate"
                                                  ),
                                                  "prepend-icon": "event",
                                                  readonly: ""
                                                },
                                                model: {
                                                  value: _vm.newToDo.date,
                                                  callback: function($$v) {
                                                    _vm.$set(
                                                      _vm.newToDo,
                                                      "date",
                                                      $$v
                                                    )
                                                  },
                                                  expression: "newToDo.date"
                                                }
                                              },
                                              on
                                            )
                                          )
                                        ]
                                      }
                                    }
                                  ]),
                                  model: {
                                    value: _vm.menu,
                                    callback: function($$v) {
                                      _vm.menu = $$v
                                    },
                                    expression: "menu"
                                  }
                                },
                                [
                                  _vm._v(" "),
                                  _c(
                                    "v-date-picker",
                                    {
                                      attrs: { "no-title": "", scrollable: "" },
                                      model: {
                                        value: _vm.newToDo.date,
                                        callback: function($$v) {
                                          _vm.$set(_vm.newToDo, "date", $$v)
                                        },
                                        expression: "newToDo.date"
                                      }
                                    },
                                    [
                                      _c("v-spacer"),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: { color: "primary" },
                                          on: {
                                            click: function($event) {
                                              _vm.menu = false
                                            }
                                          }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.$t("message.cancel"))
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: { color: "warning" },
                                          on: {
                                            click: function($event) {
                                              return _vm.$refs.menu.save(
                                                _vm.newToDo.date
                                              )
                                            }
                                          }
                                        },
                                        [_vm._v(_vm._s(_vm.$t("message.ok")))]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { attrs: { cols: "12" } },
                            [
                              _c("v-text-field", {
                                attrs: { label: _vm.$t("message.title") },
                                model: {
                                  value: _vm.newToDo.title,
                                  callback: function($$v) {
                                    _vm.$set(_vm.newToDo, "title", $$v)
                                  },
                                  expression: "newToDo.title"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "primary" },
                      nativeOn: {
                        click: function($event) {
                          _vm.dialog = false
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("message.close")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    { attrs: { color: "warning" }, on: { click: _vm.addToDo } },
                    [_vm._v(_vm._s(_vm.$t("message.add")))]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/api/index.js":
/*!***********************************!*\
  !*** ./resources/js/api/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: 'https://reactify.theironnetwork.org/data/'
}));

/***/ }),

/***/ "./resources/js/components/Charts/DoughnutChartV2.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/Charts/DoughnutChartV2.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
// Doughnut Chart Widget

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Doughnut"],
  props: ['data'],
  data: function data() {
    return {
      options: {
        legend: {
          display: false
        },
        cutoutPercentage: 70,
        padding: 20
      }
    };
  },
  mounted: function mounted() {
    var _this$data = this.data,
        labels = _this$data.labels,
        data = _this$data.data,
        backgroundColor = _this$data.backgroundColor;
    this.renderChart({
      labels: labels,
      datasets: [{
        data: data,
        backgroundColor: backgroundColor,
        borderWidth: [0, 0, 0],
        hoverBackgroundColor: backgroundColor
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartShadow.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/Charts/LineChartShadow.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
// Line Chart Shadow


/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    enableShadow: {
      type: Boolean,
      "default": true
    },
    shadowColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    dataSet: {
      type: Array,
      "default": function _default() {
        return [10, 30, 39, 65, 85, 10, 10];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0.4;
      }
    },
    dataLabels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F'];
      }
    },
    borderWidth: {
      type: Number,
      "default": function _default() {
        return 3;
      }
    },
    gradientColor1: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    gradientColor2: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    enableGradient: {
      type: Boolean,
      "default": function _default() {
        return true;
      }
    },
    borderColor: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    }
  },
  data: function data() {
    return {
      gradient1: null,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false,
              drawTicks: false
            }
          }],
          xAxes: [{
            ticks: {
              display: false
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltip: {
          enabled: true
        }
      }
    };
  },
  mounted: function mounted() {
    if (this.enableShadow !== false) {
      var ctx = this.$refs.canvas.getContext('2d');
      var _stroke = ctx.stroke;

      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].shadowColor;
        ctx.shadowBlur = 8;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 10;

        _stroke.apply(this, arguments);

        ctx.restore();
      };
    }

    var gradientColor = ' ';

    if (this.enableGradient) {
      this.gradient1 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 140, 0);
      this.gradient1.addColorStop(0, this.gradientColor1);
      this.gradient1.addColorStop(1, this.gradientColor2);
      gradientColor = this.gradient1;
    } else {
      gradientColor = this.borderColor;
    }

    this.renderChart({
      labels: this.dataLabels,
      datasets: [{
        label: 'My First dataset',
        data: this.dataSet,
        barPercentage: 0.8,
        lineTension: this.lineTension,
        borderColor: gradientColor,
        pointBorderWidth: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        pointHoverBorderWidth: 0,
        borderWidth: this.borderWidth,
        fill: false
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Charts/LineChartWithArea.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/Charts/LineChartWithArea.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
/* harmony import */ var Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Helpers/helpers */ "./resources/js/helpers/helpers.js");



/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  props: {
    dataSet: {
      type: Array,
      "default": function _default() {
        return [85, 50, 80, 90, 10, 5, 15, 60, 30, 20, 50];
      }
    },
    lineTension: {
      type: Number,
      "default": function _default() {
        return 0;
      }
    },
    dataLabels: {
      type: Array,
      "default": function _default() {
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
      }
    },
    enableXAxesLine: {
      type: Boolean,
      "default": true
    },
    color: {
      type: String,
      "default": function _default() {
        return Constants_chart_config__WEBPACK_IMPORTED_MODULE_1__["ChartConfig"].color.white;
      }
    },
    gridLines: {
      type: Boolean,
      "default": false
    }
  },
  data: function data() {
    return {
      gradient: null,
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              display: false
            },
            display: false
          }],
          xAxes: [{
            gridLines: {
              display: this.gridLines,
              drawBorder: false
            },
            ticks: {
              display: this.enableXAxesLine
            },
            display: this.enableXAxesLine
          }]
        },
        tooltip: {
          enabled: true
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: true
      }
    };
  },
  mounted: function mounted() {
    this.gradient = this.$refs.canvas.getContext("2d").createLinearGradient(0, 280, 0, 0);
    this.gradient.addColorStop(0, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])('#FFF', 0.1));
    this.gradient.addColorStop(1, Object(Helpers_helpers__WEBPACK_IMPORTED_MODULE_2__["hexToRgbA"])(this.color, 0.4));
    this.renderChart({
      labels: this.dataLabels,
      datasets: [{
        label: "Data",
        lineTension: this.lineTension,
        pointRadius: 0,
        fill: true,
        backgroundColor: this.gradient,
        borderColor: this.color,
        data: this.dataSet,
        borderWidth: 3
      }]
    }, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/Widgets/RecentSales.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/Widgets/RecentSales.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RecentSales_vue_vue_type_template_id_43263556___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecentSales.vue?vue&type=template&id=43263556& */ "./resources/js/components/Widgets/RecentSales.vue?vue&type=template&id=43263556&");
/* harmony import */ var _RecentSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecentSales.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/RecentSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RecentSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecentSales_vue_vue_type_template_id_43263556___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RecentSales_vue_vue_type_template_id_43263556___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/RecentSales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/RecentSales.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentSales.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentSales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/RecentSales.vue?vue&type=template&id=43263556&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Widgets/RecentSales.vue?vue&type=template&id=43263556& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentSales_vue_vue_type_template_id_43263556___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RecentSales.vue?vue&type=template&id=43263556& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/RecentSales.vue?vue&type=template&id=43263556&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentSales_vue_vue_type_template_id_43263556___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecentSales_vue_vue_type_template_id_43263556___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Widgets/ToDoList.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/Widgets/ToDoList.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ToDoList_vue_vue_type_template_id_12dff20f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ToDoList.vue?vue&type=template&id=12dff20f& */ "./resources/js/components/Widgets/ToDoList.vue?vue&type=template&id=12dff20f&");
/* harmony import */ var _ToDoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ToDoList.vue?vue&type=script&lang=js& */ "./resources/js/components/Widgets/ToDoList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ToDoList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ToDoList.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ToDoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ToDoList_vue_vue_type_template_id_12dff20f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ToDoList_vue_vue_type_template_id_12dff20f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Widgets/ToDoList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Widgets/ToDoList.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/Widgets/ToDoList.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ToDoList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ToDoList.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Widgets/ToDoList.vue?vue&type=template&id=12dff20f&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Widgets/ToDoList.vue?vue&type=template&id=12dff20f& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_template_id_12dff20f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ToDoList.vue?vue&type=template&id=12dff20f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Widgets/ToDoList.vue?vue&type=template&id=12dff20f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_template_id_12dff20f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ToDoList_vue_vue_type_template_id_12dff20f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/constants/chart-config.js":
/*!************************************************!*\
  !*** ./resources/js/constants/chart-config.js ***!
  \************************************************/
/*! exports provided: ChartConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartConfig", function() { return ChartConfig; });
/**
* Change all chart colors
*/
var ChartConfig = {
  color: {
    'primary': '#5D92F4',
    'warning': '#FFB70F',
    'danger': '#FF3739',
    'success': '#00D014',
    'info': '#00D0BD',
    'white': '#fff',
    'lightGrey': '#E8ECEE'
  },
  lineChartAxesColor: '#E9ECEF',
  legendFontColor: '#AAAEB3',
  // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.3)'
};

/***/ }),

/***/ "./resources/js/views/dashboard/data.js":
/*!**********************************************!*\
  !*** ./resources/js/views/dashboard/data.js ***!
  \**********************************************/
/*! exports provided: newClients, recurringClients, bounceRates, pageViews, newsLetterCampaignData, newsLetterCampaignData2, newsLetterCampaignData3, earnedToday, itemsSold, salesAndEarning, totalEarnings, netProfit, totalExpences, onlineRevenue, adCampaignPerfomanceData, profitShare, devicesShare, subscribers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newClients", function() { return newClients; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recurringClients", function() { return recurringClients; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bounceRates", function() { return bounceRates; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pageViews", function() { return pageViews; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newsLetterCampaignData", function() { return newsLetterCampaignData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newsLetterCampaignData2", function() { return newsLetterCampaignData2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newsLetterCampaignData3", function() { return newsLetterCampaignData3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "earnedToday", function() { return earnedToday; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "itemsSold", function() { return itemsSold; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "salesAndEarning", function() { return salesAndEarning; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "totalEarnings", function() { return totalEarnings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "netProfit", function() { return netProfit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "totalExpences", function() { return totalExpences; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onlineRevenue", function() { return onlineRevenue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "adCampaignPerfomanceData", function() { return adCampaignPerfomanceData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "profitShare", function() { return profitShare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "devicesShare", function() { return devicesShare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "subscribers", function() { return subscribers; });
/* harmony import */ var Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Constants/chart-config */ "./resources/js/constants/chart-config.js");
 // New clients

var newClients = {
  title: 'message.newClients',
  value: '35,455',
  data: [12, 10, 4, 15, 16, 10, 20, 12],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary
}; // Recuring clients

var recurringClients = {
  title: 'message.recurringClients',
  value: '12,110',
  data: [10, 6, 10, 3, 15, 10, 15, 16],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info
}; // Bounce Rate

var bounceRates = {
  title: 'message.bounceRate',
  value: '8,855',
  data: [12, 12, 12, 18, 8, 17, 8, 12],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger
}; // Page Views

var pageViews = {
  title: 'message.pageViews',
  value: '9,123',
  data: [10, 6, 10, 3, 15, 10, 15, 16],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
  chartColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning
}; // news letter campaign data

var newsLetterCampaignData = {
  label1: 'Data 1',
  label2: 'Data 2',
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  data1: [45, 42, 51, 19, 50, 30, 22, 20, 45, 35, 40, 37],
  data2: [32, 28, 40, 38, 32, 42, 39, 15, 9, 17, 20, 41]
}; // news letter campaign data

var newsLetterCampaignData2 = {
  data1: [19, 21, 18, 20, 23, 16, 18, 30],
  data2: [10, 8, 14, 11, 10, 12, 10, 0]
}; // news letter campaign data for news dashboard

var newsLetterCampaignData3 = {
  label1: 'Campaign 1',
  label2: 'Campaign 2',
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  data1: [50, 45, 22, 18, 25, 5, 35, 20, 45, 22, 30, 70, 40],
  data2: [40, 30, 60, 30, 35, 50, 10, 30, 25, 28, 55, 65, 80]
}; // earned Today

var earnedToday = {
  color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary,
  label: 'Earn',
  data: [75, 60, 50, 35, 90, 35, 75, 20, 10, 20, 40, 5, 30, 75, 40, 90, 35, 20, 40, 30, 50, 35, 20, 75],
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
}; // items sold

var itemsSold = {
  color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info,
  label: 'Sold',
  data: [75, 60, 50, 35, 90, 35, 75, 20, 10, 20, 40, 5, 30, 75, 40, 90, 35, 20, 40, 30, 50, 35, 20, 75],
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
}; // sales and earning

var salesAndEarning = {
  data: [70, 55, 80, 50, 60, 75, 40, 55, 45, 50, 80, 90, 30],
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
  lineTension: 0,
  color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary
}; // total earning

var totalEarnings = {
  data: [15, 30, 10, 25, 10, 35],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.danger,
  lineTension: 0.5
}; // net profit

var netProfit = {
  data: [15, 35, 10, 25, 5, 35],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info,
  lineTension: 0.5
}; // total expenses

var totalExpences = {
  data: [30, 10, 25, 14, 35, 35],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success,
  lineTension: 0.5
}; // online revenue

var onlineRevenue = {
  data: [5, 25, 10, 35, 15, 25],
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', "June"],
  borderColor: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning,
  lineTension: 0.5
}; // ad campaign perfomance data

var adCampaignPerfomanceData = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  lineChartData: {
    label: 'Sales',
    data: [60, 45, 55, 75, 40, 55, 45, 50, 40, 50, 30, 50],
    color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.info
  },
  barChartData: {
    label: 'Views',
    data: [80, 55, 80, 75, 50, 50, 40, 50, 50, 50, 70, 50, 10],
    color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].lineChartAxesColor
  },
  barChartData2: {
    label: 'Earned',
    data: [70, 45, 70, 65, 40, 40, 30, 40, 40, 40, 60, 40, 10],
    color: Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary
  }
}; // profit share

var profitShare = {
  labels: ['Support Tickets', 'Business Events', 'Others'],
  data: [300, 50, 20],
  backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
}; // devices share

var devicesShare = {
  labels: ['Website', 'iOS Devices', 'Android Devices'],
  data: [300, 50, 20],
  backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
}; // subscribers

var subscribers = {
  labels: ['Website', 'iOS Devices', 'Android Devices'],
  data: [280, 30, 60],
  backgroundColor: [Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.primary, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.success, Constants_chart_config__WEBPACK_IMPORTED_MODULE_0__["ChartConfig"].color.warning]
};

/***/ })

}]);