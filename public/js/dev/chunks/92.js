(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[92],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Chips.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Chips.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      chip1: true,
      chip2: true,
      chip3: true,
      chip4: true
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Chips.vue?vue&type=template&id=3e2fe04e&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ui-elements/Chips.vue?vue&type=template&id=3e2fe04e& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "grid-list-xl pt-0 mt-n3", attrs: { fluid: "" } },
        [
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.default"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-chip", { staticClass: "ma-2", attrs: { close: "" } }, [
                    _vm._v("Example Chip")
                  ]),
                  _vm._v(" "),
                  _c("v-chip", { staticClass: "ma-2" }, [
                    _vm._v("Example Chip")
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    { staticClass: "ma-2", attrs: { close: "" } },
                    [
                      _c("v-avatar", [
                        _c("img", {
                          attrs: {
                            src:
                              " https://randomuser.me/api/portraits/men/35.jpg ",
                            alt: "trevor "
                          }
                        })
                      ]),
                      _vm._v("\n\t\t\t\t\tTrevor Hansen\n\t\t\t\t")
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    { staticClass: "ma-2" },
                    [
                      _c("v-avatar", { staticClass: "teal " }, [_vm._v("A")]),
                      _vm._v("\n\t\t\t\t\tANZ Bank\n\t\t\t\t")
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.outline"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { outlined: "", color: "secondary" }
                    },
                    [_vm._v("Outline")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { outlined: "", color: "primary" }
                    },
                    [_vm._v("Colored")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { outlined: "", color: "red" }
                    },
                    [
                      _c(
                        "v-icon",
                        {
                          staticClass: "error--text font-sm",
                          attrs: { left: "" }
                        },
                        [_vm._v("build")]
                      ),
                      _vm._v("Icon\n\t\t\t\t")
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.label"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c("v-chip", { staticClass: "ma-2", attrs: { label: "" } }, [
                    _vm._v("Label")
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { label: "", color: "pink", "text-color": "white" }
                    },
                    [
                      _c("v-icon", { attrs: { left: "" } }, [_vm._v("label")]),
                      _vm._v("Tags\n\t\t\t\t")
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { label: "", outlined: "", color: "red" }
                    },
                    [_vm._v("Outline")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.colored"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "primary", "text-color": "white" }
                    },
                    [_vm._v("Primary")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "warning", "text-color": "white" }
                    },
                    [_vm._v("Warning")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "error", "text-color": "white" }
                    },
                    [_vm._v("Colored Chip")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "success", "text-color": "white" }
                    },
                    [_vm._v("Colored Chip")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.colored"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "primary", "text-color": "white" }
                    },
                    [_vm._v("Primary")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "warning", "text-color": "white" }
                    },
                    [_vm._v("Warning")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "error", "text-color": "white" }
                    },
                    [_vm._v("Colored Chip")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "success", "text-color": "white" }
                    },
                    [_vm._v("Colored Chip")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "app-card",
                {
                  attrs: {
                    heading: _vm.$t("message.icon"),
                    colClasses: "col-12 col-md-6"
                  }
                },
                [
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "primary", "text-color": "white" }
                    },
                    [
                      _c(
                        "v-avatar",
                        [_c("v-icon", [_vm._v("account_circle")])],
                        1
                      ),
                      _vm._v("\n\t\t\t\t\tRanee\n\t\t\t\t")
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "warning", "text-color": "white" }
                    },
                    [
                      _vm._v("Premium\n\t\t\t\t\t"),
                      _c("v-icon", { attrs: { right: "" } }, [_vm._v("star")])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "error", "text-color": "white" }
                    },
                    [
                      _vm._v("\n\t\t\t\t\t1 Year\n\t\t\t\t\t"),
                      _c("v-icon", { attrs: { right: "" } }, [_vm._v("cake")])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: { color: "info", "text-color": "white" }
                    },
                    [
                      _c("v-avatar", { staticClass: "green darken-4" }, [
                        _vm._v("1")
                      ]),
                      _vm._v("\n\t\t\t\t\tYears\n\t\t\t\t")
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-chip",
                    {
                      staticClass: "ma-2",
                      attrs: {
                        close: "",
                        color: "success",
                        "text-color": "white"
                      }
                    },
                    [
                      _c(
                        "v-avatar",
                        [_c("v-icon", [_vm._v("check_circle")])],
                        1
                      ),
                      _vm._v("\n\t\t\t\t\tConfirmed\n\t\t\t\t")
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/ui-elements/Chips.vue":
/*!**************************************************!*\
  !*** ./resources/js/views/ui-elements/Chips.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Chips_vue_vue_type_template_id_3e2fe04e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Chips.vue?vue&type=template&id=3e2fe04e& */ "./resources/js/views/ui-elements/Chips.vue?vue&type=template&id=3e2fe04e&");
/* harmony import */ var _Chips_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Chips.vue?vue&type=script&lang=js& */ "./resources/js/views/ui-elements/Chips.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Chips_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Chips_vue_vue_type_template_id_3e2fe04e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Chips_vue_vue_type_template_id_3e2fe04e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ui-elements/Chips.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ui-elements/Chips.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Chips.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Chips_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Chips.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Chips.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Chips_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ui-elements/Chips.vue?vue&type=template&id=3e2fe04e&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/ui-elements/Chips.vue?vue&type=template&id=3e2fe04e& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Chips_vue_vue_type_template_id_3e2fe04e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Chips.vue?vue&type=template&id=3e2fe04e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ui-elements/Chips.vue?vue&type=template&id=3e2fe04e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Chips_vue_vue_type_template_id_3e2fe04e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Chips_vue_vue_type_template_id_3e2fe04e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);