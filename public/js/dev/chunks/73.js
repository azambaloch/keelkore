(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[73],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/extensions/VideoPlayer.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/extensions/VideoPlayer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var video_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! video.js */ "./node_modules/video.js/dist/video.cjs.js");
/* harmony import */ var video_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(video_js__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// videojs

window.videojs = video_js__WEBPACK_IMPORTED_MODULE_0___default.a; // hls plugin for videojs6

__webpack_require__(/*! videojs-contrib-hls/dist/videojs-contrib-hls.js */ "./node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      // videojs options
      playerOptions: {
        height: '360',
        autoplay: false,
        muted: false,
        language: 'en',
        playbackRates: [0.7, 1.0, 1.5, 2.0],
        sources: [{
          type: "video/mp4",
          // mp4
          src: "http://vjs.zencdn.net/v/oceans.mp4"
        }],
        poster: "https://surmon-china.github.io/vue-quill-editor/static/images/surmon-1.jpg"
      },
      playerOptions2: {
        height: '360',
        playbackRates: [0.7, 1, 1.3, 1.5, 1.7],
        sources: [{
          type: "video/mp4",
          src: "http://7xkwa7.media1.z0.glb.clouddn.com/sample_video_L"
        }],
        poster: "https://surmon-china.github.io/vue-quill-editor/static/images/surmon-3.jpg"
      },
      playerOptions3: {
        // videojs and plugin options
        height: '360',
        sources: [{
          withCredentials: false,
          type: "application/x-mpegURL",
          src: "https://logos-channel.scaleengine.net/logos-channel/live/biblescreen-ad-free/playlist.m3u8"
        }],
        controlBar: {
          timeDivider: false,
          durationDisplay: false
        },
        flash: {
          hls: {
            withCredentials: false
          }
        },
        html5: {
          hls: {
            withCredentials: false
          }
        },
        poster: "https://surmon-china.github.io/vue-quill-editor/static/images/surmon-5.jpg"
      }
    };
  },
  methods: {
    // player is ready
    playerReadied: function playerReadied(player) {
      // seek to 0s
      console.log('example player 1 readied', player);
      player.currentTime(0);
    },
    playerReadied2: function playerReadied2(player) {
      var track = new video_js__WEBPACK_IMPORTED_MODULE_0___default.a.AudioTrack({
        id: 'my-spanish-audio-track',
        kind: 'translation',
        label: 'Spanish',
        language: 'es'
      });
      player.audioTracks().addTrack(track); // Get the current player's AudioTrackList object.

      var audioTrackList = player.audioTracks(); // Listen to the "change" event.

      audioTrackList.addEventListener('change', function () {
        // Log the currently enabled AudioTrack label.
        for (var i = 0; i < audioTrackList.length; i++) {
          var _track = audioTrackList[i];

          if (_track.enabled) {
            video_js__WEBPACK_IMPORTED_MODULE_0___default.a.log(_track.label);
            return;
          }
        }
      });
    },
    playerReadied3: function playerReadied3(player) {
      // var hls = player.tech({ IWillNotUseThisInPlugins: true }).hls
      player.tech_.hls.xhr.beforeRequest = function (options) {
        return options;
      };
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/extensions/VideoPlayer.vue?vue&type=template&id=e4ea0640&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/extensions/VideoPlayer.vue?vue&type=template&id=e4ea0640& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("page-title-bar"),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "pt-0", attrs: { fluid: "" } },
        [
          _c(
            "app-card",
            {
              attrs: {
                fullBlock: true,
                heading: _vm.$t("message.baseConfig"),
                customClasses: "mb-30"
              }
            },
            [
              _c("div", {
                attrs: {
                  target: "_blank",
                  href:
                    "https://github.com/surmon-china/vue-video-player/tree/master/examples/01-video.vue"
                }
              }),
              _vm._v(" "),
              _c("video-player", {
                ref: "videoPlayer",
                staticClass: "vjs-custom-skin",
                attrs: { options: _vm.playerOptions, playsinline: true },
                on: { ready: _vm.playerReadied }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "app-card",
            {
              attrs: {
                fullBlock: true,
                heading: _vm.$t("message.hlsLive"),
                customClasses: "mb-30"
              }
            },
            [
              _c("div", {
                attrs: {
                  target: "_blank",
                  href:
                    "https://github.com/surmon-china/vue-video-player/tree/master/examples/04-video.vue"
                }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "player" },
                [
                  _c("video-player", {
                    staticClass: "vjs-custom-skin",
                    attrs: { options: _vm.playerOptions3 },
                    on: { ready: _vm.playerReadied3 }
                  })
                ],
                1
              )
            ]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/extensions/VideoPlayer.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/extensions/VideoPlayer.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VideoPlayer_vue_vue_type_template_id_e4ea0640___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VideoPlayer.vue?vue&type=template&id=e4ea0640& */ "./resources/js/views/extensions/VideoPlayer.vue?vue&type=template&id=e4ea0640&");
/* harmony import */ var _VideoPlayer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VideoPlayer.vue?vue&type=script&lang=js& */ "./resources/js/views/extensions/VideoPlayer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VideoPlayer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VideoPlayer_vue_vue_type_template_id_e4ea0640___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VideoPlayer_vue_vue_type_template_id_e4ea0640___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/extensions/VideoPlayer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/extensions/VideoPlayer.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/extensions/VideoPlayer.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VideoPlayer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./VideoPlayer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/extensions/VideoPlayer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VideoPlayer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/extensions/VideoPlayer.vue?vue&type=template&id=e4ea0640&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/extensions/VideoPlayer.vue?vue&type=template&id=e4ea0640& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VideoPlayer_vue_vue_type_template_id_e4ea0640___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./VideoPlayer.vue?vue&type=template&id=e4ea0640& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/extensions/VideoPlayer.vue?vue&type=template&id=e4ea0640&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VideoPlayer_vue_vue_type_template_id_e4ea0640___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VideoPlayer_vue_vue_type_template_id_e4ea0640___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);