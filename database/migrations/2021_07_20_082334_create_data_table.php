<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('fname');
            $table->string('cnic');
            $table->string('address');
            $table->string('dateOfBirth');
            $table->string('contact');
            $table->string('date');
            $table->string('plotNum');
            $table->string('plotSize');
            $table->string('plotReg');
            $table->string('project');

            $table->string('nName');
            $table->string('nFname');
            $table->string('nRelation');
            $table->string('nContact');
            $table->string('nCnic');
            $table->string('nAddress');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
